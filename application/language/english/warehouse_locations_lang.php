<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// HRM Code
$lang['warehouse_locations'] = "Warehouse Locations";
$lang['warehouse_name'] = "Warehouse Name";
$lang['attention'] = "Attention";
$lang['stree1'] = "Stree1";
$lang['street2'] = "Street2";
$lang['city'] = "City";
$lang['state'] = "State";
$lang['zip_code'] = "Zip Code";
$lang['phone'] = "Phone";
$lang['email'] = "Email";

