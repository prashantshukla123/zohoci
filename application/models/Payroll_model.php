<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of payroll_model
 *
 * @author NaYeM
 */
class Payroll_Model extends MY_Model
{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_department_by_id($departments_id)
    {
        $this->db->select('tbl_departments.deptname', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->from('tbl_departments');
        $this->db->join('tbl_designations', 'tbl_departments.departments_id = tbl_designations.departments_id', 'left');
        $this->db->where('tbl_departments.departments_id', $departments_id);
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function get_emp_info_by_id($designation_id)
    {
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->select('tbl_designations.designations', FALSE);
        $this->db->from('tbl_account_details');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'left');
        $this->db->where('tbl_designations.designations_id', $designation_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_emp_salary_list($id = NULL, $designation_id = NULL)
    {
       // $this->db->select('tbl_employee_payroll.*', FALSE);
	    $this->db->select('tbl_user_salary_general_info.*', FALSE);
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->select('tbl_salary_template.*', FALSE);
        $this->db->select('tbl_hourly_rate.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_departments.deptname', FALSE);
		$this->db->from('tbl_user_salary_general_info');
		//$this->db->from('tbl_employee_payroll');
       // $this->db->join('tbl_account_details', 'tbl_employee_payroll.user_id = tbl_account_details.user_id', 'left');
	   $this->db->join('tbl_account_details', 'tbl_user_salary_general_info.user_id  = tbl_account_details.user_id', 'left');
        $this->db->join('tbl_salary_template', 'tbl_user_salary_general_info.monthly_template_id = tbl_salary_template.salary_template_id', 'left');
        $this->db->join('tbl_hourly_rate', 'tbl_user_salary_general_info.hourly_rate_id = tbl_hourly_rate.hourly_rate_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'left');
        $this->db->join('tbl_departments', 'tbl_departments.departments_id  = tbl_designations.departments_id', 'left');
		
		
        if (!empty($designation_id)) {
            $this->db->where('tbl_designations.designations_id', $designation_id);
        }
        if (!empty($id)) {
            $this->db->where('tbl_user_salary_general_info.user_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
			//echo "<pre>"; print_r($result); die;
        } else {
            if (!empty($_POST["length"]) && $_POST["length"] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
		//echo "<pre>"; print_r($result); die;
        return $result;
    }

    public function get_salary_payment_info($salary_payment_id, $result = NULL, $search_type = null)
    {

        $this->db->select('tbl_salary_payment.*', FALSE);
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_departments.deptname', FALSE);
		$this->db->select('tbl_hr.ss_number', FALSE);
        $this->db->from('tbl_salary_payment');
        $this->db->join('tbl_account_details', 'tbl_salary_payment.user_id = tbl_account_details.user_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'left');
        $this->db->join('tbl_departments', 'tbl_departments.departments_id  = tbl_designations.departments_id', 'left');
		$this->db->join('tbl_hr', 'tbl_salary_payment.user_id  = tbl_hr.user_id', 'left');
        if (!empty($search_type)) {
            if ($search_type == 'employee') {
                $this->db->where("tbl_salary_payment.user_id", $salary_payment_id);
            } elseif ($search_type == 'month') {
                $this->db->where("tbl_salary_payment.payment_month", $salary_payment_id);
            } elseif ($search_type == 'period') {
                $this->db->where("tbl_salary_payment.payment_month >=", $salary_payment_id['start_month']);
                $this->db->where("tbl_salary_payment.payment_month <=", $salary_payment_id['end_month']);
            }
        } else {
            $this->db->where("tbl_salary_payment.salary_payment_id", $salary_payment_id);
        }
        if (!empty($_POST["length"]) && $_POST["length"] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query_result = $this->db->get();
        if (!empty($result)) {
            $result = $query_result->result();
        } else {
            $result = $query_result->row();
        }
        return $result;
    }

    public function get_advance_salary_info_by_date($payment_month = NULL, $id = NULL, $user_id = NULL)
    {
        $this->db->select('tbl_advance_salary.*', FALSE);
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->from('tbl_advance_salary');
        $this->db->join('tbl_account_details', 'tbl_account_details.user_id = tbl_advance_salary.user_id', 'left');
        if ($this->session->userdata('user_type') != 1) {
            $this->db->where('tbl_advance_salary.user_id', $this->session->userdata('user_id'));
            $this->db->where('tbl_advance_salary.deduct_month', $payment_month);
            $query_result = $this->db->get();
            $result = $query_result->result();
        } elseif (!empty($id)) {
            $this->db->where('tbl_advance_salary.advance_salary_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } elseif (!empty($user_id)) {
            $this->db->where('tbl_advance_salary.status', '1');
            $this->db->where('tbl_account_details.user_id', $user_id);
            $query_result = $this->db->get();
            $result = $query_result->result();
        } else {
            $this->db->where('tbl_advance_salary.deduct_month', $payment_month);
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function view_advance_salary($id = NULL)
    {
        $this->db->select('tbl_advance_salary.*', FALSE);
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->from('tbl_advance_salary');
        $this->db->join('tbl_account_details', 'tbl_account_details.user_id = tbl_advance_salary.user_id', 'left');
        $this->db->where('tbl_advance_salary.advance_salary_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();

        return $result;
    }

    public function my_advance_salary_info($all = null)
    {
        $this->db->select('tbl_advance_salary.*', FALSE);
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->from('tbl_advance_salary');
        $this->db->join('tbl_account_details', 'tbl_account_details.user_id = tbl_advance_salary.user_id', 'left');
        if (!empty($all)) {
            $this->db->order_by('tbl_advance_salary.request_date', "DESC");
        } else {
            $this->db->where('tbl_advance_salary.user_id', $this->session->userdata('user_id'));
        }
        if (!empty($_POST["length"]) && $_POST["length"] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;

    }

    public function get_attendance_info_by_date($start_date, $end_date, $user_id)
    {
        $this->db->select('tbl_attendance.*', FALSE);
        $this->db->select('tbl_clock.*', FALSE);
        $this->db->from('tbl_attendance');
        $this->db->join('tbl_clock', 'tbl_clock.attendance_id  = tbl_attendance.attendance_id', 'left');
        $this->db->where('tbl_attendance.date_in >=', $start_date);
        $this->db->where('tbl_attendance.date_in <=', $end_date);
        $this->db->where('tbl_attendance.user_id', $user_id);
        $this->db->where('tbl_attendance.attendance_status', 1);
	   //if($start_date != '') { print_r($this->db->last_query());    }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_provident_fund_info_by_date($start_date, $end_date, $user_id = null)
    {
        $this->db->select('tbl_salary_payment.*', FALSE);
        $this->db->select('tbl_salary_payment_deduction.*', FALSE);
        $this->db->select('tbl_account_details.*', FALSE);
        $this->db->from('tbl_salary_payment');
        $this->db->join('tbl_salary_payment_deduction', 'tbl_salary_payment_deduction.salary_payment_id  = tbl_salary_payment.salary_payment_id', 'left');
        $this->db->join('tbl_account_details', 'tbl_account_details.user_id  = tbl_salary_payment.user_id', 'left');
        $this->db->where('tbl_salary_payment.payment_month >=', $start_date);
        $this->db->where('tbl_salary_payment.payment_month <=', $end_date);
        $this->db->where('tbl_salary_payment_deduction.salary_payment_deduction_label', lang('provident_fund'));
        if (!empty($user_id)) {
            $this->db->where('tbl_salary_payment.user_id', $user_id);
        }
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
	
	//for get the employee earning
	public function get_emp_earning($user_id, $payment_month = '', $payment_number = '', $excluded_tax='') {
	
	//echo $user_id."<br>".$salary_template_id."<br>".$salary_type."<br>".$payment_month."<br>".$payment_number; exit;
	
		$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();			 
				
		$salary_type = $emp_details->user_payType;
		if($emp_details->user_payType == "salary") {
		
			$salary_template_id = $emp_details->monthly_template_id;
			if(isset($excluded_tax) && !empty($excluded_tax)) {
				
					$earning_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->where('effective_date <=', date('Y-m-d'))->where('excludeFrTx',$excluded_tax)->get('tbl_employee_salary_earnings')->result();
				} else {
					$earning_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_earnings')->result();
				
				}
				
				foreach ($earning_info as $v_earning_info){
				  $totalGrossSalary += $v_earning_info->earning_amount; 
				}
				
		} else {
			
			$salary_template_id = $emp_details->hourly_rate_id;
			
			
			$makeInfo = $this->db->where('user_id', $user_id)->where('payment_month', $payment_month)->where('payment_number', $payment_number)->get('tbl_employee_hours')->result();			 
			 //echo "<pre>"; print_r($makeInfo);
			// echo  $sql = $this->db->last_query();		exit;
			// exit;
			 $PTOHours = $makeInfo[0]->pto_hours;
			 $regularHours = $makeInfo[0]->regular_hours;
			 $overTimeHours = $makeInfo[0]->overtime_hours; 
			 
			 if(isset($excluded_tax) && !empty($excluded_tax)) {
				
					$employeeEarning = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->where('effective_date <=', date('Y-m-d'))->where('excludeFrTx',$excluded_tax)->get('tbl_employee_salary_earnings')->result();
				// echo  $sql = $this->db->last_query();		exit;
			 } else {
				$employeeEarning = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_earnings')->result();
			
			}
			 // echo  $sql = $this->db->last_query();		exit;
			//echo"<pre>"; print_r($employeeEarning); die;
			 foreach($employeeEarning as $ernData){
			   if($ernData->earning_type == 'Regular hours'){	 
			     $rateOfRagularHour = $ernData->earning_amount;
			   }
			   if($ernData->earning_type == 'PTO hours'){
			     $rateOfPTOHour = $ernData->earning_amount;
			   }
			   
			   if($ernData->earning_type == 'Over time hours'){
			     $rateOfOverTimeHour = $ernData->earning_amount;	 
			   }
			   
			   if($ernData->amtType == 'fix'){
				   $ernAmt += $ernData->earning_amount;
			   }		   
			 }
			 
			 //echo "test".$PTOHours; exit;

			 		 
			 if(!empty($PTOHours)){
				 $PTOPrice = $PTOHours*$rateOfPTOHour;
			 }else{
				 $PTOPrice=0;
			 }
			 
			 if(!empty($regularHours)){
				 $rgPrice = $regularHours*$rateOfRagularHour;
			 }else{
				 $rgPrice=0;
			 }
			 
			 if(!empty($overTimeHours)){
				 $ovtPrice = $overTimeHours*$rateOfOverTimeHour;
			 }else{
				 $ovtPrice=0;
			 }
			 
			$totalGrossSalary = $PTOPrice + $rgPrice + $ovtPrice + $ernAmt;
		}
	
	
	
		
		return $totalGrossSalary;
			
		
	
	}
	
	//for get the employee taxes
	public function get_emp_taxes($taxable_income, $user_id) {
	
		$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();			 
				
		$salary_type = $emp_details->user_payType;
		if($emp_details->user_payType == "salary") {
		
			$s_template_id = $emp_details->monthly_template_id;
			
		} else {
			$s_template_id = $emp_details->hourly_rate_id;
		}
		
		
		 $salary_tax_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_tax')->result();
		 // echo  $sql = $this->db->last_query()."<br>"; exit;
		  $taxArr = array();
 		   $k1 =0;
			 foreach($salary_tax_info as $v_tax_info){
				 //echo"<pre>"; print_r($v_tax_info); exit;
                if($v_tax_info->taxCalc =='max'){
					$taxArr[] = $v_tax_info->taxWithholding;
				}elseif($v_tax_info->taxCalc=='rng'){
                    $taxArr[] = $v_tax_info->taxWithholding;
				}else{
					$taxArr[] = $v_tax_info->taxWithholding;
				}					
			 }	
			// echo "<pre>"; print_r($taxArr); exit;
			 foreach($taxArr as $tax){
				 $total_taxes +=$tax;
			 }
			 
			// echo $taxable_income."kkkk"; exit;
			 //echo $total_taxes."ok"; exit;
			//$taxable_earning = $this->get_emp_earning($user_id, $s_template_id, $salary_type, $payment_month, $payment_number, 'no');
			// echo $taxable_earning."test"; exit;
			$total_tax_val = $taxable_income*($total_taxes/100);
			
		
		
		return $total_tax_val;
		
	}
	
	//for get the employee deductions
	public function get_emp_deductions($user_id, $payment_month = '', $payment_number = '', $excluded = '') {
		
		$totalGP = $this->get_emp_earning($user_id, $payment_month, $payment_number);
		
		$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();			 
				
		$salary_type = $emp_details->user_payType;
		if($emp_details->user_payType == "salary") {
		
			$s_template_id = $emp_details->monthly_template_id;
			
		} else {
			$s_template_id = $emp_details->hourly_rate_id;
		}
		
		
		if(isset($excluded) && !empty($excluded)) {
			$salary_deduction_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('effective_date <=', date('Y-m-d'))->where('excludeFrTx', $excluded)->get('tbl_employee_salary_deduction')->result();
		} else {
			$salary_deduction_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_deduction')->result();
		}
		$deductionval = 0;
		foreach ($salary_deduction_info as $v_deduction_info){
					if($v_deduction_info->deductionBasedOn == "percentAmt"){
						$percent = $v_deduction_info->deductionPercent;
						$deduction =  $totalGP*$percent/100;
					}else{								
						$deduction= $v_deduction_info->deduction_amount;
					}
					$deductionval += $deduction;
				}
		
		
		return $deductionval;
	}
	
	//for get gross income after taxes
	public function taxable_income($gp, $taxes_excluded_amount) {
		$taxable_income = $gp - $taxes_excluded_amount;
		
		return $taxable_income;
	}
	
	//gross income after taxes
	public function gross_after_taxes($gp, $employee_taxes) {
		$gross_income_after_taxes = $gp - $employee_taxes;
		if($gross_income_after_taxes > 0) {
			$gross_income_after_taxes = $gross_income_after_taxes;
		} else {
			$gross_income_after_taxes = 0;
		}
		return $gross_income_after_taxes;
	}
	
	//for get gross income after taxes
	public function net_salary_cal($gross_after_taxes, $total_deductions) {
		$net_salary = $gross_after_taxes - $total_deductions;
		
		if($net_salary > 0) {
			$net_salary = $net_salary;
		} else {
			$net_salary = 0;
		}
		return $net_salary;
	}
	
	//for get the employee contributions
	public function get_emp_contributions($user_id, $payment_month = '', $payment_number = '') {
		
		$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();			 
				
		$salary_type = $emp_details->user_payType;
		if($emp_details->user_payType == "salary") {
		
			$s_template_id = $emp_details->monthly_template_id;
			
		} else {
			$s_template_id = $emp_details->hourly_rate_id;
		}
		
		//payment exists
		$payment_details = $this->db->where('user_id', $user_id)->where('payment_month', $payment_month)->where('payment_number', $payment_number)->get('tbl_salary_payment')->row();
		if(!empty($payment_details)) {
			$companyContribution = $this->db->where('salary_payment_id', $payment_details->salary_payment_id)->get('tbl_salary_payment_contribution')->result();
		} else {
			$companyContribution = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('contribution_date <=', date('Y-m-d'))->get('tbl_salary_company_contribution')->result();
		}
		return $companyContribution; 
		
	}
	
	//for get the employee benefits
	public function get_emp_benefits($user_id, $payment_month = '', $payment_number = '') {
	
		$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();			 
				
		$salary_type = $emp_details->user_payType;
		if($emp_details->user_payType == "salary") {
		
			$s_template_id = $emp_details->monthly_template_id;
			
		} else {
			$s_template_id = $emp_details->hourly_rate_id;
		}
		
		
		$payment_details = $this->db->where('user_id', $user_id)->where('payment_month', $payment_month)->where('payment_number', $payment_number)->get('tbl_salary_payment')->row();
		if(!empty($payment_details)) {
			$otherBenefit = $this->db->where('salary_payment_id', $payment_details->salary_payment_id)->get('tbl_salary_payment_benifit')->result();
		} else {
			$otherBenefit = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('benifit_date <=', date('Y-m-d'))->get('tbl_salary_other_benifits')->result();
		}
		return $otherBenefit; 
		
	}
	
	//get employee deduction details
	public function get_emp_deduction_details($user_id = NULL, $payment_month = NULL, $payment_number = NULL, $salary_payment_id = NULL) {
	
		if(isset($salary_payment_id) && !empty($salary_payment_id)) {
			$deduction_info = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_deduction')->result();
		} else {
			$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();

			//echo "<pre>"; print_r($emp_details); exit;
					
			$salary_type = $emp_details->user_payType;
			if($emp_details->user_payType == "salary") {
			
				$s_template_id = $emp_details->monthly_template_id;
				
			} else {
				$s_template_id = $emp_details->hourly_rate_id;
			}
			
			//payment exists
			$payment_details = $this->db->where('user_id', $user_id)->where('payment_month', $payment_month)->where('payment_number', $payment_number)->get('tbl_salary_payment')->row();
			if(!empty($payment_details)) {
				$deduction_info = $this->db->where('salary_payment_id', $payment_details->salary_payment_id)->get('tbl_salary_payment_deduction')->result();
			} else {
				$deduction_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_deduction')->result();
			}
		}
		return $deduction_info; 
		
	}
	
	//get employee taxes details
	public function get_emp_tax_details($user_id, $payment_month = '', $payment_number = '') {
	
		$emp_details = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();			 
				
		$salary_type = $emp_details->user_payType;
		if($emp_details->user_payType == "salary") {
		
			$s_template_id = $emp_details->monthly_template_id;
			
		} else {
			$s_template_id = $emp_details->hourly_rate_id;
		}
		
		//payment exists
		$payment_details = $this->db->where('user_id', $user_id)->where('payment_month', $payment_month)->where('payment_number', $payment_number)->get('tbl_salary_payment')->row();
		if(!empty($payment_details)) {
			$tax_info = $this->db->where('salary_payment_id', $payment_details->salary_payment_id)->get('tbl_salary_payment_tax')->result();
		} else {
			$tax_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $s_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_tax')->result();
		}
		return $tax_info; 
		
	}
	
	//award into details
	public function award_info_details($userId, $startDate, $endDate) {
	
		$this->payroll_model->_table_name = 'tbl_employee_award';
		$this->payroll_model->_order_by = 'user_id';
		$award_info = $this->payroll_model->get_by(array('user_id' => $userId, 'given_date >=' => $startDate, 'given_date <=' => $endDate), FALSE);
		return $award_info;
	}
	
	//total award amount
	public function total_award_amount($userId, $startDate, $endDate) {
		
		$this->payroll_model->_table_name = 'tbl_employee_award';
		$this->payroll_model->_order_by = 'user_id';
		
                if((!empty($startDate) && $startDate != 'NULL') && (!empty($endDate) && $endDate != 'NULL')) {
                    $award_info = $this->payroll_model->get_by(array('user_id' => $userId, 'given_date >=' => $startDate, 'given_date <=' => $endDate), FALSE);
                     //echo  $sql = $this->db->last_query()."<br>"; 							
                    foreach($award_info as $awardAmt){
                            //echo"<pre>"; print_r($awardAmt);
                            $awardAmount += $awardAmt->award_amount;
                    }
                }
                
		if($awardAmount > 0) {
			$awardAmount = $awardAmount;
		} else {
			$awardAmount = 0;
		}
		return $awardAmount;		
	}
	
	

}
