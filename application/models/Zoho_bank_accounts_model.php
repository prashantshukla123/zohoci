<?php

class Zoho_bank_account_Model extends MY_Model
{

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_zoho_bank_account($filterBy)
    {
        $accounts = array();
        $all_accounts = array_reverse($this->get_permission('tbl_zoho_bank_account'));
        if (empty($filterBy)) {
            return $all_accounts;
        } else {
            foreach ($all_accounts as $v_accounts) {
                if ($v_accounts->status == $filterBy) {
                    array_push($accounts, $v_accounts);
                }
            }
        }
        return $accounts;
    }
}
