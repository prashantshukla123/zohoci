<link href="<?php echo base_url() ?>assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
    .datepicker {
        z-index: 1151 !important;
    }

    .easypiechart {
        margin: 0px auto;
    }
</style>
<?php echo message_box('success'); ?>
<?php
$user_id = $this->session->userdata('user_id');

?>

<div class="row">
<div class="col-sm-6">
    <?php 
            $searchType = 'all';
            ?>

            <link href="<?php echo base_url() ?>assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet"
                  type="text/css">
            <?php
            $curency = $this->admin_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
            $gcal_api_key = config_item('gcal_api_key');
            $gcal_id = config_item('gcal_id');
            ?>
                <!--Calendar-->
            <?php /*Comment in my JavaScript*/ ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        if ($('#my_calendar').length) {
                            var date = new Date();
                            var d = date.getDate();
                            var m = date.getMonth();
                            var y = date.getFullYear();
                            var calendar = $('#my_calendar').fullCalendar({
                                googleCalendarApiKey: '<?=$gcal_api_key?>',
                                eventAfterRender: function (event, element, view) {
                                    if (event.type == 'fo') {
                                        $(element).attr('data-toggle', 'ajaxModal').addClass('ajaxModal');
                                    }
                                },
                                header: {
                                    left: 'prev,next today',
                                    center: 'title',
                                    right: 'month,agendaWeek,agendaDay'
                                },
                                selectable: true,
                                selectHelper: true,
                                eventLimit: true,
                                eventSources: [ ]
                            });
                        }
                    });</script>
            <?php /*Comment in my JavaScript*/ ?>

                <script src='<?= base_url() ?>assets/plugins/fullcalendar/moment.min.js'></script>
                <script src='<?= base_url() ?>assets/plugins/fullcalendar/fullcalendar.min.js'></script>
                <script src="<?= base_url() ?>assets/plugins/fullcalendar/gcal.min.js"></script>

                <div class="<?= $v_order->col ?> mt-lg" id="<?= $v_order->id ?>">
                    <?php //echo ajax_anchor(base_url("admin/settings/save_dashboard/$v_order->id" . '/0'), "<i class='fa fa-times-circle'></i>", array("class" => "close-btn", "title" => lang('inactive'), "data-fade-out-on-success" => "#" . $v_order->id)); ?>
                    <div class="panel panel-custom menu">
                        <header class="panel-heading">
                            <h3 class="panel-title"><?= lang('Calendar') ?></h3>
                        </header>
                        <div class="">
                            <div id="my_calendar"></div>
                        </div>
                    </div>
                </div>
      </div>     
</div>

