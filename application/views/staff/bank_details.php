<?php include_once 'assets/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<?php $user_id = $this->session->userdata('user_id'); ?>
<style type="text/css">
    #id_error_msg {
        display: none;
    }

    .form-groups-bordered > .form-group {
        padding-bottom: 0px
    }
</style>
<div class="row">
    <div class="col-sm-12 wrap-fpanel">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong><?= lang('Bank details') ?></strong>
					 <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('new_bank') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>staff/user/new_bank/<?= $user_id ?>"
                                               class="text-default text-sm ml"><?= lang('Add new') ?></a>
                                                </span>
                                </div>
                </div>
            </div>
            <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('bank') ?></th>
                                <th><?= lang('name_of_account') ?></th>
                                <th><?= lang('routing_number') ?></th>
                                <th><?= lang('account_number') ?></th>
                                <th class="hidden-print"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($all_bank_info)) {
                                foreach ($all_bank_info as $v_bank_info) { ?>
                                    <tr>
                                        <td><?= $v_bank_info->bank_name ?></td>
                                        <td><?= $v_bank_info->account_name ?></td>
                                        <td><?= $v_bank_info->routing_number ?></td>
                                        <td><?= $v_bank_info->account_number ?></td>
                                        <td class="hidden-print">
                                            <?= btn_edit_modal('staff/user/new_bank/' . $v_bank_info->user_id . '/' . $v_bank_info->employee_bank_id) ?>
                                            <?= btn_delete('staff/user/delete_user_bank/' . $v_bank_info->user_id . '/' . $v_bank_info->employee_bank_id) ?>
                                        </td>
                                    </tr>
                                <?php }
                            }
							else{
									echo"<tr><td colspan='5'>No Result Found!</td></tr>";
								}
							?>
                            </tbody>
                        </table>
                    </div>
        </div>
    </div>
</div>