<?php
$edited = can_action('24', 'edited');
$user_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_users')->row();
$designation = $this->db->where('designations_id', $profile_info->designations_id)->get('tbl_designations')->row();
if (!empty($designation)) {
    $department = $this->db->where('departments_id', $designation->departments_id)->get('tbl_departments')->row();
}

?>
<style>
.add-new-modle {
	font-weight: bold;
    font-size: 14px;
}
</style>
<div class="unwrap">

    <div class="cover-photo bg-cover">
        <div class="p-xl text-white">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="text-center ">
                    <?php if ($profile_info->avatar): ?>
                        <img src="<?php echo base_url() . $profile_info->avatar; ?>"
                             class="img-thumbnail img-circle thumb128 ">
                    <?php else: ?>
                        <img src="<?php echo base_url() ?>assets/img/user/02.jpg" alt="Employee_Image"
                             class="img-thumbnail img-circle thumb128">
                        ;
                    <?php endif; ?>
                </div>

                <h3 class="m0 text-center"><?= $profile_info->fullname ?>
                </h3>
                <p class="text-center"><?= lang('emp_id') ?>: <?php echo $profile_info->employment_id ?></p>
                <p class="text-center"><?php
                    if (!empty($department)) {
                        $dname = $department->deptname;
                    } else {
                        $dname = lang('undefined_department');
                    }
                    if (!empty($designation->designations)) {
                        $des = ' &rArr; ' . $designation->designations;
                    } else {
                        $des = '& ' . lang('designation');;
                    }
                    echo $dname . ' ' . $des;
                    if (!empty($department->department_head_id) && $department->department_head_id == $profile_info->user_id) { ?>
                        <strong
                            class="label label-warning"><?= lang('department_head') ?></strong>
                    <?php }
                    ?>

                </p>
            </div>
			 <div class="col-sm-4"></div>
        </div>

    </div>
    <div class="text-center bg-gray-dark p-lg mb-xl">
        <div class="row row-table">
            <style type="text/css">
                .user-timer ul.timer {
                    margin: 0px;
                }

                .user-timer ul.timer > li.dots {
                    padding: 6px 2px;
                    font-size: 14px;
                }

                .user-timer ul.timer li {
                    color: #fff;
                    font-size: 24px;
                    font-weight: bold;
                }

                .user-timer ul.timer li span {
                    display: none;
                }

            </style>
            
        </div>
    </div>

</div>
<?php include_once 'asset/admin-ajax.php'; ?>
<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="row mt-lg">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked navbar-custom-nav">

            <li class="<?= $active == 1 ? 'active' : '' ?>">
			  <a href="#basic_info" data-toggle="tab"><?= lang('basic_info') ?></a>
			</li>
            <li class="<?= $active == 2 ? 'active' : '' ?>">
			 <a href="#bank_details" data-toggle="tab"><?= lang('bank_details') ?></a>
            </li>
			<li class="<?= $active == 3 ? 'active' : '' ?>">
			 <a href="#home_address" data-toggle="tab"><?= lang('Address') ?></a>
            </li>
            <li class="<?= $active == 4 ? 'active' : '' ?>">
			  <a href="#document_details" data-toggle="tab"><?= lang('document_details') ?></a>
            </li>
         <?php /* ?>   <li class="<?= $active == 13 ? 'active' : '' ?>" style="margin-right: 0px; "><a href="#activities" data-toggle="tab"><?= lang('activities') ?>
             <strong class="pull-right"><?= (!empty($activities_info) ? count($activities_info) : null) ?></strong></a>
            </li>
		<?php */ ?>
        </ul>
    </div>
    <div class="col-sm-9">
        <div class="tab-content" style="border: 0;padding:0;">
            <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="basic_info" style="position: relative;">
                <div class="panel panel-custom">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong><?= $profile_info->fullname ?></strong>
                           
                                <div class="pull-right">
								<span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Update') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>staff/user/update_contact/1/<?= $profile_info->account_details_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('update') ?></a>
                                                </span>
								</div>
                           
                        </div>
                    </div>
                    <div class="panel-body form-horizontal">
                        <div class="form-group mb0  col-sm-6">
                            <label class="control-label col-sm-5"><strong><?= lang('emp_id') ?>
                                    :</strong></label>
                            <div class="col-sm-7 ">
                                <p class="form-control-static"><?= $profile_info->employment_id ?></p>

                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="control-label col-sm-5"><strong><?= lang('fullname') ?>
                                    :</strong></label>
                            <div class="col-sm-7 ">
                                <p class="form-control-static"><?= $profile_info->fullname ?></p>

                            </div>
                        </div>
                        <?php if ($this->session->userdata('user_type') == 1) { ?>
                            <div class="form-group mb0  col-sm-6">
                                <label class="control-label col-sm-5"><strong><?= lang('username') ?>
                                        :</strong></label>
                                <div class="col-sm-7 ">
                                    <p class="form-control-static"><?= $user_info->username ?></p>

                                </div>
                            </div>
                            <div class="form-group mb0  col-sm-6">
                                <label class="control-label col-sm-5"><strong><?= lang('password') ?>
                                        :</strong></label>
                                <div class="col-sm-7 ">
                                    <p class="form-control-static"><a data-toggle="modal" data-target="#myModal"
                                                                      href="<?= base_url() ?>admin/user/reset_password/<?= $user_info->user_id ?>"><?= lang('reset_password') ?></a>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                       
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('gender') ?>:</label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->gender)) { ?>
                                    <p class="form-control-static"><?php echo lang($profile_info->gender); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">

                            <label class="col-sm-5 control-label"><?= lang('date_of_birth') ?>: </label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->date_of_birth)) { ?>
                                    <p class="form-control-static"><?php echo strftime(config_item('date_format'), strtotime($profile_info->date_of_birth)); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('maratial_status') ?>:</label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->maratial_status)) { ?>
                                    <p class="form-control-static"><?php echo lang($profile_info->maratial_status); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('fathers_name') ?>: </label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->father_name)) { ?>
                                    <p class="form-control-static"><?php echo "$profile_info->father_name"; ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('mother_name') ?>: </label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->mother_name)) { ?>
                                    <p class="form-control-static"><?php echo "$profile_info->mother_name"; ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('email') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$user_info->email"; ?></p>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('phone') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->phone"; ?></p>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('mobile') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->mobile"; ?></p>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('skype_id') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->skype"; ?></p>
                            </div>
                        </div>
                        <?php if (!empty($profile_info->passport)) { ?>
                            <div class="form-group mb0  col-sm-6">
                                <label class="col-sm-5 control-label"><?= lang('passport') ?>
                                    : </label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo "$profile_info->passport"; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('present_address') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->present_address"; ?></p>
                            </div>
                        </div>
						<div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('Ethnicity') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->ethnicity"; ?></p>
                            </div>
                        </div>
						<div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('Include In US Payroll') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->include_us_payroll"; ?></p>
                            </div>
                        </div>
						<div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('Payment Type') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php if($profile_info->payment_type=='via_cheque'){ echo "Pay via cheque";}elseif($profile_info->payment_type=='via_ACH'){
									echo"Pay via ACH Direct";}else{ echo"Not selected"; } ?></p>
                            </div>
                        </div>
						
                        <?php $show_custom_fields = custom_form_label(13, $profile_info->user_id);

                        if (!empty($show_custom_fields)) {
                            foreach ($show_custom_fields as $c_label => $v_fields) {
                                if (!empty($v_fields)) {
                                    ?>
                                    <div class="form-group mb0  col-sm-6">
                                        <label class="col-sm-5 control-label"><?= $c_label ?> : </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static"><?= $v_fields ?></p>
                                        </div>
                                    </div>
                                <?php }
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="bank_details" style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= lang('bank_information') ?>
                           
                                <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('new_bank') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>staff/user/new_bank/<?= $profile_info->user_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('Add new') ?></a>
                                                </span>
                                </div>
                           
                        </h4>
                    </div>
                    <?php
                    $all_bank_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_employee_bank')->result();
                    ?>
                    <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('bank') ?></th>
                                <th><?= lang('name_of_account') ?></th>
                                <th><?= lang('routing_number') ?></th>
                                <th><?= lang('account_number') ?></th>
                                <th class="hidden-print"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($all_bank_info)) {
                                foreach ($all_bank_info as $v_bank_info) { ?>
                                    <tr>
                                        <td><?= $v_bank_info->bank_name ?></td>
                                        <td><?= $v_bank_info->account_name ?></td>
                                        <td><?= $v_bank_info->routing_number ?></td>
                                        <td><?= $v_bank_info->account_number ?></td>
                                        <td class="hidden-print">
                                            <?= btn_edit_modal('staff/user/new_bank/' . $v_bank_info->user_id . '/' . $v_bank_info->employee_bank_id) ?>
                                            <?= btn_delete('staff/user/delete_user_bank/' . $v_bank_info->user_id . '/' . $v_bank_info->employee_bank_id) ?>
                                        </td>
                                    </tr>
                                <?php }
                            }
							else{
									echo"<tr><td colspan='5'>No Result Found!</td></tr>";
								}
							?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
			
			<div class="tab-pane <?= $active == 3 ? 'active' : '' ?>" id="home_address" style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= lang('Home Address') ?>
                            
                                <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Address') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>staff/user/new_address/<?= $profile_info->user_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('Add new address') ?></a>
                                                </span>
                                </div>
                           
                        </h4>
                    </div>
                    <?php
                    $all_address_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_employee_address')->result();
                    ?>
                    <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('Street1') ?></th>
                                <th><?= lang('Street2') ?></th>
                                <th><?= lang('Zip code') ?></th>
                                <th><?= lang('City,State,Country') ?></th>
                                <th class="hidden-print"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($all_address_info)) {
                                foreach ($all_address_info as $v_add_info) { ?>
                                    <tr>
                                        <td><?= $v_add_info->street1 ?></td>
                                        <td><?= $v_add_info->street2 ?></td>
                                        <td><?= $v_add_info->zipcode ?></td>
                                        <td><?= $v_add_info->city.','.$v_add_info->state.','.$v_add_info->	country ?></td>
                                        <td class="hidden-print">
                                            <?= btn_edit_modal('staff/user/new_address/' . $v_add_info->user_id . '/' . $v_add_info->employee_add_id) ?>
                                            <?= btn_delete('staff/user/delete_user_address/' . $v_add_info->user_id . '/' . $v_add_info->employee_add_id) ?>
                                        </td>
                                    </tr>
                                <?php }
                            }
							else{
									echo"<tr><td colspan='5'>No Result Found!</td></tr>";
								}
							?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="document_details"
                 style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= lang('user_documents') ?>
                            
                                <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Add documents') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>staff/user/user_documents/<?= $profile_info->user_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('Add new') ?></a>
                                                </span>
                                </div>
                            
                        </h4>
                    </div>
                    <div class="panel-body form-horizontal">
                        <!-- CV Upload -->
                        <?php
                        $document_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_employee_document')->row();
                        if (!empty($document_info->resume)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('resume') ?> : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->resume; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('resume') ?></a>
                                        <a href="<?= base_url('staff/user/delete_documents/resume/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>

                        <?php endif; ?>
                        <?php if (!empty($document_info->offer_letter)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('offer_latter') ?> : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->offer_letter; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('offer_latter') ?></a>
                                        <a href="<?= base_url('staff/user/delete_documents/offer_letter/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->joining_letter)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('joining_latter') ?>
                                    : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->joining_letter; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('joining_latter') ?></a>
                                        <a href="<?= base_url('staff/user/delete_documents/joining_letter/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->contract_paper)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('contract_paper') ?>
                                    : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->contract_paper; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('contract_paper') ?></a>
                                        <a href="<?= base_url('staff/user/delete_documents/contract_paper/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->id_proff)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('id_prof') ?> : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->id_proff; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('id_prof') ?></a>
                                        <a href="<?= base_url('staff/user/delete_documents/id_proff/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->other_document)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('other_document') ?>
                                    : </label>
                                <div class="col-sm-8">
                                    <?php
                                    $uploaded_file = json_decode($document_info->other_document);

                                    if (!empty($uploaded_file)):
                                        foreach ($uploaded_file as $sl => $v_files):

                                            if (!empty($v_files)):
                                                ?>
                                                <p class="form-control-static">
                                                    <a href="<?php echo base_url() . 'uploads/' . $v_files->fileName; ?>"
                                                       target="_blank"
                                                       style="text-decoration: underline;"><?= $sl + 1 . '. ' . lang('view') . ' ' . lang('other_document') ?></a>
                                                </p>
                                                <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?= $active == 13 ? 'active' : '' ?>" id="activities"
                 style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title"><?= lang('all_activities'); ?></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped" id="Transation_DataTables">
                            <thead>
                            <tr>
                                <th style="width: 200px"><?= lang('date') ?></th>
                                <th style="width: 10px"><?= lang('module') ?></th>
                                <th style="width: 500px"><?= lang('activity') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($activities_info)) {
                                foreach ($activities_info as $v_activity) {
                                    ?>
                                    <tr>
                                        <td><?= display_datetime($v_activity->activity_date); ?></td>
                                        <td><?= $this->db->where('user_id', $v_activity->user)->get('tbl_account_details')->row()->fullname; ?></td>
                                        <td><?= lang($v_activity->module) ?></td>
                                        <td>
                                            <?= lang($v_activity->activity) ?>
                                            <strong> <?= $v_activity->value1 . ' ' . $v_activity->value2 ?></strong>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
