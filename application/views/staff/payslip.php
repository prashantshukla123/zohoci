<?php echo message_box('success'); ?>
<?php echo message_box('error'); 
$curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
?>
<div class="row">
    <div class="col-sm-12 wrap-fpanel">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong><?= lang('Payslips') ?></strong>
					 <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Payslips') ?>">
                                        </span>
                                </div>
                </div>
            </div>
            <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('emp_id') ?></th>
								<th><strong><?= lang('name') ?></strong></th>
								<th><strong><?= lang('basic_salary') ?></strong></th>
								<th><strong><?= lang('net_salary') ?></strong></th>
								<th><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
								<?php 
								$data = array('user_id' => $employee_info->user_id);
								$query = $this->db->select('*')->where($data)->get('tbl_salary_payment');
								$salary_info = $query->result();
								//echo "<pre>"; print_r($salary_info);
								if(!empty($salary_info)) { 
									foreach($salary_info as $salary) {
									
									//total deductions
									$data_deduction = array('salary_payment_id' => $salary->salary_payment_id);
									$query_deduction = $this->db->select('*')->where($data_deduction)->get('tbl_salary_payment_deduction');
									$deduction_info = $query_deduction->result();;

									$total_deductions = 0;
									if (!empty($deduction_info)) {
									foreach($deduction_info as $deduction) { 
												if(isset($deduction->salary_payment_deduction_value) && !empty($deduction->salary_payment_deduction_value)) {
													$total_deductions += $deduction->salary_payment_deduction_value;
												}
											
											}
									}

									//total taxes
									$data_tax = array('salary_payment_id' => $salary->salary_payment_id);
									$query_tax = $this->db->select('*')->where($data_tax)->get('tbl_salary_payment_tax');
									$taxes_info = $query_tax->result();;
									
									$total_taxes = 0;
									if (!empty($taxes_info)) {
										foreach($taxes_info as $taxes) { 
											if(isset($taxes->salary_payment_tax_value) && !empty($taxes->salary_payment_tax_value)) {
												$total_taxes += $taxes->salary_payment_tax_value;
											}
										}
									}
										
										$net_pay = $salary->grossPay - ($total_deductions+$total_taxes);
									?>
                                    <tr>
                                        <td><?php echo $employee_info->employment_id; ?></td>
                                        <td><?php echo $employee_info->fullname; ?></td>
										<td><?php echo display_money($salary->grossPay,$curency->symbol); ?></td>
										<td><?php echo display_money($net_pay,$curency->symbol); ?></td>
										<td>
											<a class="text-success" target="_blank"
												href="<?php echo base_url() ?>staff/payroll/pay_slip_pdf/<?php echo $salary->salary_payment_id. '/biweekly/'.strtotime($salary->payment_start_date).'/'.strtotime($salary->payment_end_date); ?>"><?= lang('Download Payslip') ?></a>
				
										</td>
                                    </tr>
                                <?php
									}
								}
								else {
										echo"<tr><td colspan='5'>No Result Found!</td></tr>";
								}
								?>
							</tbody>
                        </table>
                    </div>
        </div>
    </div>
</div>