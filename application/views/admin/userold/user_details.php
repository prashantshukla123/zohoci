<?php
$activities_info = $this->db->where(array('user' => $profile_info->user_id))->order_by('activity_date', 'DESC')->get('tbl_activities')->result();

$edited = can_action('24', 'edited');
$user_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_users')->row();
$designation = $this->db->where('designations_id', $profile_info->designations_id)->get('tbl_designations')->row();
if (!empty($designation)) {
    $department = $this->db->where('departments_id', $designation->departments_id)->get('tbl_departments')->row();
}
$all_project_info = $this->user_model->my_permission('tbl_project', $profile_info->user_id);
$p_started = 0;
$p_in_progress = 0;
$p_completed = 0;
$project_time = 0;
$project_time = $this->user_model->my_spent_time($profile_info->user_id, true);

if (!empty($all_project_info)) {
    foreach ($all_project_info as $v_user_project) {
        if ($v_user_project->project_status == 'started') {
            $p_started += count($v_user_project->project_status);
        }
        if ($v_user_project->project_status == 'in_progress') {
            $p_in_progress += count($v_user_project->project_status);
        }
        if ($v_user_project->project_status == 'completed') {
            $p_completed += count($v_user_project->project_status);
        }
    }
}

$tasks_info = $this->user_model->my_permission('tbl_task', $profile_info->user_id);

$t_not_started = 0;
$t_in_progress = 0;
$t_completed = 0;
$t_deferred = 0;
$t_waiting_for_someone = 0;
$task_time = 0;
$task_time = $this->user_model->my_spent_time($profile_info->user_id);
if (!empty($tasks_info)):foreach ($tasks_info as $v_tasks):
    if ($v_tasks->task_status == 'not_started') {
        $t_not_started += count($v_tasks->task_status);
    }
    if ($v_tasks->task_status == 'in_progress') {
        $t_in_progress += count($v_tasks->task_status);
    }
    if ($v_tasks->task_status == 'completed') {
        $t_completed += count($v_tasks->task_status);
    }

endforeach;
endif;
?>
<div class="unwrap">

    <div class="cover-photo bg-cover">
        <div class="p-xl text-white">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="text-center ">
                    <?php if ($profile_info->avatar): ?>
                        <img src="<?php echo base_url() . $profile_info->avatar; ?>"
                             class="img-thumbnail img-circle thumb128 ">
                    <?php else: ?>
                        <img src="<?php echo base_url() ?>assets/img/user/02.jpg" alt="Employee_Image"
                             class="img-thumbnail img-circle thumb128">
                        ;
                    <?php endif; ?>
                </div>

                <h3 class="m0 text-center"><?= $profile_info->fullname ?>
                </h3>
                <p class="text-center"><?= lang('emp_id') ?>: <?php echo $profile_info->employment_id ?></p>
                <p class="text-center"><?php
                    if (!empty($department)) {
                        $dname = $department->deptname;
                    } else {
                        $dname = lang('undefined_department');
                    }
                    if (!empty($designation->designations)) {
                        $des = ' &rArr; ' . $designation->designations;
                    } else {
                        $des = '& ' . lang('designation');;
                    }
                    echo $dname . ' ' . $des;
                    if (!empty($department->department_head_id) && $department->department_head_id == $profile_info->user_id) { ?>
                        <strong
                            class="label label-warning"><?= lang('department_head') ?></strong>
                    <?php }
                    ?>

                </p>
            </div>
			 <div class="col-sm-4"></div>
        </div>

    </div>
    <div class="text-center bg-gray-dark p-lg mb-xl">
        <div class="row row-table">
            <style type="text/css">
                .user-timer ul.timer {
                    margin: 0px;
                }

                .user-timer ul.timer > li.dots {
                    padding: 6px 2px;
                    font-size: 14px;
                }

                .user-timer ul.timer li {
                    color: #fff;
                    font-size: 24px;
                    font-weight: bold;
                }

                .user-timer ul.timer li span {
                    display: none;
                }
				.add-new-modle {
					font-weight: bold;
					font-size: 14px;
				}
            </style>
            
        </div>
    </div>

</div>
<?php include_once 'asset/admin-ajax.php'; ?>
<?= message_box('success'); ?>
<?= message_box('error'); ?>


<div class="row mt-lg">
    <div class="col-sm-3">
        <ul class="nav nav-pills nav-stacked navbar-custom-nav">

            <li class="<?= $active == 1 ? 'active' : '' ?>">
			  <a href="#basic_info" data-toggle="tab"><?= lang('basic_info') ?></a>
			</li>
            <li class="<?= $active == 2 ? 'active' : '' ?>">
			 <a href="#bank_details" data-toggle="tab"><?= lang('bank_details') ?></a>
            </li>
			<li class="<?= $active == 31 ? 'active' : '' ?>">
			 <a href="#human_resource" data-toggle="tab"><?= lang('Human Resources') ?></a>
            </li>
			<li class="<?= $active == 3 ? 'active' : '' ?>">
			 <a href="#home_address" data-toggle="tab"><?= lang('Address') ?></a>
            </li>
            <li class="<?= $active == 4 ? 'active' : '' ?>">
			  <a href="#document_details" data-toggle="tab"><?= lang('document_details') ?></a>
            </li>
         <?php /* ?>   <li class="<?= $active == 13 ? 'active' : '' ?>" style="margin-right: 0px; "><a href="#activities" data-toggle="tab"><?= lang('activities') ?>
             <strong class="pull-right"><?= (!empty($activities_info) ? count($activities_info) : null) ?></strong></a>
            </li>
		<?php */ ?>
        </ul>
    </div>
    <div class="col-sm-9">
        <div class="tab-content" style="border: 0;padding:0;">
            <div class="tab-pane <?= $active == 1 ? 'active' : '' ?>" id="basic_info" style="position: relative;">
                <div class="panel panel-custom">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong><?= $profile_info->fullname ?></strong>
                            <?php if (!empty($edited)) { ?>
                                <div class="pull-right">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Update Conatct') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/user/update_contact/1/<?= $profile_info->account_details_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('update') ?></a>
                                                </span>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="panel-body form-horizontal">
                        <div class="form-group mb0  col-sm-6">
                            <label class="control-label col-sm-5"><strong><?= lang('emp_id') ?>
                                    :</strong></label>
                            <div class="col-sm-7 ">
                                <p class="form-control-static"><?= $profile_info->employment_id ?></p>

                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="control-label col-sm-5"><strong><?= lang('fullname') ?>
                                    :</strong></label>
                            <div class="col-sm-7 ">
                                <p class="form-control-static"><?= $profile_info->fullname ?></p>

                            </div>
                        </div>
                        <?php if ($this->session->userdata('user_type') == 1) { ?>
                            <div class="form-group mb0  col-sm-6">
                                <label class="control-label col-sm-5"><strong><?= lang('username') ?>
                                        :</strong></label>
                                <div class="col-sm-7 ">
                                    <p class="form-control-static"><?= $user_info->username ?></p>

                                </div>
                            </div>
                            <div class="form-group mb0  col-sm-6">
                                <label class="control-label col-sm-5"><strong><?= lang('password') ?>
                                        :</strong></label>
                                <div class="col-sm-7 ">
                                    <p class="form-control-static"><a data-toggle="modal" data-target="#myModal"
                                                                      href="<?= base_url() ?>admin/user/reset_password/<?= $user_info->user_id ?>"><?= lang('reset_password') ?></a>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                       
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('gender') ?>:</label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->gender)) { ?>
                                    <p class="form-control-static"><?php echo lang($profile_info->gender); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">

                            <label class="col-sm-5 control-label"><?= lang('date_of_birth') ?>: </label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->date_of_birth)) { ?>
                                    <p class="form-control-static"><?php echo strftime(config_item('date_format'), strtotime($profile_info->date_of_birth)); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('maratial_status') ?>:</label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->maratial_status)) { ?>
                                    <p class="form-control-static"><?php echo lang($profile_info->maratial_status); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('fathers_name') ?>: </label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->father_name)) { ?>
                                    <p class="form-control-static"><?php echo "$profile_info->father_name"; ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('mother_name') ?>: </label>
                            <div class="col-sm-7">
                                <?php if (!empty($profile_info->mother_name)) { ?>
                                    <p class="form-control-static"><?php echo "$profile_info->mother_name"; ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('email') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$user_info->email"; ?></p>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('phone') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->phone"; ?></p>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('mobile') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->mobile"; ?></p>
                            </div>
                        </div>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('skype_id') ?> : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->skype"; ?></p>
                            </div>
                        </div>
                        <?php if (!empty($profile_info->passport)) { ?>
                            <div class="form-group mb0  col-sm-6">
                                <label class="col-sm-5 control-label"><?= lang('passport') ?>
                                    : </label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo "$profile_info->passport"; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('present_address') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->present_address"; ?></p>
                            </div>
                        </div>
						<div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('Ethnicity') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->ethnicity"; ?></p>
                            </div>
                        </div>
						<div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('Include In US Payroll') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php echo "$profile_info->include_us_payroll"; ?></p>
                            </div>
                        </div>
						<div class="form-group mb0  col-sm-6">
                            <label class="col-sm-5 control-label"><?= lang('Payment Type') ?>
                                : </label>
                            <div class="col-sm-7">
                                <p class="form-control-static"><?php if($profile_info->payment_type=='via_cheque'){ echo "Pay via cheque";}elseif($profile_info->payment_type=='via_ACH'){
									echo"Pay via ACH Direct";}else{ echo"Not selected"; } ?></p>
                            </div>
                        </div>
						
                        <?php $show_custom_fields = custom_form_label(13, $profile_info->user_id);

                        if (!empty($show_custom_fields)) {
                            foreach ($show_custom_fields as $c_label => $v_fields) {
                                if (!empty($v_fields)) {
                                    ?>
                                    <div class="form-group mb0  col-sm-6">
                                        <label class="col-sm-5 control-label"><?= $c_label ?> : </label>
                                        <div class="col-sm-7">
                                            <p class="form-control-static"><?= $v_fields ?></p>
                                        </div>
                                    </div>
                                <?php }
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div class="tab-pane <?= $active == 2 ? 'active' : '' ?>" id="bank_details" style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= lang('bank_information') ?>
                            <?php if (!empty($edited)) { ?>
                                <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('new_bank') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/user/new_bank/<?= $profile_info->user_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('Add new') ?></a>
                                                </span>
                                </div>
                            <?php } ?>
                        </h4>
                    </div>
                    <?php
                    $all_bank_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_employee_bank')->result();
                    ?>
                    <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('bank') ?></th>
                                <th><?= lang('name_of_account') ?></th>
                                <th><?= lang('routing_number') ?></th>
                                <th><?= lang('account_number') ?></th>
                                <th class="hidden-print"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($all_bank_info)) {
                                foreach ($all_bank_info as $v_bank_info) { ?>
                                    <tr>
                                        <td><?= $v_bank_info->bank_name ?></td>
                                        <td><?= $v_bank_info->account_name ?></td>
                                        <td><?= $v_bank_info->routing_number ?></td>
                                        <td><?= $v_bank_info->account_number ?></td>
                                        <td class="hidden-print">
                                            <?= btn_edit_modal('admin/user/new_bank/' . $v_bank_info->user_id . '/' . $v_bank_info->employee_bank_id) ?>
                                            <?= btn_delete('admin/user/delete_user_bank/' . $v_bank_info->user_id . '/' . $v_bank_info->employee_bank_id) ?>
                                        </td>
                                    </tr>
                                <?php }
                            }
							else{
									echo"<tr><td colspan='5'>No Result Found!</td></tr>";
								}
							?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
			<div class="tab-pane <?= $active == 31 ? 'active' : '' ?>" id="human_resource" style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h2 class="panel-title"><?= lang('Human Resources Information') ?>                         
                        </h2>
                    </div>                    
                    <div class="panel-body form-horizontal">
                     <div class="tab">
					  <button class="tablinks active" onclick="openCity(event, 'personal')">Personal</button>
					  <button class="tablinks" onclick="openCity(event, 'workStatus')">Work Status</button>
					  <button class="tablinks" onclick="openCity(event, 'education')">Education</button>
					   <button class="tablinks" onclick="openCity(event, 'emergencyContracts')">Emergency Contacts</button>
					</div>
                    
					<div id="personal" class="tabcontent" style="display:block">
					  <h4>Personal Information</h4>
					   <div class="personal-info">
					     <?php
							$all_hr_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_hr')->result();
							//echo"<pre>"; print_r($all_hr_info); die;
							?>
					     <form data-parsley-validate="" novalidate="" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/user/update_hr_info/<?php echo $profile_info->account_details_id; ?>/<?php
						  if (!empty($all_hr_info[0]->id)) {
							  echo $all_hr_info[0]->id;
						  }
						  ?>" method="post" class="form-horizontal form-groups-bordered">

						<!-- CV Upload -->
						<div class="">
							<label class="control-label">
								<?= lang('Social Security ') . ' ' . lang('number') ?> <span class="text-danger">*</span></label>
							<input required type="text" id="ss_number" name="ss_number" value="<?php
									if (!empty($all_hr_info[0]->ss_number)) {
										echo $all_hr_info[0]->ss_number;
									}
									?>" class="form-control">
							<input type="hidden" required name="hr_id" value="<?php
									if (!empty($all_hr_info[0]->id)) {
										echo $all_hr_info[0]->id;
									}
									?>" class="form-control">
						</div>
						<div class="">
							<label class="control-label">
								<?= lang('Hire Date') ?><span class="text-danger">*</span></label>
							<input type="text" required name="hire_date" value="<?php
									if (!empty($all_hr_info[0]->hire_date)) {
										echo $all_hr_info[0]->hire_date;
									}
									?>" class="datetype form-control datepicker">
						</div>

						
						<div class="">
							<label class="control-label">
								<?= lang('Release Date') ?><span class="text-danger">*</span></label>
							<input required type="text" name="release_date" value="<?php
									if (!empty($all_hr_info[0]->release_date)) {
										echo $all_hr_info[0]->release_date;
									}
									?>" class="datetype form-control datepicker">
						</div>
						
						<div class="">
							<label class="control-label">
								<?= lang('Job Description') ?></label>
							<textarea name="job_description" class="form-control"><?php
										if (!empty($all_hr_info[0]->job_description)) {
											echo $all_hr_info[0]->job_description;
										}
									?></textarea>
						</div>
						
						<div class="">
							<label class="control-label">
								<?= lang('Type') ?>
							</label>
							<select class="form-control" name="type">
							<?php 
							if($all_hr_info[0]->type){ ?>
							 <option value="<?=$all_hr_info[0]->type ?>"><?=$all_hr_info[0]->type ?></option>
							<?php } ?>
							  <option value="exempt">Exempt</option>
							  <option value="non-exempt">Non Exempt</option>
							  <option value="officer">Officer</option>
							  <option value="owner">Owner</option>
							  <option value="contractor">Contractor</option>
							</select>	
						</div>
						<div class="">
							<label class="control-label">
								<?= lang('Status') ?>
							</label>
							<select class="form-control" name="status">
							<?php 
							if($all_hr_info[0]->status){ ?>
							 <option value="<?=$all_hr_info[0]->status ?>"><?=$all_hr_info[0]->status ?></option>
							<?php } ?>
							  <option value="Full time">Full time</option>
							  <option value="Part time">Part time</option>
							  <option value="Temporary">Temporary</option>
							  <option value="Regular">Regular</option>
							  <option value="Fixed term">Fixed term</option>
							</select>	
						</div>
						
						<div class="">
							<label class="control-label">
								<?= lang('Taxable marital status') ?>
							</label>
							<select name="taxPayer" class="form-control taxPayer">
							<?php 
							if($all_hr_info[0]->taxPayer){ ?>
							 <option value="<?=$all_hr_info[0]->taxPayer ?>"><?=$all_hr_info[0]->taxPayer ?></option>
							<?php } ?>
							  <option value="any">Any</option>
						      <option value="Married">Married</option>
						      <option value="Single">Single</option>
						      <option value="Married separately">Married separately</option>
						      <option value="Head of household">Head of household</option>
						   </select>
						</div>
						
						<div class="">
							<label class="control-label">
								<?= lang('Filling type') ?>
							</label>
							<select name="fillingType" class="form-control">
							<?php 
							if($all_hr_info[0]->fillingType){ ?>
							 <option value="<?=$all_hr_info[0]->fillingType ?>"><?=$all_hr_info[0]->fillingType ?></option>
							<?php } ?>
						      <option value="any">Any</option>
							  <option value="Married_filling jointly">Married filling jointly</option>
						      <option value="Single_married_filling separately">Single married filling separately</option>
						      <option value="Head of household">Head of household</option>
						   </select>
						</div>
						
						

						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								<?= lang('close') ?>
							</button>
							<button type="submit" class="btn btn-primary">
								<?= lang('update') ?>
							</button>
						</div>
					</form>
					   </div>
					</div>

					<div id="workStatus" class="tabcontent" style="display:none">
					  <h4>Work Status</h4>
					  <div class="work-status">
					     <form data-parsley-validate="" novalidate="" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/user/update_hr_work_info/<?php echo $profile_info->account_details_id; ?>/<?php
						  if (!empty($all_hr_info[0]->id)) {
							  echo $all_hr_info[0]->id;
						  }
						  ?>" method="post" class="form-horizontal form-groups-bordered">
						   <div class="">
							<label class="control-label">
								<?= lang('Resident status') ?>
							</label>
							<select class="form-control" name="resident_status">
							  <option <?php if (!empty($all_hr_info[0]->resident_status=='Permanent Resident')) { echo"selected"; }?> value="Permanent Resident">Permanent Resident</option>
							  <option <?php if (!empty($all_hr_info[0]->resident_status=='Permanent Resident')) { echo"selected"; }?> value="Permanent Resident">Current Resident</option>
							  <option <?php if (!empty($all_hr_info[0]->resident_status=='Long Term Resident')) { echo"selected"; }?> value="Long Term Resident">Long Term Resident</option>
							</select>	
						</div>
						<div class="">
							<label class="control-label">
								<?= lang('Visa information') ?>
							</label>
							<select class="form-control" name="visa_information">
							  <option value="A">A</option>
							  <option value="A1, G-1, NATO1-6">A1, G-1, NATO1-6</option>
							  <option value="A-2, NATO1-6">A-2, NATO1-6</option>
							  <option value="B-1">B-1</option>
							  <option value="B-2">B-2</option>
							  <option value="BCC">BCC</option>
							  <option value="C">C</option>
							  <option value="E-1">E-1</option>
							  <option value="F-2">F-2</option>
							  <option value="G1- G5, NATO">G1- G5, NATO</option>
							  <option value="H1-C">H1-C</option>
							  <option value="T-1">T-1</option>
							</select>	
						</div>
						<div class="">
						<label class="control-label"><?= lang('I-9 verified') ?><span class="text-danger">*</span>
						<input type="checkbox" required name="verified" <?php
						if (!empty($all_hr_info[0]->verified)) {
							echo "checked";
						}
						?> class="form-control">
						</label>
					    </div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								<?= lang('close') ?>
							</button>
							<button type="submit" class="btn btn-primary">
								<?= lang('update') ?>
							</button>
						</div>						
						</form>
					  </div>
					</div>

					<div id="education" class="tabcontent" style="display:none">
					  <h4>Education</h4>
					  <p>No Information</p>
					</div>
					<div id="emergencyContracts" class="tabcontent" style="display:none">
					  <h4>Emergency Contacts</h4>
					  <div class="emergency-contracts">
					  <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/user/new_contact/<?= $profile_info->user_id ?>" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
					<?php
                    $all_contact_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_emergency_contact')->result();
                    ?>
                    <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('Contact name') ?></th>
                                <th><?= lang('Relationship') ?></th>
                                <th><?= lang('Phone') ?></th>
								<th class="hidden-print"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($all_contact_info)) {
                                foreach ($all_contact_info as $v_add_info) { ?>
                                    <tr>
                                        <td><?= $v_add_info->contact_name ?></td>
                                        <td><?= $v_add_info->relationship ?></td>
                                        <td><?= $v_add_info->phone ?></td>
                                        <td class="hidden-print">
                                            <?= btn_edit_modal('admin/user/new_contact/' . $v_add_info->user_id.'/'.$v_add_info->id) ?>
                                            <?= btn_delete('admin/user/delete_user_contact/' . $v_add_info->user_id . '/' . $v_add_info->id) ?>
                                        </td>
                                    </tr>
                                <?php }
                            }
							else{
									echo"<tr><td colspan='4'>No Result Found!</td></tr>";
								}
							?>
                            </tbody>
                        </table>
                    </div>
					 </div>
					</div>
					
                    </div>
                </div>
            </div>
			<div class="tab-pane <?= $active == 04 ? 'active' : '' ?>" id="home_address" style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= lang('Home Address') ?>
                            <?php if (!empty($edited)) { ?>
                                <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Address') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/user/new_address/<?= $profile_info->user_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('Add new address') ?></a>
                                                </span>
                                </div>
                            <?php } ?>
                        </h4>
                    </div>
                    <?php
                    $all_address_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_employee_address')->result();
                    ?>
                    <div class="panel-body form-horizontal">
                        <table class="table table-striped " cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th><?= lang('Street1') ?></th>
                                <th><?= lang('Street2') ?></th>
                                <th><?= lang('Zip code') ?></th>
                                <th><?= lang('City,State,Country') ?></th>
                                <th class="hidden-print"><?= lang('action') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!empty($all_address_info)) {
                                foreach ($all_address_info as $v_add_info) { ?>
                                    <tr>
                                        <td><?= $v_add_info->street1 ?></td>
                                        <td><?= $v_add_info->street2 ?></td>
                                        <td><?= $v_add_info->zipcode ?></td>
                                        <td><?= $v_add_info->city.','.$v_add_info->state.','.$v_add_info->	country ?></td>
                                        <td class="hidden-print">
                                            <?= btn_edit_modal('admin/user/new_address/' . $v_add_info->user_id . '/' . $v_add_info->employee_add_id) ?>
                                            <?= btn_delete('admin/user/delete_user_address/' . $v_add_info->user_id . '/' . $v_add_info->employee_add_id) ?>
                                        </td>
                                    </tr>
                                <?php }
                            }
							else{
									echo"<tr><td colspan='5'>No Result Found!</td></tr>";
								}
							?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="document_details"
                 style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h4 class="panel-title"><?= lang('user_documents') ?>
                            <?php if (!empty($edited)) { ?>
                                <div class="pull-right hidden-print">
                                         <span data-placement="top" data-toggle="tooltip"
                                               title="<?= lang('Add document') ?>">
                                            <a data-toggle="modal" data-target="#myModal"
                                               href="<?= base_url() ?>admin/user/user_documents/<?= $profile_info->user_id ?>"
                                               class="text-default text-sm ml add-new-modle"><?= lang('Add new') ?></a>
                                                </span>
                                </div>
                            <?php } ?>
                        </h4>
                    </div>
                    <div class="panel-body form-horizontal">
                        <!-- CV Upload -->
                        <?php
                        $document_info = $this->db->where('user_id', $profile_info->user_id)->get('tbl_employee_document')->row();
                        if (!empty($document_info->resume)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('resume') ?> : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->resume; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('resume') ?></a>
                                        <a href="<?= base_url('admin/user/delete_documents/resume/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>

                        <?php endif; ?>
                        <?php if (!empty($document_info->offer_letter)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('offer_latter') ?> : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->offer_letter; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('offer_latter') ?></a>
                                        <a href="<?= base_url('admin/user/delete_documents/offer_letter/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->joining_letter)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('joining_latter') ?>
                                    : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->joining_letter; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('joining_latter') ?></a>
                                        <a href="<?= base_url('admin/user/delete_documents/joining_letter/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->contract_paper)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('contract_paper') ?>
                                    : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->contract_paper; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('contract_paper') ?></a>
                                        <a href="<?= base_url('admin/user/delete_documents/contract_paper/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->id_proff)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('id_prof') ?> : </label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <a href="<?php echo base_url() . $document_info->id_proff; ?>"
                                           target="_blank"
                                           style="text-decoration: underline;"><?= lang('view') . ' ' . lang('id_prof') ?></a>
                                        <a href="<?= base_url('admin/user/delete_documents/id_proff/' . $document_info->document_id) ?>"
                                           class="btn btn-xs" title="" data-toggle="tooltip"
                                           data-placement="top"
                                           onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
                                           data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($document_info->other_document)): ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('other_document') ?>
                                    : </label>
                                <div class="col-sm-8">
                                    <?php
                                    $uploaded_file = json_decode($document_info->other_document);

                                    if (!empty($uploaded_file)):
                                        foreach ($uploaded_file as $sl => $v_files):

                                            if (!empty($v_files)):
                                                ?>
                                                <p class="form-control-static">
                                                    <a href="<?php echo base_url() . 'uploads/' . $v_files->fileName; ?>"
                                                       target="_blank"
                                                       style="text-decoration: underline;"><?= $sl + 1 . '. ' . lang('view') . ' ' . lang('other_document') ?></a>
                                                </p>
                                                <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane <?= $active == 13 ? 'active' : '' ?>" id="activities"
                 style="position: relative;">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <div class="panel-title"><?= lang('all_activities'); ?></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped" id="Transation_DataTables">
                            <thead>
                            <tr>
                                <th style="width: 200px"><?= lang('date') ?></th>
                                <th style="width: 10px"><?= lang('module') ?></th>
                                <th style="width: 500px"><?= lang('activity') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if (!empty($activities_info)) {
                                foreach ($activities_info as $v_activity) {
                                    ?>
                                    <tr>
                                        <td><?= display_datetime($v_activity->activity_date); ?></td>
                                        <td><?= $this->db->where('user_id', $v_activity->user)->get('tbl_account_details')->row()->fullname; ?></td>
                                        <td><?= lang($v_activity->module) ?></td>
                                        <td>
                                            <?= lang($v_activity->activity) ?>
                                            <strong> <?= $v_activity->value1 . ' ' . $v_activity->value2 ?></strong>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#Transation_DataTables').dataTable({
            paging: false,
            "bSort": false
        });
		$('.datepicker0').datepicker({
			format: 'mm/dd/yyyy'
		});
		$('.datepicker1').datepicker({
			format: 'mm/dd/yyyy'
		});
		$('.datepicker2').datepicker({
			format: 'mm/dd/yyyy'
		});
    });
</script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<style>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}

</style>

<script>
 $(document).ready(function() {
      $("#ss_number").bind("keypress", function (e) {
          var keyCode = e.which ? e.which : e.keyCode
               
          if (!(keyCode >= 48 && keyCode <= 57)) {
            $(".error").css("display", "inline");
            return false;
          }else{
            $(".error").css("display", "none");
          }
      });
	  
	  });
    $(function(){
    $(document).on("cut copy paste","#ss_number",function(e) {
        e.preventDefault();
    });	
	});
	
</script>
<?php
$color = array('37bc9b', '7266ba', 'f05050', 'ff902b', '7266ba', 'f532e5', '5d9cec', '7cd600', '91ca00', 'ff7400', '1cc200', 'bb9000', '40c400');
?>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.tooltip.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.resize.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.pie.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.time.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.categories.js"></script>
<script src="<?= base_url() ?>assets/plugins/Flot/jquery.flot.spline.min.js"></script>
