<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('New address') ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/user/update_home_address/<?php echo $profile_info->account_details_id; ?>/<?php
              if (!empty($address_info->employee_add_id)) {
                  echo $address_info->employee_add_id;
              }
              ?>" method="post" class="form-horizontal form-groups-bordered">

            <!-- CV Upload -->
            <div class="">
                <label class="control-label"><?= lang('Address ') . ' ' . lang('street 1') ?> <span class="text-danger">*</span></label>
                <input required type="text" name="street1" value="<?php
                if (!empty($address_info->street1)) {
                    echo $address_info->street1;
                }
                ?>" class="form-control">
                <input type="hidden" required name="employee_add_id" value="<?php
                if (!empty($address_info->employee_add_id)) {
                    echo $address_info->employee_add_id;
                }
                ?>" class="form-control">
            </div>
            <div class="">
                <label class="control-label"><?= lang('Street 2') ?><span
                        class="text-danger">*</span></label>
                <input type="text" required name="street2" value="<?php
                if (!empty($address_info->street2)) {
                    echo $address_info->street2;
                }
                ?>" class="form-control">
            </div>

            <div class="">
                <label class="control-label"><?= lang('Zip code') ?><span class="text-danger">*</span></label>
                <input required type="text" name="zipcode" value="<?php
                if (!empty($address_info->zipcode)) {
                    echo $address_info->zipcode;
                }
                ?>" class="form-control">
            </div>
            <div class="">
                <label
                    class="control-label"><?= lang('City') ?><span class="text-danger">*</span></label>
                <input required type="text" name="city" value="<?php
                if (!empty($address_info->city)) {
                    echo $address_info->city;
                }
                ?>" class="form-control">
            </div>
              <div class="">
                <label
                    class="control-label"><?= lang('State') ?><span class="text-danger">*</span></label>
                <input required type="text" name="state" value="<?php
                if (!empty($address_info->state)) {
                    echo $address_info->state;
                }
                ?>" class="form-control">
            </div>			
			<div class="">
                <label
                    class="control-label"><?= lang('Country') ?><span class="text-danger">*</span></label>
                <input required type="text" name="country" value="<?php
                if (!empty($address_info->country)) {
                    echo $address_info->country;
                }
                ?>" class="form-control">
            </div>
      
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>