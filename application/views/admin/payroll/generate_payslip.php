<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-custom" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong><?= lang('generate_payslip') ?></strong>
                </div>
            </div>
            <form id="form" role="form" enctype="multipart/form-data"
                  action="<?php echo base_url() ?>admin/payroll/generate_payslip" method="post"
                  class="form-horizontal form-groups-bordered">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?= lang('select_department') ?>
                        <span class="required"> *</span></label>
                        <div class="col-sm-5">
                            <select name="departments_id" class="form-control select_box">
                                <option value="all"><?= lang('All Department') ?></option>
                                <?php if (!empty($all_department_info)): foreach ($all_department_info as $v_department_info) :
                                    if (!empty($v_department_info->deptname)) {
                                        $deptname = $v_department_info->deptname;
                                    } else {
                                        $deptname = lang('undefined_department');
                                    }
                                    ?>
                                    <option value="<?php echo $v_department_info->departments_id; ?>"
                                        <?php
                                        if (!empty($departments_id)) {
                                            echo $v_department_info->departments_id == $departments_id ? 'selected' : '';
                                        }
                                        ?>><?php echo $deptname ?></option>
                                <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?= lang('select') . ' ' . lang('month') ?> <span
                                class="required"> *</span></label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" value="<?php
                                if (!empty($payment_month)) {
                                    echo $payment_month;
                                }
                                ?>" class="form-control monthyear" name="payment_month" data-format="yyyy/mm/dd" autocomplete="off">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa fa-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-5">
                            <button id="submit" type="submit" name="flag" value="1"
                                    class="btn btn-primary btn-block"><?= lang('go') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php if (!empty($flag)): ?>
    <div class="row">
        <div class="col-sm-12" data-offset="0">
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="panel-title">
                        <span>
                            <strong><?= lang('generate_payslip_for') ?><?php
                                if (!empty($payment_month)) {
                                    echo ' <span class="text-danger">' . date('F Y', strtotime($payment_month)) . '</span>';
                                }
                                ?></strong>
                        </span>
                    </div>
                </div>
                <!-- Table -->
                <table class="table table-striped " id="datatable_action" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="col-sm-1" style="width: 119px;" ><?= lang('emp_id') ?></th>
                        <th><strong><?= lang('name') ?></strong></th>
                        <th><strong><?= lang('Pay Type') ?></strong></th>
                        <th><strong><?= lang('basic_salary') ?></strong></th>
                        <th><strong><?= lang('net_salary') ?></strong></th>
                        <th><strong><?= lang('details') ?></strong></th>
                        <th><strong><?= lang('status') ?></strong></th>
                        <th><?= lang('action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php /* ?>
					<tr><td><?php echo "<pre>"; print_r($customer_payment_dates); ?></td></tr>
					<?php */ ?>
                    <?php
					
                    $akey = 0;
                    if (!empty($employee_info)):foreach ($employee_info as $v_emp_info):
                        ?>
                        <?php if (!empty($v_emp_info)):
                        $akey += count($v_emp_info);
                        $key = $akey - 1;
						
						//echo "<pre>"; print_r($v_emp_info);

                        foreach ($v_emp_info as $v_employee): 
							if($v_employee->payfrequency == 'biweekly') {
								
                         $userId = $v_employee->user_id;
 
						//echo"<pre>"; print_r($v_employee); die;
						//check year is leap year
						if(date('L', mktime(0, 0, 0, 1, 1, $payment_year)) == 1) {
							//leap year code
							$customer_payment_dates[$v_employee->user_id][$payment_year.'-12']['26'] = $customer_payment_dates[$v_employee->user_id]['2021-01'];
							$payment_date = $customer_payment_dates[$v_employee->user_id][($payment_year+1).'-01']['26']['payment_date'];
						} else {
							$payment_date = '';
						}
						
				 $month = $v_employee->salary_template_id;
				 $hour  = $v_employee->hourly_rate_id;
				 if(!empty($month)){
				    $s_template_id = $month;
				 }else{
				    $s_template_id = $hour;	 
				 }
				$employeeEarning = $this->db->where('user_id', $userId)->where('salary_template_id', $s_template_id)->get('tbl_employee_salary_earnings')->result();
					 //echo  $sql = $this->db->last_query();			 
					 //echo"<pre>"; print_r($employeeEarning); die;
					 foreach($employeeEarning as $ernData){
					   if($ernData->earning_type == 'Regular hours'){	 
						 $rateOfRagularHour += $ernData->earning_amount;
					   }
					   if($ernData->earning_type == 'PTO hours'){
						 $rateOfPTOHour += $ernData->earning_amount;
					   }
					   
					   if($ernData->earning_type == 'Over time hours'){
						 $rateOfOverTimeHour += $ernData->earning_amount;	 
					   }
					   
					   if($ernData->amtType == 'fix'){
						   $ernAmt += $ernData->earning_amount;
					   }		   
					 }
					 /* exclude from tax deduction */
			 
			
			 
			 $exclude_deduction_info = $this->db->where('user_id', $userId)->where('salary_template_id', $s_template_id)->where('excludeFrTx', "yes")->get('tbl_employee_salary_deduction')->result();
			 
				foreach ($exclude_deduction_info as $v_Exdeduction_info){
					$exclude_deduction += $v_Exdeduction_info->deduction_amount;
				}
				
				/* deduction */
				$salary_deduction_info = $this->db->where('user_id', $userId)->where('salary_template_id', $s_template_id)->where('excludeFrTx', "no")->get('tbl_employee_salary_deduction')->result();
				//echo"<pre>"; print_r($salary_deduction_info); die;
				
			 /* tax calculation */
			   
		   $salary_tax_info = $this->db->where('user_id', $userId)->where('salary_template_id', $s_template_id)->get('tbl_employee_salary_tax')->result();
		   $taxArr = array();
 		   $k1 =0;
			 foreach($salary_tax_info as $v_tax_info){
				 //echo"<pre>"; print_r($v_tax_info);
                if($v_tax_info->taxCalc =='max'){
					$taxArr[] = $v_tax_info->taxWithholding;
				}elseif($v_tax_info->taxCalc=='rng'){
                    $taxArr[] = $v_tax_info->taxWithholding;
				}else{
					$taxArr[] = $v_tax_info->taxWithholding;
				}					
			 }	
			 foreach($taxArr as $tax){
				 $total_taxes +=$tax;
			 } 
						
						$biweekly_payment_arr = $customer_payment_dates[$v_employee->user_id][$payment_month];
						//echo "<pre>"; print_r($biweekly_payment_arr); exit;
						
						
						foreach($biweekly_payment_arr as $key=>$value) { 
						
						
						$payment_number = $key+1;
			//echo"<pre>"; print_r($taxArr);
			  //$salary_info = $this->payroll_model->check_by(array('user_id' => $v_employee->user_id, 'payment_month' => $payment_month, 'pay_frequency' => "biweekly", 'payment_number' => $payment_number), 'tbl_salary_payment');
			  
			  
            $data = array('user_id' => $v_employee->user_id, 'payment_month' => $payment_month,"pay_frequency"=>"biweekly","payment_number"=>$payment_number);
			$query = $this->db->select('*')->where($data)->get('tbl_salary_payment');
			$salary_info = $query->row();
			//echo"<pre>"; print_r( $v_employee); die;			
			   
			     
			 
			  if(isset($value['payment_date']) && !empty($value['payment_date'])) {
				$end_payment_date = $value['payment_date'];
			} else {
				$end_payment_date = $payment_date;
			}
			
			/* salary calculation for wages employee */
			
			 $day = strtotime($end_payment_date);
			 $payDate = strtotime($end_payment_date);
			 $endDay = date("Y-m-d",$day);
			 $startDate = $value['payment_start_date'];
			 $endDate = $endDay;
			 
			 $makeInfo = $this->db->where('user_id', $userId)->where('start_date', $startDate)->where('end_date', $endDate)->get('tbl_employee_hours')->result();			 
			 
			 $PTOHours = $makeInfo[0]->pto_hours;
			 $regularHours = $makeInfo[0]->regular_hours;
			 $overTimeHours = $makeInfo[0]->overtime_hours; 
			 		 
			 if(!empty($PTOHours)){
				 $PTOPrice = $PTOHours*$rateOfPTOHour;
			 }else{
				 $PTOPrice=0;
			 }
			 
			 if(!empty($regularHours)){
				 $rgPrice = $regularHours*$rateOfRagularHour;
			 }else{
				 $rgPrice=0;
			 }
			 
			 if(!empty($overTimeHours)){
				 $ovtPrice = $overTimeHours*$rateOfOverTimeHour;
			 }else{
				 $ovtPrice=0;
			 }
			 
			 /* total salary*/
			 /* echo"PTO ::".$PTOPrice."<br>";
			 echo"Reg ::".$rgPrice."<br>";
			 echo"Ovt ::".$ovtPrice."<br>";
			 die; */
			 
			 $totalHourlySalary = $PTOPrice + $rgPrice + $ovtPrice + $ernAmt;
			 
			 $grossSalary = $totalHourlySalary;
			 /* Start calculation */
			 $emp_salary_info = $this->payroll_model->get_emp_salary_list($userId);
			 //echo"<pre>"; print_r($emp_salary_info); die;
                                $awardAmount = 0;
				$this->payroll_model->_table_name = 'tbl_employee_award';
				$this->payroll_model->_order_by = 'user_id';
                                if((!empty($startDate) && $startDate != 'NULL') && (!empty($endDate) && $endDate != 'NULL')) {
                                    $award_info = $this->payroll_model->get_by(array('user_id' => $userId, 'given_date >=' => $startDate, 'given_date <=' => $endDate), FALSE);
                                    foreach($award_info as $awardAmt){
                                            //echo"<pre>"; print_r($awardAmt);
                                            $awardAmount += $awardAmt->award_amount;
                                    }		
                                
                                }		 
//echo  $sql = $this->db->last_query()."<br>"; 							
					
				//echo "Award:: ".$awardAmount."<br>";
				
				$total = $totalHourlySalary + $awardAmount;	
				
				 if(!empty($exclude_deduction)){
				   $totalSalaryAfterDeduction =  $total*$exclude_deduction/100;
				   $totalGP = $total-$totalSalaryAfterDeduction;
				 }else{
					$totalGP = $total;
				 }
				
				$deductionArr = array();
				foreach ($salary_deduction_info as $v_deduction_info){
					if($v_deduction_info->deductionBasedOn =="percentAmt"){
						$percent = $v_deduction_info->deductionPercent;
						$deduction =  $totalGP*$percent/100;
					}else{								
						$deduction= $v_deduction_info->deduction_amount;
					}
					$deductionArr[] =$deduction;
				}
				$total_deduction = 0;
				//echo"<pre>"; print_r($deductionArr);
				 foreach($deductionArr as $deduction){
				 //echo"<pre>"; print_r($deduction);
				 $total_deduction +=$deduction;
				 }
				
				//echo $total_deduction."<br>"; //die;
                    /* tax calculation */
				//echo "Gp :: ".$totalGP;	
				$taxes  = $totalGP * $total_taxes / 100;
				$taxVal = round($taxes, 2);
				
				//echo "ded :: ".$total_deduction;
		        $netIncome = $totalGP-($taxVal + $total_deduction);
			                
			 
							 
							 /* End calculation */
							 
		if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) {
		?>
		<tr>
			
			<td><?php echo $v_employee->employment_id; ?></td>
			<td><?php echo $v_employee->fullname; ?></td>
			<td><?php			
				
				$set_salary = false;
				if (!empty($v_employee->salary_grade)) {
					echo $v_employee->salary_grade . ' <small>(' . ucfirst($v_employee->payfrequency) . ')</small> - '.$end_payment_date;
				} else if (!empty($v_employee->hourly_grade)) {
					echo $v_employee->hourly_grade . ' <small>(' . ucfirst($v_employee->payfrequency) . ')</small> - '.$end_payment_date;
				} else {
					echo '<span class="text-danger">' . lang('did_not_set_salary_yet') . '</span>';
					$set_salary = true;
				}
				
				?></td>
				
			<td><?php
				if (!empty($v_employee->basic_salary)) {
					echo $v_employee->basic_salary;
				} else if (!empty($v_employee->hourly_grade)) {
					echo $v_employee->hourly_rate . ' <small>(' . lang('per_hour') . ')</small>';
				} else {
					echo '-';
				}
				?></td>
			<td>
			<?php 
			$v_total_hours = $total_hours_biweekly[$v_employee->user_id][$key+1];			
			$curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
			
			  /*  if (!empty($total_hours)) {
				   foreach($total_hours as $v_total_hours) {
						
							if (!empty($v_total_hours)) {
								$total_hour = $v_total_hours['total_hours'];
								$total_minutes = $v_total_hours['total_minutes'];
								if ($total_hour > 0) {
									$hours_ammount = $total_hour * $v_employee->hourly_rate;
								} else {
									$hours_ammount = 0;
								}
								if ($total_minutes > 0) {
									$amount = round($v_employee->hourly_rate / 60, 2);
									$minutes_ammount = $total_minutes * $amount;
								} else {
									$minutes_ammount = 0;
								}
								if (!empty($advance_salary[$index])) {
									$advance_amount = $advance_salary[$index]['advance_amount'];
								} else {
									$advance_amount = 0;
								}
								if (!empty($award_info[$index])) {
									$total_award = $award_info[$index]['award_amount'];
								} else {
									$total_award = 0;
								}
								$total_amount = $hours_ammount + $minutes_ammount + $total_award - $advance_amount;
								$amt = round($total_amount,2);
								if(!empty($amt)){
									echo display_money($amt, $curency->symbol);
								}
							}
					   
				   }
			    } */
			   
				if (!empty($v_employee->basic_salary)) {
					$userId   =  $v_employee->user_id; 
					$grossSalaryInfo = $this->db->where('user_id', $userId)->get('tbl_taxable_gross_salary')->result();
							$gp = $grossSalaryInfo[0]->taxable_gross_salary;
							
						    $emp_salary_info = $this->payroll_model->get_emp_salary_list($userId);                            						
							$basic_salary   =      $emp_salary_info->basic_salary;
							$salary_template_id  = $emp_salary_info->salary_template_id;
							
							/* basic salary earning amount */
							$salary_allowance_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "no")->get('tbl_employee_salary_earnings')->result();
							//echo"<pre>"; print_r($salary_allowance_info); die;
							foreach ($salary_allowance_info as $v_allowance_info){
							  $total_salary += $v_allowance_info->earning_amount; 
							}
							
							/* Exclude from tax earning amount */
							$exclude_earning_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "yes")->get('tbl_employee_salary_earnings')->result();
							foreach ($exclude_earning_info as $v_earning_info){
							  $totalEarning += $v_earning_info->earning_amount; 
							}
							
							
							/* Exclude form tax salary deduction amount */
							$exclude_deduction_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "yes")->get('tbl_employee_salary_deduction')->result();
							foreach ($exclude_deduction_info as $v_Exdeduction_info){
								$exclude_deduction += $v_Exdeduction_info->deduction_amount;
							}
							
							  /* Total earning amount */
							  $total = $total_salary + $basic_salary;	
							 /* Total gross pay amount */ 
							 if(!empty($exclude_deduction)){
							   $totalSalaryAfterDeduction =  $total*$exclude_deduction/100;
							   $totalGP = $total-$totalSalaryAfterDeduction;
							 }else{
								$totalGP = $total;
							 }
							 
							 /* Salary deduction amount */
							 $salary_deduction_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "no")->get('tbl_employee_salary_deduction')->result();
							//echo"<pre>"; print_r($salary_deduction_info); die;
							$deductionArr = array();
							foreach ($salary_deduction_info as $v_deduction_info){
								if($v_deduction_info->deductionBasedOn =="percentAmt"){
									$percent = $v_deduction_info->deductionPercent;
									$deductionArr[] =  $totalGP*$percent/100;
								}else{								
								    $deductionArr[]= $v_deduction_info->deduction_amount;
								}
								
								//echo "<br>".$total_deduction;
							}
							/* Total deduction amount */
							foreach($deductionArr as $deduction){
								
								$total_deduction +=$deduction;
							}
							
							/* Tax amount */
							$salary_tax_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->get('tbl_employee_salary_tax')->result();
							 //echo"<pre>"; print_r($salary_tax_info); die;
							 $taxArr = array();
							 $k1 =0;
							 foreach($salary_tax_info as $v_tax_info){
								 if($v_tax_info->taxCalc =='max'){
								    if($grossSalary < $v_tax_info->maxincome_threshold){
										//echo"asdasd"; die;
										$taxArr[] = $v_tax_info->tax_amount;		
									}
								 }elseif($v_tax_info->taxCalc=='rng'){
									 $query = $this->db->query('SELECT * FROM tbl_employee_salary_tax WHERE "'.$totalGP.'" BETWEEN minIncome AND maxIncome');
											$result = $query->result();
											$taxPercent = $result[$k1]->taxPercent;
											$taxArr[] = $totalGP*$taxPercent/100;
								 $k1++;
								 }else{
									$percentOth = $v_tax_info->taxOthPercent;
									$taxArr[] = $totalGP*$percentOth/100;
								 }
									 
							 }
							 /* Total tax amount */
							 foreach($taxArr as $tax){
								 $total_taxes +=$tax;
							 }
							 
					/* Total advanced salary amount */ 
					if (!empty($advance_salary)) {
						foreach ($advance_salary as $add_index => $v_advance) {
							if ($add_index == $v_employee->user_id) {
								$total_advance = $v_advance['advance_amount'];
							}
						}
					}
					
					/*Total award amount */
					$v_award_info = $award_info[$v_employee->user_id][$key+1];
					//echo "<pre>"; print_r($v_award_info); exit;
					if (!empty($v_award_info)) {
						
								$total_award = $v_award_info['award_amount'];
						   
					}
					
					/* $v_overtime = $overtime_info[$v_employee->user_id][$key+1];
					//echo "<pre>"; print_r($v_overtime); exit;
					if (!empty($v_overtime) && !empty($v_employee->overtime_salary)) {
					   
					$total_hour = $v_overtime['overtime_hours'];
					$total_minutes = $v_overtime['overtime_minutes'];
					if ($total_hour > 0) {
						$hours_ammount = $total_hour * $v_employee->overtime_salary;
					} else {
						$hours_ammount = 0;
					}
					if ($total_minutes > 0) {
						$amount = round($v_employee->overtime_salary / 60, 2);
						$minutes_ammount = $total_minutes * $amount;
					} else {
						$minutes_ammount = 0;
					}
					$total_amount = $hours_ammount + $minutes_ammount;
						   
					} */
                    
					if (empty($total_advance)) {
						$total_advance = 0;
					}
					if (empty($total_deduction)) {
						$total_deduction = 0;
					}
					if (empty($total_award)) {
						$total_award = 0;
					}
					
					$deduction = $total_taxes + $total_deduction + $total_advance;
					
					
					echo $netSalary =  display_money($salary_info->net_salary, $curency->symbol);
				    $gp = $totalGP;
					// check existing payment by employee id and payment month
				}else{ 
					if(!empty($salary_info->net_salary)){
					echo $netSalary = display_money($salary_info->net_salary, $curency->symbol);
					$gp = $totalGP;
					}else{
						echo $netSalary = display_money(00, $curency->symbol);
					}
				}
								
				if(!empty($netSalary)){				
				?>
				<input type="hidden" name="makePayment[<?php echo $v_employee->user_id; ?>][<?php echo $key; ?>]" value="<?php echo $v_employee->basic_salary."/".$netSalary."/".$v_employee->payment_type."/".$v_employee->departments_id."/".$payment_month."/biweekly"."/".$payment_number."/".strtotime($end_payment_date)."/".strtotime($value['payment_start_date'])."/".$v_employee->fullname."/".$gp; ?>" />
				<?php }else{ ?>				
				<input type="hidden" name="makePayment[<?php echo $v_employee->user_id; ?>][<?php echo $key; ?>]" value="<?php echo"0/".$amt."/".$v_employee->payment_type."/".$v_employee->departments_id."/".$payment_month."/biweekly"."/".$payment_number."/".strtotime($end_payment_date)."/".strtotime($value['payment_start_date'])."/".$v_employee->fullname."/".$gp; ?>" /> 
				<?php } ?>
				</td>
			<td><?php if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) { ?>
					<a href="<?php echo base_url() ?>admin/payroll/salary_payment_details/<?php echo $salary_info->salary_payment_id. '/biweekly/'.strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date); ?>"
					   class="btn btn-info btn-xs" title="View" data-toggle="modal"
					   data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>
				<?php } else { ?>
					<a href="<?php echo base_url() ?>admin/payroll/view_payment_details/<?php echo $v_employee->user_id . '/' . $payment_month.'/biweekly/'.strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date); ?>"
					   class="btn btn-info btn-xs" title="View" data-toggle="modal"
					   data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>

				<?php } ?>
				
				
			</td>
			<td>
				<?php if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) { ?>
					<span class="label label-success"><?= lang('paid') ?></span>							
					<?php
				} else {
					if (empty($set_salary)) {
						?>
						<span class="label label-danger"><?= lang('unpaid') ?></span>
						
					<?php }
				} ?>
			</td>
			<td>
			   <a class="text-success" target="_blank"
					   href="<?php echo base_url() ?>admin/payroll/receive_generated/<?php echo $salary_info->salary_payment_id. '/biweekly/'.strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date); ?>"><?= lang('generate_payslip') ?></a>
					
				
			</td>
		</tr>
	<?php  } 
	}
	} 
						endforeach; ?>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endif; ?>

<!-- end -->
