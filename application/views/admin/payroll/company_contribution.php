<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="panel panel-custom"><!-- *********     Employee Search Panel ***************** -->
            <div class="panel-heading">
                <div class="panel-title">
                    <strong><?= lang('Company Contribution') ?></strong>
                </div>
            </div>
            <form id="form" role="form" enctype="multipart/form-data"
                  action="<?php echo base_url() ?>admin/payroll/company_contribution" method="post" class="form-horizontal form-groups-bordered" autocomplete="off">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?= lang('search_type') ?> <span class="required"> *</span></label>

                        <div class="col-sm-5">
                            <select required name="search_type" id="search_type" class="form-control ">
                                <option value=""><?= lang('select') . ' ' . lang('search_type') ?></option>
                                <option value="employee" <?php if (!empty($search_type)) {
                                    echo $search_type == 'employee' ? 'selected' : '';
                                } ?>><?php echo lang('by') . ' ' . lang('employee') ?></option>

                                <option value="month" <?php if (!empty($search_type)) {
                                    echo $search_type == 'month' ? 'selected' : '';
                                } ?>><?php echo lang('by') . ' ' . lang('month') ?></option>

                                <option value="period" <?php if (!empty($search_type)) {
                                    echo $search_type == 'period' ? 'selected' : '';
                                } ?>><?php echo lang('by') . ' ' . lang('period') ?></option>
								
								
								
                            </select>
                        </div>
                    </div>
                    
                    <div class="by_employee"
                         style="display: <?= !empty($search_type) && $search_type == 'employee' ? 'block' : 'none' ?>">
                        <div class="form-group">
                            <label for="field-1"
                                   class="col-sm-3 control-label"><?= lang('employee') . ' ' . lang('name') ?>
                                <span
                                    class="required"> *</span></label>

                            <div class="col-sm-5">
                                <select class="by_employee form-control select_box" style="width: 100%" name="user_id">
                                    <option value=""><?= lang('select_employee') ?>...</option>
                                    <?php
                                    $all_employee = $this->payroll_model->get_all_employee();
                                    if (!empty($all_employee)): ?>
                                        <?php foreach ($all_employee as $dept_name => $v_all_employee) : ?>
                                            <optgroup label="<?php echo $dept_name; ?>">
                                                <?php if (!empty($v_all_employee)):foreach ($v_all_employee as $v_employee) : ?>
                                                    <option value="<?php echo $v_employee->user_id; ?>"
                                                        <?php
                                                        if (!empty($user_id)) {
                                                            echo $v_employee->user_id == $user_id ? 'selected' : '';
                                                        }
                                                        ?>><?php echo $v_employee->fullname . ' ( ' . $v_employee->designations . ' )' ?></option>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                            </optgroup>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
						<div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?= lang('Select Category');  ?>
                             <span class="required"> *</span></label>
                            <div class="col-sm-5">
                                <select class="by_category form-control select_box" style="width: 100%" name="category_id">
                                    <option><?= lang('All Category') ?>...</option>
                                    <?php
									$all_category = $this->db->select('*')->get('tbl_contribution_category')->result();
                                    if (!empty($all_category)): ?>
                                        <?php foreach ($all_category as $category) : ?>
                                           <option <?php if($category_id == $category->id){ echo"selected"; } ?> value="<?php echo $category->id; ?>"><?php echo $category->category_name; ?></option>
                                         <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="by_month"
                         style="display: <?= !empty($search_type) && $search_type == 'month' ? 'block' : 'none' ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('Select') . ' ' . lang('month') ?> <span
                                    class="required"> *</span></label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" value="<?php
                                    if (!empty($by_month)) {
                                        echo $by_month;
                                    }
                                    ?>" class="form-control monthyear by_month" name="by_month" data-format="yyyy/mm/dd" autocomplete="off">

                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?= lang('Select Category');  ?>
                             <span class="required"> *</span></label>
                            <div class="col-sm-5">
                                <select class="by_category form-control select_box" style="width: 100%" name="category_id">
                                    <option><?= lang('All Category') ?>...</option>
                                    <?php
									$all_category = $this->db->select('*')->get('tbl_contribution_category')->result();
                                    if (!empty($all_category)): ?>
                                        <?php foreach ($all_category as $category) : ?>
                                           <option <?php if($category_id == $category->id){ echo"selected"; } ?> value="<?php echo $category->id; ?>"><?php echo $category->category_name; ?></option>
                                         <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="by_period"
                         style="display: <?= !empty($search_type) && $search_type == 'period' ? 'block' : 'none' ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('start') . ' ' . lang('month') ?> <span
                                    class="required"> *</span></label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" value="<?php
                                    if (!empty($start_month)) {
                                        echo $start_month;
                                    }
                                    ?>" class="by_period form-control monthyear" name="start_month"
                                           data-format="yyyy/mm/dd">

                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?= lang('end') . ' ' . lang('month') ?> <span
                                    class="required"> *</span></label>
                            <div class="col-sm-5">
                                <div class="input-group">
                                    <input type="text" value="<?php
                                    if (!empty($end_month)) {
                                        echo $end_month;
                                    }
                                    ?>"class="by_period form-control monthyear" name="end_month"
                                           data-format="yyyy/mm/dd">

                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa fa-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="border-none">
                        <label for="field-1" class="col-sm-3 control-label"></label>
                        <div class="col-sm-5">
                            <button id="submit" type="submit" name="flag" value="1"
                                    class="btn btn-primary btn-block"><?= lang('go') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- ******************** Employee Search Panel Ends ******************** -->
    </div>
</div>
<?php 
//echo"Hi"; print_r($search_type); die;
if (!empty($search_type) && $search_type != 'activities') {
if ($search_type == 'period') {
    $by = ' - ' . ' ' . date('F-Y', strtotime($start_month)) . ' ' . lang('TO') . ' ' . date('F-Y', strtotime($end_month));
    $pdf = $start_month . 'n' . $end_month;
	$sdate = $start_month;
	$edate = $end_month;
	
}
if ($search_type == 'month') {
    $by = ' - ' . ' ' . date('F-Y', strtotime($by_month));
    $pdf = $by_month;
	$pdf_cat = $category_id;
}


if ($search_type == 'employee') {
    $user_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
    $by = ' - ' . ' ' . ' ' . $user_info->fullname;
    $pdf = $user_id;
	$pdf_cat = $category_id;
}
 /* echo $pdf;
 echo "kdf".$pdf_cat; */
 
?>
<div id="payment_history" class="all_payment_history">
    <div class="show_print" style="width: 100%; border-bottom: 2px solid black;margin-bottom: 30px">
        <!-- show when print start-->
        <table style="width: 100%; vertical-align: middle;">
            <tr>
                <td style="width: 50px; border: 0px;">
                    <img style="width: 50px;height: 50px;margin-bottom: 5px;" src="<?= base_url() . config_item('company_logo') ?>" alt="" class="img-circle"/>
                </td>

                <td style="border: 0px;">
                    <p style="margin-left: 10px; font: 14px lighter;"><?= config_item('company_name') ?></p>
                </td>
            </tr>
        </table>
    </div>
    <!--  **************** show when print End ********************* -->
    <div class="panel panel-custom">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <div class="panel-title">
                <strong><?= lang('Company Contribution') . ' ' . $by ?></strong>
                <div class="pull-right"><!-- set pdf,Excel start action -->
                    <label class="hidden-print control-label pull-left hidden-xs">
                       
                </div><!-- set pdf,Excel start action -->
            </div>
        </div>
		<?php 
         $currency = $this->payroll_model->check_by(array('code' => config_item('default_currency')), 'tbl_currencies');
		 //echo $search_type; die;
          if($search_type == 'employee'){
			  
			$contributionInfoInfo =  $this->db->select('t1.*, t2.*')
			 ->from('tbl_salary_payment as t1')
			 ->where('t1.user_id', $pdf)->or_where('t2.companyCategory_id',$pdf_cat)
			 ->join('tbl_salary_payment_contribution as t2', 't1.salary_payment_id = t2.salary_payment_id', 'INNER')
			 ->get()->result();
			 $str = $this->db->last_query();
              //echo "<pre>"; print_r($str);	exit;
			   
		  }elseif($search_type == 'month'){
			 // echo $pdf_cate; die;
			 $contributionInfoInfo =  $this->db->select('t1.*, t2.*')
			 ->from('tbl_salary_payment as t1')
			 ->where('t1.payment_month', $pdf)->or_where('t2.companyCategory_id', $pdf_cat)
			 ->join('tbl_salary_payment_contribution as t2', 't1.salary_payment_id = t2.salary_payment_id', 'INNER')
			 ->get()->result();
			 $str = $this->db->last_query();
   /* 
					echo "<pre>";
					print_r($str);
					exit; */
		  }elseif($search_type == 'period'){ 
		     $contributionInfoInfo =  $this->db->select('t1.*, t2.*')
			 ->from('tbl_salary_payment as t1')
			 ->join('tbl_salary_payment_contribution as t2', 't1.salary_payment_id = t2.salary_payment_id', 'LEFT')
			 ->where('t1.payment_month >=', $sdate)
			 ->where('t1.payment_month <=', $edate)
			 
			 ->get()->result();
		  }
		  
		 // echo $sdate."<br />".$edate;
		  
		//echo"<pre>"; print_r($contributionInfoInfo); 
		?>
        <!-- Table -->
        <table class="table table-striped " id="DataTables" cellspacing="0" width="100%">
            <thead>
            <tr>
               <th><?= lang('S.No') ?></th>
               <th><?= lang('Name') ?></th>
			   <th><?= lang('Pay Period') ?></th>
               <th><?= lang('Payment Date') ?></th>
               <th><?= lang('Contribution Type') ?></th>
               <th><?= lang('Contribution Amount') ?>(<?php echo $currency->symbol; ?>)</th>
               
			</tr>
            </thead>
            <tbody>
			<?php 
			if(!empty($contributionInfoInfo)){
			$count = 1;
			foreach($contributionInfoInfo as $contribution){ 
			$userId = $contribution->user_id;
			$useInfo = $this->db->where('user_id', $userId)->get('tbl_account_details')->result();
			//echo"<pre>"; print_r($useInfo);
			if(!empty($contribution->salary_payment_contribution_value)){
				$date =  date_create($contribution->paid_date);
                $paiddate =  date_format($date,"Y/m/d");
				$val = $contribution->salary_payment_contribution_value;
				
				
			?>
				<tr>
				    <td><?=$count?></td>
				    <td><?=$useInfo[0]->fullname ?></td>
					<td><?=$contribution->payment_start_date."-".$contribution->payment_end_date ?></td>
					<td><?= $paiddate ?></td>					
					<td><?=$contribution->salary_payment_contribution_label ?></td>
					<td><?php if(!empty($val)){  echo "$".$val; }?></td>
					<td></td>
				</tr>
			<?php 
			}
			$count++; }
			}else{ ?>
			      <tr>
				    <td colspan="6">No record found!</td>
				</tr>			
			<?php } ?>
            </tbody>
        </table>
    </div><!--************ Payment History End***********-->
    <?php } ?>
</div>
<script>
$(function() { 
    $('#search_type').change(function(){
        if($('#search_type').val() == 'category') {
            $('.by_category').show(); 
        } 
    });
});
</script>