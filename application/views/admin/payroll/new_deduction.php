<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('New Deduction') ?></h4>
    </div>
	
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/payroll/save_deduction_info/<?php echo $profile_info->user_id; ?>/<?php
              if (!empty($deduction_info->deduction_id)) {
                  echo $deduction_info->deduction_id;
              }
              ?>" method="post" class="form-horizontal form-groups-bordered">

            <!-- CV Upload -->
			<?php 
			if($salary_info->user_payType =='salary'){ ?>
			<input type="hidden" name="salary_template_id" value="<?=$salary_info->monthly_template_id ?>"/>
			<?php }else{ ?>
			<input type="hidden" name="hourly_template_id" value="<?=$salary_info->hourly_rate_id ?>"/>
			<?php } ?>
			<input type="hidden" name="salary_type" value="<?=$salary_info->user_payType ?>"/>
            <div class="">
                <label class="control-label"><?= lang('Deduction type ') ?> <span class="text-danger">*</span></label>
                <input required type="text" name="deduction_type" value="<?php
                if (!empty($deduction_info->deduction_type)) {
                    echo $deduction_info->deduction_type;
                }
                ?>" class="form-control">
                <input type="hidden" required name="deduction_id" value="<?php
                if (!empty($deduction_info->deduction_id)) {
                    echo $deduction_info->deduction_id;
                }
                ?>" class="form-control">
            </div>
			<div class="">
                <label class="control-label"><?= lang('Deduction Based On') ?></label>
				<select name="deductionBasedOn" id="deductionBasedOn" class="form-control">
				  <option value="percentAmt">Percentage</option>
				  <option value="fixAmt">Fixed</option>				  
				</select>
            </div>
            <div class="deductionPercent">
                <label class="control-label"><?= lang('Deduction percentage(%)') ?></label>
				<input type="text" maxlength="7" name="deductionPercent"  id="deductionPercent" value="<?php
                if (!empty($deduction_info->deductionPercent)) {
                    echo $deduction_info->deductionPercent;
                }
                ?>" class="form-control">
            </div>
			
			<div class="deductionAmount" style="display:none">
                <label class="control-label"><?= lang('Deduction amount($)') ?></label>
				<input type="text" name="deduction_amount" maxlength="7" id="deductionAmount" value="<?php
                if (!empty($deduction_info->deduction_amount)) {
                    echo $deduction_info->deduction_amount;
                }
                ?>" class="form-control">
               
            </div>
			
			
            <div class="form-group">
                    <div class="col-sm-6">
                     <label class="control-label"><?= lang('Exclude from taxes') ?> </label>	
                    </div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input type="radio" name="excludeFrTx" value="yes" id="excludeFrTx">
						 Yes
						</div>
					</div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input type="radio" name="excludeFrTx" value="no" id="excludeFrTx">
						 No
						</div>
					</div>       
            </div>			 
            <div class="">
                <label class="control-label"><?= lang('Effective date') ?><span class="text-danger">*</span></label>
				<div class="input-group">
                        <input type="text" onkeydown="return false" name="effective_date" placeholder="" class="form-control effective_date" required="" value="<?php
						if (!empty($deduction_info->effective_date)) {
							echo $deduction_info->effective_date;
						}
						?>" data-parsley-id="8">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
               
            </div>
            	
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>
<script>
$(function() {
	  $('#deductionPercent').on('input', function() {
		this.value = this.value
		  .replace(/[^\d.]/g, '')             // numbers and decimals only
		  .replace(/(^[\d]{2})[\d]/g, '$1')   // not more than 2 digits at the beginning
		  .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
		  .replace(/(\.[\d]{4})./g, '$1');    // not more than 4 digits after decimal
	  });
	});
   $(document).ready(function() {
	   
	   $('body').on('change', '#deductionBasedOn', function() {
		  var selectedVal = this.value;
		  if(selectedVal=='fixAmt'){			  
			  $(".deductionPercent").css("display","none");
			  $(".deductionAmount").css("display","block");
		  }else{
			  $(".deductionAmount").css("display","none");
			  $(".deductionPercent").css("display","block");
			  
		  }
		});
		
		
        $('#deductionAmount').keypress(function (event) {
            return isNumber(event, this)
        });
		
    });
   // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
$("input.effective_date").datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		startDate: new Date()
	});    
</script>