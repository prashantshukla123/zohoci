<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('New Earnings') ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/payroll/save_earning_info/<?php echo $profile_info->user_id; ?>/<?php
              if (!empty($earning_info->earning_id)) {
                  echo $earning_info->earning_id;
              }
              ?>" method="post" class="form-horizontal form-groups-bordered">

            <!-- CV Upload -->
			<?php 
			
			if($salary_info->user_payType =='salary'){ ?>
			<input type="hidden" name="salary_template_id" value="<?=$salary_info->monthly_template_id ?>"/>
			<input type="hidden" name="salary_type" value="<?=$salary_info->user_payType ?>"/>
			<?php }else{ ?>
			<input type="hidden" name="hourly_template_id" value="<?=$salary_info->hourly_rate_id ?>"/>
			<input type="hidden" name="salary_type" value="hourly"/>
			<?php } ?>
			
			
            <div class="">
                <label class="control-label"><?= lang('Earning type ') ?> <span class="text-danger">*</span></label>
                <input  <?php if($earning_info->amtType =='perhour'){echo "readonly"; } ?>  required type="text" name="earning_type" value="<?php
                if (!empty($earning_info->earning_type)) {
                    echo $earning_info->earning_type;
                }
                ?>" class="form-control">
                <input type="hidden" required name="earning_id" value="<?php
                if (!empty($earning_info->earning_id)) {
                    echo $earning_info->earning_id;
                }
                ?>" class="form-control">
				<input type="hidden" required name="amtType" value="<?php if (!empty($earning_info->amtType)) { echo $earning_info->amtType; } ?>" class="form-control">
            </div>
            <div class="">
                <label class="control-label"><?= lang('Earning amount($)') ?><span
                        class="text-danger">*</span></label>
                <input type="text" required name="earning_amount"  maxlength="7" id="earningAmount" value="<?php
                if (!empty($earning_info->earning_amount)) {
                    echo $earning_info->earning_amount;
                }
                ?>" class="form-control">
            </div>
			
			<div class="form-group">
                    <div class="col-sm-6">
                     <label class="control-label"><?= lang('Exclude from taxes') ?> </label>	
                    </div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input type="radio" name="excludeFrTx" value="yes" id="excludeFrTx">
						 Yes
						</div>
					</div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input type="radio" name="excludeFrTx" value="no" id="excludeFrTx">
						 No
						</div>
					</div>
					                
            </div>
			
            <div class="">
                <label class="control-label"><?= lang('Effective date') ?><span class="text-danger">*</span></label>
				<div class="input-group">
                        <input type="text" onkeydown="return false" name="effective_date" placeholder="" class="form-control effective_date" required="" value="<?php
						if (!empty($earning_info->effective_date)) {
							echo $earning_info->effective_date;
						}
						?>" data-parsley-id="8">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
               
            </div>
            	
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>
<script>
   $(document).ready(function() {
        $('#earningAmount').keypress(function (event) {
            return isNumber(event, this)
        });
		
    });
   // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
	
	$("input.effective_date").datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		startDate: new Date()
	});
</script>