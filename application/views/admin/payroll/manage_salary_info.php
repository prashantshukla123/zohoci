<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!--<div class="btn" style="margin-bottom:10px;">
		<input type="button" value="Go Back" onclick="goBack()" />
	</div> -->
<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang('manage_salary_details')." For ".$employee_info[0]->fullname ?></strong>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-12">
         <div class=" mt-lg">
		 <div class="tab-pane active" id="general_info"
                     style="position: relative;">
                    <div class="panel panel-custom">
                        <div class="panel-body">
                           <form  role="form" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/payroll/manage_salary_genInfo/<?php if($salary_general_info[0]->id ){ echo $salary_general_info[0]->id;} ?> " method="post" class="form-horizontal form-groups-bordered" id="myform">
						  
						  <input type="hidden" name="user_id" value="<?php echo $userId ?>" />
						  <input type="hidden" name="payroll_id" value="<?php echo $salary_general_info[0]->id ?>" />
                        <div class="row">
                            <div class="col-sm-12 form-groups-bordered">
                                <div class="form-group" id="border-none">
                                 <label for="field-1" class="col-sm-3 control-label"><?= lang('Location') ?></label>
                                  <div class="col-sm-5">
								  <?php 
								   if(!empty($user_location[0]->warehouse_name)){
								   $location = $user_location[0]->warehouse_name.','.$user_location[0]->city;
								   }else{
									   $location='';
								   }
								   if(isset($location) && !empty($location)) {
								  ?>
								  <input readonly type="text" class="input-sm form-control" value="<?=$location?>" name="location_id">
								  <?php } else { echo "N/A"; } ?>
								  </div>								  
								</div>								  
                               <div class="form-group" id="border-none">
                                 <label for="field-1" class="col-sm-3 control-label"><?= lang('Pay Type') ?><span class="required"> *</span></label>
                                  <div class="col-sm-5">
                                   <select class="  form-control select_box" id="userPayType" style="width: 100%" name="user_payType"> 
								     <option value="">Pay Type</option>
								     <option <?php if($salary_general_info[0]->user_payType=='hourly'){ echo"selected"; }?> value="hourly">Hourly</option>
								     <option <?php if($salary_general_info[0]->user_payType=='salary'){ echo"selected"; }?> value="salary">Salary</option>
                                    </select>
                                    </div>
                                </div>
								<?php 
								if($salary_general_info[0]->hourly_rate_id !=0){
								  $display_mth = 'display:none';	
								}elseif($salary_general_info[0]->monthly_template_id !=0){
								  $display_hr = 'display:none';
								}
								?>
								<div style="<?php echo $display_hr; ?>" id="hourly" class="form-group" id="border-none">
                                 <label for="field-1" class="col-sm-3 control-label"><?= lang('Select Hourly Grade') ?><span class="required"> *</span></label>
                                  <div class="col-sm-5">
                                   <select name="hourly_rate_id" id="hourly_rate_id" class="form-control select_box">
									<option value=""><?= lang('select_hourly_grade') ?></option>
									<?php if (!empty($hourly_grade)) : 
									foreach ($hourly_grade as $v_hourly_grade) : ?>
									<option value="<?php echo $v_hourly_grade->hourly_rate_id ?>"
									<?php
									if ($v_hourly_grade->hourly_rate_id == $salary_general_info[0]->hourly_rate_id) {
									echo'selected ';
									}
									?> >
									<?php echo $v_hourly_grade->hourly_grade ?></option>;
									<?php endforeach; ?>
									<?php endif; ?>
									</select>
                                    </div>
                                </div>
								
								<div style="<?php echo $display_mth; ?>" id="monthly" class="form-group" id="border-none">
                                 <label for="field-1" class="col-sm-3 control-label"><?= lang('Select Monthly Grade') ?><span class="required"> *</span></label>
                                  <div class="col-sm-5">
									<select name="monthly_template_id" id="monthly_template_id" class="form-control select_box">
									<option value=""><?= lang('select_monthly_grade') ?></option>
									<?php if (!empty($salary_grade)) : foreach ($salary_grade as $v_salary_info) : ?>
									<option value="<?php echo $v_salary_info->salary_template_id ?>"
									<?php
									if ($v_salary_info->salary_template_id == $salary_general_info[0]->monthly_template_id) {
									echo'selected ';
									}
									?> >
									<?php echo $v_salary_info->salary_grade ?></option>;
									<?php endforeach; ?>
									<?php endif; ?>
									</select>
                                    </div>
                                </div>
								
								<div id="payfrequency" class="form-group" id="border-none">
                                 <label for="field-1" class="col-sm-3 control-label"><?= lang('Pay frequency') ?><span class="required"> *</span></label>
                                  <div class="col-sm-5">
                                   <select class="  form-control select_box" style="width: 100%" name="payfrequency" required="true"> 
								     <option value="">Select frequency</option>						   
									 <option <?php if($salary_general_info[0]->payfrequency=='biweekly'){ echo "selected";}?> value="biweekly">Bi-weekly Pay</option> 									
                                    </select>
                                    </div>
                                </div>
								

                                <div class="btn-bottom-toolbar text-right">
                                 <button type="submit" class="btn btn-sm btn-primary"><?= lang('save') ?></button>
                                    
                                </div>

                            </div>
                        </div>
                    </form>
                    </div>
                    </div>
                </div>
				
		<?php if(!empty($salary_general_info[0]->user_payType)){ ?>		
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
               	<li class="earnings_list active"><a href="#earning_info" data-toggle="tab"><?= lang('Earnings')?></a>
                </li>

                <li class="my_leave"><a href="#texes"data-toggle="tab"><?= lang('Taxes') ?></a>
                </li>
                <li class="all_leave"><a href="#deductions"data-toggle="tab"><?= lang('Deductions') ?></a>
                </li>                
                <li class=""><a href="#companyContributions"data-toggle="tab"><?= lang('Company Contributions') ?></a>
                </li>
				<li class=""><a href="#other"data-toggle="tab"><?= lang('Other Benefits And Information') ?></a>
                </li>	
                
            </ul>
			
            <div class="tab-content" style="border: 0;padding:0;">                
				<div class="tab-pane active" id="earning_info" style="position: relative;">
                    <div class="panel panel-custom">
					<?php
						$earning_info = $this->db->where('user_id', $userId)->get('tbl_employee_salary_earnings')->result();
						
						?>
                        <div class="panel-body">
                          <div class="earning-history">
						  <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/payroll/new_earning/<?php echo $userId; ?>" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
						 
						<div class="panel-body form-horizontal">
							<table class="table table-striped " cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= lang('Earning type') ?></th>
									<th><?= lang('Earning amount') ?></th>
									<?php //if($earning_info[0]->allowance_type !=='Wage'){ ?>
									<th><?= lang('Exclude from tax') ?></th>
									<th><?= lang('Effective date') ?></th>
									<?php //} ?>
									<th class="hidden-print"><?= lang('action') ?></th>
								</tr>
								</thead>
								<tbody>
								<?php if (!empty($earning_info)) {
									foreach ($earning_info as $earning) { ?>
										<tr>
											<td><?= $earning->earning_type ?></td>
											<?php if($earning->allowance_type=='hourly'){
											 ?>
											<td>
											<?php if($earning->amtType == 'perhour'){ ?>
											<?= "$".$earning->earning_amount."/hour" ?>
											<?php }else{ ?>
											 <?= "$".$earning->earning_amount ?>
											<?php } ?>
											</td>
											<?php }else{?>
											<td><?= "$".$earning->earning_amount ?></td>
											<?php }?>
											<?php //if($earning->allowance_type !=='Wage'){ ?>
											<td><?php if($earning->excludeFrTx == 'yes') { echo "Yes"; } else { echo "No"; } ?></td>
											<td><?php if($earning->effective_date != '0000-00-00') { echo $earning->effective_date; } else { echo '-'; } ?></td>
											<?php //} ?>
											<td class="hidden-print">
												<?= btn_edit_modal('admin/payroll/new_earning/' . $earning->user_id.'/'.$earning->	earning_id) ?>
												<?= btn_delete('admin/payroll/delete_employee_earning/' . $earning->user_id . '/' . $earning->earning_id) ?>
											</td>
										</tr>
									<?php }
								}else{
									echo"<tr><td colspan='4'>No Result Found!</td></tr>";
								}
								?>
								</tbody>
							</table>
						</div>
						 </div>
                        </div>
                    </div>
                </div>
                 <div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="texes" style="position: relative;">
                    <div class="panel panel-custom">
                        <div class="panel-body">
                          <div class="earning-history">
						  <?php 
						  	$grossSalaryInfo = $this->db->where('user_id', $userId)->get('tbl_taxable_gross_salary')->result();
							$gp = $grossSalaryInfo[0]->taxable_gross_salary;
							
						    $emp_salary_info = $this->payroll_model->get_emp_salary_list($userId);
                            //echo"<pre>"; print_r($emp_salary_info); die; 						
							$basic_salary   =        $emp_salary_info->basic_salary;
							$salary_template_id   =        $emp_salary_info->salary_template_id;
							
							$salary_allowance_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "no")->get('tbl_employee_salary_earnings')->result();
							//echo"<pre>"; print_r($salary_allowance_info); die;
							foreach ($salary_allowance_info as $v_allowance_info){
							  $total_salary += $v_allowance_info->earning_amount; 
							}
							
							$exclude_earning_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "yes")->get('tbl_employee_salary_earnings')->result();
							foreach ($exclude_earning_info as $v_earning_info){
							  $totalEarning += $v_earning_info->earning_amount; 
							}
							
							
							
							$exclude_deduction_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "yes")->get('tbl_employee_salary_deduction')->result();
							foreach ($exclude_deduction_info as $v_Exdeduction_info){
								$exclude_deduction += $v_Exdeduction_info->deduction_amount;
							}
							
							$total = $total_salary + $basic_salary;	
							
							 if(!empty($exclude_deduction)){
							   $totalSalaryAfterDeduction =  $total*$exclude_deduction/100;
							   $totalGP = $total-$totalSalaryAfterDeduction;
							 }else{
								$totalGP = $total;
							 }
							 $salary_deduction_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->where('excludeFrTx', "no")->get('tbl_employee_salary_deduction')->result();
							//echo"<pre>"; print_r($salary_deduction_info); die;
							$deductionArr = array();
							foreach ($salary_deduction_info as $v_deduction_info){
								if($v_deduction_info->deductionBasedOn =="percentAmt"){
									$percent = $v_deduction_info->deductionPercent;
									$deductionArr[] =  $totalGP*$percent/100;
								}else{								
								    $deductionArr[]= $v_deduction_info->deduction_amount;
								}
								
								//echo "<br>".$total_deduction;
							}
							foreach($deductionArr as $deduction){
								
								$total_deduction +=$deduction;
							}
							
							$salary_tax_info = $this->db->where('user_id', $userId)->where('salary_template_id', $salary_template_id)->get('tbl_employee_salary_tax')->result();
							//echo  $str = $this->db->last_query();
							 //echo"<pre>"; print_r($salary_tax_info); die;
							 $taxArr = array();
							 $k1 =0;
							 foreach($salary_tax_info as $v_tax_info){
								 if($v_tax_info->taxCalc =='max'){
								    if($grossSalary < $v_tax_info->maxincome_threshold){
										//echo"asdasd"; die;
										$taxArr[] = $v_tax_info->tax_amount;		
									}
								 }elseif($v_tax_info->taxCalc=='rng'){
									 $query = $this->db->query('SELECT * FROM tbl_employee_salary_tax WHERE "'.$totalGP.'" BETWEEN minIncome AND maxIncome');
											$result = $query->result();
											$taxPercent = $result[$k1]->taxPercent;
											$taxArr[] = $totalGP*$taxPercent/100;
								 $k1++;
								 }else{
									$percentOth = $v_tax_info->taxOthPercent;
									$taxArr[] = $totalGP*$percentOth/100;
								 }
									 
							 }
							 foreach($taxArr as $tax){
								 $total_taxes +=$tax;
							 }
							 //echo"<pre>"; print_r($total_taxes); die;
							 
							 //netpay
							 $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
							
                            ?>						  
						  <form style="border-bottom: solid 1px #dde6e9;display: flex;padding-bottom: 11px;" action="<?php echo base_url() ?>admin/payroll/manage_taxable_gross_salary/" method="post">
						  <div class="col-sm-12">
						   <div class="col-sm-6">
								<label class="control-label"><?= lang('Gross Calculation Salary(Annually)') ?></label>
								<input type="text" required id="taxable_gross_salary" name="taxable_gross_salary" class="form-control" value="<?php if($gp){ echo $gp; }?>">
								<input type="hidden" name="user_id" value="<?php echo $userId; ?>" class="form-control">
								
							</div>
							<div class="col-sm-6" style="margin-top: 25px;">							
								<button type="submit" class="btn btn-sm btn-primary" >Submit</button>
							</div>							 
							</div>
						  </form>
						  <div id="alert" style="display:none"> Please add Gross Calculation Salary(Annually)') </div>
						  <?php 
						$grossSalaryInfo = $this->db->where('user_id', $userId)->get('tbl_taxable_gross_salary')->result();
						$grosspay = $grossSalaryInfo[0]->taxable_gross_salary;
						if(!empty($grosspay)){
						?>
						  <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/payroll/new_tax/<?php echo $userId; ?>" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
						  
						<?php  }else{ ?>
						      <a href="javascript:void(0);"  id="taxNew" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
						<?php }
						$tax_info = $this->db->select('tbl_employee_salary_tax.*,tbl_tax_categories.tax_category_name','tbl_tax_categories.*')->where('user_id', $userId)->join('tbl_tax_categories', 'tbl_tax_categories.id = tbl_employee_salary_tax.taxCategory', 'left')->get('tbl_employee_salary_tax')->result();
						 /* echo $this->db->last_query(); 
						echo"<pre>"; print_r($tax_info); die; */
						?>
						<div class="panel-body form-horizontal">
							<table class="table table-striped " cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= lang('Tax type') ?></th>
									<th><?= lang('Tax amount') ?></th>
									<th><?= lang('Withholding Percent') ?></th>
									<th><?= lang('Tax type category') ?></th>
									<th><?= lang('Effective date') ?></th>
									<th class="hidden-print"><?= lang('action') ?></th>
								</tr>
								</thead>
								<tbody>
								<?php								
								if (!empty($tax_info)) {
									$k =0;
									foreach ($tax_info as $tax) { 
										$taxCalc = $tax->taxCalc;
										// die;
										if($taxCalc =='max'){
											?>
											<tr>										
												<td><?= $tax->tax_type ?></td>		
												<td><?= display_money($tax->tax_amount, $curency->symbol); ?></td>
												<td><?= $tax->taxWithholding."%"?></td>
												<td><?= $tax->tax_category_name ?></td>
												<td><?= $tax->effective_date ?></td>
												<td class="hidden-print">
													<?= btn_edit_modal('admin/payroll/new_tax/' . $tax->user_id.'/'.$tax->tax_id) ?>
													<?= btn_delete('admin/payroll/delete_employee_tax/' . $tax->user_id . '/' . $tax->tax_id) ?>
												</td>
											</tr>
										   <?php 
										   }elseif($taxCalc =='other'){
											//echo"asdasd"; die;
											$percentOth = $tax->taxOthAmount;
											?>
											<tr>										
												<td><?= $tax->tax_type ?></td>		
												<td><?= display_money($percentOth, $curency->symbol); ?></td>
												<td><?= $tax->taxWithholding."%"?></td>
												<td><?= $tax->tax_category_name ?></td>
												<td><?= $tax->effective_date ?></td>
												<td class="hidden-print">
													<?= btn_edit_modal('admin/payroll/new_tax/' . $tax->user_id.'/'.$tax->tax_id) ?>
													<?= btn_delete('admin/payroll/delete_employee_tax/' . $tax->user_id . '/' . $tax->tax_id) ?>
												</td>
											</tr>
											<?php 										
										}else{ 	
											
											?>										
												<tr>										
													<td><?= $tax->tax_type ?></td>
													<td><?= display_money($tax->taxRangeAmount, $curency->symbol); ?></td>
													<td><?=$tax->taxWithholding."%"?></td>
													<td><?= $tax->tax_category_name ?></td>
													<td><?= $tax->effective_date ?></td>
													<td class="hidden-print">
														<?= btn_edit_modal('admin/payroll/new_tax/' . $tax->user_id.'/'.$tax->tax_id) ?>
														<?= btn_delete('admin/payroll/delete_employee_tax/' . $tax->user_id . '/' . $tax->tax_id) ?>
													</td>
												</tr>
										<?php 
										$k++;
										}								
									}								
								}else{
									echo"<tr><td colspan='4'>No Result Found!</td></tr>";
								}
								?>
								</tbody>
							</table>
						</div>
						</div>
                        </div>
                    </div>
                </div>
				 <div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="deductions" style="position: relative;">
                    <div class="panel panel-custom">
                        <div class="panel panel-custom">
							<div class="panel-body">
							  <div class="earning-history">
							  <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/payroll/new_deduction/<?php echo $userId; ?>" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
							<?php
							$deduction_info = $this->db->where('user_id', $userId)->get('tbl_employee_salary_deduction')->result();
							 //echo"<pre>"; print_r($deduction_info);
							?>
							<div class="panel-body form-horizontal">
								<table class="table table-striped " cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= lang('Deduction type') ?></th>
										<th><?= lang('Deduction amount') ?></th>
										<th><?= lang('Exclude from tax') ?></th>
										<th><?= lang('Effective date') ?></th>
										<th class="hidden-print"><?= lang('action') ?></th>
									</tr>
									</thead>
									<tbody>
									<?php if (!empty($deduction_info)) {
										foreach ($deduction_info as $deduction) { ?>
											<tr>
												<td><?= $deduction->deduction_type ?></td>
												<?php if($deduction->deductionBasedOn=='percentAmt'){ ?>
												<td><?= $deduction->deductionPercent."%" ?></td>
												<?php }else{ ?>
												 <td><?= "$".$deduction->deduction_amount ?></td>	
												<?php } ?>
												<td><?= $deduction->excludeFrTx ?></td>
												<td><?= $deduction->effective_date ?></td>
												<td class="hidden-print">
													<?= btn_edit_modal('admin/payroll/new_deduction/' . $deduction->user_id.'/'.$deduction->deduction_id) ?>
													<?= btn_delete('admin/payroll/delete_employee_deduction/' . $deduction->user_id . '/' . $deduction->deduction_id) ?>
												</td>
											</tr>
										<?php }
									}
									else{
									echo"<tr><td colspan='4'>No Result Found!</td></tr>";
								    } 
									?>
									</tbody>
								</table>
							</div>
							 </div>
							</div>
						</div>
                    </div>
                </div>
				
                <div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="companyContributions" style="position: relative;">
                    <div class="panel panel-custom">
                       <div class="panel-body">
						  <div class="earning-history">
						  <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/payroll/new_contribution/<?php echo $userId; ?>" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
						<?php
						$contribution_info = $this->db->where('user_id', $userId)->get('tbl_salary_company_contribution')->result();
						?>
						<div class="panel-body form-horizontal">
							<table class="table table-striped " cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= lang('Contribution type') ?></th>
									<th><?= lang('Contribution amount') ?></th>
									<th><?= lang('Effective date') ?></th>
									<th class="hidden-print"><?= lang('action') ?></th>
								</tr>
								</thead>
								<tbody>
								<?php if (!empty($contribution_info)) {
									foreach ($contribution_info as $contribution) { ?>
										<tr>
											<td><?= $contribution->contribution_type ?></td>
											<?php if($contribution->contributionBasedOn=='percentAmt'){ ?>
											<td><?= $contribution->contributionPercent."%" ?></td>
											<?php }else{?>
											<td><?= "$".$contribution->contribution_amount ?></td>
											<?php } ?>
											<td><?= $contribution->contribution_date ?></td>
											<td class="hidden-print">
												<?= btn_edit_modal('admin/payroll/new_contribution/' . $contribution->user_id.'/'.$contribution->id) ?>
												<?= btn_delete('admin/payroll/delete_employee_contribution/' . $contribution->user_id . '/' . $contribution->id) ?>
											</td>
										</tr>
									<?php }
								} 
								else{
									echo"<tr><td colspan='4'>No Result Found!</td></tr>";
								}
								?>
								</tbody>
							</table>
						</div>
						 </div>
						</div>
                    </div>
                </div>
				
				<div class="tab-pane <?= $active == 4 ? 'active' : '' ?>" id="other" style="position: relative;">
                    <div class="panel panel-custom">
                       <div class="panel-body">
						  <div class="earning-history">
						  <a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/payroll/new_other/<?php echo $userId; ?>" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
						<?php
						$benifits_info = $this->db->where('user_id', $userId)->get('tbl_salary_other_benifits')->result();
						?>
						<div class="panel-body form-horizontal">
							<table class="table table-striped " cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= lang('Benefits type') ?></th>
									<th><?= lang('Benefits amount') ?></th>
									<th><?= lang('Effective date') ?></th>
									<th class="hidden-print"><?= lang('action') ?></th>
								</tr>
								</thead>
								<tbody>
								<?php if (!empty($benifits_info)) {
									foreach ($benifits_info as $benifit_info) { ?>
										<tr>
											<td><?= $benifit_info->benifit_type ?></td>
											<?php if($benifit_info->benifitBasedOn=='percentAmt'){ ?>
											<td><?= $benifit_info->	benifitPercent."%" ?></td>
											<?php }else{?>
											<td><?= "$".$benifit_info->benifit_amount ?></td>
											<?php } ?>
											<td><?= $benifit_info->benifit_date ?></td>
											<td class="hidden-print">
												<?= btn_edit_modal('admin/payroll/new_other/' . $benifit_info->user_id.'/'.$benifit_info->id) ?>
												<?= btn_delete('admin/payroll/delete_employee_benifit/' . $benifit_info->user_id . '/' . $benifit_info->id) ?>
											</td>
										</tr>
									<?php }
								} 
								else{
									echo"<tr><td colspan='4'>No Result Found!</td></tr>";
								}
								?>
								</tbody>
							</table>
						   </div>
						 </div>
						</div>						
                    </div>
                </div>
				
				
            </div>
        </div>
		
		<?php } ?>
    
	</div>
    </div>
</div>
    
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script>
  $('#myform').validate({ // initialize the plugin
            rules: {
          user_payType:{ required: true},
          payfrequency:{ required: true},
          hourly_rate_id:{ required: true},
          monthly_template_id:{ required: true},
         }
      });
</script>
<script>


$(document).ready(function(){
  $("#taxNew").click(function(){
    $("#alert").fadeIn(3000);	
	$("#alert").fadeOut(8000);
  });
});
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(':checkbox').on('change', function () {
            var th = $(this), id = th.prop('id');
            if (th.is(':checked')) {
                $(':checkbox[id="' + id + '"]').not($(this)).prop('checked', false);
            }
        });
    });
	
	$(function() {
    $('#userPayType').change(function(){
		 if($('#userPayType').val() == 'hourly') {
			  
				$('#hourly').show(); 
				$('#monthly').hide();
			} else if($('#userPayType').val() == 'salary'){
				$('#hourly').hide();
				$('#monthly').show();
               				
			} 
		});
	});
	
	
	$(function(){
    $(document).on("cut copy paste","#earningAmount",function(e) {
        e.preventDefault();
    });
	$(document).on("cut copy paste","#taxable_gross_salary",function(e) {
        e.preventDefault();
    });
    });
	
	$(document).ready(function() {
      $("#taxable_gross_salary").bind("keypress", function (e) {
          var keyCode = e.which ? e.which : e.keyCode
               
          if (!(keyCode >= 48 && keyCode <= 57)) {
            $(".error").css("display", "inline");
            return false;
          }else{
            $(".error").css("display", "none");
          }
      });
	  
	  });
	  
	  function goBack() {
  window.history.back();
}
</script>
