<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!--<div class="btn" style="margin-bottom:10px;">
		<input type="button" value="Go Back" onclick="history.back(-1)" />
	</div> -->
<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang('Employee salary list') ?></strong>
        </div>
    </div>
	

    <!-- Table -->
    <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th><?= lang('emp_id') ?></th>
            <th><?= lang('name') ?></th>
            <th><?= lang('Pay Type') ?></th>
        <?php /*?>    <th><?= lang('basic_salary') ?></th>
            <th><?= lang('overtime') ?>
                <small>(<?= lang('per_hour') ?>)</small>
            </th><?php */?>
            <th><?= lang('action') ?></th>

        </tr>
        </thead>
        <tbody>

        <script type="text/javascript">
            $(document).ready(function () {
                list = base_url + "admin/payroll/employee_salaryList";
            });
        </script>
        </tbody>
    </table>
</div>
