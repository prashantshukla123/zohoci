<div id="payment_receipt" style="background-color:#fff;">

    <style type="text/css">
    .earning-statement	{max-width: 1140px;  margin: 0 auto;}
	.earning-statement h2{padding: 0; margin: 0;}
	.earning-statement .header{background-color:#f1f1f1;}	
	.earning-statement .highlight{background-color: #f5f5f5;border-top: solid 2px #9c9c9c; border-bottom: solid 2px #9c9c9c;}
	.earning-statement th,td{padding: 3px;}
	.earning-statement th{background-color: #f5f5f5;}
	.earning-statement .p-0{padding: 0;}
	@media(max-width:767px){
		.earning-statement .mobile-width{width: 100%; float:left;}
		.earning-statement .mobile-height{height:10px;}
		
	}
    </style>
<?php $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();  ?>

    <div class="bd">
         <div style="text-align: right;margin:2px;" class="hidden-print">

          <a href="<?= base_url() ?>admin/payroll/send_payslip_toemail/<?= $employee_salary_info->salary_payment_id ?>/<?= $other_payment_details['pay_frequency'] ?>/<?= $other_payment_details['payment_start_date'] ?>/<?= $other_payment_details['payment_end_date'] ?>"
               class="btn btn-danger btn-xs" data-toggle="tooltip"
               data-placement="top" title="" data-original-title="<?= lang('send_email') ?>"><span <i
                    class="fa fa-envelope-o"></i></span></a> 
            <?php echo btn_pdf('admin/payroll/pay_slip_pdf/' . $employee_salary_info->salary_payment_id.'/'.$other_payment_details['pay_frequency'].'/'.$other_payment_details['payment_start_date'].'/'.$other_payment_details['payment_end_date']); ?>

        </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;" class="earning-statement">
  <tbody>
    <tr>
      <td align="left" valign="top" class="header">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
		  <tbody>
			<tr>
			  <td width="47%" align="left" valign="top" class="mobile-width">
				<table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-collapse: separate;">
					  <tbody>
						<tr>
						  <td align="left" valign="top">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
								  <tbody>
									<tr>
									  <td align="left" width="40%"><strong>Company</strong></td>
									  <td align="left" width="60%"><strong>Department</strong></td>
									</tr>
									<tr>
									  <td><?php echo $company_name; ?></td>
									  <td><?php echo $employee_salary_info->deptname; ?></td>
									</tr>
								  </tbody>
								</table>
						  </td>
						</tr>
						<tr>
						  <td align="left" valign="top">
								<?php echo $company_address; ?>
							</td>
						</tr>
						<tr>
						  <td align="left" valign="top" class="p-0">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
							  <tbody>
							  <tr>
									  <td valign="top" height="25" align="left"></td>
									 </tr>
								<tr>
								  <td align="left" width="30%"><strong>Social Security Number</strong></td>
								   <td align="center" width="5%">:</td>
								  <td align="left" width="50%"><?php echo $employee_salary_info->ss_number; ?></td>
								</tr>
								<tr>
								  <td align="left"><strong>Taxable Marital Status</strong></td>
								   <td align="center">:</td>
								  <td align="left"><?php echo ucfirst($employee_salary_info->maratial_status); ?></td>
								</tr>
								
							  </tbody>
							</table>

							</td>
						</tr>
					  </tbody>
					</table>

				</td>
			  <td width="6%" class="mobile-width"></td>
			  <td width="47%" align="left" valign="top" class="mobile-width">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;">
					  <tbody>
						<tr>
						  <td width="70%" align="left" valign="top">
							  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
								  <tbody>
									<tr>
									  <td valign="top" align="left"><h2>Earning Statement</h2></td>
									</tr>
									<tr>
									  <td valign="top" align="left">
											<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
										  <tbody>
											<tr>
											  <td align="left" width="45%"><strong>Period Start:</strong></td>
											  <td width="10%">:</td>
											  <td align="left" width="45%"><?php echo date('Y-m-d',$other_payment_details['payment_start_date']); ?></td>
											</tr>
											<tr>
											  <td align="left"><strong>Period End:</strong></td>
											  <td>:</td>
											  <td align="left"><?php echo date('Y-m-d',$other_payment_details['payment_end_date']); ?></td>
											</tr>
										  </tbody>
										</table>	
									 </td>
									</tr>
									   <tr>
									  <td valign="top" height="15" align="left"></td>
									 </tr>
									<tr>
									  <td valign="top" align="left">
										  <strong><?php echo $employee_salary_info->fullname; ?><br>
										  <?php echo $employee_salary_info->present_address; ?></strong>
										</td>
									</tr>
								  </tbody>
								</table>

						

							</td>
						  <td width="30%" align="left" valign="top">
						  <img src="<?php echo base_url() ?>uploads/logo-ozone1.png" alt=""></td>
						</tr>
					  </tbody>
					</table>

				</td>
			</tr>
		  </tbody>
		</table>	
	 </td>
    </tr>
	  <tr>
	  <td align="left" valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="10" style="border-collapse: collapse;">
		  <tbody>
			<tr>
			  <td align="left" valign="top" width="47%" class="mobile-width">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
				  
				  <!-- earning information -->
				<?php if (!empty($allowance_info)) { ?>
				  <thead>
					<tr>
					  <th align="left">Earnings</th>
					  <th align="left">Amount(<?php echo $curency->symbol; ?>)</th>
					  <th align="right">This Period(<?php echo $curency->symbol; ?>)</th>
					 <!-- <th align="right">Year to Date</th> -->
					</tr>
				 </thead>
				<?php } ?>
				 
				<tbody>		
					
					<?php $total_earning = 0;
						  $exclude_from_taxes = 0;
						if (!empty($allowance_info)) {
							foreach($allowance_info as $allowance) { $total_earning += $allowance->salary_payment_allowance_value; ?>
							<tr>
							  <td><em><strong><?php echo $allowance->salary_payment_allowance_label; ?></strong></em></td>
							  <td><?php echo $allowance->salary_payment_allowance_value; ?></td>
							  <td align=""><?php echo $allowance->salary_payment_allowance_value; if($allowance->exclude_from_taxes == 'yes') { echo "*"; $exclude_from_taxes += $allowance->salary_payment_allowance_value; } ?></td>
							 <!-- <td align="right">16,640.00</td> -->
							</tr>
					<?php } 
						}
						
						if(isset($basic_payment_info[0]->grossPay) && !empty($basic_payment_info[0]->grossPay)) {
							$gross_pay = $basic_payment_info[0]->grossPay;
						} else {
							$gross_pay = $total_earning;
						}
					?>
					
					<tr>
					  <td>&nbsp;</td>					  
					  <td  class="highlight"><strong>Gross Pay</strong></td>
					  <td align="" class="highlight" ><strong><?php echo display_money($gross_pay,$curency->symbol); ?></strong></td>
					</tr>
					
					
					<?php if (!empty($deduction_info)) { ?>
						<tr><td colspan="3" height="20"></td></tr>
						<tr>
						  <th align="left" colspan="2"><strong>Deductions</strong></th>
						  <th>&nbsp;</th>
						<!--  <th>&nbsp;</th> -->
						</tr>
					<?php } ?>
					
					
					
					<?php $total_deductions = 0;
					if (!empty($deduction_info)) {
						foreach($deduction_info as $deduction) { 
							if(isset($deduction->salary_payment_deduction_value) && !empty($deduction->salary_payment_deduction_value)) {
									$total_deductions += $deduction->salary_payment_deduction_value;
								}
						?>
							<tr>
							  <td colspan="2"><em><?php echo $deduction->salary_payment_deduction_label; ?></em></td>
							  <td align="">- <?php echo display_money($deduction->salary_payment_deduction_value, $curency->symbol); if($deduction->exclude_from_taxes == 'yes') { echo "*"; $exclude_from_taxes += $deduction->salary_payment_deduction_value; } ?></td>
							  <!-- <td align="right">2,111.20</td> -->
							</tr>
						<?php } 
					}
					?>
					
					<?php if (!empty($taxes_info)) { ?>
						<tr><td colspan="3" height="20"></td></tr>
						<tr>
						  <th align="left" colspan="2"><strong>Taxes</strong></th>
						  <th>&nbsp;</th>
						<!--  <th>&nbsp;</th> -->
						</tr>
					<?php } ?>
					
					<?php 
						$total_taxes = 0;
						if (!empty($taxes_info)) {
							foreach($taxes_info as $taxes) { 
								if(isset($taxes->salary_payment_tax_value) && !empty($taxes->salary_payment_tax_value)) {
									$total_taxes += $taxes->salary_payment_tax_value;
								} 
							?>
								<tr>
								  <td colspan="2"><em><?php echo $taxes->salary_payment_tax_label; ?></em></td>
								  <td align="">- <?php echo display_money($taxes->salary_payment_tax_value, $curency->symbol); ?></td>
								  <!-- <td align="right">2,111.20</td> -->
								</tr>
							<?php } 
						}
					?>
					
					<?php if (!empty($employee_salary_info->fine_deduction) || $employee_salary_info->fine_deduction != 0) { ?>
						<tr><td colspan="3" height="20"></td></tr>
						<tr>
						  <th align="left" colspan="2"><strong>Fine Deduction</strong></th>
						  <th>&nbsp;</th>
						<!--  <th>&nbsp;</th> -->
						</tr>
					<?php } 
						
						if (!empty($employee_salary_info->fine_deduction) || $employee_salary_info->fine_deduction != 0)  {
							
							?>
								<tr>
								  <td colspan="2"><em>Fine Deduction</em></td>
								  <td align="">- <?php echo display_money($employee_salary_info->fine_deduction, $curency->symbol); ?></td>
								  <!-- <td align="right">2,111.20</td> -->
								</tr>
							<?php 
						}
					?>
					
					
					<!-- <tr><td colspan="5" height="20"></td></tr>
					<tr>
					  <th>&nbsp;</th>
					  <th colspan="2" align="left">Other</th>					  
					  <th>&nbsp;</th>
					  <th>&nbsp;</th>
					</tr>
					
					<tr>
					  <td><em><strong>Investment 1</strong></em></td>
					  <td colspan="2">Bond</td>					  
					  <td align="right">-5.00</td>
					  <td align="right">100.00</td>
					</tr>
					
					<tr>
					  <td>&nbsp;</td>					  
					  <td colspan="2" class="highlight"><strong>Net Pay</strong></td>
					  <td align="right" class="highlight"><strong>$291.90</strong></td>
					  <td align="right"></td>
					</tr>
					-->
					
					<tr>
					  <td></td>					  
					  <td class="highlight"> <strong>Net Pay</strong></td>
					  <td align="" class="highlight"> <strong><?php echo display_money(($gross_pay-($total_deductions+$total_taxes+$employee_salary_info->fine_deduction)), $curency->symbol); ?></strong></td>
					</tr>
					
				<tr><td colspan="4" height="10"></td></tr>
					<tr>
					  <td>&nbsp;</td>					  
					  <td colspan="3"><strong>*Excluded from federal taxable wages</strong></td>					 
					</tr>
					<tr><td colspan="4" height="10"></td></tr>
					<tr>
					  <td>&nbsp;</td>					  
					  <td colspan="3">Your federal wages this period are <?php echo display_money(($gross_pay-$exclude_from_taxes),$curency->symbol); ?></td>					 
					</tr> 
				  </tbody>
				</table>
				</td>
			  <td align="left" valign="top" width="6%" class="mobile-width"></td>
			  <td align="left" valign="top" width="47%" class="mobile-width">
				<?php /* ?>	
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
					 <?php if (!empty($contribution_info)) { ?>
						<thead>
						<tr>
						  <th align="left" width="250">Company Contribution</th>
						  <th align="right">This Period</th>
						 <!-- <th align="right">Total to date</th>  -->      
						</tr>
						</thead>
					 <?php } ?>
						<tbody>
						<?php if (!empty($contribution_info)) {
								foreach($contribution_info as $contribution) { ?>
							<tr>
							  <td><?php echo $contribution->salary_payment_contribution_label; ?></td>
							  <td align="left">$<?php echo number_format($contribution->salary_payment_contribution_value,2); ?></td>
							  <!-- <td align="right">27.00</td> -->
							</tr>
						<?php } 
							}
						?>
						
					</tbody>
				</table>
				<?php */ ?>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-collapse: separate;">
					 <?php if (!empty($benefits_info)) { ?>
						<thead>
						<tr>
						  <th align="left">Other Benefits and Information</th>
						  <th align="right">This Period</th>
						 <!-- <th align="right">Total to date</th>  -->      
						</tr>
						</thead>
					 <?php } ?>
						<tbody>
						<?php if (!empty($benefits_info)) {
								foreach($benefits_info as $benefits) { ?>
							<tr>
							  <td><?php echo $benefits->salary_payment_benifit_label; ?></td>
							  <td align="left"><?php echo display_money($benefits->salary_payment_benifit_value,$curency->symbol); ?></td>
							  <!-- <td align="right">27.00</td> -->
							</tr>
						<?php } 
							}
						?>
						
							
							<tr>
								<th colspan="3" align="left">Important Notes</th>
							</tr>
							<tr>
								<td align="left" valign="top" colspan="3">
								<?php echo $employee_salary_info->comments; ?>
								</td>
							</tr>
					</tbody>
				</table>
			</td>
			</tr>
		  </tbody>
		</table>
  
		</td>
	  </tr>
 
  </tbody>
</table>
		
    </div>
</div>
<script type="text/javascript">
    function payment_receipt(payment_receipt) {
        var printContents = document.getElementById(payment_receipt).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>