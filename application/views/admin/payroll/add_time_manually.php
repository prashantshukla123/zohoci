<div class="panel panel-custom" data-collapsed="0">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang('add_time_manually') ?></strong>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                    class="sr-only">Close</span></button>
        </div>
    </div>
    <div class="panel-body">
	<?php if(isset($employee_id) && !empty($employee_id)) { $empid = $employee_id; ?>
	<?php } else { $empid = ''; } ?>
        <form id="time_validation" data-parsley-validate="" novalidate=""
              action="<?php echo base_url() ?>admin/payroll/saved_manual_time/<?php echo $empid; ?>"
              method="post" class="form-horizontal form-groups-bordered">
            <?php
            $check_head = $this->db->where('department_head_id', $this->session->userdata('user_id'))->get('tbl_departments')->row();
            $role = $this->session->userdata('user_type');
         
                ?>
				<input type="hidden" name="payment_number" value="<?php echo $payment_number; ?>">
				<input type="hidden" name="department_id" value="<?php echo $department_id; ?>">
				<input type="hidden" name="payment_month" value="<?php echo $payment_month; ?>">
                <div class="form-group" id="border-none">
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('employee') ?> <span
                            class="required">*</span></label>
                    <div class="col-sm-8">
                        <select name="user_id" style="width: 100%" id="employee"
                                class="form-control select_box">
                            <?php if (!empty($all_employee)): ?>
                                <option value="<?php echo $all_employee->user_id; ?>"><?php echo $all_employee->fullname; ?></option>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            
            <div class="form-group" id="border-none">
                <div class="col-sm-6">
                    <label class="control-label col-sm-6"><?= lang('Start Date') ?> </label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            <input type="text" name="start_date" class="form-control"
                                   value="<?= date('Y-m-d', $start_date) ?>" readonly>
                        </div>
                    </div>
                </div>
				<div class="col-sm-5">
                    <label class="control-label col-sm-5"><?= lang('End Date') ?> </label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <input type="text" name="end_date" class="form-control"
                                   value="<?= date('Y-m-d', $end_date) ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
			
			<div class="form-group" id="border-none">
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('Total Hours') ?> <span
                            class="required">*</span></label>
                    <div class="col-sm-8">

					<?php 
						$total_regular_hours = '';
						if(isset($added_hours->regular_hours) && !empty($added_hours->regular_hours)) {
							$total_regular_hours = $added_hours->regular_hours+$added_hours->pto_hours+$added_hours->overtime_hours;
						}
					?>
                        <input type="text" name="total_regular_hours" min="0" data-parsley-type="number" id="total_regular_hours" class="form-control" value="<?php echo $total_regular_hours; ?>" required>

                        
                    </div>
            </div>
			
			<div class="form-group" id="border-none">
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('Regular Hours') ?> </label>
                    <div class="col-sm-8">

                        <input type="text" name="regular_hours" data-parsley-type="number" id="regular_hours" min="0" class="form-control" value="<?php echo (isset($added_hours->regular_hours) && !empty($added_hours->regular_hours))?$added_hours->regular_hours:''; ?>" readonly>

                      
                    </div>
            </div>
			
			<div class="form-group" id="border-none">
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('PTO Hours') ?></label>
                    <div class="col-sm-8">

                        <input type="text" name="pto_hours" data-parsley-type="number" min="0" id="pto_hours" class="form-control" value="<?php echo (isset($added_hours->pto_hours) && !empty($added_hours->pto_hours))?$added_hours->pto_hours:''; ?>">

                    </div>
            </div>
			
			<div class="form-group" id="border-none">
                    <label for="field-1" class="col-sm-3 control-label"><?= lang('Overtime Hours') ?></label>
                    <div class="col-sm-8">

                        <input type="text" name="overtime_hours" data-parsley-type="number" min="0" id="overtime_hours" class="form-control" value="<?php echo (isset($added_hours->overtime_hours) && !empty($added_hours->overtime_hours))?$added_hours->overtime_hours:''; ?>">

                    </div>
            </div>
            
            <div class="form-group" id="border-none">
                <div class="col-sm-6">
                    <label class="control-label col-sm-4"></label>
                    <div class="col-sm-4 ">
                        <button type="submit"
                                class="btn btn-block btn-primary"><?= lang('update') ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
	//js for add regular , pto and overtime hours for hourly employee in make payment
$(document).ready(function () {
	$("input#total_regular_hours").blur(function(){ 
		var total_regular_hours = $(this).val();
		var pto_hours = $("#pto_hours").val();
		var overtime_hours = $("#overtime_hours").val();
		
		if(pto_hours) {
			total_regular_hours = total_regular_hours - pto_hours;
		}
		
		if(overtime_hours) {
			total_regular_hours = total_regular_hours - overtime_hours;
		}
		
		$('#regular_hours').val(total_regular_hours);
		
		if(total_regular_hours < 0) {
			alert("Something wrong, please check and try again"); 
			return false;
		}
		
	});
	
	$("input#pto_hours").blur(function(){
	
		var pto_hours = $(this).val();
		var total_regular_hours = $("#total_regular_hours").val();
		var overtime_hours = $("#overtime_hours").val();
		
		if(pto_hours) {
			total_regular_hours = total_regular_hours - pto_hours;
		}
		
		if(overtime_hours) {
			total_regular_hours = total_regular_hours - overtime_hours;
		}
		
		$('#regular_hours').val(total_regular_hours);
		
		if(total_regular_hours < 0) {
			alert("Something wrong, please check and try again"); return false;
		}
		
	});
	
	$("input#overtime_hours").blur(function(){
		
		var overtime_hours = $(this).val();
		var pto_hours = $("#pto_hours").val();
		var total_regular_hours = $("#total_regular_hours").val();
		
		if(pto_hours) {
			total_regular_hours = total_regular_hours - pto_hours;
		}
		
		if(overtime_hours) {
			total_regular_hours = total_regular_hours - overtime_hours;
		}
		
		$('#regular_hours').val(total_regular_hours);
		
		if(total_regular_hours < 0) {
			alert("Something wrong, please check and try again"); return false;
		}
		
	});
});
</script>