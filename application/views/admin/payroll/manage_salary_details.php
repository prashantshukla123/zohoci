<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<!--<div class="btn" style="margin-bottom:10px;">
		<input type="button" value="Go Back" onclick="history.back(-1)" />
	</div>
	-->
<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang('manage_salary_details') ?></strong>
        </div>
    </div>
    <form id="form" role="form" enctype="multipart/form-data"
          action="<?php echo base_url() ?>admin/payroll/manage_salary_details" method="post"
          class="form-horizontal form-groups-bordered">
        <div class="panel-body">
            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label"><?= lang('select_department') ?> <span
                        class="required"> *</span></label>

                <div class="col-sm-5">
                    <select name="departments_id" class="form-control select_box" required>
                        <option value="all"><?= lang('All Department') ?></option>
                        <?php if (!empty($all_department_info)): 
						foreach ($all_department_info as $v_department_info) :
                            if (!empty($v_department_info->deptname)) {
                                $deptname = $v_department_info->deptname;
                            } else {
                                $deptname = lang('undefined_department');
                            }
                            ?>
                            <option value="<?php echo $v_department_info->departments_id; ?>"
                                <?php
                                if (!empty($departments_id)) {
                                    echo $v_department_info->departments_id == $departments_id ? 'selected' : '';
                                }
                                ?>><?php echo $deptname ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="col-sm-2">
                    <button type="submit" id="sbtn" value="1" name="flag" class="btn btn-primary">Go
                    </button>
                </div>
            </div>
        </div>
    </form>
        <?php if (!empty($flag)): ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= lang('employee') . ' ' . lang('name') ?></th>
                    <th><?= lang('Action') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($employee_info)):foreach ($employee_info as $key => $v_emp_info): ?>
                    <?php if (!empty($v_emp_info)):foreach ($v_emp_info as $v_employee): ?>
                        <tr>
                            <td><input type="hidden" name="user_id[]"
                                       value="<?php echo $v_employee->user_id ?>"> <?php echo $v_employee->fullname; ?>
                            </td>
                           
                            <td style="width: 25%">
                                <div class="pull-left">					
								<a href="<?=base_url()."admin/payroll/manage_salary_info/".$v_employee->user_id ?>">
								  <button type="submit" id="sbtn" value="1" name="flag" class="btn btn-primary">Mange salary</button>
								</a>
                                </div>
                            </td>                            
                        </tr>
                    <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>

                <?php endif; ?>
                <?php if (empty($employee_info[0])) { ?>
                    <tr>
                        <td>
                            <?= lang('nothing_to_display') ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
           
            <!-- Hidden value when update  Start-->
            <input type="hidden" name="departments_id" value="<?php echo $departments_id ?>"/>
            <?php
            if (!empty($salary_grade_info)) {
                foreach ($salary_grade_info as $v_grade_salary_info) {
                    foreach ($v_grade_salary_info as $v_gsalary_info) {

                        if (!empty($v_gsalary_info)) {
                            ?>
                            <input type="hidden" name="payroll_id[]" value="<?php echo $v_gsalary_info->payroll_id ?>"/>
                            <?php
                        }
                    }
                }
            }
            ?>
        <?php endif; ?>
    
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(':checkbox').on('change', function () {
            var th = $(this), id = th.prop('id');
            if (th.is(':checked')) {
                $(':checkbox[id="' + id + '"]').not($(this)).prop('checked', false);
            }
        });
    });
</script>
