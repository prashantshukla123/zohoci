<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('New Tax') ?></h4>
    </div>
	<?php //echo "<pre>"; print_r($profile_info); exit; ?>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/payroll/save_tax_info/<?php echo $profile_info->user_id; ?>/<?php
              if (!empty($tax_info->tax_id)) {
                  echo $tax_info->tax_id;
              }
              ?>" method="post" id="taxForm" class="form-horizontal form-groups-bordered">

            <!-- CV Upload -->
			<?php 
			
			if($salary_info->user_payType =='salary'){ ?>
			<input type="hidden" name="salary_template_id" value="<?=$salary_info->monthly_template_id ?>"/>			
			<?php }else{ ?>
			<input type="hidden" name="hourly_template_id" value="<?=$salary_info->hourly_rate_id ?>"/>
			<?php } ?>			
			<input type="hidden" name="salary_type" value="<?=$salary_info->user_payType ?>"/>
			
            <div class="">
                <label class="control-label"><?= lang('Tax type ') ?> <span class="text-danger">*</span></label>
				<input type="text" name="tax_type" placeholder="" class="form-control" required="" value="<?php if (!empty($tax_info->tax_type)) {echo $tax_info->tax_type;	} ?>">                
                <input type="hidden" required name="tax_id" value="<?php
                if (!empty($tax_info->tax_id)) {
                    echo $tax_info->tax_id;
                }
                ?>" class="form-control">
            </div>	
			<div class="">
                <label class="control-label"><?= lang('Tax type category ') ?> <span class="text-danger">*</span></label>

				
				<select name="taxCategory" class="form-control" required>
							<option value="">Select Category</option>
							<?php foreach($tax_categories as $tax_category) { ?>
							<option value="<?php echo $tax_category->id; ?>" <?php if($tax_info->taxCategory == $tax_category->id) { ?>selected<?php } ?>><?php echo $tax_category->tax_category_name; ?></option>
							<?php } ?>
						</select>
								               

                <input type="hidden" required name="tax_id" value="<?php
                if (!empty($tax_info->tax_id)) {
                    echo $tax_info->tax_id;
                }
                ?>" class="form-control">
            </div>
			
			
			<div class="form-group">
                    <div class="col-sm-6">
                     <label class="control-label"><?= lang('Tax Calculation based upon ') ?> </label>	
                    </div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input type="radio" name="taxCalc" value="max" id="taxCalcMax" <?php if($tax_info->taxCalc =='max'){ echo"checked";} ?>>
						 Max Tax Threshold
						</div>
					</div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input  type="radio" name="taxCalc" value="rng" id="taxCalcRng" <?php if($tax_info->taxCalc =='rng'){ echo"checked";} ?>>
						 Income tax range
						</div>
					</div>
					<div class="col-sm-12">
                        <div class="input-group1">
						 <input  type="radio" name="taxCalc" value="other" id="taxCalcOth" <?php if($tax_info->taxCalc =='other'){ echo"checked";} ?>>
						 Other
						</div>
					</div>
					                
            </div>
			<?php if(!empty($tax_info->taxCalc && $tax_info->taxCalc=='rng')){ ?>
			   <div class="form-group txAmt" style='display:none'>
			<?php }elseif(!empty($tax_info->taxCalc && $tax_info->taxCalc=='max')){ ?>
			   <div class="form-group txAmt" style='display:block'>
			<?php }elseif(!empty($tax_info->taxCalc &&  $tax_info->taxCalc=='other')){ ?>
			   <div class="form-group txAmt" style='display:none'>
			<?php }elseif(empty($tax_info->taxCalc)){ ?>
			   <div class="form-group txAmt" style='display:none'>
			<?php }?>
			    <div class="col-sm-12">
                  <label class="control-label"><?= lang('Max income threshold($)') ?></label>
				  <input type="text" name="maxincome_threshold" id="taxMaxIncome" placeholder="" class="form-control" value="<?php if($tax_info->maxincome_threshold){ echo $tax_info->maxincome_threshold; }?>" >
				  <span id="taxAlert"></span>
				</div>
               <div class="col-sm-12">
                  <label class="control-label"><?= lang('Tax Amount($)') ?></label>
				  <input type="text" name="tax_amount" maxlength="7"  id="taxAmount" class="form-control" value="<?php if($tax_info->tax_amount){ echo $tax_info->tax_amount; }?>">
				</div> 				
            </div>
			<?php if(!empty($tax_info->taxCalc && $tax_info->taxCalc=='max')){ ?>
			   <div class="form-group tax-range" style='display:none'>
			<?php }elseif(!empty($tax_info->taxCalc && $tax_info->taxCalc=='rng')){ ?>
			   <div class="form-group tax-range" style='display:block'>
			<?php }elseif(!empty($tax_info->taxCalc &&  $tax_info->taxCalc=='other')){ ?>
			   <div class="form-group tax-range" style='display:none'>		 
			<?php }elseif(empty($tax_info->taxCalc)){ ?>
			   <div class="form-group tax-range" style='display:none'>
			<?php } ?>			
				<div class="col-sm-4">
				<div class="form-group" id="border-none">
					<div class="col-sm-12">
					 <label class="control-label"><?= lang('Minimum Income($)') ?> </label>
					</div>
					<div class="col-sm-12">
						<div class="input-group1">
							<input type="text" name="minIncome" id="minIncome" class="form-control" maxlength="7" value="<?php if($tax_info->minIncome){ echo $tax_info->minIncome; }?>">
						</div>
					</div>                
				</div>
				</div>
				<div class="col-sm-4">
				<div class="form-group" id="border-none">
					<div class="col-sm-12">
					 <label class="control-label"><?= lang('Max income($)') ?> </label>
					</div>
					<div class="col-sm-12">
						<div class="input-group1">
							<input type="text" maxlength="7" name="maxIncome" id="maxIncome" class="form-control" value="<?php if($tax_info->maxIncome){ echo $tax_info->maxIncome; }?>">
						</div>
					</div>                
				</div>
				</div>
				<div class="col-sm-4">
				<div class="form-group" id="border-none">
					<div class="col-sm-12">
					 <label class="control-label"><?= lang('Tax amount($)') ?> </label>
					</div>
					<div class="col-sm-12">
						<div class="input-group1">
							<input type="text" maxlength="7" name="taxRangeAmount" id="taxPercent" class="form-control" value="<?php if($tax_info->taxRangeAmount){ echo $tax_info->taxRangeAmount; }?>">
						</div>
					</div>                
				</div>
				</div>						
			</div>
			<?php if(!empty($tax_info->taxCalc && $tax_info->taxCalc=='max')){ ?>
			  <div class="form-group other-tax" style='display:none'>
			<?php }elseif(!empty($tax_info->taxCalc && $tax_info->taxCalc=='rng')){ ?>
			   <div class="form-group other-tax" style='display:none'>
			<?php }elseif(!empty($tax_info->taxCalc &&  $tax_info->taxCalc=='other')){ ?>
			  <div class="form-group other-tax" style='display:block'>	 
			<?php }elseif(empty($tax_info->taxCalc)){ ?>
			  <div class="form-group other-tax" style='display:none'>
			<?php } ?>		    
			    <div class="col-sm-12">
				<div class="form-group" id="border-none">
					<div class="col-sm-12">
					 <label class="control-label"><?= lang('Tax amount($)') ?> </label>
					</div>
					<div class="col-sm-12">
						<div class="input-group1">
							<input type="text"  maxlength="7" name="taxOthAmount" id="taxOthPercent" class="form-control" value="<?php if($tax_info->taxOthAmount){ echo $tax_info->taxOthAmount; }?>">
						</div>
					</div>                
				</div>
				</div>
			 </div>
			
            <div class="">
                <label class="control-label"><?= lang('Effective date') ?><span class="text-danger">*</span></label>
				<div class="input-group">
                        <input type="text"  onkeydown="return false" name="effective_date" placeholder="" class="form-control effective_date" required="" value="<?php
						if (!empty($tax_info->effective_date)) {
							echo $tax_info->effective_date;
						}
						?>" data-parsley-id="8">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
               
            </div>          
			
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="button" id="taxSubmit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script>
$(document).ready(function(){
  $("#taxSubmit").click(function(){   
   /* var maxIncome  = $('#taxMaxIncome').val();
	  if(maxIncome == ''){
	    $('#taxAlert').text('Please fill value');
	  }else{
		 $( "#taxForm" ).submit();
	  } */
	  $( "#taxForm" ).submit();
	  
 });
});
</script>

<script>	
	$(document).ready(function(){
	$('body').on('click', '#taxCalcMax', function() {	  		  
		$(".tax-range").css("display","none");
		$(".other-tax").css("display","none");
		$(".txAmt").css("display","block");
	  });
	  $('body').on('click', '#taxCalcRng', function() {
		$(".txAmt").css("display","none");
		$(".other-tax").css("display","none");
		$(".tax-range").css("display","block");
	  });
	  $('body').on('click', '#taxCalcOth', function() {
		$(".txAmt").css("display","none");
		$(".tax-range").css("display","none");
		$(".other-tax").css("display","block");
	  });
	  
	  $('body').on('blur', '#taxOthPercent', function() {
		var taxval =  $(this).val();
		var taxSalary =  $('#taxable_gross_salary').val();
		/* alert(taxval);
		alert(taxSalary); */
		if(parseInt(taxSalary) < parseInt(taxval)){
		 alert('Tax amount should not be greater than calculated salary');
		}
	  });	  
	  
	  $('body').on('blur', '#taxPercent', function() {
		var taxval =  $(this).val();
		var taxSalary =  $('#taxable_gross_salary').val();
		if(parseInt(taxSalary) < parseInt(taxval)){
		 alert('Tax amount should not be greater than calculated salary');
		}
	  });
	  
	  $('body').on('blur', '#taxAmount', function() {
		var taxval =  $(this).val();
		var taxSalary =  $('#taxable_gross_salary').val();
		if(parseInt(taxSalary) < parseInt(taxval)){
		 alert('Tax amount should not be greater than calculated salary');
		}
	  });
	  
	});
    $(document).ready(function() {
        $('#minIncome').keypress(function (event) {
            return isNumber(event, this)
        });
		$('#maxIncome').keypress(function (event) {
            return isNumber(event, this)
        });
		$('#taxMaxIncome').keypress(function (event) {
            return isNumber(event, this)
        });
		$('#taxPercent').keypress(function (event) {
            return isNumber(event, this)
        });
		$('#taxAmount').keypress(function (event) {
            return isNumber(event, this)
        });
		$('#taxOthPercent').keypress(function (event) {
            return isNumber(event, this)
        });
		
	});
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {
          
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
$(function(){
    $(document).on("cut copy paste","#minIncome",function(e) {
        e.preventDefault();
    });
	$(document).on("cut copy paste","#maxIncome",function(e) {
        e.preventDefault();
    });
	$(document).on("cut copy paste","#taxPercent",function(e) {
        e.preventDefault();
    });
    });

$("input.effective_date").datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		startDate: new Date()
	});
</script>