<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="panel panel-custom">   
	 <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang($title); ?></strong>
        </div>
    </div>

    <!-- Table -->
	<form role="form" data-parsley-validate="" novalidate="" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/payroll/make_bulk_payment/" method="post">
    <table class="table table-striped DataTables" cellspacing="0" width="100%">
        <thead>
		   <tr>
				<th><?= lang('User name') ?></th>
				<th><?= lang('Payment mode') ?></th>
				<th><?= lang('Basic salary') ?></th>
				<th><?= lang('Net salary') ?></th>
				<th><?= lang('Payment start date') ?></th>
				<th><?= lang('End payment date') ?></th>
				<th><?= lang('Fine Deduction') ?></th>
				<th><?= lang('Payment Amount') ?></th>
			</tr>
        </thead>
        <tbody>
		 <?php 
		 if(!empty($allListUser)){
			 $count ='1';
			 foreach($allListUser as $paymentData){
				 //echo"<pre>"; print_r($paymentData); die;
				 if($paymentData['payment_mode']=='via_cheque'){
				  $payMode ="Pay via cheque"; 	 
				 }else{
				  $payMode ="Pay via ACH"; 
				 }
			 ?>
           <tr id="table_0" role="row">
			   <td tabindex="0">
			   <input type="hidden" name="payment_type_number[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['user_id'].$paymentData['payment_number']; ?>" />
			   <input type="hidden" name="total_deduction[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['total_deduction'] ?>" />
			   <input type="hidden" name="total_tax[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['total_tax'] ?>" />
			   <input type="hidden" name="taxable_income[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['taxable_income'] ?>" />
			   <input type="hidden" name="departments_id[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['departments_id'] ?>" />
			    <input type="hidden" name="grossPay[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['grossPay'] ?>" />
			   <input type="hidden" name="payment_number[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['payment_number'] ?>" />
			   <input type="hidden" name="user_id[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['user_id'] ?>" />
			   <?= $paymentData['user_name'] ?></td>
			   <input type="hidden" name="pay_frequency[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['pay_frequency'] ?>" />			  
			   <input type="hidden" name="payment_month[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['payment_month'] ?>" />
			   
			   <td tabindex="0">
			   <select name="payment_mode[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]">
			    <option <?php if($paymentData['payment_mode']=='via_cheque'){ echo"selected"; } ?>value="via_cheque">Pay via cheque</option>
			    <option <?php if($paymentData['payment_mode']=='via_ACH'){ echo"selected"; } ?> value="via_ACH">Pay via ACH</option>
			   </select></td>
			   <td tabindex="0">
			   <input type="hidden" name="basicSalary[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['basicSalary'] ?>" />
			   <?= $paymentData['basicSalary'] ?></td>
			   <td tabindex="0">
			   <input type="hidden" name="netSalary[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['netSalary'] ?>" />
			   <?= number_format($paymentData['netSalary'],2) ?></td>
			   <td tabindex="0">
			   <input type="hidden" name="payment_start_date[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= $paymentData['payment_start_date'] ?>" />
			   <?= $paymentData['payment_start_date'] ?></td>
			   <td tabindex="0">
			   <input type="hidden" name="end_payment_date[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= strtotime($paymentData['end_payment_date']) ?>" />
			   <?= $paymentData['end_payment_date'] ?></td>	
			   <td tabindex="0">
			   <?php $dd = $paymentData['netSalary']; ?>
			   <input type="text" id="<?php echo $count; ?>" data-id="<?php echo  ltrim(str_replace(',','',$paymentData['netSalary'])); ?>" name="fine_deduction[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" class="fine_deduction"  maxlength="<?= strlen($dd); ?>" />
			   </td>
			   <td tabindex="0">
			   <?php 
			    $nt = $paymentData['netSalary'];
				$ntA = explode("$", $nt);
			   ?>
			   <input class="net_updates" type="text" readonly="true" id="netSalary-<?php echo $count; ?>" readonly="readonly" name="payment_amount[<?php echo $paymentData['user_id'].$paymentData['payment_number']; ?>][]" value="<?= ltrim(str_replace(',','',number_format($paymentData['netSalary'],2)))?>" />
			   </td>
		   </tr>
		 <?php $count++; } 
		 }else{
			 echo"<tr><td>No data found!</td></tr>";
		 }
		 ?>		 
        </tbody>
          		
    </table>
	<?php if(!empty($allListUser)){ ?>
	<div class="extra-field">
	
		  <?php /*?><div class="form-group">
		  <label>Select Account Type</label>
		  <?php 
		  $this->db->select('*');

          $query = $this->db->where('user_id', $paymentData['user_id'])->get('tbl_employee_bank');

		  $results = $query->result(); 
		  ?>
		    <select name="account" class="form-control" tabindex="-1">
			<option>Select Account Type</option>
			<?php foreach($results as $sData){ ?>
			  <option value="<?= $sData->employee_bank_id ?>"><?= $sData->bank_name ?></option>
			<?php } ?>
			</select>
		 </div><?php */?>
		 <div class="form-group">
		     <label>Important notes</label>
		    <textarea class="form-control" tabindex="-1" name="comment" rows="4"></textarea>
		 </div>
		 <div class="form-group" style="text-align:right">
		 <button id="submit" type="submit" name="flag" value="1" class="btn btn-primary pay-btn">Make Payment</button>
		 </div>
     </div>
	<?php } ?>
					
    </form>
	
</div>
<script>

$(document).ready(function(){

$('.fine_deduction').keypress(function (event) {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
        event.preventDefault();
    }
});
	
	
  $(".fine_deduction").blur(function(){
   var kval = $(this).val();
   var nsval = $(this).attr('data-id');
   var nsId = $(this).attr('id');
  // var st = nsval.split("$");
   //var price = st[1];
   var cur_net_sal = $("#netSalary-"+nsId).val();
   var total = parseFloat(cur_net_sal) - parseFloat(kval);
   //console.log(total);
    $("#netSalary-"+nsId).val(total);
  });
});


</script>