<?php $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row(); ?>
<div id="printableArea">
    <div class="modal-header ">
        <h4 class="modal-title" id="myModalLabel"><?= lang('employee_salary_details') ?>
            <div class="pull-right ">
            <?php /* ?>    <span><?php echo btn_pdf('admin/payroll/make_pdf/' . $emp_salary_info->user_id); ?></span> <?php */ ?>
                <button class="btn btn-xs btn-danger" type="button" data-toggle="tooltip" title="Print"
                        onclick="printDiv('printableArea')"><i class="fa fa-print"></i></button>
            </div>
        </h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <div class="show_print" style="width: 100%; border-bottom: 2px solid black;margin-bottom: 30px">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <td style="width: 50px; border: 0px;">
                        <img style="width: 50px;height: 50px;margin-bottom: 5px;"
                             src="<?= base_url() . config_item('company_logo') ?>" alt="" class="img-circle"/>
                    </td>

                    <td style="border: 0px;">
                        <p style="margin-left: 10px; font: 14px lighter;"><?= config_item('company_name') ?></p>
                    </td>
                </tr>
            </table>
        </div><!-- show when print start-->
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-2 col-sm-2">
                    <div class="fileinput-new thumbnail"
                         style="width: 144px; height: 158px; margin-top: 14px; margin-left: 16px; background-color: #EBEBEB;">
                        <?php if ($emp_salary_info->avatar): ?>
                            <img src="<?php echo base_url() . $emp_salary_info->avatar; ?>"
                                 style="width: 142px; height: 148px; border-radius: 3px;">
                        <?php else: ?>
                            <img src="<?php echo base_url() ?>/img/user.png" alt="Employee_Image">
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-1 col-sm-1">
                    &nbsp;
                </div>
                <div class="col-lg-8 col-sm-8 ">
                    <div>
                        <div style="margin-left: 20px;">
                            <h3><?php echo $emp_salary_info->fullname; ?></h3>
                            <hr class="mt0"/>
                            <table class="table-hover">
                                <tr>
                                    <td><strong><?= lang('emp_id') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?php echo "$emp_salary_info->employment_id"; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= lang('departments') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?php echo "$emp_salary_info->deptname"; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= lang('designation') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?php echo "$emp_salary_info->designations"; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= lang('joining_date') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?= strftime(config_item('date_format'), strtotime($emp_salary_info->joining_date)) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <!-- ********************************* Salary Details Panel ***********************-->
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?= lang('salary_details') ?></strong>
                    </div>
                </div>
                <div class="panel-body1">
				<div class="">
                        <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('Pay Frequency') ?>:</strong></label>
                        <p class="form-control-static"><?php echo ucfirst($emp_salary_info->payfrequency); ?></p>
                    </div>
                    <div class="">
                        <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('salary_grade') ?>:</strong></label>
                        <p class="form-control-static"><?php 
						 if(!empty($emp_salary_info->salary_grade)){
					    	echo $emp_salary_info->salary_grade;
						 }else{
							echo $emp_salary_info->hourly_grade;
						 }
						?></p>
                    </div>
					<div class="">
                        <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('Pay Type') ?>:</strong></label>
                        <p class="form-control-static"><?php 
						 if(!empty($emp_salary_info->salary_grade)){
					    	echo 'Salary';
						 }else{
							echo 'Hourly';
						 }
						?></p>
                    </div>
                     <?php /*?> <div class="">
                        <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('basic_salary') ?>
                                :</strong>
                        </label>
                        <p class="form-control-static"><?php
                            $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
                            echo display_money($emp_salary_info->basic_salary, $curency->symbol);
                            ?></p>
                    </div>
					<?php */?>
                   <?php /*?> <div class="">
                        <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('overtime') ?>
                                <small>(<?= lang('per_hour') ?>)</small>
                                :</strong> </label>
                        <p class="form-control-static">
                            <?php
                            if (!empty($emp_salary_info->overtime_salary)) {
                                echo display_money($emp_salary_info->overtime_salary, $curency->symbol);
                            }
                            ?>
                        </p>
                    </div><?php */?>

                    <!-- ***************** Salary Details  Ends *********************-->
               <?php 
			   if (!empty($salary_allowance_info)) { ?>
                    <!-- ******************-- Allowance Panel Start **************************-->
                    <div class="earning">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <strong><?= lang('Earnings') ?></strong>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php
								
                                $total_salary = 0;
                                if (!empty($salary_allowance_info)):foreach ($salary_allowance_info as $v_allowance_info):
                                    ?>
                                    <div class="">
                                        <label
                                            class="col-sm-6 control-label"><strong><?php echo ucwords($v_allowance_info->earning_type); ?>
                                                : </strong></label>
                                        <p class="form-control-static"><?php 
										
										if($v_allowance_info->earning_type == 'Regular hours' || $v_allowance_info->earning_type == 'PTO hours' || $v_allowance_info->earning_type == 'Over time hours') {
                                            
                                            echo display_money($v_allowance_info->earning_amount, $curency->symbol).'/hour';
                                        }  else {  
                                                echo display_money($v_allowance_info->earning_amount, $curency->symbol);
                                        }
										//echo display_money($v_allowance_info->earning_amount, $curency->symbol) ?></p>
                                    </div>
                                    <?php $total_salary += $v_allowance_info->earning_amount; ?>
                                <?php endforeach; ?>
                                <?php else: ?>
                                    <h2> <?= lang('nothing_to_display') ?></h2>
                                <?php endif; ?>
                            </div>
							
                        </div>
                    </div><!-- ********************Allowance End ******************-->
			   <?php } ?>
                     <?php 
					 
					 if (!empty($salary_deduction_info)) { ?>
                    <!-- ************** Deduction Panel Column  **************-->
                    <div class="deduction">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <strong><?= lang('deductions') ?></strong>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php
                                $total_deduction = 0;
                                if (!empty($salary_deduction_info)):
								foreach ($salary_deduction_info as $v_deduction_info):
                                    ?>
                                    <div class="">
                                        <label
                                            class="col-sm-6 control-label"><strong><?php echo ucwords($v_deduction_info->deduction_type); ?>
                                                : </strong></label>
                                        <p class="form-control-static"><?php
											if($v_deduction_info->deductionBasedOn == 'percentAmt') {
												echo $v_deduction_info->deductionPercent."%";
											} else {
												echo display_money($v_deduction_info->deduction_amount, $curency->symbol);
											}
                                            ?></p>
                                    </div>
                                    <?php //$total_deduction += $v_deduction_info->deduction_amount ?>
                                <?php endforeach; ?>
                                <?php else: ?>
                                    <h2> <?= lang('nothing_to_display') ?></h2>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div><!-- ****************** Deduction End  *******************-->
					 <?php } ?>
					<?php 
					if (!empty($salary_tax_info)){ ?>
					<!-- ************** Tax Panel Column  **************-->
                    <div class="tax">
                        <div class="panel panel-custom">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <strong><?= lang('Taxes') ?></strong>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php
                                $total_tax = 0;
                                if (!empty($salary_tax_info)):
								foreach ($salary_tax_info as $v_tax_info):
                                    ?>
                                    <div class="">
                                        <label
                                            class="col-sm-6 control-label"><strong><?php echo ucwords($v_tax_info->tax_type); ?>
                                                : </strong></label>
                                        <p class="form-control-static"><?php
										   /*  if($v_tax_info->taxCalc=='max'){
                                            echo display_money($v_tax_info->tax_amount, $curency->symbol);
											 }elseif($v_tax_info->taxCalc=='rng'){
												 echo display_money($v_tax_info->taxRangeAmount, $curency->symbol);
											 }else{
												 echo display_money($v_tax_info->taxOthAmount, $curency->symbol);
											 }
											 */
											 echo $v_tax_info->taxWithholding."%";
                                            ?></p>
                                    </div>
                                    <?php 
									 if($v_tax_info->taxCalc=='max'){
									     $total_tax += $v_tax_info->tax_amount;
								     }elseif($v_tax_info->taxCalc=='rng'){
										 $total_tax += $v_tax_info->taxRangeAmount;
									 }else{
										 $total_tax += $v_tax_info->taxOthAmount;
									 }
										 
										
									?>
                                <?php endforeach; ?>
                                <?php else: ?>
                                    <h2> <?= lang('nothing_to_display') ?></h2>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div><!-- ****************** tax End  *******************-->
					<?php } ?>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal-footer hidden-print">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right col-sm-8">
                <div class="col-sm-2 pull-right" style="margin-right: -31px;">
                    <button type="button" class="btn col-sm-12 pull-right btn-default btn-block"
                            data-dismiss="modal"><?= lang('close') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function printDiv(printableArea) {
        var printContents = document.getElementById(printableArea).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

