<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang('Company contributions categories') ?></strong>
			<div class="pull-right"><a data-toggle="modal" data-target="#myModal" href="<?= base_url() ?>admin/payroll/new_company_category" class="btn btn-primary btn-xs"><?= lang('Add new') ?></a>
			</div>
        </div>
    </div>
	
    <!-- Table -->
    <table class="table table-striped DataTables" id="datatable_action"  cellspacing="0" width="100%">
        <thead>
		   <tr>
				<th><?= lang('S.no') ?></th>
				<th><?= lang('Category Name') ?></th>
				<th><?= lang('Action') ?></th>
			</tr>
        </thead>
        <tbody>
		<?php 
		 $i =1;
		foreach($company_category as $category) { 
			 //$action = anchor(base_url('admin/depreciation_types/delete_depreciation_types/' . $depreciation_type->id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "onclick"=>"return confirm('You are about to delete a record. This cannot be undone. Are you sure?')", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';
              
		?>
			<tr role="row">
				<td><?php echo $i; ?></td>
				<td><?php echo $category->category_name; ?></td>
				<td>
					<?= btn_edit_modal('admin/payroll/new_company_category/' . $category->id) ?>
					<?= btn_delete('admin/payroll/delete_company_category/' . $category->id) ?>
				</td>
			</tr>
		<?php $i++; } ?>
        </tbody>
    </table>
	
</div>