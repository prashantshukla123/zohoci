<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('Company Contribution Category') ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/payroll/save_company_category/<?php
              if (!empty($category_info->id)) {
                  echo $category_info->id;
              }
              ?>" method="post" class="form-horizontal form-groups-bordered">

            
            <div class="">
                <label class="control-label"><?= lang('Category Name') ?><span
                        class="text-danger">*</span></label>
                <input type="text" required name="category_name" id="category_name" value="<?php
                if (!empty($category_info->category_name)) {
                    echo $category_info->category_name;
                }
                ?>" class="form-control">
            </div>
			
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>