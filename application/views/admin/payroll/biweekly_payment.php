<?php $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row(); ?>

<?php 	
 $userId = $v_employee->user_id;
 
  //echo"<pre>"; print_r($v_employee); die;
//check year is leap year
		if(date('L', mktime(0, 0, 0, 1, 1, $payment_year)) == 1) {
			//leap year code
			$customer_payment_dates[$v_employee->user_id][$payment_year.'-12']['26'] = $customer_payment_dates[$v_employee->user_id]['2021-01'];
			$payment_date = $customer_payment_dates[$v_employee->user_id][($payment_year+1).'-01']['26']['payment_date'];
		} else {
			$payment_date = '';
		}
		
		$biweekly_payment_arr = $customer_payment_dates[$v_employee->user_id][$payment_month];

		$monthly_template_id = $v_employee->salary_template_id;
		$hourly_template_id  = $v_employee->hourly_rate_id;
		 
		if(!empty($monthly_template_id)){
			$s_template_id = $monthly_template_id;
		}else{
			$s_template_id = $hourly_template_id;	 
		}
		

		foreach($biweekly_payment_arr as $key=>$value) {
			
			$payment_number = $key+1;
			
			
			if(isset($value['payment_date']) && !empty($value['payment_date'])) {
				$end_payment_date = $value['payment_date'];
			} else {
				$end_payment_date = $payment_date;
			}
			
			/* salary calculation for wages employee */
			
			 $day = strtotime($end_payment_date);
			 $payDate = strtotime($end_payment_date);
			 $endDay = date("Y-m-d",$day);
			 $startDate = $value['payment_start_date'];
			 $endDate = $endDay;
			
			
			 
            $data = array('user_id' => $v_employee->user_id, 'payment_month' => $payment_month,"pay_frequency"=>"biweekly","payment_number"=>$payment_number);
			$query = $this->db->select('*')->where($data)->get('tbl_salary_payment');
			$salary_info = $query->row();
			//echo"<pre>"; print_r( $salary_info); die;			
			   

			if(!empty($salary_info)) {
				$netSalary = $salary_info->net_salary;
			} else {
				
				if(!empty($monthly_template_id)){
					$s_template_id = $monthly_template_id;
					$salary_type = "salary";
				}else{
					$s_template_id = $hourly_template_id;
					$salary_type = "hourly";					
				}
				
				//get total allowance
				$employeeEarning = $this->payroll_model->get_emp_earning($v_employee->user_id, $payment_month, $payment_number);
				//echo $employeeEarning; 
				$total_award_amount = $this->payroll_model->total_award_amount($v_employee->user_id, $startDate, $endDate);
				
				$employeeEarning = $employeeEarning+$total_award_amount;
				
				$gp = $employeeEarning;
				
				//get total taxes
				//$employeeTaxes = $this->payroll_model->get_emp_taxes($v_employee->user_id, $s_template_id, $salary_type, $payment_month, $payment_number);
				//echo $employeeTaxes."ok"; exit;
				//get total deduction
				$employeeDeductions = $this->payroll_model->get_emp_deductions($v_employee->user_id, $payment_month, $payment_number);
				
				//excluded tax
				$employee_excluded_Earnings = $this->payroll_model->get_emp_earning($v_employee->user_id, $payment_month, $payment_number,'yes');
				
				//get total deduction
				$employee_excluded_Deductions = $this->payroll_model->get_emp_deductions($v_employee->user_id, $payment_month, $payment_number,'yes');
				
				$total_exclude_earning_deduction = $employee_excluded_Earnings+$employee_excluded_Deductions;
				
				//get total deduction
				$taxable_income = $this->payroll_model->taxable_income($employeeEarning, $total_exclude_earning_deduction);
				
				//echo $s_template_id."hh"; exit;
				
				$employeeTaxes = $this->payroll_model->get_emp_taxes($taxable_income, $v_employee->user_id);
				
				//gross incom after taxes
				$gross_after_taxes = $this->payroll_model->gross_after_taxes($gp, $employeeTaxes);
				
				//echo $gross_after_taxes."tt"; exit;
				
				
				
				
				//echo $employeeTaxes."test"; exit;
				
				$netSalary = $this->payroll_model->net_salary_cal($gross_after_taxes, $employeeDeductions);
				
				
				//get total contribution
				//$employeeContributions = $this->payroll_model->get_emp_contributions($v_employee->user_id, $s_template_id, $salary_type, $payment_month, $payment_number);
				
				//get total benefits
				//$employeeBenefits = $this->payroll_model->get_emp_benefits($v_employee->user_id, $s_template_id, $salary_type, $payment_month, $payment_number);
		
			}

			     
			 
			/* Start calculation */
			 $emp_salary_info = $this->payroll_model->get_emp_salary_list($userId);
			 //echo"<pre>"; print_r($emp_salary_info); die;
			 
			?>
		<tr>
			<td>
			<?php 
			//echo date("Y-m-d",strtotime($end_payment_date)); die;
			 $day = strtotime($end_payment_date);
			 $payDate = strtotime($end_payment_date);
			 $day = date("Y-m-d",$day);
			if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) {
			}else{ 
			 if(strtotime(date('D, d M Y')) >= strtotime($end_payment_date))
			 {
			?>
			<input  type="checkbox" data-title="payDate<?php echo $key;?>" data-id="<?php echo $payDate; ?>" name="paymentCheck[]" value ="<?php echo $v_employee->user_id."-".$key; ?>" class="payment_type paycheck" />
			 <?php }else{ ?>
			 <input disabled="true" type="checkbox" data-title="payDate<?php echo $key;?>" data-id="<?php echo $payDate; ?>" name="paymentCheck[]" value ="<?php echo $v_employee->user_id."-".$key; ?>" class="payment_type paycheck" />
			<?php
			 }
			} ?>
			</td>
			<td><?php echo date('Y-m-d', strtotime($value['payment_start_date'])).' - '.date('Y-m-d', strtotime($end_payment_date)); ?></td>
			<td><?php echo $v_employee->fullname; ?></td>
			<td><?php			
				
				$set_salary = false;
				if (!empty($v_employee->salary_grade)) {
					echo $v_employee->salary_grade . ' <small>(' . ucfirst($v_employee->payfrequency) . ')</small>';
				} else if (!empty($v_employee->hourly_grade)) {
					echo $v_employee->hourly_grade . ' <small>(' . ucfirst($v_employee->payfrequency) . ')</small>';
				} else {
					echo '<span class="text-danger">' . lang('did_not_set_salary_yet') . '</span>';
					$set_salary = true;
				}
				
				?></td>
				<td><?php 
				if($v_employee->payment_type=='via_cheque'){
				  $payMode ="Pay via cheque"; 	 
				 }else if($v_employee->payment_type=='via_ACH'){
				  $payMode ="Pay via ACH"; 
				 } else {
					$payMode ="-";
				 }
				echo $payMode; ?></td>
			<td><?php
				if (!empty($v_employee->basic_salary)) {
					echo $v_employee->basic_salary;
					$basic_salary = $v_employee->basic_salary;
				} else if (!empty($v_employee->hourly_grade)) {
					echo $v_employee->hourly_rate . ' <small>(' . lang('per_hour') . ')</small>';
					$basic_salary = $v_employee->hourly_rate."-per hour";
				} else {
					echo '-';
					$basic_salary = '';
				}
				?></td>
			<td>
			<?php 
					
					echo display_money($netSalary,$curency->symbol);
								
				if(!empty($netSalary)){				
				?>
				<input type="hidden" name="makePayment[<?php echo $v_employee->user_id; ?>][<?php echo $key; ?>]" value="<?php echo $basic_salary."/".$netSalary."/".$v_employee->payment_type."/".$v_employee->departments_id."/".$payment_month."/biweekly"."/".$payment_number."/".strtotime($end_payment_date)."/".strtotime($value['payment_start_date'])."/".$v_employee->fullname."/".$gp."/".$taxable_income.'/'.$employeeDeductions.'/'.$employeeTaxes; ?>" />
				<?php }else{ ?>				                             
				<input type="hidden" name="makePayment[<?php echo $v_employee->user_id; ?>][<?php echo $key; ?>]" value="<?php echo"0/".$amt."/".$v_employee->payment_type."/".$v_employee->departments_id."/".$payment_month."/biweekly"."/".$payment_number."/".strtotime($end_payment_date)."/".strtotime($value['payment_start_date'])."/".$v_employee->fullname."/".$gp."/".$taxable_income.'/'.$employeeDeductions.'/'.$employeeTaxes; ?>" /> 
				<?php } ?>
				</td>
			<td><?php if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) { ?>
					<a href="<?php echo base_url() ?>admin/payroll/salary_payment_details/<?php echo $salary_info->salary_payment_id. '/biweekly/'.strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date); ?>"
					   class="btn btn-info btn-xs" title="View" data-toggle="modal"
					   data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>
				<?php } else { ?>
					<a href="<?php echo base_url() ?>admin/payroll/view_payment_details/<?php echo $v_employee->user_id . '/' . $payment_month.'/biweekly/'.strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date).'/'.$payment_number; ?>"
					   class="btn btn-info btn-xs" title="View" data-toggle="modal"
					   data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>

				<?php } ?>
				
				
			</td>
			<td>
				<?php if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) { ?>
					<span class="label label-success"><?= lang('paid') ?></span>							
					<?php
				} else {
					if (empty($set_salary)) {
						?>
						<span class="label label-danger"><?= lang('unpaid') ?></span>
						
					<?php }
				} ?>
			</td>
			<td>
			    <?php  //to add the hourly employee attendance.
					if (!empty($v_employee->hourly_grade)) { ?>
						<?php if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) {  }else{?>
						<a href="<?= base_url() ?>admin/payroll/add_time_manually/<?php echo $v_employee->user_id; ?>/<?php echo strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date).'/'.$payment_number.'/'.$v_employee->departments_id."/".$payment_month; ?>" class="text-success"
						   data-toggle="modal" data-placement="top" data-target="#myModal">Manage Hours</a> | 
						<?php } ?>
				<?php } //end ?>
				<?php if (!empty($salary_info) && $salary_info->user_id == $v_employee->user_id) { ?>
					<a class="text-success" target="_blank"
					   href="<?php echo base_url() ?>admin/payroll/receive_generated/<?php echo $salary_info->salary_payment_id. '/biweekly/'.strtotime($value['payment_start_date']).'/'.strtotime($end_payment_date); ?>"><?= lang('generate_payslip') ?></a>
					| <a class="" href="<?php echo base_url() ?>admin/payroll/make_payment/<?php echo $v_employee->user_id . '/' . $v_employee->departments_id . '/' . $payment_month. '/biweekly/' . $payment_number.'/'.strtotime($end_payment_date).'/'.strtotime($value['payment_start_date']); ?>/viewpaymenthistory"><?= lang('Payment History') ?>
						</a>
				<?php } else {
					if (!empty($set_salary)) {
						?>
						<a class="text-warning text-bold" target="_blank"
						   href="<?php echo base_url() ?>admin/payroll/manage_salary_details/<?php echo $v_employee->departments_id; ?>"><?= lang('set_slary') ?></a>
							| <a class="" href="<?php echo base_url() ?>admin/payroll/make_payment/<?php echo $v_employee->user_id . '/' . $v_employee->departments_id . '/' . $payment_month. '/biweekly/' . $payment_number.'/'.strtotime($end_payment_date).'/'.strtotime($value['payment_start_date']); ?>/viewpaymenthistory"><?= lang('Payment History') ?>
						</a>
					<?php }else {
							?> <a class="" href="<?php echo base_url() ?>admin/payroll/make_payment/<?php echo $v_employee->user_id . '/' . $v_employee->departments_id . '/' . $payment_month. '/biweekly/' . $payment_number.'/'.strtotime($end_payment_date).'/'.strtotime($value['payment_start_date']); ?>/viewpaymenthistory"><?= lang('Payment History') ?>
									</a>
							<?php	
						}

					} 

				 ?>
			</td>
		</tr>
	<?php  } ?>