<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('New Other Benefits') ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/payroll/save_other_benifits/<?php echo $profile_info->user_id; ?>/<?php
              if (!empty($benifit_info->id)) {
                  echo $benifit_info->id;
              }
              ?>" method="post" class="form-horizontal form-groups-bordered">
            <!-- CV Upload -->
			<?php 
			if($salary_info->user_payType =='salary'){ ?>
			<input type="hidden" name="salary_template_id" value="<?=$salary_info->monthly_template_id ?>"/>
			<?php }else{ ?>
			<input type="hidden" name="hourly_template_id" value="<?=$salary_info->hourly_rate_id ?>"/>
			<?php } ?>
			<input type="hidden" name="salary_type" value="<?=$salary_info->user_payType ?>"/>
			
            <div class="">
                <label class="control-label"><?= lang('Benefits type ') ?> <span class="text-danger">*</span></label>
                <input required type="text" name="benifit_type" value="<?php
                if (!empty($benifit_info->benifit_type)) {
                    echo $benifit_info->benifit_type;
                }
                ?>" class="form-control">
                <input type="hidden" required name="id" value="<?php
                if (!empty($benifit_info->id)) {
                    echo $benifit_info->id;
                }
                ?>" class="form-control">
            </div>
			<div class="">
                <label class="control-label"><?= lang('Benefits Based On') ?></label>
				<select name="benifitBasedOn" id="benifitBasedOn" class="form-control">
				  <option value="percentAmt">Percentage</option>
				  <option value="fixAmt">Fixed</option>				  
				</select>
            </div>
			<div class="benifitPercent">
                <label class="control-label"><?= lang('Benefits percentage(%)') ?></label>
				<input type="text" name="benifitPercent" maxlength="7"  id="benifitPercent" value="<?php
                if (!empty($benifit_info->benifitPercent)) {
                    echo $benifit_info->benifitPercent;
                }
                ?>" class="form-control">
            </div>
            <div class="benifitAmount" style="display:none">
                <label class="control-label"><?= lang('Benefits amount($)') ?></label>
                <input type="text" name="benifit_amount" id="benifitAmt" maxlength="7" value="<?php
                if (!empty($benifit_info->benifit_amount)) {
                    echo $benifit_info->benifit_amount;
                }
                ?>" class="form-control">
            </div>

            <div class="">
                <label class="control-label"><?= lang('Effective date') ?><span class="text-danger">*</span></label>
				<div class="input-group">
                        <input type="text" onkeydown="return false" name="benifit_date" placeholder="" class="form-control effective_date" required="" value="<?php
						if (!empty($benifit_info->benifit_date)) {
							echo $benifit_info->benifit_date;
						}
						?>" data-parsley-id="8">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
               
            </div>
            	
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>
<script>
$(function() {	
	$('#benifitPercent').on('input', function() {
		this.value = this.value
		  .replace(/[^\d.]/g, '')             // numbers and decimals only
		  .replace(/(^[\d]{2})[\d]/g, '$1')   // not more than 2 digits at the beginning
		  .replace(/(\..*)\./g, '$1')         // decimal can't exist more than once
		  .replace(/(\.[\d]{4})./g, '$1');    // not more than 4 digits after decimal
	  });
	});
   $(document).ready(function() {
	   $('body').on('change', '#benifitBasedOn', function() {
		  var selectedVal = this.value;
		  if(selectedVal=='fixAmt'){			  
			  $(".benifitPercent").css("display","none");
			  $(".benifitAmount").css("display","block");
		  }else{
			  $(".benifitAmount").css("display","none");
			  $(".benifitPercent").css("display","block");
			  
		  }
		});
		
        $('#benifitAmt').keypress(function (event) {
            return isNumber(event, this)
        });
		
    });
   // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
$("input.effective_date").datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		startDate: new Date()
	});     
</script>