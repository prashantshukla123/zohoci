<div id="printableArea">
<?php
$startDate = date('Y-m-d', $other_payment_details['payment_start_date']);
$endDate = date('Y-m-d', $other_payment_details['payment_end_date']);

$employeeEarning = $this->payroll_model->get_emp_earning($employee_info->user_id, $payment_month, $payment_number);

$total_award_amount = $this->payroll_model->total_award_amount($v_employee->user_id, $startDate, $endDate);
				
$employeeEarning = $employeeEarning+$total_award_amount;

$gp = $employeeEarning;
							
$employeeDeductions = $this->payroll_model->get_emp_deductions($employee_info->user_id, $payment_month, $payment_number);
	
$employee_excluded_Earnings = $this->payroll_model->get_emp_earning($employee_info->user_id, $payment_month, $payment_number,'yes');
	
//get total deduction
$employee_excluded_Deductions = $this->payroll_model->get_emp_deductions($employee_info->user_id, $payment_month, $payment_number,'yes');

$total_exclude_earning_deduction = $employee_excluded_Earnings+$employee_excluded_Deductions;

//get total deduction
$taxable_income = $this->payroll_model->taxable_income($employeeEarning, $total_exclude_earning_deduction);

//echo $s_template_id."hh"; exit;

$employeeTaxes = $this->payroll_model->get_emp_taxes($taxable_income, $employee_info->user_id);
$gross_after_taxes = $this->payroll_model->gross_after_taxes($gp, $employeeTaxes);
?>


    <div class="modal-header ">
        <h4 class="modal-title" id="myModalLabel"><?= lang('payment_details') ?>
            <div class="pull-right ">
                <button class="btn btn-xs btn-danger" type="button" data-toggle="tooltip" title="Print" onclick="printDiv('printableArea')"><i class="fa fa-print"></i></button>
            </div>
        </h4>
    </div>
	<?php //echo"<pre>"; print_r($employee_info);  ?>
    <div class="modal-body wrap-modal wrap">
        <div class="show_print" style="width: 100%; border-bottom: 2px solid black;margin-bottom: 30px">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <td style="width: 50px; border: 0px;">
                        <img style="width: 50px;height: 50px;margin-bottom: 5px;"
                             src="<?= base_url() . config_item('company_logo') ?>" alt="" class="img-circle"/>
                    </td>

                    <td style="border: 0px;">
                        <p style="margin-left: 10px; font: 14px lighter;"><?= config_item('company_name') ?></p>
                    </td>
                </tr>
            </table>
        </div><!-- show when print start-->

        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-2 col-sm-2">
                    <div class="fileinput-new thumbnail"
                         style="width: 144px; height: 158px; margin-top: 14px; margin-left: 16px; background-color: #EBEBEB;">
                        <?php if ($employee_info->avatar): ?>
                            <img src="<?php echo base_url() . $employee_info->avatar; ?>"
                                 style="width: 142px; height: 148px; border-radius: 3px;">
                        <?php else: ?>
                            <img src="<?php echo base_url() ?>/img/user.png" alt="Employee_Image">
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-1 col-sm-1">
                    &nbsp;
                </div>
                <div class="col-lg-8 col-sm-8 ">
                    <div>
                        <div style="margin-left: 20px;">
                            <h3><?php echo $employee_info->fullname; ?></h3>
                            <hr class="mt0"/>
                            <table class="table-hover">
                                <tr>
                                    <td><strong><?= lang('emp_id') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?php echo "$employee_info->employment_id"; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= lang('departments') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?php echo "$employee_info->deptname"; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= lang('designation') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?php echo "$employee_info->designations"; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?= lang('joining_date') ?></strong> :</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td><?= strftime(config_item('date_format'), strtotime($employee_info->joining_date)) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <!-- ********************************* Salary Details Panel ***********************-->
            <div class="panel panel-custom">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?= lang('salary_details') ?></strong>
                    </div>
                </div>
                <div class="panel-body1">
                    <div class="">
                        <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('salary_month') ?>
                                :</strong></label>
                        <p class="form-control-static"><?php echo date('F Y', strtotime($payment_month)); ?></p>
                    </div>
					<?php if(isset($other_payment_details['pay_frequency']) && !empty($other_payment_details['pay_frequency'])) { ?>
						<div class="">
							<label for="field-1" class="col-sm-5 control-label"><strong><?= lang('Pay Frequency') ?>
									:</strong></label>
							<p class="form-control-static"><?php echo ucfirst($other_payment_details['pay_frequency']); ?></p>
						</div>
					<?php } ?>
					<?php if(isset($other_payment_details['payment_start_date']) && !empty($other_payment_details['payment_start_date'])) { ?>
						<div class="">
							<label for="field-1" class="col-sm-5 control-label"><strong><?= lang('Pay Period') ?>
									:</strong></label>
							<p class="form-control-static"><?php echo date('D, d M Y', $other_payment_details['payment_start_date']).' - '.date('D, d M Y', $other_payment_details['payment_end_date']); ?></p>
						</div>
					<?php } ?>
                    <?php
                    $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();

                    if (!empty($total_hours)) :
                        ?>
                        <div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('hourly_grade'); ?>
                                    :</strong> </label>
                            <p class="form-control-static"><?php echo "Hourly"; ?></p>
                        </div>
						
                        <div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('Regular hourly rate'); ?>
                                    :</strong> </label>
                            <p class="form-control-static"><?php
                                echo display_money($employee_info->hourly_rate, $curency->symbol);
                                ?></p>
                        </div>
						<div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('PTO hourly rate'); ?>
                                    :</strong> </label>
                            <p class="form-control-static"><?php
                                echo display_money($employee_info->ptohours_rate, $curency->symbol);
                                ?></p>
                        </div>
						<div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('Overtime hourly rate'); ?>
                                    :</strong> </label>
                            <p class="form-control-static"><?php
                                echo display_money($employee_info->overtimehours_rate, $curency->symbol);
                                ?></p>
                        </div>
						<?php //echo "<pre>"; print_r($total_hours); die;?>
                        <div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('Regular Hours'); ?>
                                    :</strong> </label>
                            <p class="form-control-static">
                                <strong><?php echo $regular_hours; ?></strong>
                            </p>
                        </div>
						<div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('PTO Hours'); ?>
                                    :</strong> </label>
                            <p class="form-control-static">
                                <strong><?php echo $pto_hours; ?></strong>
                            </p>
                        </div>
						<div class="">
                            <label for="field-1"
                                   class="col-sm-5 control-label"><strong><?php echo lang('Overtime Hours'); ?>
                                    :</strong> </label>
                            <p class="form-control-static">
                                <strong><?php echo $overtime_hours; ?></strong>
                            </p>
                        </div>

                        <?php
                         $total_hour = $total_hours;
						 $month = $employee_info->salary_template_id;
						 $hour  = $employee_info->hourly_rate_id;
						 if(!empty($month)){
							$s_template_id = $month;
						 }else{
							$s_template_id = $hour;	 
						 }
						 $uid = $employee_info->user_id;
						 //echo"<pre>"; print_r($employee_info); die;
						 //echo  $sql = $this->db->last_query();			 
						 //echo"<pre>"; print_r($employeeEarning); die;
						 foreach($allowance_info as $ernData){
						   if($ernData->earning_type == 'Regular hours'){	 
							 $rateOfRagularHour += $ernData->earning_amount;
						   }
						   if($ernData->earning_type == 'PTO hours'){
							 $rateOfPTOHour += $ernData->earning_amount;
						   }
						   
						   if($ernData->earning_type == 'Over time hours'){
							 $rateOfOverTimeHour += $ernData->earning_amount;	 
						   }
						   
						   if($ernData->amtType == 'fix'){
							   $ernAmt += $ernData->earning_amount;
						   }					   
						   
						 }
						
						$rg_hour = $regular_hours;
                        $pto_hour = $pto_hours;
                        $ovt_hour = $overtime_hours;
						
						$rg_amount =  $rg_hour * $rateOfRagularHour;
						$pto_amount =  $pto_hour * $rateOfPTOHour;
						$ovt_amount =  $ovt_hour * $rateOfOverTimeHour;
						
						
                        $total_hours_amount = round($rg_amount + $pto_amount + $ovt_amount + $ernAmt ,2);
                        
						?>
						
                    <?php else: ?>
                        <div class="">
                            <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('salary_grade') ?>
                                    :</strong>
                            </label>
                            <p class="form-control-static"><?php
                                echo $employee_info->salary_grade;
                                ?></p>
                        </div>
                        <div class="">
                            <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('basic_salary') ?>
                                    :</strong>
                            </label>
                            <p class="form-control-static"><?php
                                echo display_money($employee_info->basic_salary, $curency->symbol);
                                ?></p>
                        </div>
                        <?php if (!empty($employee_info->overtime_salary)): ?>
                            <div class="">
                                <label for="field-1" class="col-sm-5 control-label"><strong><?= lang('overtime') ?>
                                        <small> (<?= lang('per_hour') ?>)</small>
                                        :</strong> </label>
                                <p class="form-control-static"><?php
                                    echo display_money($employee_info->overtime_salary, $curency->symbol);
                                    ?></p>
                            </div>
                            <div class="">
                                <label for="field-1"
                                       class="col-sm-5 control-label"><strong><?php echo lang('overtime_hour'); ?>
                                        :</strong> </label>
                                <p class="form-control-static">
                                    <strong><?php echo $overtime_info['overtime_hours'] . ' : ' . $overtime_info['overtime_minutes'] . ' ' . lang('m'); ?></strong>
                                </p>
                            </div>
                            <div class="">
                                <label for="field-1"
                                       class="col-sm-5 control-label"><strong><?= lang('overtime_amount') ?>
                                        :</strong> </label>
                                <p class="form-control-static"><?php
                                    if (!empty($overtime_info)) {
                                        $overtime_hour = $overtime_info['overtime_hours'];
                                        $overtime_minutes = $overtime_info['overtime_minutes'];
                                        if ($overtime_hour > 0) {
                                            $ov_hours_ammount = $overtime_hour * $employee_info->overtime_salary;
                                        } else {
                                            $ov_hours_ammount = 0;
                                        }
                                        if ($overtime_minutes > 0) {
                                            $ov_amount = round($employee_info->overtime_salary / 60, 2);
                                            $ov_minutes_ammount = $overtime_minutes * $ov_amount;
                                        } else {
                                            $ov_minutes_ammount = 0;
                                        }
										
									$overtime_amount = $ov_hours_ammount + $ov_minutes_ammount;
									
                                    }
                                    echo display_money($overtime_amount, $curency->symbol);
                                    ?></p>

                            </div>
                            <?php
                        endif;
                        ?>
                        <?php
                       
                       // $total_hours_amount = $employee_info->basic_salary + $overtime_amount;
                       // $total_hours_amount = $employee_info->basic_salary;
                    endif;
                    ?>

                   <!-- ******************-- Allowance Panel Start **************************-->
                    <?php
                    $total_allowance = 0;
                    if (!empty($allowance_info)):
                        ?>
                        <div class="earning">
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <strong><?= lang('Earnings') ?></strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    foreach ($allowance_info as $v_allowance) :
                                        ?>
                                        <div class="">
                                            <label
                                                class="col-sm-6 control-label"><strong><?php echo $v_allowance->earning_type ?>
                                                    : </strong></label>
                                            <p class="form-control-static">
                                                <?php echo display_money($v_allowance->earning_amount, $curency->symbol) ?></p>
                                        </div>
                                        <?php
                                        $total_allowance += $v_allowance->earning_amount;
                                    endforeach;
									
									$award_info = $this->payroll_model->award_info_details($v_employee->user_id, $startDate, $endDate);
									if (!empty($award_info)) : 
									foreach ($award_info as $v_award_info) :
									?>
										<div class="">
											<label for="field-1" class="col-sm-5 control-label"><strong><?= lang('award_name') ?>
													<small>( <?php echo $v_award_info->award_name; ?> )</small>
													:</strong> </label>
											<p class="form-control-static"><?php
												echo display_money($v_award_info->award_amount, $curency->symbol);
												?></p>
										</div>
										<?php
									endforeach;
									endif;
                                    ?>
                                </div>
                            </div>
                        </div><!-- ********************Allowance End ******************-->
                    <?php endif; ?>
					
					<?php $total_hours_amount = $total_hours_amount + $total_allowance; ?>
					
					<!-- ******************-- Deduction Panel Start **************************-->
                    <?php
                    $deduction_info = $this->payroll_model->get_emp_deduction_details($employee_info->user_id, $payment_month, $payment_number);
                    if (!empty($deduction_info)):
                        ?>
                        <div class="earning">
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <strong><?= lang('Deduction') ?></strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    foreach ($deduction_info as $v_deduction) :
                                        ?>
                                        <div class="">
                                            <label
                                                class="col-sm-6 control-label"><strong><?php echo $v_deduction->deduction_type ?>
                                                    : </strong></label>
                                            <p class="form-control-static">
                                                <?php 
												if($v_deduction->deductionBasedOn == 'fixAmt'){
												$total_deduction +=$v_deduction->deduction_amount;
												echo display_money($v_deduction->deduction_amount, $curency->symbol);
												}else{
													$rate = $v_deduction->deductionPercent."%";
													$amt = $gp * $rate / 100;
													echo display_money($amt, $curency->symbol);
												}
											?></p>
                                        </div>
                                        <?php
                                       
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        </div><!-- ********************deduction End ******************-->
                    <?php endif; ?>
					
					<!-- ******************-- Tax Panel Start **************************-->
                    <?php
					
                   $tax_info = $this->payroll_model->get_emp_tax_details($employee_info->user_id, $payment_month, $payment_number);
                    if (!empty($tax_info)):
                        ?>
                        <div class="earning">
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <strong><?= lang('Taxes') ?></strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php
									
                                    foreach ($tax_info as $v_tax) :
                                        ?>
                                        <div class="">
                                            <label
                                                class="col-sm-6 control-label"><strong><?php echo $v_tax->tax_type ?>
                                                    : </strong></label>
                                            <p class="form-control-static">
                                                <?php 
												//echo $total_hours_amount."fdgtfd";
												  $percent = $v_tax->taxWithholding;
												  $taxAmt = $taxable_income * $percent / 100;
												  echo display_money($taxAmt, $curency->symbol);
													
												?></p>
                                        </div>
                                        <?php
                                        
                                    endforeach;
									$totalTax = $employeeTaxes;
                                    ?>
                                </div>
                            </div>
                        </div><!-- ********************tax End ******************-->
                    <?php endif; ?>
					
					<!-- ******************-- Tax Panel Start **************************-->
                    <?php
                   $contribution_info = $this->payroll_model->get_emp_contributions($employee_info->user_id, $payment_month, $payment_number);
                   if (!empty($contribution_info)):
                        ?>
                        <div class="earning">
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <strong><?= lang('Company contribution') ?></strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php
									 foreach ($contribution_info as $v_cont) :
                                        ?>
                                        <div class="">
                                            <label
                                                class="col-sm-6 control-label"><strong><?php echo $v_cont->contribution_type ?>
                                                    : </strong></label>
                                            <p class="form-control-static">
                                                <?php 
												if($v_cont->contributionBasedOn == 'fixAmt') {
													echo display_money($v_cont->contribution_amount, $curency->symbol); 
												} else {
													echo $v_cont->contributionPercent."%";
												}
												?></p>
                                        </div>
                                        <?php
                                        
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        </div><!-- ********************contribution End ******************-->
                    <?php endif; ?>
					
					<!-- ******************-- Tax Panel Start **************************-->
                    <?php
					$benefit_info = $this->payroll_model->get_emp_benefits($employee_info->user_id, $payment_month, $payment_number);
                    if (!empty($benefit_info)):
                        ?>
                        <div class="earning">
                            <div class="panel panel-custom">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <strong><?= lang('Company other benefit') ?></strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    foreach ($benefit_info as $v_benefit) :
                                        ?>
                                        <div class="">
                                            <label
                                                class="col-sm-6 control-label"><strong><?php echo $v_benefit->benifit_type ?>
                                                    : </strong></label>
                                            <p class="form-control-static">
                                                <?php 
												if($v_benefit->benifitBasedOn == 'fixAmt') {
													echo display_money($v_benefit->benifit_amount, $curency->symbol); 
												} else {
													echo $v_benefit->benifitPercent."%";
												}
												?></p>
                                        </div>
                                        <?php
                                      endforeach;
                                    ?>
                                </div>
                            </div>
                        </div><!-- ********************Benefit End ******************-->
                    <?php endif; ?>
                </div>
            </div><!-- ***************** Salary Details  Ends *********************-->
			
            <!-- ************** Total Salary Details Start  **************-->
            <div class="form-horizontal col-sm-8 pull-right">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong><?= lang('total_salary_details') ?></strong>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="">
                            <label class="col-sm-6 control-label"><strong><?= lang('gross_salary') ?>
                                    : </strong></label>
                            <p class="form-control-static"><?php
							
								
                                echo display_money($gp, $curency->symbol);
                                ?></p>
                        </div>
						
						
                        <div class="">
                            <label class="col-sm-6 control-label"><strong><?= lang('total_deduction') ?>
                                    : </strong></label>
                            <p class="form-control-static"><?php
                               
                                echo display_money($employeeDeductions, $curency->symbol);
                                ?></p>
                        </div>
						 <div class="">
                            <label class="col-sm-6 control-label"><strong><?= lang('Total Tax') ?>
                                    : </strong></label>
                            <p class="form-control-static"><?php
							
								//$total_deduction = $advance_amount + $deduction;
                                echo display_money($employeeTaxes, $curency->symbol);
                                ?></p>
                        </div>
						
					<?php /* ?>	<div class="">
                            <label class="col-sm-6 control-label"><strong><?= lang('Award Amount') ?>
                                    : </strong></label>
                            <p class="form-control-static"><?php
                                echo display_money($total_award, $curency->symbol);
                                ?></p>
                        </div>
					<?php */ ?>
						
                        <div class="">
                            <label class="col-sm-6 control-label"><strong><?= lang('net_salary') ?> : </strong></label>
                            <p class="form-control-static"><?php
								$net_salary = $this->payroll_model->net_salary_cal($gross_after_taxes, $employeeDeductions);
                                echo display_money($net_salary, $curency->symbol);
                                ?></p>
                        </div>
                        <?php if (!empty($salary_payment_info->fine_deduction)): ?>
                            <div class="">
                                <label class="col-sm-6 control-label"><strong> <?= lang('fine_deduction') ?> : </strong></label>
                                <p class="form-control-static"><?php
                                    echo display_money($salary_payment_info->fine_deduction, $curency->symbol);
                                    ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="">
                            <label class="col-sm-6 control-label"><strong><?= lang('Payment Amount') ?> : </strong></label>
                            <p class="form-control-static"><?php
                                if (!empty($salary_payment_info->fine_deduction)) {
                                    $paid_amount = $net_salary - $salary_payment_info->fine_deduction;
                                } else {
                                    $paid_amount = $net_salary;
                                }
                                echo display_money($paid_amount, $curency->symbol);
                                ?></p>
                        </div>
                    </div>
                </div>
            </div><!-- ****************** Total Salary Details End  *******************-->
        </div>
    </div>
</div>
<div class="modal-footer hidden-print">
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right col-sm-8">
                <div class="col-sm-2 pull-right" style="margin-right: -31px;">
                    <button type="button" class="btn col-sm-12 pull-right btn-default btn-block"
                            data-dismiss="modal"><?= lang('close') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function printDiv(printableArea) {
        var printContents = document.getElementById(printableArea).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

