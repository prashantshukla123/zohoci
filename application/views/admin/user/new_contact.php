<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('New Contact') ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate="" enctype="multipart/form-data"
              action="<?php echo base_url() ?>admin/user/update_hr_contact_info/<?php echo $profile_info->account_details_id; ?>/<?php
              if (!empty($contact_info->id)) {
                  echo $contact_info->id;
              }
              ?>" method="post" class="form-horizontal form-groups-bordered">

            <div class="">
							<label class="control-label">
								<?= lang('Contacts') . ' ' . lang('name') ?> <span class="text-danger">*</span></label>
							<input required type="text" name="contact_name" value="<?php
									if (!empty($contact_info->contact_name)) {
										echo $contact_info->contact_name;
									}
									?>" class="form-control">
							<input type="hidden" required name="ec_id" value="<?php
									if (!empty($contact_info->id)) {
										echo $contact_info->id;
									}
									?>" class="form-control">
						</div>
						<div class="">
							<label class="control-label">
								<?= lang('Relationship') ?> <span class="text-danger">*</span></label>
							<input required type="text" name="relationship" value="<?php
									if (!empty($contact_info->relationship)) {
										echo $contact_info->relationship;
									}
									?>" class="form-control">							
						</div>
						<div class="">
							<label class="control-label">
								<?= lang('Phone') ?> <span class="text-danger">*</span></label>
							<input required type="text" name="phone" value="<?php
									if (!empty($contact_info->phone)) {
										echo $contact_info->phone;
									}
									?>" class="form-control">							
						</div>
						<div class="">
							<label class="control-label">
								<?= lang('Address') ?></label>
							<input type="text" name="emergency_address" value="<?php
									if (!empty($contact_info->emergency_address)) {
										echo $contact_info->emergency_address;
									}
									?>" class="form-control">							
						</div>
						  
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								<?= lang('close') ?>
							</button>
							<button type="submit" class="btn btn-primary">
								<?= lang('update') ?>
							</button>
						</div>
        </form>
    </div>
</div>