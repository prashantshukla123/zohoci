<div class="panel panel-custom">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= lang('user') . ' ' . lang('details') ?></h4>
    </div>
    <div class="modal-body wrap-modal wrap">
        <form data-parsley-validate="" novalidate=""
              action="<?php echo base_url() ?>admin/user/update_details/<?php if (!empty($profile_info->account_details_id)) echo $profile_info->account_details_id; ?>"
              method="post" class="form-horizontal form-groups-bordered">
            <div class="form-group">
                <label class="col-sm-3 control-label"><?= lang('emp_id') ?> <span
                        class="required">*</span></label>

                <div class="col-sm-7">
                    <input type="text" id="employment_id" name="employment_id" required
                           placeholder="<?= lang('enter') . ' ' . lang('employment_id') ?>"
                           class="form-control" value="<?php
                    if (!empty($profile_info->employment_id)) {
                        echo $profile_info->employment_id;
                    }
                    ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label"><?= lang('gender') ?>
                    <span class="required">*</span></label>
                <div class="col-sm-7">
                    <select name="gender" class="form-control" required>
                        <option
                            value="male" <?= (!empty($profile_info->gender) && $profile_info->gender == 'male' ? 'selected' : null) ?>><?= lang('male') ?></option>
                        <option
                            value="female" <?= (!empty($profile_info->gender) && $profile_info->gender == 'female' ? 'selected' : null) ?>><?= lang('female') ?></option>

                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><?= lang('date_of_birth') ?> <span
                        class="required">*</span></label>

                <div class="col-sm-7">
                    <div class="input-group">
                        <input type="text" id="date_of_birth" name="date_of_birth"
                               placeholder="<?= lang('enter') . ' ' . lang('date_of_birth') ?>"
                               class="form-control datepicker" required value="<?php
                        if (!empty($profile_info->date_of_birth)) {
                            echo $profile_info->date_of_birth;
                        }
                        ?>">
                        <div class="input-group-addon">
                            <a href="#"><i class="fa fa-calendar"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label"><?= lang('maratial_status') ?>
                    <span class="required">*</span></label>

                <div class="col-sm-7">
                    <select name="maratial_status" class="form-control" required>
                        <option
                            value="married" <?= (!empty($profile_info->maratial_status) && $profile_info->maratial_status == 'married' ? 'selected' : null) ?>><?= lang('married') ?></option>
                        <option
                            value="unmarried" <?= (!empty($profile_info->maratial_status) && $profile_info->maratial_status == 'unmarried' ? 'selected' : null) ?>><?= lang('unmarried') ?></option>
                        <option
                            value="widowed" <?= (!empty($profile_info->maratial_status) && $profile_info->maratial_status == 'widowed' ? 'selected' : null) ?>><?= lang('widowed') ?></option>
                        <option
                            value="divorced" <?= (!empty($profile_info->maratial_status) && $profile_info->maratial_status == 'divorced' ? 'selected' : null) ?>><?= lang('divorced') ?></option>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?= lang('fathers_name') ?> <span
                        class="required">*</span></label>

                <div class="col-sm-7">
                    <input type="text" id="father_name" name="father_name" required
                           placeholder="<?= lang('enter') . ' ' . lang('fathers_name') ?>"
                           class="form-control" value="<?php
                    if (!empty($profile_info->father_name)) {
                        echo $profile_info->father_name;
                    }
                    ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?= lang('mother_name') ?> <span
                        class="required">*</span></label>

                <div class="col-sm-7">
                    <input type="text" id="mother_name" name="mother_name" required
                           placeholder="<?= lang('enter') . ' ' . lang('mother_name') ?>"
                           class="form-control" value="<?php
                    if (!empty($profile_info->mother_name)) {
                        echo $profile_info->mother_name;
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('phone') ?> </strong></label>
                <div class="col-sm-5">
                    <input id="phone" type="text" class="input-sm form-control" value="<?php
                    if (!empty($profile_info)) {
                        echo $profile_info->phone;
                    }
                    ?>" name="phone" placeholder="<?= lang('eg') ?> <?= lang('user_placeholder_phone') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('mobile') ?> </strong> <span class="text-danger">*</span></label>
                <div class="col-sm-5">
                    <input type="text" class="input-sm form-control" value="<?php
                    if (!empty($profile_info)) {
                        echo $profile_info->mobile;
                    }
                    ?>" id="mobile" name="mobile" placeholder="<?= lang('eg') ?> <?= lang('user_placeholder_mobile') ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('skype_id') ?> </strong></label>
                <div class="col-sm-5">
                    <input type="text" class="input-sm form-control" value="<?php
                    if (!empty($profile_info)) {
                        echo $profile_info->skype;
                    }
                    ?>" name="skype" placeholder="<?= lang('eg') ?> <?= lang('user_placeholder_skype') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('passport') ?> </strong></label>
                <div class="col-sm-5">
                    <input type="text" class="input-sm form-control" value="<?php
                    if (!empty($profile_info)) {
                        echo $profile_info->passport;
                    }
                    ?>" name="passport">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('present_address') ?> </strong></label>
                <div class="col-sm-5">
                        <textarea class="input-sm form-control" value="" name="present_address"><?php
                            if (!empty($profile_info)) {
                                echo $profile_info->present_address;
                            }
                            ?></textarea>
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('Ethnicity') ?> </strong></label>
                <div class="col-sm-5">
				<select class="form-control" name="ethnicity">
					<option value=""><?= lang('Select Ethnicity') ?></option>
					<option <?php if($profile_info->ethnicity == 'American Indian or Alaska Native') { echo 'selected'; } ?> value="American Indian or Alaska Native">American Indian or Alaska Native</option>
					<option <?php if($profile_info->ethnicity == 'Asian') { echo 'selected'; } ?> value="Asian">Asian</option>
					<option <?php if($profile_info->ethnicity == 'Black or African American') { echo 'selected'; } ?> value="Black or African American">Black or African American</option>
					<option <?php if($profile_info->ethnicity == 'Hispanic or Latino') { echo 'selected'; } ?> value="Hispanic or Latino">Hispanic or Latino</option>
					<option <?php if($profile_info->ethnicity == 'Native Hawaiian or Other Pacific Islander') { echo 'selected'; } ?> value="Native Hawaiian or Other Pacific Islander">Native Hawaiian or Other Pacific Islander</option>
					<option <?php if($profile_info->ethnicity == 'White') { echo 'selected'; } ?> value="White">White</option>
                 </select>
				
				</div>
            </div>
			<div class="form-group">
                               <label for="field-1" class="col-sm-3 control-label"><strong><?= lang('Location') ?></strong> </label>
                                <div class="col-sm-5">
								
                                  <select class=" form-control " style="width: 100%" name="user_location"> 
								  <option value=""><?= lang('Select location') ?></option>
								  <?php  //echo $login_info->user_location."ok";
                                        $locales = $this->db->get('tbl_warehouse_locations')->result();
                                        foreach ($locales as $loc) :
										?>
                                            <option value="<?= $loc->location_id ?>" <?php if ($login_info->user_location == $loc->location_id) { 
                                                           ?> selected="" <?php } ?>><?= $loc->warehouse_name.", ".$loc->city ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
							
							
			<div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('US Payroll') ?> </strong></label>
                <div class="col-sm-5">
				<select class="form-control" name="include_us_payroll">
					<option value=""><?= lang('Include In US Payroll') ?></option>
					<option <?php if($profile_info->include_us_payroll == 'Yes') { echo 'selected'; } ?> value="Yes">Yes</option>
					<option <?php if($profile_info->include_us_payroll == 'No') { echo 'selected'; } ?> value="No">No</option>
                 </select>
				</div>
            </div>
			
			 <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('Dependents Federal') ?> </strong></label>
                <div class="col-sm-5">
                    <input type="text" class="input-sm form-control" value="<?php
                    if (!empty($profile_info)) {
                        echo $profile_info->dependents_federal;
                    }
                    ?>" name="dependents_federal">
                </div>
            </div>
			
			 <div class="form-group">
                <label class="col-sm-3 control-label"><strong><?= lang('Dependents State') ?> </strong></label>
                <div class="col-sm-5">
                    <input type="text" class="input-sm form-control" value="<?php
                    if (!empty($profile_info)) {
                        echo $profile_info->dependents_state;
                    }
                    ?>" name="dependents_state">
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
                <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
            </div>
        </form>
    </div>
</div>

<script>
function isNumber(evt, element) {
          
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    } 
	
	$(document).ready(function() {
        $('#employment_id').keypress(function (event) {
			//alert('Hi');
            return isNumber(event, this)
        });
		$('#phone').keypress(function (event) {
			//alert('Hi');
            return isNumber(event, this)
        });
		$('#mobile').keypress(function (event) {
			//alert('Hi');
            return isNumber(event, this)
        });
		
	});
	
	$(function(){
		$(document).on('cut copy paste','#employment_id',function(e) {
			e.preventDefault();
		});
        $(document).on('cut copy paste','#phone',function(e) {
			e.preventDefault();
		});
        $(document).on('cut copy paste','#mobile',function(e) {
			e.preventDefault();
		});		
    });

$(function() {
  $('#father_name').keydown(function (e) {  
    if (e.shiftKey || e.ctrlKey || e.altKey) {    
      e.preventDefault();      
    } else {    
      var key = e.keyCode;      
      if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {      
        e.preventDefault();        
      }
    }    
  });
  
  $('#mother_name').keydown(function (e) {  
    if (e.shiftKey || e.ctrlKey || e.altKey) {    
      e.preventDefault();      
    } else {    
      var key = e.keyCode;      
      if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {      
        e.preventDefault();        
      }
    }    
  });
  $('#mother_name').keydown(function (e) {  
    if (e.shiftKey || e.ctrlKey || e.altKey) {    
      e.preventDefault();      
    } else {    
      var key = e.keyCode;      
      if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {      
        e.preventDefault();        
      }
    }    
  });
  


  
});
var FromEndDate = new Date();

   $(function(){
     $('.datepicker').datepicker({
       format: 'mm-dd-yyyy',
       endDate: FromEndDate, 
       autoclose: true
     });
   });
</script>