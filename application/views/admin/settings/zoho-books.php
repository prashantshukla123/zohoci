<?php echo message_box('success') ?>
<?php echo message_box('error') ?>
<div class="row">
    <!-- Start Form -->
    <div class="col-lg-12">
        <div class="panel panel-custom">
			<header class="panel-heading ">
				<h3 class="panel-title"><?= lang('Zoho Books Integration') ?></h3>
			</header>
			<div class="panel-body">
				<div class="col-xs-12 clearfix">
					<p class="pull-left"><strong>Zoho Books Integration Access</strong></p>
					<div class="pull-right">
						<label class="switch">
							<input id="zoho_books_integration_access" name="zoho_books_integration_access" type="checkbox" <?= ($zoho_books_integration_access) ? 'checked' : '' ?>>
							<span></span>
						</label>
					</div>
				</div>
				<hr/>
				
				<div class="row accessPanel" style="display : <?= ($zoho_books_integration_access) ? '' : 'none' ?>">
					<form id="zohoBooksMapping" method="post" action="">
					<div class="col-xs-12 col-sm-8 col-md-9 form-group">
						<label class="panel-title"><?= lang('Bank Account') ?></label><br/>
						<small><?= lang('This is the bank account from which you pay your employees') ?></small>
						<select class="form-control" name="bankAccounts" id="bankAccounts">
							<?php
								if(count($all_accounts) > 0){
									echo "<option value=''>Select Bank Account</option>";
									foreach($all_accounts as $account){
							?>
									<option value="<?= $account->account_id ?>"> <?= $account->account_name ?> </option>
							<?php
									}
								}else{
							?>
								<option>no result found</option>
							<?php } ?>
						</select>
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-3 text-center">
						<label class="">&nbsp;</label><br/><br/>
						<button type="button" class="btn btn-default btn-lrg refreshAccounts">
							<i class="fa fa-refresh"></i>&nbsp; Refresh Accounts
						</button>
					</div>
					
					<br/><br/>
					
					<div class="col-lg-12 clearfix">&nbsp;</div>
					
					<div class="col-lg-12">
						<h3 class="panel-title"><?= lang('Map Accounts in Zoho Books') ?></h3>
						<small>Please select a Zoho books account to sync your payroll transactions.</small>
						
						<button type="button" class="btn btn-default btn-lrg zohoBooksAccounts" style="float:right">
							<i class="fa fa-refresh"></i>&nbsp; Refresh Zoho books Accounts
						</button>
					</div>
					
					<div class="clearfix"></div>
					
					<div class="col-lg-12 table-responsive">
						<!-- Load the zoho-books accounts mapping tablr -->
						<?php $this->load->view('admin/settings/zoho-books-mapping.php') ?>
						<!-- End of zoho-books accounts mapping tablr -->
						
						<br/><br/>
						<div class="form-group">
							<button type="submit" class="btn btn-sm btn-primary">Save Mapping</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
    </div>
</div>

<script type="text/javascript">
	$(document).on("change", "#zoho_books_integration_access", function () {
		var value = 0;
		if($(this).prop("checked")){
			value = 1;
		}
		
		$.ajax({
			type: 'post',
			url: '<?php echo base_url("admin/settings/ajax"); ?>',
			data: {'type' : 'zoho_access', 'value' : value},
			dataType: 'json',
			beforeSend: function() {
				$('div.wrapper').find('section:first').prepend('<div id="loader-wrapper" style="display: none"><div id="loader"></div></div>');
				$('#loader-wrapper').fadeIn();
			}
		}).done(function (res) {
			$('#loader-wrapper').fadeOut(function () {
				$('#loader-wrapper').remove();
			});
			
			if(res.status){
				if(value == 1){
					$('.accessPanel').slideDown();
				}else{
					$('.accessPanel').slideUp();
				}
			}
			
		}).fail(function (err) {
			$('#loader-wrapper').fadeOut(function () {
				$('#loader-wrapper').remove();
			});
			
			console.log('Error : ', err);
			
			alert('There was a problem with AJAX');
		});
	});
	
    $(document).on("click", ".refreshAccounts", function (event) {
        $.ajax({
			type: 'post',
			url: '<?php echo base_url("admin/settings/ajax"); ?>',
			data: {'type' : 'refresh-accounts'},
			dataType: 'json',
			beforeSend: function() {
				$('div.wrapper').find('section:first').prepend('<div id="loader-wrapper" style="display: none"><div id="loader"></div></div>');
				$('#loader-wrapper').fadeIn();
			}
		}).done(function (res) {
			$('#loader-wrapper').fadeOut(function () {
				$('#loader-wrapper').remove();
			});
			
			if(res.status){
				if(res.data != '' || res.data != null){
					$('#bankAccounts').html(res.data);
				}
				
				alert("Bank Accounts are updated, please select account.");
			}else{
				alert(res.message);
			}
		}).fail(function (err) {
			$('#loader-wrapper').fadeOut(function () {
				$('#loader-wrapper').remove();
			});
			
			console.log('Error : ', err);
			
			alert('There was a problem with AJAX');
		});
    });
	
	$(document).on("click", ".zohoBooksAccounts", function (event) {
        $.ajax({
			type: 'post',
			url: '<?php echo base_url("admin/settings/ajax"); ?>',
			data: {'type' : 'refresh-zoho-books-accounts', 'bankAccounts' : $('#bankAccounts').val()},
			dataType: 'json',
			beforeSend: function() {
				$('div.wrapper').find('section:first').prepend('<div id="loader-wrapper" style="display: none"><div id="loader"></div></div>');
				$('#loader-wrapper').fadeIn();
			}
		}).done(function (res) {
			$('#loader-wrapper').fadeOut(function () {
				$('#loader-wrapper').remove();
			});
			
			if(res.status){
				$('.mappingTabel').replaceWith(res.data);
				
				alert("Zoho Books Accounts Updated.");
			}else{
				alert(res.message);
			}
		}).fail(function (err) {
			$('#loader-wrapper').fadeOut(function () {
				$('#loader-wrapper').remove();
			});
			
			console.log('Error : ', err);
			
			alert('There was a problem with AJAX');
		});
    });
	
	$(document).on("click", ".openClose", function(){
		var key = $(this).data('key');
		
		if($(this).hasClass('opened')){
			$('.child_'+key).slideDown();
			$(this).removeClass('btn-success opened').addClass('btn-danger closed').html('<i class="fa fa-minus"></i>');
		}else{
			$('.child_'+key).slideUp();
			$(this).removeClass('btn-danger closed').addClass('btn-success opened').html('<i class="fa fa-plus"></i>');
		}
	});

	$('#zohoBooksMapping').submit(function(event) {
		event.preventDefault(); 
		
		$("#bankAccounts").nextAll('span').remove();
			
		if($("#bankAccounts").val() != ''){
			var req = $.ajax({
				url: '<?php echo base_url("admin/settings/ajax"); ?>',
				type: 'post',
				data: {'type' : 'zohoBooksMapping-save', 'form' : $(this).serialize()},
				dataType: 'json',
				beforeSend: function() {
					$('div.wrapper').find('section:first').prepend('<div id="loader-wrapper" style="display: none"><div id="loader"></div></div>');
					$('#loader-wrapper').fadeIn();
				}
			});
			req.done(function(data) {
				$('#loader-wrapper').fadeOut(function () {
					$('#loader-wrapper').remove();
				});
				if (data.status) {
					
				}
				alert(data.message);
			}).fail(function (err) {
				$('#loader-wrapper').fadeOut(function () {
					$('#loader-wrapper').remove();
				});
				
				//console.log('Error : ', err);
				
				alert('There was a problem with AJAX');
			});
		}else{
			$('<span class="text-danger">Bank account is required field.</span>').insertAfter($('#bankAccounts'));
			$("#bankAccounts").focus();
			
			alert("Please select bank account first.")
		}
	});

	$(document).on('change', '#bankAccounts', function(event) {
		event.preventDefault(); 
		
		$("#bankAccounts").nextAll('span').remove();
			
		if($("#bankAccounts").val() != ''){
			var req = $.ajax({
				url: '<?php echo base_url("admin/settings/ajax"); ?>',
				type: 'post',
				data: {'type' : 'get-zohoBooksMapping', 'bankAccounts' : $('#bankAccounts').val()},
				dataType: 'json',
				beforeSend: function() {
					$('div.wrapper').find('section:first').prepend('<div id="loader-wrapper" style="display: none"><div id="loader"></div></div>');
					$('#loader-wrapper').fadeIn();
				}
			});
			req.done(function(res) {
				$('#loader-wrapper').fadeOut(function () {
					$('#loader-wrapper').remove();
				});
				if (res) {
					$('.mappingTabel').replaceWith(res.data);
				}else{
					alert(res.message)
				}
			}).fail(function (err) {
				$('#loader-wrapper').fadeOut(function () {
					$('#loader-wrapper').remove();
				});
				alert('There was a problem with AJAX');
			});
		}else{
			$('<span class="text-danger">Bank account is required field.</span>').insertAfter($('#bankAccounts'));
			$("#bankAccounts").focus();
		}
	});


</script>