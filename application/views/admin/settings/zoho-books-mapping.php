<table class="table mappingTabel">
	<thead>
		<tr>
			<th colspan="2" width="30%">Pay Type</th>
			<th colspan="2" width="35%">EXPENSE ACCOUNT</th>
			<th colspan="2" width="35%">LIABILITY / ASSET ACCOUNT</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 1; foreach($pay_type as $key=>$types){ ?>
			<tr>
				<td colspan="6">
					<button type="button" class="btn btn-xs <?=($i == 1) ? 'btn-danger closed' : 'btn-success opened'?> openClose" data-key="<?=$key?>">	
						<i class="fa <?=($i == 1) ? 'fa-minus' : 'fa-plus'?>"></i>
					</button>
					&nbsp;
					<b><?php echo ucfirst(str_replace('_',' ',$key)); ?></b>
				</td>
			</tr>
			<?php 
				if(!empty($types) && count($types) > 0) {
					foreach($types as $type){
			?>
				<tr class="child_<?=$key?>" style="display:<?=($i == 1) ? '' : 'none'?>">
					<td> &nbsp </td>
					<td>
						<?php echo ucfirst(str_replace('_',' ',$type)); ?>
					</td>
					<td colspan="2">
						<select name='<?="payType[$key][$type][expense]"?>' class="form-control expenseSelect" style="width:95%">
							<?php
								if(!empty($zoho_books_accounts) && count($zoho_books_accounts) > 0){
									foreach($zoho_books_accounts as $account){
										if($account->account_type == 'expense'){
											$selected = '';
											if(isset($mappingData) && array_key_exists($key, $mappingData)){
												if(array_key_exists($type, $mappingData[$key]) && $mappingData[$key][$type]['expense'] == $account->account_id){
													$selected = "selected='selected'";
												}
											}
											echo "<option value='".$account->account_id."' ".$selected.">".$account->account_name."</option>";
										}
									}
								}
							?>
						</select>
					</td>
					<td colspan="2">
						<select name='<?="payType[$key][$type][liability]"?>' class="form-control liabilitySelect" style="width:95%">
							<?php
								if(!empty($zoho_books_accounts) && count($zoho_books_accounts) > 0){
									foreach($zoho_books_accounts as $account){
										if($account->account_type == 'other_current_liability'){
											$selected = '';
											if(isset($mappingData) && array_key_exists($key, $mappingData)){
												if(array_key_exists($type, $mappingData[$key]) && $mappingData[$key][$type]['liability'] == $account->account_id){
													$selected = "selected='selected'";
												}
											}
											echo "<option value='".$account->account_id."' ".$selected.">".$account->account_name."</option>";
										}
									}
								}
							?>
						</select>
					</td>
				</tr>
			<?php 
					} 
				}else{
			?>
				<tr class="child_<?=$key?>" style="display:<?=($i == 1) ? '' : 'none'?>">
					<td colspan="6"> <center>No <?php echo ucfirst(str_replace('_',' ',$key)); ?> found</center> </td>
				</tr>
			<?php } $i++; } ?>
	</tbody>
</table>