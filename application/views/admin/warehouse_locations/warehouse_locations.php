<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="panel panel-custom">
    <div class="panel-heading">
        <div class="panel-title">
            <strong><?= lang('Warehouse Locations') ?></strong>
			<div class="pull-right"><a href="<?php echo base_url(); ?>admin/warehouse_locations/update_warehouses"><i title="Refresh Warehouse Locations" class="fa fa-refresh"></i></a></div>
        </div>
    </div>

    <!-- Table -->
    <table class="table table-striped DataTables " id="DataTables" cellspacing="0" width="100%">
        <thead>
		   <tr>
				<th><?= lang('Warehouse Name') ?></th>
				<th><?= lang('Street1') ?></th>
				<th><?= lang('Street2') ?></th>
				<th><?= lang('City') ?></th>
				<th><?= lang('State') ?></th>
				<th><?= lang('Zip Code') ?></th>
				<th><?= lang('Phone') ?></th>
				<th><?= lang('Email') ?></th>
			</tr>
        </thead>
        <tbody>

        <script type="text/javascript">
            $(document).ready(function () {
                list = base_url + "admin/warehouse_locations/warehouseList";
            });
        </script>
        </tbody>
    </table>
</div>