<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Payroll extends Staff_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('payroll_model');
    }

    

    public function payslips()
    {
	
		$user_id = $this->session->userdata('user_id');
		$data['breadcrumbs'] = lang('Payslips');
        $data['title'] = lang('Payslips');
		//$employee_info = array();
		$this->payroll_model->_table_name = 'tbl_designations';
        $this->payroll_model->_order_by = 'designations_id';
		$designation_info = $this->payroll_model->get();
		
		$data['employee_info'] = $this->payroll_model->get_emp_salary_list($user_id);
					
				
		//echo "<pre>"; print_r($data['employee_info']); exit;
		
       // $data['all_bank_info'] = $this->db->where('user_id', $user_id)->get('tbl_employee_bank')->result();
		$data['subview'] = $this->load->view('staff/payslip', $data, TRUE);
        $this->load->view('staff/_layout_main', $data);
    }
	
	public function pay_slip_pdf($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {
		// check existing_recept_no by where
        $where = array('salary_payment_id' => $salary_payment_id);
        $check_existing_recipt_no = $this->payroll_model->check_by($where, 'tbl_salary_payslip');

        if (!empty($check_existing_recipt_no)) {
            $data['payslip_number'] = $check_existing_recipt_no->payslip_number;
        } else {
            $this->payroll_model->_table_name = "tbl_salary_payslip"; //table name
            $this->payroll_model->_primary_key = "payslip_id";
            $payslip_id = $this->payroll_model->save($where);

            $pdata['payslip_number'] = date('Ym') . $payslip_id;
            $this->payroll_model->save($pdata, $payslip_id);
		}
		
        $data['title'] = lang('generate_payslip');
        $data['employee_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);
		$data['company_name'] = config_item('company_name');
		$data['company_address'] = config_item('company_address');
		
		
		//echo "<pre>"; print_r($data['employee_salary_info']); exit;
		
		$this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		$this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['basic_payment_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		if($data['basic_payment_info'][0]->salary_type == 'wage') {
			//hours information
			$this->payroll_model->_table_name = "tbl_employee_hours"; // table name
			$this->payroll_model->_order_by = "id"; // $id
			$data['employee_hours_info'] = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id, 'payment_number' => $data['employee_salary_info']->payment_number), FALSE);
		}
		//echo "<pre>"; print_r($data['employee_hours_info']); exit;
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}

        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//taxes information
		$this->payroll_model->_table_name = "tbl_salary_payment_tax"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['taxes_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//company contribution information
		$this->payroll_model->_table_name = "tbl_salary_payment_contribution"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['contribution_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//benefits information
		$this->payroll_model->_table_name = "tbl_salary_payment_benifit"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['benefits_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		if($data['basic_payment_info'][0]->salary_type == 'wage') {
			$viewfile = $this->load->view('staff/payslip_info_pdf', $data, TRUE);
		} else {
			$viewfile = $this->load->view('staff/payslip_info_salaried_pdf', $data, TRUE);
		}

        $this->load->helper('dompdf');
        pdf_create($viewfile, slug_it(lang('salary_details') . '- ' . $data['employee_salary_info']->fullname));

    }
}
