<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends Staff_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('payroll_model');
    }

    public function bank_details()
    {
		$user_id = $this->session->userdata('user_id');
		$data['breadcrumbs'] = lang('Bank details');
        $data['title'] = lang('Bank details');
		
        $data['all_bank_info'] = $this->db->where('user_id', $user_id)->get('tbl_employee_bank')->result();
		$data['subview'] = $this->load->view('staff/bank_details', $data, TRUE);
        $this->load->view('staff/_layout_main', $data);
    }
	
	public function new_bank($user_id, $bank_id = null)
    {
        $data['title'] = lang('new_bank');

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        if (!empty($bank_id)) {
            $data['bank_info'] = $this->db->where('employee_bank_id', $bank_id)->get('tbl_employee_bank')->row();
        }
        $data['modal_subview'] = $this->load->view('staff/new_bank', $data, FALSE);
        $this->load->view('staff/_layout_modal', $data);
    }
	
	public function update_bank_info($user_id, $bank_id = null)
    {
			$bank_data = $this->user_model->array_from_post(array('bank_name', 'routing_number', 'account_name', 'account_number', 'type_of_account'));
            $bank_data['user_id'] = $user_id;
            $this->user_model->_table_name = "tbl_employee_bank"; // table name
            $this->user_model->_primary_key = "employee_bank_id"; // $id

            if (!empty($bank_id)) {
                $activity = 'activity_update_user_bank';
                $msg = lang('update_bank_info');
                $this->user_model->save($bank_data, $bank_id);
            } else {
                $activity = 'activity_new_user_bank';
                $msg = lang('save_bank_info');
                $bank_id = $this->user_model->save($bank_data);
            }
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $bank_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->user_model->_table_name = 'tbl_activities';
            $this->user_model->_primary_key = "activities_id";
            $this->user_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('staff/user/user_details/' . $profile_info->user_id . '/' . '2'); //redirect page
        
    }
	
	public function delete_user_bank($user_id, $bank_id)
    {
        $this->user_model->_table_name = "tbl_employee_bank"; // table name
        $this->user_model->_primary_key = "employee_bank_id"; // $id
        $this->user_model->delete($bank_id);

        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $bank_id,
            'activity' => 'activity_delete_user_bank',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);
        $type = 'success';
        $msg = lang('delete_user_bank');
        set_message($type, $msg);
        redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '2');
    }

    public function payslips()
    {
		$user_id = $this->session->userdata('user_id');
		$data['breadcrumbs'] = lang('Payslips');
        $data['title'] = lang('Payslips');
		//$employee_info = array();
		$this->payroll_model->_table_name = 'tbl_designations';
        $this->payroll_model->_order_by = 'designations_id';
		$designation_info = $this->payroll_model->get();
		
		$data['employee_info'] = $this->payroll_model->get_emp_salary_list($user_id);
					
				
		//echo "<pre>"; print_r($employee_info); exit;
		
       // $data['all_bank_info'] = $this->db->where('user_id', $user_id)->get('tbl_employee_bank')->result();
		$data['subview'] = $this->load->view('staff/payslip', $data, TRUE);
        $this->load->view('staff/_layout_main', $data);
    }
	
	public function pay_slip_pdf($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {
		// check existing_recept_no by where
        $where = array('salary_payment_id' => $salary_payment_id);
        $check_existing_recipt_no = $this->payroll_model->check_by($where, 'tbl_salary_payslip');

        if (!empty($check_existing_recipt_no)) {
            $data['payslip_number'] = $check_existing_recipt_no->payslip_number;
        } else {
            $this->payroll_model->_table_name = "tbl_salary_payslip"; //table name
            $this->payroll_model->_primary_key = "payslip_id";
            $payslip_id = $this->payroll_model->save($where);

            $pdata['payslip_number'] = date('Ym') . $payslip_id;
            $this->payroll_model->save($pdata, $payslip_id);
		}
		
        $data['title'] = lang('generate_payslip');
        $data['employee_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);
		$data['company_name'] = config_item('company_name');
		$data['company_address'] = config_item('company_address');
		
		
		//echo "<pre>"; print_r($data['employee_salary_info']); exit;
		
		$this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		$this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['basic_payment_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		if($data['basic_payment_info'][0]->salary_type == 'wage') {
			//hours information
			$this->payroll_model->_table_name = "tbl_employee_hours"; // table name
			$this->payroll_model->_order_by = "id"; // $id
			$data['employee_hours_info'] = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id, 'payment_number' => $data['employee_salary_info']->payment_number), FALSE);
		}
		//echo "<pre>"; print_r($data['employee_hours_info']); exit;
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}

        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//taxes information
		$this->payroll_model->_table_name = "tbl_salary_payment_tax"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['taxes_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//company contribution information
		$this->payroll_model->_table_name = "tbl_salary_payment_contribution"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['contribution_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//benefits information
		$this->payroll_model->_table_name = "tbl_salary_payment_benifit"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['benefits_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		if($data['basic_payment_info'][0]->salary_type == 'wage') {
			$viewfile = $this->load->view('staff/payslip_info_pdf', $data, TRUE);
		} else {
			$viewfile = $this->load->view('staff/payslip_info_salaried_pdf', $data, TRUE);
		}

        $this->load->helper('dompdf');
        pdf_create($viewfile, slug_it(lang('salary_details') . '- ' . $data['employee_salary_info']->fullname));

    }
	
	public function user_details($id = null, $active = null)
    {
		$id = $this->session->userdata('user_id');
        if ($id == my_id() || !empty(admin())) {
            $data['title'] = lang('user_details');
            $data['id'] = $id;
            if (!empty($active)) {
                $data['active'] = $active;
            } else {
                $data['active'] = 1;
            }
            $date = $this->input->post('date', true);
            if (!empty($date)) {
                $data['date'] = $date;
            } else {
                $data['date'] = date('Y-m');
            }
          
            // get all expense list by year and month
           

           
            // get all expense list by year and month
           $data['profile_info'] = $this->db->where('user_id', $id)->get('tbl_account_details')->row();

           $data['subview'] = $this->load->view('staff/user_details', $data, TRUE);
            $this->load->view('staff/_layout_main', $data);
        } else {
            set_message('error', lang('there_in_no_value'));
            redirect('staff/dashboard');
        }
    }
	
	 public function update_contact($update = null, $id = null)
    { 
        $data['title'] = lang('update_contact');
        $data['update'] = $update;
        $data['profile_info'] = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
		$data['user_details'] = $this->db->where('user_id', $id)->get('tbl_users')->row();
		//echo "<pre>"; print_r($data['profile_info']); exit;
        $data['modal_subview'] = $this->load->view('staff/update_contact', $data, FALSE);
        $this->load->view('staff/_layout_modal', $data);
    }
	
	/********* User home address ***********/
	
	public function new_address($user_id, $address_id = null)
    {
        $data['title'] = lang('new_address');

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        if (!empty($address_id)) {
            $data['address_info'] = $this->db->where('employee_add_id', $address_id)->get('tbl_employee_address')->row();
        }
        $data['modal_subview'] = $this->load->view('staff/new_address', $data, FALSE);
        $this->load->view('staff/_layout_modal', $data);
    }
	
	public function update_home_address($user_id, $address_id = null)
    {
			$address_data = $this->user_model->array_from_post(array('street1', 'street2', 'zipcode', 'city', 'state','country'));
            $address_data['user_id'] = $user_id;
            $this->user_model->_table_name = "tbl_employee_address"; // table name
            $this->user_model->_primary_key = "employee_add_id"; // $id

            if (!empty($address_id)) {
                $activity = 'activity_update_user_address';
                $msg = lang('update_address_info');
                $this->user_model->save($address_data, $address_id);
            } else {
                $activity = 'activity_new_user_address';
                $msg = lang('save_address_info');
                $address_id = $this->user_model->save($address_data);
            }
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $address_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->user_model->_table_name = 'tbl_activities';
            $this->user_model->_primary_key = "activities_id";
            $this->user_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('staff/user/user_details/' . $profile_info->user_id . '/' . '3'); //redirect page
       
    }
	
	public function delete_user_address($user_id, $address_id)
    {
        $this->user_model->_table_name = "tbl_employee_address"; // table name
        $this->user_model->_primary_key = "employee_add_id"; // $id
        $this->user_model->delete($address_id);

        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $address_id,
            'activity' => 'activity_delete_user_address',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);
        $type = 'success';
        $msg = lang('delete user address');
        set_message($type, $msg);
        redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '3');
    }
	
	public function update_details($id)
    {
		//update profile info
        $data = $this->user_model->array_from_post(array('fullname', 'employment_id', 'gender', 'date_of_birth', 'maratial_status', 'father_name', 'mother_name', 'phone', 'mobile', 'skype', 'present_address', 'ethnicity', 'include_us_payroll', 'passport'));

        $this->user_model->_table_name = 'tbl_account_details'; // table name
        $this->user_model->_primary_key = 'account_details_id'; // $id
        $this->user_model->save($data, $id);
		
		//update email details
		$data = $this->user_model->array_from_post(array('email', 'alternative_email', 'company_email'));
		$this->user_model->_table_name = 'tbl_users'; // table name
        $this->user_model->_primary_key = 'user_id'; // $id
        $this->user_model->save($data, $id);

        $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $id,
            'activity' => 'activity_update_user',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->user_model->_table_name = 'tbl_activities';
        $this->user_model->_primary_key = "activities_id";
        $this->user_model->save($activities);

        $message = lang('update_user_info');
        $type = 'success';
        set_message($type, $message);
        redirect('staff/user/user_details/' . $profile_info->user_id); //redirect page
    }
	
	public function user_documents($id)
    {
        $data['title'] = lang('update_documents');
        $data['profile_info'] = $this->db->where('user_id', $id)->get('tbl_account_details')->row();
        $data['document_info'] = $this->db->where('user_id', $id)->get('tbl_employee_document')->row();
        $data['modal_subview'] = $this->load->view('staff/user_documents', $data, FALSE);
        $this->load->view('staff/_layout_modal', $data);
    }
	
	public function update_documents($id)
    {
       
            $profile_info = $this->db->where('account_details_id', $id)->get('tbl_account_details')->row();
            // ** Employee Document Information Save and Update Start  **
            // Resume File upload

            if (!empty($_FILES['resume']['name'])) {
                $old_path = $this->input->post('resume_path', true);
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->user_model->uploadAllType('resume');
                $val == TRUE || redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '4');
                $document_data['resume_filename'] = $val['fileName'];
                $document_data['resume'] = $val['path'];
                $document_data['resume_path'] = $val['fullPath'];
            }
            // offer_letter File upload
            if (!empty($_FILES['offer_letter']['name'])) {
                $old_path = $this->input->post('offer_letter_path', true);
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->user_model->uploadAllType('offer_letter');
                $val == TRUE || redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '4');
                $document_data['offer_letter_filename'] = $val['fileName'];
                $document_data['offer_letter'] = $val['path'];
                $document_data['offer_letter_path'] = $val['fullPath'];
            }
            // joining_letter File upload
            if (!empty($_FILES['joining_letter']['name'])) {
                $old_path = $this->input->post('joining_letter_path', true);
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->user_model->uploadAllType('joining_letter');
                $val == TRUE || redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '4');
                $document_data['joining_letter_filename'] = $val['fileName'];
                $document_data['joining_letter'] = $val['path'];
                $document_data['joining_letter_path'] = $val['fullPath'];
            }

            // contract_paper File upload
            if (!empty($_FILES['contract_paper']['name'])) {
                $old_path = $this->input->post('contract_paper_path', true);
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->user_model->uploadAllType('contract_paper');
                $val == TRUE || redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '4');
                $document_data['contract_paper_filename'] = $val['fileName'];
                $document_data['contract_paper'] = $val['path'];
                $document_data['contract_paper_path'] = $val['fullPath'];
            }
            // id_proff File upload
            if (!empty($_FILES['id_proff']['name'])) {
                $old_path = $this->input->post('id_proff_path', true);
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->user_model->uploadAllType('id_proff');
                $val == TRUE || redirect('staff/user/user_details/' . $profile_info->user_id. '/' . '4');
                $document_data['id_proff_filename'] = $val['fileName'];
                $document_data['id_proff'] = $val['path'];
                $document_data['id_proff_path'] = $val['fullPath'];
            }

            $fileName = $this->input->post('fileName', true);
            $path = $this->input->post('path', true);
            $fullPath = $this->input->post('fullPath', true);
            $size = $this->input->post('size', true);
            $is_image = $this->input->post('is_image', true);

            if (!empty($fileName)) {
                foreach ($fileName as $key => $name) {
                    $old['fileName'] = $name;
                    $old['path'] = $path[$key];
                    $old['fullPath'] = $fullPath[$key];
                    $old['size'] = $size[$key];
                    $old['is_image'] = $is_image[$key];
                    $result[] = $old;
                }

                $document_data['other_document'] = json_encode($result);
            }

            if (!empty($_FILES['other_document']['name']['0'])) {
                $old_path_info = $this->input->post('upload_path');
                if (!empty($old_path_info)) {
                    foreach ($old_path_info as $old_path) {
                        unlink($old_path);
                    }
                }
                $mul_val = $this->user_model->multi_uploadAllType('other_document');
                $document_data['other_document'] = json_encode($mul_val);
            }

            if (!empty($result) && !empty($mul_val)) {
                $file = array_merge($result, $mul_val);
                $document_data['other_document'] = json_encode($file);
            }

            $document_data['user_id'] = $profile_info->user_id;

            $this->user_model->_table_name = "tbl_employee_document"; // table name
            $this->user_model->_primary_key = "document_id"; // $id
            $document_id = $this->input->post('document_id', TRUE);
            if (!empty($document_id)) {
                $this->user_model->save($document_data, $document_id);
            } else {
                $this->user_model->save($document_data);
            }

            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $id,
                'activity' => 'activity_documents_update',
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->user_model->_table_name = 'tbl_activities';
            $this->user_model->_primary_key = "activities_id";
            $this->user_model->save($activities);

            $message = lang('emplyee_documents_update');
            $type = 'success';
            set_message($type, $message);
            redirect('staff/user/user_details/' . $profile_info->user_id . '/' . '4'); //redirect page
       
    }

    
}
