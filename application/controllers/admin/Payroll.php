<?php

class Payroll extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payroll_model');
    }

    public function salary_template($id = null)
    {
		
        $data['title'] = lang('salary_template_details');

        if (!empty($id)) {

            $this->payroll_model->_table_name = "tbl_salary_template"; // table name
            $this->payroll_model->_order_by = "salary_template_id"; // $id
            $data['salary_template_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), TRUE);

            // get salary allowance info by  salary template id
            $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
            $this->payroll_model->_order_by = "salary_allowance_id"; // $id
            $data['salary_allowance_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

			
            //echo"<pre>"; print_r($data); die;
			
            $data['active'] = 2;
        } else {
            $data['active'] = 1;
        }
        $data['subview'] = $this->load->view('admin/payroll/salary_template', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function salary_templateList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_salary_template';
            $this->datatables->column_order = array('salary_grade', 'basic_salary', 'overtime_salary');
            $this->datatables->column_search = array('salary_grade', 'basic_salary', 'overtime_salary');
            $this->datatables->order = array('salary_template_id' => 'desc');

            $fetch_data = make_datatables();

            $edited = can_action('94', 'edited');
            $deleted = can_action('94', 'deleted');

            $data = array();
            foreach ($fetch_data as $_key => $v_salary_info) {

                $action = null;
                $sub_array = array();
                $sub_array[] = $_key + 1;

                $title = null;
                $title .= '<a data-toggle="modal" data-target="#myModal_lg" class="text-info" href="' . base_url() . 'admin/payroll/salary_template_details/' . $v_salary_info->salary_template_id . '">' . $v_salary_info->salary_grade . '</a>';
                $sub_array[] = $title;

                $sub_array[] = display_money($v_salary_info->basic_salary, default_currency());

                if (!empty($v_salary_info->overtime_salary)) {
                    $overtime_salary = $v_salary_info->overtime_salary;
                } else {
                    $overtime_salary = 0;
                }
               // $sub_array[] = display_money($overtime_salary, default_currency());

                $action .= btn_view_modal('admin/payroll/salary_template_details/' . $v_salary_info->salary_template_id) . ' ';
                if (!empty($edited)) {
                    $action .= btn_edit('admin/payroll/salary_template/' . $v_salary_info->salary_template_id) . ' ';
                }
                if (!empty($deleted)) {
                    $action .= ajax_anchor(base_url('admin/payroll/delete_salary_template/' . $v_salary_info->salary_template_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';
                }
                $sub_array[] = $action;
                $data[] = $sub_array;

            }

            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function set_salary_details($id = NULL)
    {

		//echo"<pre>"; print_r($_POST); die;
        $created = can_action('94', 'created');
        $edited = can_action('94', 'edited');
        if (!empty($created) || !empty($edited) && !empty($id)) {
// inout data to salate template
            $template_data = $this->payroll_model->array_from_post(array('salary_grade', 'basic_salary', 'overtime_salary'));
			
// ************* Save into tbl_salary_template *************
            $this->payroll_model->_table_name = "tbl_salary_template"; // table name
            $this->payroll_model->_primary_key = "salary_template_id"; // $id
            $salary_template_id = $this->payroll_model->save($template_data, $id);
            //echo"<pre>"; print_r($salary_template_id); die;
            // inout data salary_allowance information
            // Input data defualt salary_allowance
            $allowance_label = $this->input->post('allowance_label', TRUE);
            $allowance_value = $this->input->post('allowance_value', TRUE);
            
            // check defualt salary_allowance empty or not
			$asalary_allowance_data = array();
            if (!empty($allowance_label)) {
				$k = 0;
				foreach($allowance_label as $dataVal){
                   $asalary_allowance_data['allowance_label'][] = $dataVal;
                   $asalary_allowance_data['allowance_value'][] = $allowance_value[$k];
				$k++;
				}
            }
           
			 //echo "<pre>"; print_r($asalary_allowance_data); die;

// check salary_allowance data empty or not 
// if not empty then save into table
            if (!empty($asalary_allowance_data['allowance_label'])) {
                foreach ($asalary_allowance_data['allowance_label'] as $hkey => $h_salary_allowance_label) {
					
                    $alsalary_allowance_data['salary_template_id'] = $salary_template_id;
                    $alsalary_allowance_data['allowance_label'] = $h_salary_allowance_label;
                    $alsalary_allowance_data['allowance_value'] = $asalary_allowance_data['allowance_value'][$hkey];
                     //echo "<pre>"; print_r($alsalary_allowance_data); die;
// *********** save defualt value into tbl_salary_allowance    *******************
                    $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
                    $this->payroll_model->_primary_key = "salary_allowance_id"; // $id
                    $this->payroll_model->save($alsalary_allowance_data);
                }
            }
// save add more value into tbl_salary_allowance
           /*  $salary_allowance_label = $this->input->post('allowance_label', TRUE);
            $salary_allowance_value = $this->input->post('allowance_value', TRUE);
// input id for update
            $salary_allowance_id = $this->input->post('salary_allowance_id', TRUE);
			
            if (!empty($salary_allowance_label)) {
                foreach ($salary_allowance_label as $key => $v_salary_allowance_label) {
                    if (!empty($salary_allowance_value[$key])) {
                        $salary_allowance_data['salary_template_id'] = $salary_template_id;
                        $salary_allowance_data['allowance_label'] = $v_salary_allowance_label;
                        $salary_allowance_data['allowance_value'] = $salary_allowance_value[$key];
// *********** save add more value into tbl_salary_allowance    *******************
                        $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
                        $this->payroll_model->_primary_key = "salary_allowance_id"; // $id
                        if (!empty($salary_allowance_id[$key])) {
                            $allowance_id = $salary_allowance_id[$key];
                            $this->payroll_model->save($salary_allowance_data, $allowance_id);
                        } else {
                            $this->payroll_model->save($salary_allowance_data);
                        }
                    }
                }
            } */
// inout data Deduction information
// Input data defualt salary_allowance
           /*  $provident_fund = $this->input->post('provident_fund', TRUE);
            $tax_deduction = $this->input->post('tax_deduction', TRUE);
// check defualt Deduction empty or not
            if (!empty($provident_fund)) {
                $ddeduction_data['deduction_label'][] = lang('provident_fund');
                $ddeduction_data['deduction_value'][] = $provident_fund;
            }
            if (!empty($tax_deduction)) {
                $ddeduction_data['deduction_label'][] = lang('tax_deduction');
                $ddeduction_data['deduction_value'][] = $tax_deduction;
            }
            if (!empty($ddeduction_data['deduction_label'])) {
                foreach ($ddeduction_data['deduction_label'] as $dkey => $d_deduction_label) {
                    $adeduction_data['salary_template_id'] = $salary_template_id;
                    $adeduction_data['deduction_label'] = $d_deduction_label;
                    $adeduction_data['deduction_value'] = $ddeduction_data['deduction_value'][$dkey];

// *********** save defualt value into tbl_salary_allowance    *******************
                    $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
                    $this->payroll_model->_primary_key = "salary_deduction_id"; // $id
                    $this->payroll_model->save($adeduction_data);
                }
            } */
// check Deduction data empty or not
// if not empty then save into table

// input salary deduction id for update
           /*  $salary_deduction_id = $this->input->post('salary_deduction_id', TRUE);
// save add more value into tbl_deduction
            $deduction_label = $this->input->post('deduction_label', TRUE);
            $deduction_value = $this->input->post('deduction_value', TRUE);
            if (!empty($deduction_label)) {


                foreach ($deduction_label as $key => $v_deduction_label) {
                    if (!empty($deduction_value[$key])) {

                        $deduction_data['salary_template_id'] = $salary_template_id;
                        $deduction_data['deduction_label'] = $v_deduction_label;
                        $deduction_data['deduction_value'] = $deduction_value[$key];
// *********** save add more value into tbl_deductio    *******************
                        $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
                        $this->payroll_model->_primary_key = "salary_deduction_id"; // $id

                        if (!empty($salary_deduction_id[$key])) {
                            $deduction_id = $salary_deduction_id[$key];
                            $this->payroll_model->save($deduction_data, $deduction_id);
                        } else {
                            $this->payroll_model->save($deduction_data);
                        }
                    }
                }
            } */
			//Tax deduction
			
			/* $salary_tax_id = $this->input->post('salary_tax_id', TRUE);
			$tax_label = $this->input->post('tax_label', TRUE);
            $tax_value = $this->input->post('tax_value', TRUE);
            if (!empty($tax_label)) {


                foreach ($tax_label as $key => $v_tax_label) {
                    if (!empty($tax_value[$key])) {

                        $tax_data['salary_template_id'] = $salary_template_id;
                        $tax_data['tax_label'] = $v_tax_label;
                        $tax_data['tax_value'] = $tax_value[$key];
                        // *********** save add more value into tbl_deductio    *******************
                        $this->payroll_model->_table_name = "tbl_salary_taxes"; // table name
                        $this->payroll_model->_primary_key = "salary_tax_id"; // $id

                        if (!empty($salary_tax_id[$key])) {
                            $tax_id = $salary_deduction_id[$key];
                            $this->payroll_model->save($tax_data, $salary_tax_id);
                        } else {
                            $this->payroll_model->save($tax_data);
                        }
                    }
                }
            } */
			
			
			
			
			
            if (!empty($id)) {
                $activity = 'activity_salary_template_update';
                $msg = lang('salary_template_update');
            } else {
                $activity = 'activity_salary_template_added';
                $msg = lang('salary_template_added');
            }
            // save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'payroll',
                'module_field_id' => $id,
                'activity' => $activity,
                'icon' => 'fa-money',
                'link' => 'admin/payroll/view_salary_template/' . $salary_template_id,
                'value1' => $template_data['salary_grade'],
            );
            // Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            $message = $msg;
            set_message($type, $message);
        }
        redirect('admin/payroll/salary_template');
    }

    public function view_salary_template($id)
    {
        $data['title'] = lang('total_salary_details');
// get salary_template_info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_template"; // table name
        $this->payroll_model->_order_by = "salary_template_id"; // $id
        $data['salary_template_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), TRUE);

// get salary allowance info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
        $this->payroll_model->_order_by = "salary_allowance_id"; // $id
        $data['salary_allowance_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

// get salary deduction info by salary template id
        $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
        $this->payroll_model->_order_by = "salary_deduction_id"; // $id
        $data['salary_deduction_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

        $data['subview'] = $this->load->view('admin/payroll/salary_template_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function salary_template_details($id)
    {
        $data['title'] = lang('total_salary_details');
// get salary_template_info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_template"; // table name
        $this->payroll_model->_order_by = "salary_template_id"; // $id
        $data['salary_template_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), TRUE);

// get salary allowance info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
        $this->payroll_model->_order_by = "salary_allowance_id"; // $id
        $data['salary_allowance_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

// get salary deduction info by salary template id
        $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
        $this->payroll_model->_order_by = "salary_deduction_id"; // $id
        $data['salary_deduction_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

        $data['subview'] = $this->load->view('admin/payroll/salary_template_details', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function salary_template_pdf($id)
    {
        $data['title'] = lang('total_salary_details');
// get salary_template_info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_template"; // table name
        $this->payroll_model->_order_by = "salary_template_id"; // $id
        $data['salary_template_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), TRUE);

// get salary allowance info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
        $this->payroll_model->_order_by = "salary_allowance_id"; // $id
        $data['salary_allowance_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

// get salary deduction info by salary template id
        $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
        $this->payroll_model->_order_by = "salary_deduction_id"; // $id
        $data['salary_deduction_info'] = $this->payroll_model->get_by(array('salary_template_id' => $id), FALSE);

        $viewfile = $this->load->view('admin/payroll/salary_template_pdf', $data, TRUE);
        $this->load->helper('dompdf');
        pdf_create($viewfile, slug_it(lang('salary_template') . '-' . $data['salary_template_info']->salary_grade));
    }

    public function delete_salary_template($id)
    {
        $deleted = can_action('94', 'deleted');
        if (!empty($deleted)) {
            // check into employee payroll table
            // if is exist then do not delete this else delete the id
            $salary_template_info = $this->payroll_model->check_by(array('salary_template_id' => $id), 'tbl_salary_template');

            $check_existing_template = $this->payroll_model->check_by(array('salary_template_id' => $id), 'tbl_employee_payroll');
            if (!empty($check_existing_template)) {
                $type = 'error';
                $message = lang('salary_template_already_used');
            } else {
                // *********** Delete into tbl_salary_template *******************
                $this->payroll_model->_table_name = "tbl_salary_template"; // table name
                $this->payroll_model->_primary_key = "salary_template_id"; // $id
                $this->payroll_model->delete($id);
                // *********** Delete into tbl_salary_allowance *******************
                $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
                $this->payroll_model->delete_multiple(array('salary_template_id' => $id));

                // *********** Delete into tbl_salary_deduction *******************
                $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
                $this->payroll_model->delete_multiple(array('salary_template_id' => $id));

                $type = 'success';
                $message = lang('salary_template_deleted');

                // save into activities
                $activities = array(
                    'user' => $this->session->userdata('user_id'),
                    'module' => 'payroll',
                    'module_field_id' => $id,
                    'activity' => 'activity_salary_template_delete',
                    'icon' => 'fa-money',
                    'value1' => $salary_template_info->salary_grade,
                    'value2' => $salary_template_info->basic_salary,
                );
                // Update into tbl_project
                $this->payroll_model->_table_name = "tbl_activities"; //table name
                $this->payroll_model->_primary_key = "activities_id";
                $this->payroll_model->save($activities);
            }

            set_message($type, $message);
        }
        redirect('admin/payroll/salary_template');
    }

    public function hourly_rate($id = NULL)
    {
        $data['title'] = lang('hourly_rate');

        if (!empty($id)) {
// get salary template deatails
            $this->payroll_model->_table_name = "tbl_hourly_rate"; // table name
            $this->payroll_model->_order_by = "hourly_rate_id"; // $id
            $data['hourly_rate'] = $this->payroll_model->get_by(array('hourly_rate_id' => $id), TRUE);

            $data['active'] = 2;
        } else {
            $data['active'] = 1;
        }

        $data['subview'] = $this->load->view('admin/payroll/hourly_rate', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function hourly_rateList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_hourly_rate';
            $this->datatables->column_order = array('hourly_grade', 'hourly_rate', 'ptohours_rate', 'overtimehours_rate');
            $this->datatables->column_search = array('hourly_grade', 'hourly_rate', 'ptohours_rate', 'overtimehours_rate');
            $this->datatables->order = array('hourly_rate_id' => 'desc');

            $fetch_data = make_datatables();

            $edited = can_action('95', 'edited');
            $deleted = can_action('95', 'deleted');

            $data = array();
            foreach ($fetch_data as $_key => $v_hourly_rate) {

                $action = null;
                $sub_array = array();
                $sub_array[] = $_key + 1;

                $sub_array[] = $v_hourly_rate->hourly_grade;

                $sub_array[] = display_money($v_hourly_rate->hourly_rate, default_currency());
				
				$sub_array[] = display_money($v_hourly_rate->ptohours_rate, default_currency());
				
				$sub_array[] = display_money($v_hourly_rate->overtimehours_rate, default_currency());

                if (!empty($edited)) {
                    $action .= btn_edit('admin/payroll/hourly_rate/' . $v_hourly_rate->hourly_rate_id) . ' ';
                }
                if (!empty($deleted)) {
                    $action .= anchor(base_url('admin/payroll/delete_hourly_rate/' . $v_hourly_rate->hourly_rate_id), "<i class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "onclick"=>"return confirm('You are about to delete a record. This cannot be undone. Are you sure?')", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';
                }
                $sub_array[] = $action;
                $data[] = $sub_array;

            }

            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function set_hourly_rate($id = null)
    {
        $created = can_action('95', 'created');
        $edited = can_action('95', 'edited');
        if (!empty($created) || !empty($edited) && !empty($id)) {
            $data = $this->payroll_model->array_from_post(array('hourly_grade', 'hourly_rate', 'ptohours_rate', 'overtimehours_rate'));
            $where = array('hourly_grade' => $data['hourly_grade']);
// duplicate value check in DB
            if (!empty($id)) { // if id exist in db update data
                $hourly_rate_id = array('hourly_rate_id !=' => $id);
                $activity = 'activity_hourly_template_update';
                $msg = lang('hourly_template_update');
            } else { // if id is not exist then set id as null
                $hourly_rate_id = null;
                $activity = 'activity_hourly_template_added';
                $msg = lang('hourly_template_added');
            }
// check whether this input data already exist or not
            $check_hourly_rate = $this->payroll_model->check_update('tbl_hourly_rate', $where, $hourly_rate_id);
            if (!empty($check_hourly_rate)) { // if input data already exist show error alert
// massage for user
                $type = 'error';
                $message = lang('hourly_template_already_exist');
            } else {
                $this->payroll_model->_table_name = 'tbl_hourly_rate';
                $this->payroll_model->_primary_key = 'hourly_rate_id';
                $this->payroll_model->save($data, $id);

                $type = 'success';
                $message = $msg;
            }
            // save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'payroll',
                'module_field_id' => $id,
                'activity' => $activity,
                'icon' => 'fa-money',
                'value1' => $data['hourly_grade'],
                'value2' => $data['hourly_rate'],
            );
            // Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);
            set_message($type, $message);
        }
        redirect('admin/payroll/hourly_rate');
    }

    public function delete_hourly_rate($id)
    {
        $deleted = can_action('95', 'deleted');
        if (!empty($deleted)) {
            // check into employee payroll table
            // if is exist then do not delete this else delete the id
            $hourly_template = $this->payroll_model->check_by(array('hourly_rate_id' => $id), 'tbl_hourly_rate');

            $check_existing_template = $this->payroll_model->check_by(array('hourly_rate_id' => $id), 'tbl_employee_payroll');
            if (!empty($check_existing_template)) {
                $type = 'error';
                $message = lang('hourly_template_already_exist');
            } else {
                $this->payroll_model->_table_name = 'tbl_hourly_rate';
                $this->payroll_model->_primary_key = 'hourly_rate_id';
                $this->payroll_model->delete($id);

                $type = 'success';
                $message = lang('hourly_template_deleted');
            }
            // save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'payroll',
                'module_field_id' => $id,
                'activity' => 'activity_hourly_template_deleted',
                'icon' => 'fa-money',
                'value1' => $hourly_template->hourly_grade,
                'value2' => $hourly_template->hourly_rate,
            );
            // Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            set_message($type, $message);
        }
        redirect('admin/payroll/hourly_rate');
    }
	
     public function manage_salary_genInfo($payroll_id=NULL){	
		
		//echo"<pre>"; print_r($_POST); die;
        $user_id = $this->input->post('user_id', TRUE);
        $payroll_id = $this->input->post('payroll_id', TRUE);
        $user_payType = $this->input->post('user_payType', TRUE);
        $hourly_rate_id = $this->input->post('hourly_rate_id', TRUE);

        $salary_template_id = $this->input->post('monthly_template_id', TRUE);
        $payfrequency = $this->input->post('payfrequency', TRUE);
		
		//echo"<pre>"; print_r($salaryData); die;
		  if($user_payType=='salary'){
            $data['user_id'] = $user_id;
            $data['monthly_template_id'] = $salary_template_id;
            $data['hourly_rate_id'] = 0;			
            $data['user_payType'] = $user_payType;
            $data['payfrequency'] = $payfrequency;
		  }else{
			$data['user_id'] = $user_id;
            $data['monthly_template_id'] = 0;
            $data['hourly_rate_id'] = $hourly_rate_id;			
            $data['user_payType'] = $user_payType;
            $data['payfrequency'] = $payfrequency;
			  
		  }
			
		  if($user_payType=='salary'){
			  
			 $basicEarning = $this->db->where('salary_template_id', $salary_template_id)->get('tbl_salary_template')->result();  
			  //echo"<pre>"; print_r($basicEarning); die;
			  foreach($basicEarning as $earning){
        				  
			     $checkExit = $this->db->where('user_id', $user_id)->where('earning_type',"basic salary")->get('tbl_employee_salary_earnings')->result();
			     if(empty($checkExit)){
					//echo"fdgdf"; die;
				    $earningData = array(
						'user_id' => $user_id,
						'earning_type' => "basic salary",
						'earning_amount	' =>$earning->basic_salary,
						'salary_template_id' =>$salary_template_id,
						'effective_date' => date('Y-m-d'),
						'allowance_type'=>'salary',
						'excludeFrTx'=>'no'
					);
					
					$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
					$this->payroll_model->_primary_key = "earning_id";
					$this->payroll_model->save($earningData);	
				   }else{
					   //echo"346345"; die;
					  $earningData = array(
							'user_id' => $user_id,
							'earning_type' => "basic salary",
							'earning_amount	' =>$earning->basic_salary,
							'salary_template_id' =>$salary_template_id,
							'effective_date' => date('Y-m-d'),
							'allowance_type'=>'salary',
						    'excludeFrTx'=>'no'
						);
						
					$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
					$this->payroll_model->_primary_key = "earning_id";
					$this->payroll_model->save($earningData,$checkExit[0]->earning_id); 
					   
				   }
			  }
           
		   $defaultEarning = $this->db->where('salary_template_id', $salary_template_id)->get('tbl_salary_allowance')->result();
		   
			foreach($defaultEarning as $deVal){				
			 $checkExit = $this->db->where('user_id', $user_id)->where('earning_type', $deVal->allowance_label)->get('tbl_employee_salary_earnings')->result();
				//echo"<pre>"; print_r($checkExit);
				if(empty($checkExit)){
					//echo"fdgdf"; die;
				$earningData = array(
						'user_id' => $user_id,
						'earning_type' => $deVal->allowance_label,
						'earning_amount	' =>$deVal->allowance_value,
						'salary_template_id' =>$salary_template_id,
						'effective_date' => date('Y-m-d'),
						'allowance_type'=>'salary'
					);
					
				$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
				$this->payroll_model->_primary_key = "earning_id";
				$this->payroll_model->save($earningData);	
			   }else{
				   //echo"346345"; die;
				  $earningData = array(
						'user_id' => $user_id,
						'earning_type' => $deVal->allowance_label,
						'earning_amount	' =>$deVal->allowance_value,
						'salary_template_id' =>$salary_template_id,
						'effective_date' => date('Y-m-d'),
						'allowance_type'=>'salary'
					);
					
				$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
				$this->payroll_model->_primary_key = "earning_id";
				$this->payroll_model->save($earningData,$checkExit[0]->earning_id); 
				   
			   }
			   
			   
			}
			
				
		   //Delete wage
		   $allData = $this->db->where('user_id', $user_id)->where('allowance_type', 'wage')->get('tbl_employee_salary_earnings')->result();
		    foreach($allData as $data_val){
			  $erID = $data_val->earning_id;
			  $this->payroll_model->_table_name = "tbl_employee_salary_earnings"; // table name
			  $this->payroll_model->_primary_key = "earning_id"; // $id
			  $this->payroll_model->delete($erID);
		    }
			
		  }else{
			  
          //Wage			  
		  $allData = $this->db->where('user_id', $user_id)->where('allowance_type', 'salary')->get('tbl_employee_salary_earnings')->result();
		    foreach($allData as $data_val){
			  $erID = $data_val->earning_id;
			  $this->payroll_model->_table_name = "tbl_employee_salary_earnings"; // table name
			  $this->payroll_model->_primary_key = "earning_id"; // $id
			  $this->payroll_model->delete($erID);
		    }
			
            $defaultHourlyrate = $this->db->where('hourly_rate_id', $hourly_rate_id)->get('tbl_hourly_rate')->result();
		    $arr = array("Regular hours","PTO hours","Over time hours");
			$i =0;
			$hourlyArr = array(); 
			foreach($defaultHourlyrate as $deVal){
			$hourlyArr[] = $deVal->hourly_rate;
			$hourlyArr[] = $deVal->ptohours_rate;
			$hourlyArr[] = $deVal->overtimehours_rate;
			}
			
			foreach($hourlyArr as $deVal){	
			//echo"<pre>"; print_r($hourlyArr); die;
			 $checkExit = $this->db->where('user_id', $user_id)->where('earning_type', $arr[$i])->get('tbl_employee_salary_earnings')->result();						
			  $hourlyAMt = $deVal;
			  
			  
				if(empty($checkExit)){
					//echo"fdgdf"; die;
				$earningData = array(
						'user_id' => $user_id,
						'earning_type' => $arr[$i],
						'earning_amount	' =>$hourlyAMt,
						'salary_template_id' =>$hourly_rate_id,
						'effective_date' => date('Y-m-d'),
						'allowance_type'=>'Wage',
						'amtType'=>'perhour'
					);
				//echo"<pre>"; print_r($earningData); die;	
				$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
				$this->payroll_model->_primary_key = "earning_id";
				$this->payroll_model->save($earningData);	
			   }else{
				   //echo"346345"; die;
				  $earningData = array(
						'user_id' => $user_id,
						'earning_type' => $arr[$i],
						'earning_amount	' =>$hourlyAMt,
						'salary_template_id' =>$hourly_rate_id,
						'effective_date' => date('Y-m-d'),
						'allowance_type'=>'Wage',
						'amtType'=>'perhour'
					);
					
				$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
				$this->payroll_model->_primary_key = "earning_id";
				$this->payroll_model->save($earningData,$checkExit[0]->earning_id); 
				   
			   }
			$i++;			   
			}
		  }		  
		   
		  //echo "<pre>"; print_r($data); die;
            // save into tbl employee payroll
            $this->payroll_model->_table_name = "tbl_user_salary_general_info"; // table name
            $this->payroll_model->_primary_key = "id"; // $id
			if (!empty($payroll_id)) {
                $id = $payroll_id;
                $this->payroll_model->save($data, $id);
            } else {
                $id = $this->payroll_model->save($data);
            }
			
			
			$salaryData = array(
			'user_id' => $user_id,
			'salary_template_id' => $salary_template_id ,
			'hourly_rate_id' =>$hourly_rate_id
			);
			$salaryArr = $this->db->where('user_id', $user_id)->get('tbl_employee_payroll')->result();
			
			if($salaryData){
				if (!empty($salaryArr)) {
				
                 $this->db->where('user_id', $user_id);					
			     $this->db->update('tbl_employee_payroll',$salaryData);
				}else{
				
				 $this->db->insert('tbl_employee_payroll',$salaryData);		
			  
				}
			}
			
                      
        
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'payroll',
            'module_field_id' => $id,
            'activity' => 'activity_user_salary_details_update',
            'icon' => 'fa-money',
            'value1' => 'Manage user salary general information ',
        );
        // Update into tbl_project
        $this->payroll_model->_table_name = "tbl_activities"; //table name
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);
        
        $type = 'success';
        $message = lang('Added user salary general information');
        set_message($type, $message);
        redirect('admin/payroll/manage_salary_info/'.$user_id);
    }
	
    public function manage_salary_details($departments_id = NULL)
    {
        $data['title'] = lang('manage_salary_details');
        // retrive all data from department table
        $data['all_department_info'] = $this->db->get('tbl_departments')->result();

        $flag = $this->input->post('flag', TRUE);
        if (!empty($flag) || !empty($departments_id)) { // check employee id is empty or not
            $data['flag'] = 1;
            if (!empty($departments_id)) {
                $data['departments_id'] = $departments_id;
            } else {
                $data['departments_id'] = $this->input->post('departments_id', TRUE);
            }
            // get all designation info by Department id
			if($data['departments_id'] == 'all') {
				$designation_info = $this->db->get('tbl_designations')->result();
			} else {
				$designation_info = $this->db->where('departments_id', $data['departments_id'])->get('tbl_designations')->result();
			}
            
            
            if (!empty($designation_info)) {
                foreach ($designation_info as $v_designatio) {
                    $data['employee_info'][] = $this->payroll_model->get_emp_info_by_id($v_designatio->designations_id);
                    $employee_info = $this->payroll_model->get_emp_info_by_id($v_designatio->designations_id);
                    foreach ($employee_info as $value) {
                        // get all salary Template info
                        $data['salary_grade_info'][] = $this->db->where('user_id', $value->user_id)->get('tbl_employee_payroll')->result();
                    }
                }
            }
            // get all Hourly payment info
            $data['hourly_grade'] = $this->db->get('tbl_hourly_rate')->result();
            // get all salary Template info
            $data['salary_grade'] = $this->db->get('tbl_salary_template')->result();
        }
        $data['subview'] = $this->load->view('admin/payroll/manage_salary_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_salary_details()
    {
		
        // inout data to salate template
        $user_id = $this->input->post('user_id', TRUE);

        $hourly_status = $this->input->post('hourly_status', TRUE);
        $hourly_rate_id = $this->input->post('hourly_rate_id', TRUE);

        $monthly_status = $this->input->post('monthly_status', TRUE);
        $salary_template_id = $this->input->post('salary_template_id', TRUE);
        $payroll_id = $this->input->post('payroll_id', TRUE);
		
        foreach ($user_id as $key => $v_emp_id) {
            $data['user_id'] = $v_emp_id;
            $data['salary_template_id'] = NULL;
            $data['hourly_rate_id'] = NULL;
            if (!empty($hourly_status)) {
                foreach ($hourly_status as $v_hourly) {
                    if ($v_emp_id == $v_hourly) {
                        $data['hourly_rate_id'] = $hourly_rate_id[$key];
                        $data['salary_template_id'] = NULL;
                    }
                }
            }
            if (!empty($monthly_status)) {
                foreach ($monthly_status as $v_monthly) {
                    if ($v_emp_id == $v_monthly) {
                        $data['salary_template_id'] = $salary_template_id[$key];
                        $data['hourly_rate_id'] = NULL;
                    }
                }
            }
            // save into tbl employee payroll
            $this->payroll_model->_table_name = "tbl_employee_payroll"; // table name
            $this->payroll_model->_primary_key = "payroll_id"; // $id
            if (!empty($payroll_id[$key])) {
                $id = $payroll_id[$key];
                $this->payroll_model->save($data, $id);
            } else {
                $id = $this->payroll_model->save($data);
            }
        }
        $departments_id = $this->input->post('departments_id', TRUE);
        $dept_info = $this->db->where('departments_id', $departments_id)->get('tbl_departments')->row();
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'payroll',
            'module_field_id' => $id,
            'activity' => 'activity_salary_details_update',
            'icon' => 'fa-money',
            'value1' => $dept_info->deptname,
        );
        // Update into tbl_project
        $this->payroll_model->_table_name = "tbl_activities"; //table name
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);

        $type = 'success';
        $message = lang('salary_details_updated');
        set_message($type, $message);
        redirect('admin/payroll/employee_salary_list');
    }

    public function delete_salary($id)
    {
        $emp_salary_info = $this->db->where('payroll_id', $id)->get('tbl_employee_payroll')->row();
        $template_name = '';
        if (!empty($emp_salary_info->salary_template_id)) {
            $template_info = $this->db->where('salary_template_id', $id)->get('tbl_salary_template')->row();
            if (!empty($template_info)) {
                $template_name = $template_info->salary_grade;
            }
        } elseif (!empty($emp_salary_info->hourly_rate_id)) {
            $hourly_template = $this->db->where('hourly_rate_id', $id)->get('tbl_hourly_rate')->row();
            if (!empty($hourly_template)) {
                $template_name = $hourly_template->hourly_grade;
            }
        }
        // save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'payroll',
            'module_field_id' => $id,
            'activity' => 'activity_delete_employee_salary_list',
            'icon' => 'fa-money',
            'value1' => $template_name,
        );
        $this->payroll_model->_table_name = "tbl_activities"; //table name
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);


        $this->payroll_model->_table_name = "tbl_employee_payroll"; // table name
        $this->payroll_model->_primary_key = "payroll_id"; // $id
        $this->payroll_model->delete($id);

        $type = "success";
        $message = lang('salary_information_deleted');
        set_message($type, $message);
        redirect('admin/payroll/employee_salary_list');
    }

    public function employee_salary_list()
    {
        $data['title'] = lang('employee_salary_details');
        $data['subview'] = $this->load->view('admin/payroll/employee_salary_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function employee_salaryList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_user_salary_general_info';
            $this->datatables->join_table = array('tbl_account_details', 'tbl_salary_template', 'tbl_hourly_rate', 'tbl_designations', 'tbl_departments');
            $this->datatables->join_where = array('tbl_user_salary_general_info.user_id = tbl_account_details.user_id', 'tbl_user_salary_general_info.monthly_template_id = tbl_salary_template.salary_template_id', 'tbl_user_salary_general_info.hourly_rate_id = tbl_hourly_rate.hourly_rate_id', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'tbl_departments.departments_id  = tbl_designations.departments_id');
            $this->datatables->column_order = array('tbl_account_details.employment_id', 'tbl_account_details.fullname', 'tbl_salary_template.salary_grade', 'tbl_salary_template.basic_salary', 'tbl_hourly_rate.hourly_grade', 'tbl_hourly_rate.hourly_rate', 'tbl_salary_template.overtime_salary');
            $this->datatables->column_search = array('tbl_account_details.employment_id', 'tbl_account_details.fullname', 'tbl_salary_template.salary_grade', 'tbl_salary_template.basic_salary', 'tbl_hourly_rate.hourly_grade', 'tbl_hourly_rate.hourly_rate', 'tbl_salary_template.overtime_salary');
            $this->datatables->order = array('id' => 'desc');

            $fetch_data = make_datatables();
			
			//echo "<pre>"; print_r($fetch_data); exit;

            $data = array();
            foreach ($fetch_data as $_key => $v_emp_salary) {

                $action = null;
                $sub_array = array();
                $sub_array[] = $v_emp_salary->employment_id;
                 if (!empty($v_emp_salary->salary_grade)) {
					$grade = $v_emp_salary->salary_grade;
				}else{
					$grade = $v_emp_salary->hourly_grade;
				}
				
                $title = null;
                if (!empty($v_emp_salary->salary_grade)) {
                    $title = '<a data-toggle="modal" data-target="#myModal_lg" class="text-info" href="' . base_url() . 'admin/payroll/view_salary_details/' . $v_emp_salary->monthly_template_id . '/' . $v_emp_salary->user_id . '">' . $v_emp_salary->fullname . '</a>';
                } elseif (!empty($v_emp_salary->hourly_grade)) {
                    $title = '<a data-toggle="modal" data-target="#myModal_lg" class="text-info" href="' . base_url() . 'admin/payroll/view_salary_details/' . $v_emp_salary->hourly_rate_id . '/' . $v_emp_salary->user_id . '">' . $v_emp_salary->fullname . '</a>';
                }else{
				   $title = $v_emp_salary->fullname;
				}
				
                $sub_array[] = $title;

                if (!empty($v_emp_salary->salary_grade)) {
                    //$grade = $v_emp_salary->salary_grade . ' <small>(' . ucfirst($v_emp_salary->payfrequency) . ')</small>';
					$grade = 'Salary';
                } else {
                    $grade = 'Hourly ';
                }
                $sub_array[] = $grade;

                if (!empty($v_emp_salary->basic_salary)) {
                    $basic_salary = display_money($v_emp_salary->basic_salary, default_currency());
                } else {
                    $basic_salary = display_money($v_emp_salary->hourly_rate, default_currency()) . ' <small>(' . lang("per_hour") . ')</small>';
                }
                /* $sub_array[] = $basic_salary;

                if (!empty($v_emp_salary->overtime_salary)) {
                    $overtime_salary = display_money($v_emp_salary->overtime_salary, default_currency());
                } else {
                    $overtime_salary = 0;
                }
                $sub_array[] = $overtime_salary;
 */              
				 
 
                if (!empty($v_emp_salary->salary_grade)) {
                    $action .= btn_view_modal('admin/payroll/view_salary_details/' . $v_emp_salary->monthly_template_id . '/' . $v_emp_salary->user_id) . ' ';
                }else{
					 $action .= btn_view_modal('admin/payroll/view_salary_details/' . $v_emp_salary->hourly_rate_id . '/' . $v_emp_salary->user_id) . ' ';
				}
              /*  
				if ($this->session->userdata('user_type') == '1') {
                    $action .= ajax_anchor(base_url('admin/payroll/delete_salary/' . $v_emp_salary->payroll_id), "<i
    class='btn btn-xs btn-danger fa fa-trash-o'></i>", array("class" => "", "title" => lang('delete'), "data-fade-out-on-success" => "#table_" . $_key)) . ' ';
                }
				*/
				
               $action .= btn_edit('admin/payroll/manage_salary_info/' . $v_emp_salary->user_id) . ' ';
		
			   
			   

                $sub_array[] = $action;
				
                $data[] = $sub_array;

            }
            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function view_salary_details($salary_template_id, $id)
    {
        if (empty($salary_template_id)) {
            $type = "error";
            $message = lang('operation_failed');
            set_message($type, $message);
            if (empty($_SERVER['HTTP_REFERER'])) {				
                redirect('admin/payroll/employee_salary_list');
            } else {				
                redirect($_SERVER['HTTP_REFERER']);
            }
			
        }
        
        $data['title'] = lang('view_salary_details');
        // get all employee salary info   by id
        $data['emp_salary_info'] = $this->payroll_model->get_emp_salary_list($id);
		//echo "<pre>"; print_r($data['emp_salary_info']); exit;

		// get salary allowance info by  salary template id
        $this->payroll_model->_table_name = "tbl_employee_salary_earnings"; // table name
        $this->payroll_model->_order_by = "earning_id"; // $id
        $data['salary_allowance_info'] = $this->payroll_model->get_by(array('salary_template_id' => $salary_template_id, 'user_id' => $id), FALSE);

		// get salary deduction info by salary template id
        $this->payroll_model->_table_name = "tbl_employee_salary_deduction"; // table name
        $this->payroll_model->_order_by = "deduction_id"; // $id
        $data['salary_deduction_info'] = $this->payroll_model->get_by(array('salary_template_id' => $salary_template_id, 'user_id' => $id), FALSE);
		
		// get salary deduction info by salary template id
        $this->payroll_model->_table_name = "tbl_employee_salary_tax"; // table name
        $this->payroll_model->_order_by = "tax_id"; // $id
        $data['salary_tax_info'] = $this->payroll_model->get_by(array('salary_template_id' => $salary_template_id, 'user_id' => $id), FALSE);
		
		
         //echo"<pre>"; print_r($data);  
        $data['subview'] = $this->load->view('admin/payroll/employee_salary_details', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function make_pdf($id)
    {
		//echo"AA gya";
		//echo"<pre>"; print_r($data); die;
        $data['title'] = lang('view_salary_details');
// get all employee salary info  by id
        $data['emp_salary_info'] = $this->payroll_model->get_emp_salary_list($id);

// get salary allowance info by  salary template id
        $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
        $this->payroll_model->_order_by = "salary_allowance_id"; // $id
        $data['salary_allowance_info'] = $this->payroll_model->get_by(array('salary_template_id' => $data['emp_salary_info']->salary_template_id), FALSE);

// get salary deduction info by salary template id
        $this->payroll_model->_table_name = "tbl_salary_deduction"; // table name
        $this->payroll_model->_order_by = "salary_deduction_id"; // $id
        $data['salary_deduction_info'] = $this->payroll_model->get_by(array('salary_template_id' => $data['emp_salary_info']->salary_template_id), FALSE);

        $viewfile = $this->load->view('admin/payroll/employee_salary_pdf', $data, TRUE);
        $this->load->helper('dompdf');
        pdf_create($viewfile, slug_it(lang('salary_details') . '- ' . $data['emp_salary_info']->fullname));
    }

    public function add_advance_salary($id = null)
    {
        // active check with current month
        $data['current_month'] = date('m');
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
        $data['all_employee'] = $this->payroll_model->get_all_employee();
        if ($id == 'true') {
            $advance_salary_id = $this->uri->segment(5);
        } else {
            $advance_salary_id = $id;
        }
        if (!empty($advance_salary_id)) {
            $data['advance_salary'] = $this->db->where('advance_salary_id', $advance_salary_id)->get('tbl_advance_salary')->row();
        }
        $data['modal_subview'] = $this->load->view('admin/payroll/add_advance_salary', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function save_advance_salary($id = null)
    {
        $data['advance_amount'] = $this->input->post('advance_amount', TRUE);
//receive form input by post
        $user_id = $this->input->post('user_id', TRUE);
        if (!empty($user_id)) {
            $data['user_id'] = $user_id;
        } else {
            $data['user_id'] = $this->session->userdata('user_id');
        }

        $this->load->model('global_model');
        $basic_salary = $this->global_model->get_advance_amount($data['user_id']);
        if (!empty($basic_salary)) {
            if ($basic_salary < $data['advance_amount']) {
// messages for user
                $type = "error";
                $message = lang('exced_basic_salary');
                set_message($type, $message);
                redirect('admin/payroll/advance_salary');
            }
        } else {
            $type = "error";
            $message = lang('you_can_not_apply');
            set_message($type, $message);
            redirect('admin/payroll/advance_salary');
        }

        $data['reason'] = $this->input->post('reason', TRUE);
        $data['deduct_month'] = $this->input->post('deduct_month', TRUE);

        if ($this->session->userdata('user_type') == 1) {
            $data['status'] = 1;
        }
//save data in database
        $this->payroll_model->_table_name = "tbl_advance_salary"; // table name
        $this->payroll_model->_primary_key = "advance_salary_id"; // $id
        $id = $this->payroll_model->save($data, $id);

// save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'payroll',
            'module_field_id' => $id,
            'activity' => 'activity_apply_advance_salary',
            'icon' => 'cc-mastercard',
            'link' => 'admin/payroll/view_advance_salary/' . $id,
            'value1' => $this->db->where('user_id', $data['user_id'])->get('tbl_account_details')->row()->fullname,
            'value2' => $data['advance_amount'],
        );

// Update into tbl_project
        $this->payroll_model->_table_name = "tbl_activities"; //table name
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);

        $advance_salary_info = $this->payroll_model->check_by(array('advance_salary_id' => $id), 'tbl_advance_salary');
        $profile_info = $this->payroll_model->check_by(array('user_id' => $advance_salary_info->user_id), 'tbl_account_details');
// send email to departments head
        if ($advance_salary_info->status == 0) {
            if (!empty($profile_info->designations_id)) {
// get departments head user id
                $designation_info = $this->payroll_model->check_by(array('designations_id' => $profile_info->designations_id), 'tbl_designations');
// get departments head by departments id
                $dept_head = $this->payroll_model->check_by(array('departments_id' => $designation_info->departments_id), 'tbl_departments');

                if (!empty($dept_head->department_head_id)) {
                    $advance_salary_email = config_item('advance_salary_email');
                    if (!empty($advance_salary_email) && $advance_salary_email == 1) {

                        $email_template = $this->payroll_model->check_by(array('email_group' => 'advance_salary_email'), 'tbl_email_templates');
                        $user_info = $this->payroll_model->check_by(array('user_id' => $dept_head->department_head_id), 'tbl_users');
                        $message = $email_template->template_body;
                        $subject = $email_template->subject;
                        $username = str_replace("{NAME}", $profile_info->fullname, $message);
                        $Link = str_replace("{LINK}", base_url() . 'admin/payroll/view_advance_salary/' . $id, $username);
                        $message = str_replace("{SITE_NAME}", config_item('company_name'), $Link);
                        $data['message'] = $message;
                        $message = $this->load->view('email_template', $data, TRUE);

                        $params['subject'] = $subject;
                        $params['message'] = $message;
                        $params['resourceed_file'] = '';
                        $params['recipient'] = $user_info->email;
                        $this->payroll_model->send_email($params);
                    }

                    $notifyUser = array($dept_head->department_head_id);
                    if (!empty($notifyUser)) {
                        foreach ($notifyUser as $v_user) {
                            add_notification(array(
                                'to_user_id' => $v_user,
                                'description' => 'not_advance_salary_request',
                                'icon' => 'cc-mastercard',
                                'link' => 'admin/payroll/view_advance_salary/' . $id,
                                'value' => lang('by') . ' ' . $this->session->userdata('name'),
                            ));
                        }
                    }
                    if (!empty($notifyUser)) {
                        show_notification($notifyUser);
                    }
                }
            }
        }
// messages for user
        $type = "success";
        $message = lang('advance_salary_supmited');
        set_message($type, $message);
        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect('admin/payroll/advance_salary');
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function advance_salary($details = null)
    {
// list view
        if (!empty($details)) {
            $data['active'] = 1;
            $data['switch'] = 1;
        }
        $data['title'] = lang('advance_salary');
// active check with current month
        $data['current_month'] = date('m');
        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
// get all expense list by year and month
        $data['advance_salary_info'] = $this->get_advance_salary_info($data['year']);

        $data['subview'] = $this->load->view('admin/payroll/advance_salary', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function my_advance_salaryList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_advance_salary';
            $this->datatables->join_table = array('tbl_account_details');
            $this->datatables->join_where = array('tbl_account_details.user_id=tbl_advance_salary.user_id');
            $this->datatables->column_order = array('tbl_account_details.employment_id', 'tbl_account_details.fullname', 'advance_amount', 'deduct_month', 'request_date');
            $this->datatables->column_search = array('tbl_account_details.employment_id', 'tbl_account_details.fullname', 'advance_amount', 'deduct_month', 'request_date');
            $this->datatables->order = array('advance_salary_id' => 'desc');
            $where = array('tbl_advance_salary.user_id' => $this->session->userdata('user_id'));
            $fetch_data = make_datatables($where);
            $data = array();
            foreach ($fetch_data as $_key => $my_salary) {
                $staff_details = get_staff_details($my_salary->user_id);
                $action = null;
                $sub_array = array();
                $sub_array[] = $staff_details->employment_id;
                $sub_array[] = $staff_details->fullname;
                $sub_array[] = display_money($my_salary->advance_amount, default_currency());
                $sub_array[] = display_date($my_salary->request_date);
                $sub_array[] = date('M,Y', strtotime($my_salary->deduct_month));
                if ($my_salary->status == '0') {
                    $status = '<span class="label label-warning">' . lang('pending') . '</span>';
                } elseif ($my_salary->status == '1') {
                    $status = '<span class="label label-success"> ' . lang('accepted') . '</span>';
                } elseif ($my_salary->status == '2') {
                    $status = '<span class="label label-danger">' . lang('rejected') . '</span>';
                } else {
                    $status = '<span class="label label-info">' . lang('paid') . '</span>';
                }
                $sub_array[] = $status;
                if ($this->session->userdata('user_type') == 1) {
                    $sub_array[] = '<a href="' . base_url() . 'admin/payroll/advance_salary_details/' . $my_salary->advance_salary_id . '"
                               class="btn btn-info btn-xs" title="' . lang('view') . '" data-toggle="modal"
                               data-target="#myModal"><span class="fa fa-list-alt"></span></a>';
                }
                $data[] = $sub_array;
            }

            render_table($data, $where);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function all_advance_salaryList()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_advance_salary';
            $this->datatables->join_table = array('tbl_account_details');
            $this->datatables->join_where = array('tbl_account_details.user_id=tbl_advance_salary.user_id');
            $this->datatables->column_order = array('tbl_account_details.employment_id', 'tbl_account_details.fullname', 'advance_amount', 'deduct_month', 'request_date');
            $this->datatables->column_search = array('tbl_account_details.employment_id', 'tbl_account_details.fullname', 'advance_amount', 'deduct_month', 'request_date');
            $this->datatables->order = array('tbl_advance_salary.request_date' => 'desc');
            $fetch_data = make_datatables();

            $data = array();
            foreach ($fetch_data as $_key => $my_salary) {

                $action = null;
                $sub_array = array();
                $sub_array[] = $my_salary->employment_id;
                $sub_array[] = $my_salary->fullname;
                $sub_array[] = display_money($my_salary->advance_amount, default_currency());
                $sub_array[] = display_date($my_salary->request_date);
                $sub_array[] = date('M,Y', strtotime($my_salary->deduct_month));
                if ($my_salary->status == '0') {
                    $status = '<span class="label label-warning">' . lang('pending') . '</span>';
                } elseif ($my_salary->status == '1') {
                    $status = '<span class="label label-success"> ' . lang('accepted') . '</span>';
                } elseif ($my_salary->status == '2') {
                    $status = '<span class="label label-danger">' . lang('rejected') . '</span>';
                } else {
                    $status = '<span class="label label-info">' . lang('paid') . '</span>';
                }
                $sub_array[] = $status;
                if ($this->session->userdata('user_type') == 1) {
                    $sub_array[] = '<a href="' . base_url() . 'admin/payroll/advance_salary_details/' . $my_salary->advance_salary_id . '"
                               class="btn btn-info btn-xs" title="' . lang('view') . '" data-toggle="modal"
                               data-target="#myModal"><span class="fa fa-list-alt"></span></a>';
                }
                $data[] = $sub_array;
            }

            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }


    public function get_advance_salary_info($year, $month = NULL)
    {// this function is to create get monthy recap report
        if (!empty($month)) {
            $advance_salary_info = $this->payroll_model->get_advance_salary_info_by_date($month); // get all report by start date and in date
        } else {
            for ($i = 1; $i <= 12; $i++) { // query for months
                if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                    $month = $year . "-" . '0' . $i;
                } else {
                    $month = $year . "-" . $i;
                }
                $advance_salary_info[$i] = $this->payroll_model->get_advance_salary_info_by_date($month); // get all report by start date and in date
            }
        }
        return $advance_salary_info; // return the result
    }

    public function advance_salary_pdf($year, $month)
    {
        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $month = $year . "-" . '0' . $month;
        } else {
            $month = $year . "-" . $month;
        }
        $data['advance_salary_info'] = $this->get_advance_salary_info($year, $month);

        $month_name = date('F', strtotime($year . '-' . $month)); // get full name of month by date query
        $data['monthyaer'] = $month_name . '  ' . $year;

        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/payroll/advance_salary_pdf', $data, TRUE);
        pdf_create($viewfile, slug_it(lang('advance_salary_report') . ' - ' . $data['monthyaer']));
    }

    public function advance_salary_details($id)
    {
        $data['title'] = lang('advance_salary_details');
        $data['advance_salary_info'] = $this->payroll_model->view_advance_salary($id);
        $data['subview'] = $this->load->view('admin/payroll/advance_salary_details', $data, false);
        $this->load->view('admin/_layout_modal', $data);
    }

    public function view_advance_salary($id)
    {
        $data['title'] = lang('advance_salary_details');
        $data['advance_salary_info'] = $this->payroll_model->view_advance_salary($id);
        $data['subview'] = $this->load->view('admin/payroll/advance_salary_details', $data, true);
        $this->load->view('admin/_layout_main', $data);
    }


    public function change_status($status, $id)
    {
        if (empty($status)) {
            $type = "error";
            $message = lang('operation_failed');
            set_message($type, $message);
            if (empty($_SERVER['HTTP_REFERER'])) {
                redirect('admin/payroll/advance_salary');
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $data['status'] = $status;
        $data['advance_salary'] = $this->payroll_model->check_by(array('advance_salary_id' => $id), 'tbl_advance_salary');

        $data['modal_subview'] = $this->load->view('admin/leave_management/_change_status', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);

    }

    public function set_salary_status($status, $id)
    {
        $data['status'] = $status;
        $data['approve_by'] = $this->session->userdata('user_id');

        $where = array('advance_salary_id' => $id);
        $this->payroll_model->set_action($where, $data, 'tbl_advance_salary');

        $advance_salary_info = $this->payroll_model->check_by(array('advance_salary_id' => $id), 'tbl_advance_salary');
        $profile_info = $this->payroll_model->check_by(array('user_id' => $advance_salary_info->user_id), 'tbl_account_details');

        if ($advance_salary_info->status == '0') {
            $status = lang('pending');
        } elseif ($advance_salary_info->status == '1') {
            $status = lang('accepted');
            $this->send_email_by_status($advance_salary_info, true);
        } elseif ($advance_salary_info->status == '2') {
            $status = lang('rejected');
            $this->send_email_by_status($advance_salary_info);
        } else {
            $status = lang('paid');
        }
// save into activities
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'payroll',
            'module_field_id' => $id,
            'activity' => 'activity_advance_salary_update',
            'icon' => 'cc-mastercard',
            'link' => 'admin/payroll/view_advance_salary/' . $id,
            'value1' => $profile_info->fullname . ' ' . lang('request_date') . strftime(config_item('date_format'), strtotime($advance_salary_info->request_date)),
            'value2' => $status . ' ' . lang('amount') . ': ' . $advance_salary_info->advance_amount,
        );
// Update into tbl_project
        $this->payroll_model->_table_name = "tbl_activities"; //table name
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);

        $type = "success";
        $message = lang('advance_salary_status_update');
        set_message($type, $message);
        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect('admin/payroll/advance_salary');
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function send_email_by_status($advance_salary_info, $approve = null)
    {
        $user_info = $this->payroll_model->check_by(array('user_id' => $advance_salary_info->user_id), 'tbl_users');
        $curency = $this->db->where('code', config_item('default_currency'))->get('tbl_currencies')->row();
        $advance_salary_email = config_item('advance_salary_email');
        if (!empty($advance_salary_email) && $advance_salary_email == 1) {

            if (!empty($approve)) {
                $email_template = $this->payroll_model->check_by(array('email_group' => 'advance_salary_approve_email'), 'tbl_email_templates');
                $description = 'not_advance_salary_approve';
            } else {
                $email_template = $this->payroll_model->check_by(array('email_group' => 'advance_salary_reject_email'), 'tbl_email_templates');
                $description = 'not_advance_salary_reject';
            }
            $message = $email_template->template_body;
            $subject = $email_template->subject;
            $advance_amount = str_replace("{AMOUNT}", display_money($advance_salary_info->advance_amount, $curency->symbol), $message);
            $deduct_month = str_replace("{DEDUCT_MOTNH}", date('Y M', strtotime('deduct_month')), $advance_amount);
            $message = str_replace("{SITE_NAME}", config_item('company_name'), $deduct_month);
            $data['message'] = $message;
            $message = $this->load->view('email_template', $data, TRUE);

            $params['subject'] = $subject;
            $params['message'] = $message;
            $params['resourceed_file'] = '';
            $params['recipient'] = $user_info->email;

            $this->payroll_model->send_email($params);
        } else {
            return true;
        }
        $notifyUser = array($user_info->user_id);
        if (!empty($notifyUser)) {
            foreach ($notifyUser as $v_user) {
                add_notification(array(
                    'to_user_id' => $v_user,
                    'description' => $description,
                    'icon' => 'cc-mastercard',
                    'link' => 'admin/payroll/view_advance_salary/' . $advance_salary_info->advance_salary_id,
                    'value' => display_money($advance_salary_info->advance_amount, $curency->symbol),
                ));
            }
        }
        if (!empty($notifyUser)) {
            show_notification($notifyUser);
        }
    }
	public function selected_user_payment(){
		//echo"<pre>"; print_r($_POST); die;
		$data['title'] = lang('Selected user list for make payment');
	    $flag = $this->input->post('flag', TRUE);
		$paymentCheck = $this->input->post('paymentCheck', TRUE);
		$makePayment[] = $this->input->post('makePayment', TRUE);
        $dataArr = array();			
		foreach($paymentCheck as $pcVal){
			$userArr = explode("-",$pcVal);
			//echo"<pre>"; print_r($pcVal);
			$user_id = $userArr[0];
			$key = $userArr[1]; 
            //$dataArr['user_id'] =   $user_id;     		
			$payment = $makePayment[$user_id];
			foreach($makePayment as $dataVal){
            $data_Arr = explode("/",$dataVal[$user_id][$key]);	$payment_startdate = date('Y-m-d', $data_Arr['8']);$end_paymentdate = date('Y-m-d',$data_Arr['7']);			
			//echo"<pre>"; print_r($data_Arr); exit;
			$dataArr[] = array(
			'end_payment_date' => $end_paymentdate,
			'payment_start_date' => $payment_startdate,
			'salary_type' => $end_paymentdate,
			'payment_number'=>$data_Arr['6'],
			'pay_frequency'=>$data_Arr['5'],
			'payment_month'=>$data_Arr['4'],
			'departments_id' =>$data_Arr['3'],
			'payment_mode' => $data_Arr['2'],
			'basicSalary' => $data_Arr['0'],
			'netSalary' => $data_Arr['1'],
			'user_id'=>$user_id,
			'user_name'=>$data_Arr['9'],
			'grossPay'=>$data_Arr['10'],
			'taxable_income'=>$data_Arr['11'],
			'total_deduction'=>$data_Arr['12'],
			'total_tax'=>$data_Arr['13'],
			);	
			}
			
		} 
		$data['allListUser'] = $dataArr;
		//echo"<pre>"; print_r($data); die;
		$data['subview'] = $this->load->view('admin/payroll/payment_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
	       
	}
	
    public function make_bulk_payment(){
		
		//echo"<pre>"; print_r($_REQUEST); die;
		
		$flag = $this->input->post('flag', TRUE);
		$departmentsID = $this->input->post('departments_id', TRUE);
		$grossPay = $this->input->post('grossPay', TRUE);
		//echo "gross pay";
		//echo "<pre>"; print_r($grossPay);
		
		$taxable_income = $this->input->post('taxable_income', TRUE);
		$total_deduction = $this->input->post('total_deduction', TRUE);
		$total_tax = $this->input->post('total_tax', TRUE);
		$userID = $this->input->post('user_id', TRUE);
		$payment_type_number1 = $this->input->post('payment_type_number', TRUE);
		
		$pay_frequency = $this->input->post('pay_frequency', TRUE);		
		$payment_month = $this->input->post('payment_month', TRUE);		
		
		//echo "payment month";
		//echo "<pre>"; print_r($payment_month);
		
		$payment_mode = $this->input->post('payment_mode', TRUE);		
		$basicSalary = $this->input->post('basicSalary', TRUE);		
		$netSalary = $this->input->post('netSalary', TRUE);	
//echo "net salary";
		//echo "<pre>"; print_r($netSalary);
		
		$payment_start_date = $this->input->post('payment_start_date', TRUE);	
//echo "<pre>"; print_r($payment_start_date); exit;		
		$end_payment_date = $this->input->post('end_payment_date', TRUE);		
		$payment_amount = $this->input->post('payment_amount', TRUE);		
		//$account = $this->input->post('account', TRUE);		
		$comment = $this->input->post('comment', TRUE);		
		$payment_number = $this->input->post('payment_number', TRUE);	
		$fine_deduction = $this->input->post('fine_deduction', TRUE);		
		//echo"<pre>"; print_r($paymentUnique);
		
		//echo "payment type number";
		//echo "<pre>"; print_r($payment_type_number1);
		//exit;
		
		$data['title'] = "Make Bulk Payment";
		// retrive all data from department table
		//echo "<pre>"; print_r($userID); exit;
		//echo "<pre>"; print_r($payment_type_number);
		
		if(!empty($payment_type_number1)){		
        foreach($payment_type_number1 as $key11=>$Mdata){
			$payment_type_number =  $key11;
			//echo "<pre>"; print_r($departmentsID); exit;
			
			$user_id =  $userID[$payment_type_number][0];
			//$payment_type_number = $Mdata[0];
			$departments_id = $departmentsID[$payment_type_number][0];
			$grossPay1 = $grossPay[$payment_type_number][0];
			$totalGP = $grossPay1;
			$payment_month1 = $payment_month[$payment_type_number][0];
			$pay_frequency1 = $pay_frequency[$payment_type_number][0];
			$payment_number1 = $payment_number[$payment_type_number][0];
			$paymentstartdate1 = $payment_start_date[$payment_type_number][0];
		  //  $payment_startdate = $payment_start_date[$payment_type_number][0];
			$endpaymentdate = $end_payment_date[$payment_type_number][0];
			$end_paymentdate = date('Y-m-d',$endpaymentdate);			
			$salary_type = $end_paymentdate;
			$payment_type1 = $payment_mode[$payment_type_number][0];
			$basicSalary1 = $basicSalary[$payment_type_number][0];
			$netSalary1 = $netSalary[$payment_type_number][0];
			$taxable_income1 = $taxable_income[$payment_type_number][0];
			$total_deduction1 = $total_deduction[$payment_type_number][0];
			$total_tax1 = $total_tax[$payment_type_number][0];
			
			$taxableIncome = $taxable_income1;
			$fine_deduction1 = $fine_deduction[$payment_type_number][0];
			
			$netPay = $payment_amount[$payment_type_number][0];
			
			//$payment_amount = $netSalary - $fine_deduction;
					
			
            $data['all_department_info'] = $this->db->get('tbl_departments')->result();
			
			$this->payroll_model->_table_name = "tbl_user_salary_general_info"; // table name
			$this->payroll_model->_order_by = "id"; //
			$template_info = $this->payroll_model->get_by(array('user_id' => $user_id), FALSE);
            
            $data['user_id'] = $user_id;
            $data['staff_details'] = get_staff_details($user_id);
            $total_slary_amount = 0;
            
			
            $data['payment_month'] = $payment_month1;
            $data['payment_flag'] = 1;
            $data['departments_id'] = $departments_id;
			$data['pay_frequency'] = $pay_frequency1;
			$data['payment_number'] = $payment_number1;
			$data['salary_type'] = $template_info[0]->user_payType;
			$data['payment_start_date'] = $paymentstartdate1;
			$payment_end_date = $end_paymentdate;
			$data['payment_end_date'] = $payment_end_date;
			//$data['payment_end_date'] = $payment_end_date;
			// get employee info by employee id
            $data['employee_info'] =$this->payroll_model->get_emp_salary_list($user_id);
			// get all allowance info by salary template id
            if (!empty($data['employee_info']->salary_template_id)) {
                $data['allowance_info'] = $this->get_allowance_info_by_id($data['employee_info']->salary_template_id, $user_id, 'salary');
            }
			
		/*
		if($data['employee_info']->payment_type == 'via_cheque'){
			$payment_type ="Pay via cheque"; 
		}else{
		    $payment_type ="Pay via ACH";
		}
		*/
		
		
		$payment_type = $payment_type1;
		
		
		
		$data1 = array(
            'pay_frequency' => $data['pay_frequency'],
            'payment_number' => $data['payment_number'],
			'basic_salary' => $basicSalary1,
            'grossPay' => $grossPay1,
			'total_tax' => $total_tax1,
			'total_deduction' => $total_deduction1,
			'net_salary' => $netSalary1,
			'netPay' => $netPay,
            'salary_type' => $template_info[0]->user_payType,
            'payment_start_date' => $paymentstartdate1,
			'payment_end_date' => $data['payment_end_date'],
            'fine_deduction' => $fine_deduction1,
            'user_id' => $data['user_id'],
            'payment_month' => $payment_month1,
            'payment_type' =>$payment_type1,
            'comments' => $comment,
			'deduct_from'=>'0',
        );
		//echo"<pre>"; print_r($data1); die;
		/* *********Start*********** */
		
		 //$payment_start_date = $data1['payment_start_date'];
		 $payment_end_date = $data1['payment_end_date'];
           
        // save into tbl employee paymenet
        $this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_primary_key = "salary_payment_id"; // $id
        
		//echo"<pre>"; print_r($data1); die;
        $details_data['salary_payment_id'] = $this->payroll_model->save($data1);
      
	   //die;
        // get employee info by employee id
        $employee_info = $this->payroll_model->get_emp_salary_list($data1['user_id']);
         //echo"<pre>"; print_r( $data1);
		 
		 
		if (!empty($employee_info->monthly_template_id)) {
		   
		   $salary_template_name = $employee_info->salary_grade;
		   $salary_template_id1 = $employee_info->monthly_template_id;
		   $allowance_info = $this->employee_allowance($salary_template_id1,$data1['user_id']);
		   
		} else {
			$salary_template_name = $employee_info->hourly_grade;
			$salary_template_id1 = $employee_info->hourly_rate_id;
			$allowance_info = $this->employee_allowance($salary_template_id1,$data1['user_id']);
		}
		   
		   
		 if (!empty($allowance_info)) {
                foreach ($allowance_info as $v_allowance_info) {
				$aldata['salary_payment_id'] = $details_data['salary_payment_id'];
				$aldata['salary_payment_allowance_label'] = $v_allowance_info->earning_type;
				$aldata['salary_payment_allowance_value'] = $v_allowance_info->earning_amount;
				$aldata['exclude_from_taxes'] = $v_allowance_info->excludeFrTx;
				
				//  save into tbl employee paymenet
				$this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
				$this->payroll_model->_primary_key = "salary_payment_allowance_id"; // $id
				$this->payroll_model->save($aldata);
                }
            }
		
		
		$award_info = $this->employee_award($data1['user_id'], $paymentstartdate1, $payment_end_date);
		
		if (!empty($award_info)) {
					foreach($award_info as $awardAmt){
						//echo"<pre>"; print_r($awardAmt);
						$awardData['salary_payment_id'] = $details_data['salary_payment_id'];
						$awardData['salary_payment_allowance_label'] = $awardAmt->award_name;
						$awardData['salary_payment_allowance_value'] = $awardAmt->award_amount;
						$this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
						$this->payroll_model->_primary_key = "salary_payment_allowance_id"; // $id
						$this->payroll_model->save($awardData);
					}
		}
		
		
		// get all taxes info by salary template id
			// ************ Save all taxes info **********
			
		$taxes_info = $this->employee_taxes($salary_template_id1, $data1['user_id']);
		
		if (!empty($taxes_info)) {
                foreach ($taxes_info as $v_tax_info) {
                   
					
					 if($v_tax_info->taxCalc=='max'){
						$percent = $v_tax_info->taxWithholding;
                        $tAmt = $taxable_income1 * $percent / 100;						
						$salary_payment_tax_value = $tAmt;
					 }elseif($v_tax_info->taxCalc=='rng'){
					 $percent = $v_tax_info->taxWithholding;
                     $tAmt = $taxable_income1 * $percent / 100;						
                     $salary_payment_tax_value = $tAmt;
					 }else{
					 $percent = $v_tax_info->taxWithholding;
                     $tAmt = ($taxable_income1*$percent)/100;						
                     $salary_payment_tax_value = $tAmt;
					 }
					
					
					$taxesData['salary_payment_id'] = $details_data['salary_payment_id'];
					$taxesData['salary_payment_tax_label'] = $v_tax_info->tax_type;
					$taxesData['salary_payment_tax_value'] = $salary_payment_tax_value;
					$taxesData['salary_payment_tax_category'] = $v_tax_info->taxCategory;
					
					
					$this->payroll_model->_table_name = "tbl_salary_payment_tax"; // table name
					$this->payroll_model->_primary_key = "salary_payment_tax_id"; // $id
					$this->payroll_model->save($taxesData);
                }
            }
			
			 	
		// get all deduction info by salary template id
			// ************ Save all deduction info **********
			
		$deduction_info = $this->employee_deductions($salary_template_id1, $data1['user_id']);
		
		if (!empty($deduction_info)) {
                foreach ($deduction_info as $v_deduction_info) {
                   
					
					if($v_deduction_info->deductionBasedOn == "percentAmt"){
						$percent = $v_deduction_info->deductionPercent;
						$deduction_amt =  $totalGP*$percent/100;
					}else{								
						$deduction_amt= $v_deduction_info->deduction_amount;
					}
					
					
					$deductionData['salary_payment_id'] = $details_data['salary_payment_id'];
					$deductionData['salary_payment_deduction_label'] = $v_deduction_info->deduction_type;
					$deductionData['salary_payment_deduction_value'] = $deduction_amt;
					$deductionData['exclude_from_taxes'] = $v_deduction_info->excludeFrTx;
					
					$this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
					$this->payroll_model->_primary_key = "salary_payment_deduction"; // $id
					$this->payroll_model->save($deductionData);
                }
            }
			
			
			/* Company contribution */
			$contribution_info = $this->employee_contributions($salary_template_id1, $data1['user_id']);
			
			//echo"<pre>"; print_r($contribution_info); die;
			
            if (!empty($contribution_info)) {	
                foreach ($contribution_info as $v_contribution_info) {
                    $salary_payment_contribution_label = $v_contribution_info->contribution_type;
					if($v_contribution_info->contributionBasedOn=='fixAmt'){
                    $salary_payment_contribution_value = $v_contribution_info->contribution_amount;
					}else{
						$perc = $v_contribution_info->contributionPercent;
						$cAmt = $totalGP * $perc / 100;
						$salary_payment_contribution_value = $cAmt;					
					}
                    
					
					$contributionData['salary_payment_id'] = $details_data['salary_payment_id'];
					$contributionData['salary_payment_contribution_label'] = $salary_payment_contribution_label;
					$contributionData['salary_payment_contribution_value'] = $salary_payment_contribution_value;
					$contributionData['contributionBasedOn'] = $v_contribution_info->contributionBasedOn;
					$contributionData['companyCategory_id'] = $v_contribution_info->companyCategory_id;
					
					$this->payroll_model->_table_name = "tbl_salary_payment_contribution"; // table name
					$this->payroll_model->_primary_key = "salary_payment_contribution_id"; // $id
					$this->payroll_model->save($contributionData);
					
					
                }
            }
			
			/* Other benefits */
			
			/* Company contribution */
			$benefits_info = $this->employee_benefits($salary_template_id1, $data1['user_id']);
			
			if (!empty($benefits_info)) {	
                foreach ($benefits_info as $v_benefit_info) {
                    $salary_payment_benefit_label = $v_benefit_info->benifit_type;
					if($v_benefit_info->benifitBasedOn=='fixAmt'){
                    $salary_payment_benefit_value = $v_benefit_info->benifit_amount;
					}else{
						$perc = $v_benefit_info->benifitPercent;
						$bAmt = $totalGP * $perc / 100;
						$salary_payment_benefit_value = $bAmt;					
					}
                    
					
					$benefitData['salary_payment_id'] = $details_data['salary_payment_id'];
					$benefitData['salary_payment_benifit_label'] = $salary_payment_benefit_label;
					$benefitData['salary_payment_benifit_value'] = $salary_payment_benefit_value;
					$benefitData['benifitBasedOn'] = $v_benefit_info->benifitBasedOn;
					
					$this->payroll_model->_table_name = "tbl_salary_payment_benifit"; // table name
					$this->payroll_model->_primary_key = "salary_payment_benifit_id"; // $id
					$this->payroll_model->save($benefitData);
					
					
                }
            }
			
			
			//save salary payment details...
			$details_data1['salary_payment_id'] = $details_data['salary_payment_id'];
			$details_data1['salary_payment_details_label'] = $salary_template_name;
			$details_data1['salary_payment_details_value'] = $netPay;
			$this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
			$this->payroll_model->_primary_key = "salary_payment_details_id"; // $id
			$this->payroll_model->save($details_data1);
			
			//save activities
			$activities = array(
			'user' => $user_id,
			'module' => 'payroll',
			'activity' => 'activity_make_payment',
			'icon' => 'fa-list-ul',
			'value1' => $employee_info->fullname,
			'value2' => date('F Y', strtotime($payment_month1)),
            );
// Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);
           
			
        }
		
	$type = 'success';
    //$message = lang('payment_information_update');
	$message = 'Payment successfully.';
	
	 set_message($type, $message);
     redirect('admin/payroll/make_payment/' . '0' . '/' . $data['employee_info']->departments_id . '/' . $data['payment_month']);
	 
	}
}  
public function make_payment($user_id = NULL, $departments_id = NULL, $payment_month = NULL, $pay_frequency = NULL, $payment_number = NULL, $salary_type = NULL,  $payment_start_date = NULL,  $view_payment_history = NULL)
	{
		
		//echo $payment_month; die;
	    //echo"<pre>";print_r($_POST); die;
		$paymenetType = $_REQUEST['payType']; 
		$selectedDpt = $_REQUEST['dptID']; 
		$selectedMnth = $_REQUEST['payMth']; 
		
        $data['title'] = "Make Payment";
          // retrive all data from department table
        $data['all_department_info'] = $this->db->get('tbl_departments')->result();
        //echo"<pre>";print_r($data); die;
		if ($user_id != 0 && !empty($payment_month)) {			
           // check payment history by employee id
            $check_existing_payment = $this->db->where('user_id', $user_id)->get('tbl_salary_payment')->result();

            $data['user_id'] = $user_id;
            $data['staff_details'] = get_staff_details($user_id);
            $total_slary_amount = 0;
			
            if (!empty($check_existing_payment)) {
                foreach ($check_existing_payment as $key => $v_paymented_id) {
                    $salary_payment_id = $v_paymented_id->salary_payment_id;
                    $data['emp_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);
                    $data['salary_payment_info'][] = $this->payroll_model->get_salary_payment_info($salary_payment_id, true);

                    $this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
                    $this->payroll_model->_order_by = "salary_payment_id"; // $id
                    $salary_payment_history = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_details')->result();
                    if (!empty($salary_payment_history)) {
                        foreach ($salary_payment_history as $v_payment_history) {
                            if (is_numeric($v_payment_history->salary_payment_details_value)) {
                                if ($v_payment_history->salary_payment_details_label == 'overtime_salary') {
                                    $rate = $v_payment_history->salary_payment_details_value;
                                } elseif ($v_payment_history->salary_payment_details_label == 'hourly_rates') {
                                    $rate = $v_payment_history->salary_payment_details_value;
                                }
                                $total_slary_amount += $v_payment_history->salary_payment_details_value;
                            }
                        }
                    }
                    $salary_allowance_info = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_allowance')->result();
                    $total_allowance = 0;
                    if (!empty($salary_allowance_info)) {
                        foreach ($salary_allowance_info as $v_salary_allowance_info) {
                            $total_allowance += $v_salary_allowance_info->salary_payment_allowance_value;
                        }
                    }
                    if (!empty($rate)) {
                        $rate = $rate;
                    } else {
                        $rate = 0;
                    }

                    $data['total_paid_amount'][] = $total_slary_amount + $total_allowance - $rate;
                    $salary_deduction_info = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_deduction')->result();
                    $total_deduction = 0;
                    if (!empty($salary_deduction_info)) {
                        foreach ($salary_deduction_info as $v_salary_deduction_info) {
                            $total_deduction += $v_salary_deduction_info->salary_payment_deduction_value;
                        }
                    }
                    $data['total_deduction'][] = $total_deduction;
                }
            }
			
			
            $data['payment_month'] = $payment_month;
            $data['payment_flag'] = 1;
            $data['departments_id'] = $departments_id;
			$data['pay_frequency'] = $pay_frequency;
			$data['payment_number'] = $payment_number;
			
			$this->payroll_model->_table_name = "tbl_user_salary_general_info"; // table name
			$this->payroll_model->_order_by = "id"; //
			$template_info = $this->payroll_model->get_by(array('user_id' => $user_id), FALSE);
			
			$data['salary_type'] = $template_info[0]->user_payType;
			$data['payment_start_date'] = date('Y-m-d', $payment_start_date);
			$payment_end_date = date('Y-m-d', $salary_type);
			$data['payment_end_date'] = $payment_end_date;
			
			
			// get employee info by employee id
            $data['employee_info'] = $this->payroll_model->get_emp_salary_list($user_id);
           // get all allowance info by salary template id
		   
            if (!empty($data['employee_info']->monthly_template_id)) {
				
                $data['allowance_info'] = $this->get_allowance_info_by_id($data['employee_info']->monthly_template_id, $user_id, 'salary');
				
                // get all deduction info by salary template id
                $data['deduction_info'] = $this->get_deduction_info_by_id($data['employee_info']->monthly_template_id, $user_id, 'salary');
				
                // get all overtime info by month and employee id
                /* $data['overtime_info'] = $this->get_overtime_info_by_id($user_id, $data['payment_month'], $data['payment_start_date'], $payment_end_date); */
            }
			
            // get all advance salary info by month and employee id
            // $data['advance_salary'] = $this->get_advance_salary_info_by_id($user_id, $data['payment_month']);
			// get award info by employee id and payment month
			// get award info by employee id and payment date
            // $this->payroll_model->_table_name = 'tbl_employee_award';
            // $this->payroll_model->_order_by = 'user_id';
			
			//$data['award_info'] = $this->payroll_model->get_by(array('user_id' => $user_id, 'award_date' => $data['payment_month'], 'given_date>=' => $data['payment_start_date'], 'given_date<=' => $payment_end_date), FALSE);
			
            // $data['award_info'] = $this->payroll_model->get_by(array('user_id' => $user_id, 'given_date>=' => $data['payment_start_date'], 'given_date<=' => $payment_end_date), FALSE);

			// check hourly payment info
			// if exist count total hours in a month
			// get hourly payment info by id
            if (!empty($data['employee_info']->hourly_rate_id)) {
				//for month payment
                $data['total_hours'] = $this->get_total_hours_in_month($user_id, $data['payment_month'], $data['payment_start_date'], $payment_end_date);
				
			}
            if (!empty($data['total_hours'])) {
                if ($data['total_hours'] == 0 && $data['total_minutes'] == 0) {
					//echo"<pre>"; print_r($data); die;
                    $type = 'error';
                    $message = '<strong>' . $data['employee_info']->fullname . ' ' . '</strong>' . lang('working_hour_empty');
                    set_message($type, $message);
                    redirect('admin/payroll/make_payment/' . '0' . '/' . $data['employee_info']->departments_id . '/' . $data['payment_month']);
                }
            }
			
        } else {
			
            $flag = $this->input->post('flag', TRUE);
            $departments_id = $this->input->post('departments_id', TRUE);
            if (!empty($flag) || !empty($departments_id)) {
				// check employee id is empty or not
				
                $data['flag'] = 1;
                if (!empty($departments_id)) {
                    $data['departments_id'] = $departments_id;
                } else {
                    $data['departments_id'] = 'all';
                }
                if (!empty($payment_month)) {
                    $data['payment_month'] = $payment_month;
                } else {
                    $data['payment_month'] = $this->input->post('payment_month', TRUE);
                }
				
				// get all designation info by Department id
				if($data['departments_id'] == 'all') {
					$designation_info = $this->db->get('tbl_designations')->result();
				} else {
					$designation_info = $this->db->where('departments_id', $data['departments_id'])->get('tbl_designations')->result();
				}
                
				//added for biweekly salary list
				$payment_year_arr = explode("-", $data['payment_month']);
				$data['payment_year'] = $payment_year_arr[0];
				//end
				//echo "<pre>"; print_r($designation_info); exit;
				if (!empty($designation_info)) {
                    foreach ($designation_info as $v_designatio) {
                        $data['employee_info'][] = $this->payroll_model->get_emp_salary_list('', $v_designatio->designations_id);
						
						$employee_info = $this->payroll_model->get_emp_salary_list('', $v_designatio->designations_id);
						
						foreach ($employee_info as $value) {
						//echo "<pre>"; print_r($value);
						
						/*
						* payroll biweekly calculation code
						* payroll biweekly calculation code
						*/
						$payroll_cal_day=config_item('payment_day');
						
						date_default_timezone_set('America/Los_Angeles');
						$start = new DateTime(date("Y-m-d", strtotime("first ".$payroll_cal_day." 2018-12")));
						$customer_payment_dates = array();
						$global_payperiod_dates = array();
						
						$payment_start_day_of_year = $data['payment_year']."-01"."-01";
						$last_day_of_year = $data['payment_year']."-12"."-31";
						
						for($i=0;$i<27;$i++) {
						
							//if(strtotime($payment_start_day_of_year) <= strtotime($last_day_of_year)){
								
								$target = new DateTime($payment_start_day_of_year);                      
								 
								/* -- Begin of new code --*/
								$targetPayment = clone $target;
								
								$intervalBetweenTargetAndStart = $target->diff($start);
								
								$daysBeforePayment = 14 - $intervalBetweenTargetAndStart->days % 14;
						
								$targetPayment->modify("+".$daysBeforePayment." days");
								
								$PaymentStartDate = clone $targetPayment;
								$PaymentStartDate->modify("-14 days");
								
								$customer_payment_dates[$targetPayment->format('Y').'-'.$targetPayment->format('m')][$i]['payment_start_date'] = $PaymentStartDate->format('Y-m-d');
								$customer_payment_dates[$targetPayment->format('Y').'-'.$targetPayment->format('m')][$i]['payment_date'] = $targetPayment->format('D, d M Y');
								
								$global_payperiod_dates[$targetPayment->format('Y').'-'.$targetPayment->format('m')][$i]['payment_start_date'] = $PaymentStartDate->format('D, d M Y');
								$global_payperiod_dates[$targetPayment->format('Y').'-'.$targetPayment->format('m')][$i]['payment_date'] = $targetPayment->format('D, d M Y');
								
								
								$payment_start_day_of_year = $targetPayment->format('Y-m-d');
								
							//}
						}
						
						
						$data['global_payperiod_dates'] = $global_payperiod_dates;
						$data['customer_payment_dates'][$value->user_id] = $customer_payment_dates;
						//echo "<pre>";
						//print_r($data['customer_payment_dates'][$value->user_id]);
						//exit;
						/*
						* payroll biweekly calculation code end
						*/
							
// get all allowance info by salary template id
//echo "<pre>"; print_r($value); exit;
                            if (!empty($value->monthly_template_id)) {
                                $data['allowance_info'][$value->user_id] = $this->get_allowance_info_by_id($value->monthly_template_id, $value->user_id, 'salary');
// get all deduction info by salary template id
                                $data['deduction_info'][$value->user_id] = $this->get_deduction_info_by_id($value->monthly_template_id, $value->user_id, 'salary');
// get all overtime info by month and employee id
								
								//for biweekly payment
								$pay_arr = $data['customer_payment_dates'][$value->user_id][$data['payment_month']];
								//echo "<pre>"; print_r($pay_arr); exit;
								foreach($pay_arr as $key1=>$val1) {
									$overtime_end_payment_date = date('Y-m-d', strtotime($val1['payment_date']));
									$data['overtime_info'][$value->user_id][$key1+1] = $this->get_overtime_info_by_id($value->user_id, $data['payment_month'], $val1['payment_start_date'], $overtime_end_payment_date);
								}
							}
// get all advance salary info by month and employee id
                            $data['advance_salary'][$value->user_id] = $this->get_advance_salary_info_by_id($value->user_id, $data['payment_month']);
// get award info by employee id and payment month by monthly
                           // $data['award_info'][$value->user_id] = $this->get_award_info_by_id($value->user_id, $data['payment_month']);

						   // get award info by employee id for the biweekly
						   //for biweekly payment
							$pay_arr = $data['customer_payment_dates'][$value->user_id][$data['payment_month']];
							//echo "<pre>"; print_r($pay_arr); exit;
							foreach($pay_arr as $key1=>$val1) {
								$award_end_payment_date = date('Y-m-d', strtotime($val1['payment_date']));
								$data['award_info'][$value->user_id][$key1+1] = $this->get_award_info_by_id($value->user_id, $data['payment_month'], $val1['payment_start_date'], $award_end_payment_date);
							}
// check hourly payment info
// if exist count total hours in a month
// get hourly payment info by id
                            if (!empty($value->hourly_rate_id)) {
                                //for monthly payment
								$data['total_hours'][$value->user_id] = $this->get_total_hours_in_month($value->user_id, $data['payment_month']);
								
								//for biweekly payment
								$pay_arr = $data['customer_payment_dates'][$value->user_id][$data['payment_month']];
								
								foreach($pay_arr as $key1=>$val1) {
									$end_payment_date = date('Y-m-d', strtotime($val1['payment_date']));
									$data['total_hours_biweekly'][$value->user_id][$key1+1] = $this->get_total_hours_in_month($value->user_id, $data['payment_month'], $val1['payment_start_date'], $end_payment_date);
								}
							}
                        }
                    }
                }
            }
        }
		 
		
		$data['view_payment_history'] = 'make_payment';
		if($view_payment_history != NULL) {
			$data['view_payment_history'] = $view_payment_history;
			$data['title'] = "Payment History";
		}
		
		//echo"<pre>"; print_r($data); die;
        $data['subview'] = $this->load->view('admin/payroll/make_payment', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
	
	public function get_allowance_info_by_id($salary_template_id, $user_id = NULL, $wage_type = NULL)
    {
        
		if($user_id) {
			$salary_allowance_info = $this->db->where('salary_template_id', $salary_template_id)->where('allowance_type', $wage_type)->where('user_id', $user_id)->where('effective_date <=', date('Y-m-d'));
			$salary_allowance_info = $salary_allowance_info->get('tbl_employee_salary_earnings')->result();
		} else {
			$salary_allowance_info = $this->db->where('salary_template_id', $salary_template_id)->get('tbl_salary_allowance')->result();
		}
		
        $total_allowance = 0;
        foreach ($salary_allowance_info as $v_allowance_info) {
            $total_allowance += $v_allowance_info->earning_amount;
        }
        return $total_allowance;
    }

    public function get_deduction_info_by_id($salary_template_id, $user_id = NULL, $wage_type = NULL)
    {
		
		if($user_id) {
			$salary_deduction_info = $this->db->where('salary_template_id', $salary_template_id)->where('wage_type', $wage_type)->where('user_id', $user_id)->where('effective_date <=', date('Y-m-d'));
			$salary_deduction_info = $salary_deduction_info->get('tbl_employee_salary_deduction')->result();
		} else {
			$salary_deduction_info = $this->db->where('salary_template_id', $salary_template_id)->get('tbl_salary_deduction')->result();
		}
		
		$total_deduction = 0;
        foreach ($salary_deduction_info as $v_deduction_info) {
            $total_deduction += $v_deduction_info->deduction_amount;
        }
        return $total_deduction;
    }

    public function get_total_hours_in_month($user_id, $payment_month, $start_date = NULL, $end_date = NULL)
    {
		if($start_date == NULL && $end_date == NULL) {
			$start_date = $payment_month . '-' . '01';
			$end_date = $payment_month . '-' . '31';
		} 
		//echo $start_date."<br>";
		//echo $end_date; exit;
        $attendance_info = $this->payroll_model->get_attendance_info_by_date($start_date, $end_date, $user_id); // get all report by start date and in date


        $total_hh = 0;
        $total_mm = 0;
        foreach ($attendance_info as $v_clock_time) {
// calculate the start timestamp
            $startdatetime = strtotime($v_clock_time->date_in . " " . $v_clock_time->clockin_time);
// calculate the end timestamp
            $enddatetime = strtotime($v_clock_time->date_out . " " . $v_clock_time->clockout_time);
// calulate the difference in seconds
            $difference = $enddatetime - $startdatetime;
            $years = abs(floor($difference / 31536000));
            $days = abs(floor(($difference - ($years * 31536000)) / 86400));
            $hours = abs(floor(($difference - ($years * 31536000) - ($days * 86400)) / 3600));
            $mins = abs(floor(($difference - ($years * 31536000) - ($days * 86400) - ($hours * 3600)) / 60));#floor($difference / 60);
            $total_mm += $mins;
            $total_hh += $hours;
        }
		
        if ($total_mm > 59) {
            $total_hh += intval($total_mm / 60);
            $total_mm = intval($total_mm % 60);
        }
		
        $result['total_hours'] = $total_hh;
        $result['total_minutes'] = $total_mm;
        return $result;
    }

    public function get_advance_salary_info_by_id($user_id, $payment_month)
    {

        $advance_salary_info = $this->payroll_model->get_advance_salary_info_by_date($payment_month, '', $user_id); // get all report by start date and in date
        $advance_amount = 0;
        foreach ($advance_salary_info as $v_advance_salary) {
            $advance_amount += $v_advance_salary->advance_amount;
        }
        $result['advance_amount'] = $advance_amount;
        return $result;

    }

    public function get_award_info_by_id($user_id, $payment_month, $payment_start_date = NULL, $payment_end_date = NULL)
    {
        $this->payroll_model->_table_name = 'tbl_employee_award';
        $this->payroll_model->_order_by = 'user_id';
		//for monthly payment
       // $award_info = $this->payroll_model->get_by(array('user_id' => $user_id, 'award_date' => $payment_month), FALSE);
		//for biweekly payment
		if($payment_start_date != NULL && $payment_end_date != NULL) {
			$award_info = $this->payroll_model->get_by(array('user_id' => $user_id, 'given_date>=' => $payment_start_date, 'given_date<=' => $payment_end_date), FALSE);
        } else {
			$award_info = $this->payroll_model->get_by(array('user_id' => $user_id, 'award_date' => $payment_month), FALSE);
		}
		$result['award_amount'] = 0;
        foreach ($award_info as $v_award_info) {
            $result['award_amount'] += $v_award_info->award_amount;
        }
        if (!empty($result)) {
            return $result;
        }
    }

    public function get_overtime_info_by_id($user_id, $payment_month, $start_date = NULL, $end_date = NULL)
    {
		if($start_date == NULL && $end_date == NULL) {
			$start_date = $payment_month . '-' . '01';
			$end_date = $payment_month . '-' . '31';
		}
        $this->payroll_model->_table_name = "tbl_overtime"; //table name
        $this->payroll_model->_order_by = "overtime_id";
        $all_overtime_info = $this->payroll_model->get_by(array('overtime_date >=' => $start_date, 'overtime_date <=' => $end_date, 'user_id' => $user_id), FALSE); // get all report by start date and in date
        $hh = 0;
        $mm = 0;
        foreach ($all_overtime_info as $overtime_info) {
            $hh += $overtime_info->overtime_hours;
            $mm += date('i', strtotime($overtime_info->overtime_hours));
        }
        if ($hh > 1 && $hh < 10 || $mm > 1 && $mm < 10) {
            $total_mm = '0' . $mm;
            $total_hh = '0' . $hh;
        } else {
            $total_mm = $mm;
            $total_hh = $hh;
        }
        if ($total_mm > 59) {
            $total_hh += intval($total_mm / 60);
            $total_mm = intval($total_mm % 60);
        }
        $result['overtime_hours'] = $total_hh;
        $result['overtime_minutes'] = $total_mm;
        return $result;
    }

    public function view_payment_details($user_id, $payment_month, $pay_frequency = NULL, $payment_start_date = NULL,  $payment_end_date = NULL,  $payment_number = NULL)
    {
        if (empty($user_id)) {
            $type = "error";
            $message = lang('operation_failed');
            set_message($type, $message);
            if (empty($_SERVER['HTTP_REFERER'])) {
                redirect('admin/payroll/make_payment');
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        $data['title'] = 'Payment Salary Details';
        $data['payment_month'] = $payment_month;
        $data['payment_flag'] = 1;
		
		$data['payment_number'] = $payment_number;
		
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}
		
		if($payment_start_date != 'NULL' && $payment_end_date != 'NULL') {
				$payment_start_date = date('Y-m-d', $payment_start_date);
				$payment_end_date = date('Y-m-d', $payment_end_date);
			}
			
			// get employee info by employee id
            $data['employee_info'] = $this->payroll_model->get_emp_salary_list($user_id);
			
		// get award info by employee id and payment month
		// get award info by employee id and payment date
				
        $this->payroll_model->_table_name = 'tbl_employee_award';
        $this->payroll_model->_order_by = 'user_id';
        $data['award_info'] = $this->payroll_model->get_by(array('user_id' => $user_id, 'given_date>=' => $payment_start_date, 'given_date<=' => $payment_end_date), FALSE);
		
        
		if (!empty($data['employee_info']->hourly_rate_id)) {			
          
		  $hourlyInfo = $this->db->where('user_id', $user_id)->where('start_date', $payment_start_date)->where('end_date', $payment_end_date)->get('tbl_employee_hours')->result();
		  //echo"<pre>"; print_r($employee_info); 
		  $rgH = $hourlyInfo[0]->regular_hours;
		  $ptoH = $hourlyInfo[0]->pto_hours;
		  $ovtH = $hourlyInfo[0]->overtime_hours;
		  $total = $rgH + $ptoH + $ovtH;
		  
		  $data['regular_hours'] =   $rgH;
		  $data['pto_hours'] =   $ptoH;
		  $data['overtime_hours'] =   $ovtH;
		  $data['total_hours'] =   $total;
        }
		
		if($data['employee_info']->hourly_rate_id){
			$salary_template_id = $data['employee_info']->hourly_rate_id;
		}else{
			$salary_template_id = $data['employee_info']->monthly_template_id;
		}
			
        $data['allowance_info'] = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->where('effective_date <=', date('Y-m-d'))->get('tbl_employee_salary_earnings')->result();
		

		//$deduction = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->get('tbl_employee_salary_deduction')->result();
		//$data['deduction_info'] = $deduction;
		
		//$tax = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->get('tbl_employee_salary_tax')->result();
		//$data['tax_info'] = $tax;
		
		//$companyContribution = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->get('tbl_salary_company_contribution')->result();
		//$data['contribution_info'] = $companyContribution; 
		
		
		//$otherBenefit = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_template_id)->get('tbl_salary_other_benifits')->result();
		//$data['benefit_info'] = $otherBenefit;
		
		
		
        //echo"<pre>"; print_r($data); die;
      // redirect('admin/payroll/make_payment/' . '0' . '/' . $data['employee_info']->departments_id . '/' . $data1['payment_month']);
		$data['subview'] = $this->load->view('admin/payroll/view_payment_details', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
	}

	
    public function get_payment($id = NULL)
    {
// input data
        $data = $this->payroll_model->array_from_post(array('user_id', 'pay_frequency', 'payment_number', 'salary_type', 'payment_start_date', 'payment_month', 'fine_deduction', 'payment_type', 'comments'));
         //echo "<pre>"; print_r($_REQUEST); exit; 

		$payment_start_date = $data['payment_start_date'];
		$payment_end_date = date('Y-m-d',strtotime($data['salary_type']));

// save into tbl employee paymenet
        $this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_primary_key = "salary_payment_id"; // $id
        if (!empty($id)) {
            $details_data['salary_payment_id'] = $id;
            $this->payroll_model->save($data, $id);
        } else {
            $data['deduct_from'] = 0;
            $details_data['salary_payment_id'] = $this->payroll_model->save($data);
        }
// get employee info by employee id
        $employee_info = $this->payroll_model->get_emp_salary_list($data['user_id']);

// get all allowance info by salary template id
        if (!empty($employee_info->monthly_template_id)) {
            $salary_payment_details_label[] = lang('salary_grade');
            $salary_payment_details_value[] = $employee_info->salary_grade;

            $salary_payment_details_label[] = lang('basic_salary');
            $salary_payment_details_value[] = $employee_info->basic_salary;
            if (!empty($employee_info->overtime_salary)) {
                $salary_payment_details_label[] = 'overtime_salary';
                $salary_payment_details_value[] = $employee_info->overtime_salary;
            }
// ************ Save all allwance info **********
			if($data['pay_frequency'] == 'biweekly') {
				$this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
				$this->payroll_model->_order_by = 'salary_template_id';
				$allowance_info = $this->payroll_model->get_by(array('salary_template_id' => $employee_info->monthly_template_id, 'user_id' => $data['user_id'], 'effective_date <=' => date('Y-m-d')), FALSE);
			} else {
				$this->payroll_model->_table_name = 'tbl_salary_allowance';
				$this->payroll_model->_order_by = 'salary_template_id';
				$allowance_info = $this->payroll_model->get_by(array('salary_template_id' => $employee_info->monthly_template_id), FALSE);
			}
			//echo"<pre>"; print_r($allowance_info);
			if (!empty($allowance_info)) {
				//echo"dfgdfgd"; die;
                foreach ($allowance_info as $v_allowance_info) {
                    $aldata['salary_payment_id'] = $details_data['salary_payment_id'];
                    $aldata['salary_payment_allowance_label'] = $v_allowance_info->earning_type;
                    $aldata['salary_payment_allowance_value'] = $v_allowance_info->earning_amount;

//  save into tbl employee paymenet
                    $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
                    $this->payroll_model->_primary_key = "salary_payment_allowance_id"; // $id
                    $this->payroll_model->save($aldata);
                }
            }
// get all deduction info by salary template id
// ************ Save all deduction info **********
			if($data['pay_frequency'] == 'biweekly') {
				$this->payroll_model->_table_name = 'tbl_employee_salary_deduction';
				$this->payroll_model->_order_by = 'salary_template_id';
				$deduction_info = $this->payroll_model->get_by(array('salary_template_id' => $employee_info->monthly_template_id, 'user_id' => $data['user_id'], 'effective_date <=' => date('Y-m-d')), FALSE);
			} else {
				$this->payroll_model->_table_name = 'tbl_salary_deduction';
				$this->payroll_model->_order_by = 'salary_template_id';
				$deduction_info = $this->payroll_model->get_by(array('salary_template_id' => $employee_info->monthly_template_id), FALSE);
			}
			
            if (!empty($deduction_info)) {
                foreach ($deduction_info as $v_deduction_info) {
                    $salary_payment_deduction_label[] = $v_deduction_info->deduction_type;
                    $salary_payment_deduction_value[] = $v_deduction_info->deduction_amount;
                }
            }
// ************ Save all Overtime info **********
// get all overtime info by month and employee id
            $overtime_info = $this->get_overtime_info_by_id($data['user_id'], $data['payment_month'], $payment_start_date, $payment_end_date);
            $salary_payment_details_label[] = lang('overtime_hour');
            $salary_payment_details_value[] = $overtime_info['overtime_hours'] . ':' . $overtime_info['overtime_minutes'];

            $overtime_hour = $overtime_info['overtime_hours'];
            $overtime_minutes = $overtime_info['overtime_minutes'];
            if ($overtime_hour > 0) {
                $ov_hours_ammount = $overtime_hour * $employee_info->overtime_salary;
            } else {
                $ov_hours_ammount = 0;
            }
            if ($overtime_minutes > 0) {
                $ov_amount = round($employee_info->overtime_salary / 60, 2);
                $ov_minutes_ammount = $overtime_minutes * $ov_amount;
            } else {
                $ov_minutes_ammount = 0;
            }
            $overtime_amount = $ov_hours_ammount + $ov_minutes_ammount;
            $salary_payment_details_label[] = lang('overtime_amount');
            $salary_payment_details_value[] = $overtime_amount;
        }
// ************ Save all Advance Salary info **********
// get all advance salary info by month and employee id
        $advance_salary = $this->get_advance_salary_info_by_id($data['user_id'], $data['payment_month']);
        if ($advance_salary['advance_amount']) {
            $salary_payment_deduction_label[] = lang('advance_amount');
            $salary_payment_deduction_value[] = $advance_salary['advance_amount'];
            $advance_salary_info = $this->payroll_model->check_by(array('user_id' => $data['user_id'], 'deduct_month' => $data['payment_month']), 'tbl_advance_salary');
            if (!empty($advance_salary_info)) {
                $this->payroll_model->_table_name = "tbl_advance_salary"; // table name
                $this->payroll_model->_primary_key = "advance_salary_id"; // $id
                $advnce_slry_date['status'] = 3;
                $this->payroll_model->save($advnce_slry_date, $advance_salary_info->advance_salary_id);
            }
        }
// ************ Save all Hourly info **********
// check hourly payment info
// if exist count total hours in a month
// get hourly payment info by id
        if (!empty($employee_info->hourly_rate_id)) {
			$total_hours = $this->get_total_hours_in_month($data['user_id'], $data['payment_month'], $payment_start_date, $payment_end_date);
            $salary_payment_details_label[] = lang('hourly_grade');
            $salary_payment_details_value[] = $employee_info->hourly_grade;

            $salary_payment_details_label[] = 'hourly_rates';
            $salary_payment_details_value[] = $employee_info->hourly_rate;

            $salary_payment_details_label[] = lang('total_hour');
            $salary_payment_details_value[] = $total_hours['total_hours'] . ':' . $total_hours['total_minutes'];

            $total_hour = $total_hours['total_hours'];
            $total_minutes = $total_hours['total_minutes'];
            if ($total_hour > 0) {
                $hours_ammount = $total_hour * $employee_info->hourly_rate;
            } else {
                $hours_ammount = 0;
            }
            if ($total_minutes > 0) {
                $amount = round($employee_info->hourly_rate / 60, 2);
                $minutes_ammount = $total_minutes * $amount;
            } else {
                $minutes_ammount = 0;
            }
            $total_hours_amount = $hours_ammount + $minutes_ammount;
            $salary_payment_details_label[] = lang('amount');
            $salary_payment_details_value[] = $total_hours_amount;
        }
// get award info by employee id and payment date
        $this->payroll_model->_table_name = 'tbl_employee_award';
        $this->payroll_model->_order_by = 'user_id';
        $award_info = $this->payroll_model->get_by(array('user_id' => $data['user_id'], 'given_date>=' => $payment_start_date, 'given_date<=' => $payment_end_date), FALSE);
        if (!empty($award_info)) {
            foreach ($award_info as $v_award_info) {
                $salary_payment_details_label[] = lang('award_name') . '
<small> ( ' . $v_award_info->award_name . ' )</small>';
                $salary_payment_details_value[] = $v_award_info->award_amount;
            }
        }
        if (!empty($salary_payment_details_label)) {
            foreach ($salary_payment_details_label as $key => $payment_label) {
                $details_data['salary_payment_details_label'] = $payment_label;
                $details_data['salary_payment_details_value'] = $salary_payment_details_value[$key];

//  save into tbl employee paymenet
                $this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
                $this->payroll_model->_primary_key = "salary_payment_details_id"; // $id
                $this->payroll_model->save($details_data);
            }
        }
        if (!empty($salary_payment_deduction_label)) {
            foreach ($salary_payment_deduction_label as $dkey => $deduction_label) {
                $ddetails_data['salary_payment_id'] = $details_data['salary_payment_id'];
                $ddetails_data['salary_payment_deduction_label'] = $deduction_label;
                $ddetails_data['salary_payment_deduction_value'] = $salary_payment_deduction_value[$dkey];

//  save into tbl employee paymenet
                $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
                $this->payroll_model->_primary_key = "salary_payment_deduction_id"; // $id
                $this->payroll_model->save($ddetails_data);
            }
        }
        if (!empty($employee_info->hourly_rate_id) || !empty($employee_info->monthly_template_id)) {

            $deduct_from_account = $this->input->post('deduct_from_account', true);
            if (!empty($deduct_from_account)) {
                $account_id = $this->input->post('account_id', true);
                if (empty($account_id)) {
                    $account_id = config_item('default_account');
                }
                if (!empty($account_id)) {
                    $reference = lang('salary_month') . ' : ' . date('F Y', strtotime($data['payment_month'])) . ' ' . lang('salary_payment') . ' ' . lang('for') . ' ' . $employee_info->fullname . ' ' . lang('and') . ' ' . lang('comments') . ': ' . $data['comments'];
// save into tbl_transaction
                    $tr_data = array(
                        'name' => lang('salary_payment') . ' ' . lang('for') . ' ' . $employee_info->fullname,
                        'type' => 'Expense',
                        'amount' => $this->input->post('payment_amount', TRUE),
                        'debit' => $this->input->post('payment_amount', TRUE),
                        'date' => date('Y-m-d'),
                        'paid_by' => '0',
                        'payment_methods_id' => $this->input->post('payment_type', TRUE),
                        'reference' => lang('salary_month') . ' ' . $this->input->post('payment_month'),
                        'notes' => lang('this_expense_from_salary_payment', $reference),
                        'permission' => 'all',
                    );
                    $account_info = $this->payroll_model->check_by(array('account_id' => $account_id), 'tbl_accounts');
                    if (!empty($account_info)) {
                        $ac_data['balance'] = $account_info->balance - $tr_data['amount'];
                        $this->payroll_model->_table_name = "tbl_accounts"; //table name
                        $this->payroll_model->_primary_key = "account_id";
                        $this->payroll_model->save($ac_data, $account_info->account_id);

                        $aaccount_info = $this->payroll_model->check_by(array('account_id' => $account_id), 'tbl_accounts');
                        $tr_data['total_balance'] = $aaccount_info->balance;
                        $tr_data['account_id'] = $account_id;
// save into tbl_transaction
                        $this->payroll_model->_table_name = "tbl_transactions"; //table name
                        $this->payroll_model->_primary_key = "transactions_id";
                        $return_id = $this->payroll_model->save($tr_data);

// save into activities
                        $activities = array(
                            'user' => $this->session->userdata('user_id'),
                            'module' => 'transactions',
                            'module_field_id' => $return_id,
                            'activity' => 'activity_new_expense',
                            'icon' => 'fa-building-o',
                            'link' => 'admin/transactions/view_details/' . $return_id,
                            'value1' => $account_info->account_name,
                            'value2' => $this->input->post('payment_amount', TRUE),
                        );
// Update into tbl_project
                        $this->payroll_model->_table_name = "tbl_activities"; //table name
                        $this->payroll_model->_primary_key = "activities_id";
                        $this->payroll_model->save($activities);

                        $this->payroll_model->_table_name = "tbl_salary_payment"; // table name
                        $this->payroll_model->_primary_key = "salary_payment_id"; // $id
                        $deduct_account['deduct_from'] = $account_id;
                        $this->payroll_model->save($deduct_account, $details_data['salary_payment_id']);
                    }
                }
            }
// save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'payroll',
                'module_field_id' => $id,
                'activity' => 'activity_make_payment',
                'icon' => 'fa-list-ul',
                'value1' => $employee_info->fullname,
                'value2' => date('F Y', strtotime($data['payment_month'])),
            );
// Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);
        }

        $type = 'success';
        $message = lang('payment_information_update');
        set_message($type, $message);
        redirect('admin/payroll/make_payment/0/' . $employee_info->departments_id . '/' . $data['payment_month']);
    }

    public function salary_payment_details($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {
        $data['title'] = lang('manage_salary_details') . ' ' . lang('details');

        $data['salary_payment_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);

        $this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//gross pay
		$this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_grosspay_details'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}

       // $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
       // $this->payroll_model->_order_by = "salary_payment_id"; // $id
        //$data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);	
		
		//$tax = $this->db->where('salary_template_id', $salary_payment_id)->get('tbl_employee_salary_tax')->result();
		//$data['tax_info'] = $tax;
		
		//$tax = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_tax')->result();
		//$data['tax_info'] = $tax;
		
		//$companyContribution = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_contribution')->result();
		//$data['contribution_info'] = $companyContribution; 
		
		
		//$otherBenefit = $this->db->where('salary_payment_id', $salary_payment_id)->get('tbl_salary_payment_benifit')->result();
		//$data['benefit_info'] = $otherBenefit;
		
		//echo"<pre>"; print_r($data); 
        $data['subview'] = $this->load->view('admin/payroll/salary_payment_details', $data, FALSE);
        $this->load->view('admin/_layout_modal_lg', $data);
    }

    public function salary_payment_details_pdf($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {

        $data['salary_payment_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);

        $this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}

        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
// get all employee salary info  by id
        $viewfile = $this->load->view('admin/payroll/salary_payment_details_pdf', $data, TRUE);

        $this->load->helper('dompdf');
        pdf_create($viewfile, slug_it(lang('salary_details') . '- ' . $data['salary_payment_info']->fullname));
    }
	
	
	public function pay_slip_pdf($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {
		// check existing_recept_no by where
        $where = array('salary_payment_id' => $salary_payment_id);
        $check_existing_recipt_no = $this->payroll_model->check_by($where, 'tbl_salary_payslip');

        if (!empty($check_existing_recipt_no)) {
            $data['payslip_number'] = $check_existing_recipt_no->payslip_number;
        } else {
            $this->payroll_model->_table_name = "tbl_salary_payslip"; //table name
            $this->payroll_model->_primary_key = "payslip_id";
            $payslip_id = $this->payroll_model->save($where);

            $pdata['payslip_number'] = date('Ym') . $payslip_id;
            $this->payroll_model->save($pdata, $payslip_id);

            $payslip_email = config_item('payslip_email');
            if (!empty($payslip_email) && $payslip_email == 1) {
                $this->send_payslip($salary_payment_id, true);
            }
            redirect('admin/payroll/receive_generated/' . $salary_payment_id);

        }
        $data['title'] = lang('generate_payslip');
        $data['employee_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);
		$data['company_name'] = config_item('company_name');
		$data['company_address'] = config_item('company_address');
		
		
		//echo "<pre>"; print_r($data['employee_salary_info']); exit;
		
		$this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		$this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['basic_payment_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
	/*	$this->payroll_model->_table_name = "tbl_user_salary_general_info"; // table name
		$this->payroll_model->_order_by = "id"; //
        $template_info = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id), FALSE);
		*/
		
		if($data['basic_payment_info'][0]->salary_type != 'salary') {
			//hours information
			$this->payroll_model->_table_name = "tbl_employee_hours"; // table name
			$this->payroll_model->_order_by = "id"; // $id
			$data['employee_hours_info'] = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id, 'payment_number' => $data['employee_salary_info']->payment_number), FALSE);
		}
		//echo "<pre>"; print_r($data['employee_hours_info']); exit;
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}

        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//taxes information
		$this->payroll_model->_table_name = "tbl_salary_payment_tax"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['taxes_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//company contribution information
		$this->payroll_model->_table_name = "tbl_salary_payment_contribution"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['contribution_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//benefits information
		$this->payroll_model->_table_name = "tbl_salary_payment_benifit"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['benefits_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		if($data['basic_payment_info'][0]->salary_type != 'salary') {
			$viewfile = $this->load->view('admin/payroll/payslip_info_pdf', $data, TRUE);
		} else {
			$viewfile = $this->load->view('admin/payroll/payslip_info_salaried_pdf', $data, TRUE);
		}

        $this->load->helper('dompdf');
        pdf_create($viewfile, slug_it(lang('salary_details') . '- ' . $data['employee_salary_info']->fullname));

       // $data['subview'] = $this->load->view('admin/payroll/payslip_info', $data, TRUE);
       // $this->load->view('admin/_layout_main', $data);
    }
	
	
	public function send_payslip_toemail($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {
        // check existing_recept_no by where
        $where = array('salary_payment_id' => $salary_payment_id);
        $check_existing_recipt_no = $this->payroll_model->check_by($where, 'tbl_salary_payslip');

        if (!empty($check_existing_recipt_no)) {
            $data['payslip_number'] = $check_existing_recipt_no->payslip_number;
        } 
        $data['title'] = lang('generate_payslip');
        $data['employee_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);
		$data['company_name'] = config_item('company_name');
		$data['company_address'] = config_item('company_address');
		
		
		//echo "<pre>"; print_r($data['employee_salary_info']); exit;
		
		$this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		$this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['basic_payment_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
	/*	$this->payroll_model->_table_name = "tbl_user_salary_general_info"; // table name
		$this->payroll_model->_order_by = "id"; //
        $template_info = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id), FALSE);
		*/
		
		//hours information
		if($data['basic_payment_info'][0]->salary_type == 'wage') {
			$this->payroll_model->_table_name = "tbl_employee_hours"; // table name
			$this->payroll_model->_order_by = "id"; // $id
			$data['employee_hours_info'] = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id, 'payment_number' => $data['employee_salary_info']->payment_number), FALSE);
		}
		//echo "<pre>"; print_r($data['employee_hours_info']); exit;
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}

        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//taxes information
		$this->payroll_model->_table_name = "tbl_salary_payment_tax"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['taxes_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//company contribution information
		$this->payroll_model->_table_name = "tbl_salary_payment_contribution"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['contribution_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//benefits information
		$this->payroll_model->_table_name = "tbl_salary_payment_benifit"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['benefits_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

		if($data['basic_payment_info'][0]->salary_type == 'wage') {
			$viewfile = $this->load->view('admin/payroll/payslip_info_pdf', $data, TRUE);
		} else {
			$viewfile = $this->load->view('admin/payroll/payslip_info_salaried_pdf', $data, TRUE);
		}

      

        $email_template = $this->payroll_model->check_by(array('email_group' => 'payslip_generated_email'), 'tbl_email_templates');

        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $NAME = str_replace("{NAME}", $data['employee_salary_info']->fullname, $message);
		$pay_period = date('Y-m-d', $payment_start_date)." - ".date('Y-m-d', $payment_end_date);
        $month_year = str_replace("{PAY_PERIOD}", $pay_period, $NAME);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $month_year);

        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;
        $params['message'] = $message;
       
		
		$this->load->helper('dompdf');
		$file_name = slug_it(lang('salary_details') . '- ' . $data['employee_salary_info']->fullname).time();
        $resourceed_file = pdf_create($viewfile, $file_name, TRUE,'',TRUE);
		$params['resourceed_file'] = base_url().'uploads/'.$file_name.'.pdf';
		
		//echo $params['resourceed_file']; exit;
        $login_info = $this->payroll_model->check_by(array('user_id' => $data['employee_salary_info']->user_id), 'tbl_users');
       // $params['recipient'] = $login_info->email;
		$params['recipient'] = "mukesh.s@dotsquares.com";

        $result = $this->payroll_model->send_email($params);

        if (!empty($result)) {
// save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'payroll',
                'module_field_id' => $salary_payment_id,
                'activity' => 'activity_payslip_send',
                'icon' => 'fa-list-ul',
                'value1' => $data['employee_salary_info']->fullname,
                'value2' => date('F Y', strtotime($data['employee_salary_info']->payment_month)),
            );
// Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            $message = lang('payslip_information_successfully_send');
        } else {
            $type = 'error';
            $message = lang('something_went_wrong');
        }
        if ($auto) {
            return true;
        } else {
            set_message($type, $message);
            redirect('admin/payroll/make_payment');
        }

    }

    public function generate_payslip()
    {
        $data['title'] = "Generate Payslip";
        // retrive all data from department table
        $this->payroll_model->_table_name = "tbl_departments"; //table name
        $this->payroll_model->_order_by = "departments_id";
        $data['all_department_info'] = $this->payroll_model->get();
        $flag = $this->input->post('flag', TRUE);
        if (!empty($flag)) { // check employee id is empty or not
            $data['flag'] = 1;
            $data['departments_id'] = $this->input->post('departments_id', TRUE);
            $data['payment_month'] = $this->input->post('payment_month', TRUE);
			
			$payment_year_arr = explode("-", $data['payment_month']);
				$data['payment_year'] = $payment_year_arr[0];

            // get all designation info by Department id
            $this->payroll_model->_table_name = 'tbl_designations';
            $this->payroll_model->_order_by = 'designations_id';
			if($data['departments_id'] == 'all') {
				$designation_info = $this->payroll_model->get();
			} else {
				$designation_info = $this->payroll_model->get_by(array('departments_id' => $data['departments_id']), FALSE);
			}
            
            if (!empty($designation_info)) {
                    foreach ($designation_info as $v_designatio) {
                        $data['employee_info'][] = $this->payroll_model->get_emp_salary_list('', $v_designatio->designations_id);
						
						$employee_info = $this->payroll_model->get_emp_salary_list('', $v_designatio->designations_id);
						
						foreach ($employee_info as $value) {
						//echo "<pre>"; print_r($value);
						
						/*
						* payroll biweekly calculation code
						* payroll biweekly calculation code
						*/
						$payroll_cal_day=config_item('payment_day');
						
						date_default_timezone_set('America/Los_Angeles');
						$start = new DateTime(date("Y-m-d", strtotime("first ".$payroll_cal_day." 2018-12")));
						$customer_payment_dates = array();
						
						$payment_start_day_of_year = $data['payment_year']."-01"."-01";
						$last_day_of_year = $data['payment_year']."-12"."-31";
						
						for($i=0;$i<27;$i++) {
						
							//if(strtotime($payment_start_day_of_year) <= strtotime($last_day_of_year)){
								
								$target = new DateTime($payment_start_day_of_year);                      
								 
								/* -- Begin of new code --*/
								$targetPayment = clone $target;
								
								$intervalBetweenTargetAndStart = $target->diff($start);
								
								$daysBeforePayment = 14 - $intervalBetweenTargetAndStart->days % 14;
						
								$targetPayment->modify("+".$daysBeforePayment." days");
								
								$PaymentStartDate = clone $targetPayment;
								$PaymentStartDate->modify("-14 days");
								
								$customer_payment_dates[$targetPayment->format('Y').'-'.$targetPayment->format('m')][$i]['payment_start_date'] = $PaymentStartDate->format('Y-m-d');
								$customer_payment_dates[$targetPayment->format('Y').'-'.$targetPayment->format('m')][$i]['payment_date'] = $targetPayment->format('D, d M Y');
								$payment_start_day_of_year = $targetPayment->format('Y-m-d');
								
							//}
						}
						
						

						$data['customer_payment_dates'][$value->user_id] = $customer_payment_dates;
						//echo "<pre>";
						//print_r($data['customer_payment_dates'][$value->user_id]);
						//exit;
						/*
						* payroll biweekly calculation code end
						*/
							
							// get all allowance info by salary template id
							//echo "<pre>"; print_r($value); exit;
                            if (!empty($value->monthly_template_id)) {
                                $data['allowance_info'][$value->user_id] = $this->get_allowance_info_by_id($value->monthly_template_id, $value->user_id, 'salary');
								// get all deduction info by salary template id
                                $data['deduction_info'][$value->user_id] = $this->get_deduction_info_by_id($value->monthly_template_id, $value->user_id, 'salary');
								// get all overtime info by month and employee id
								
								//for biweekly payment
								$pay_arr = $data['customer_payment_dates'][$value->user_id][$data['payment_month']];
								//echo "<pre>"; print_r($pay_arr); exit;
								foreach($pay_arr as $key1=>$val1) {
									$overtime_end_payment_date = date('Y-m-d', strtotime($val1['payment_date']));
									$data['overtime_info'][$value->user_id][$key1+1] = $this->get_overtime_info_by_id($value->user_id, $data['payment_month'], $val1['payment_start_date'], $overtime_end_payment_date);
								}
							}
							// get all advance salary info by month and employee id
                            $data['advance_salary'][$value->user_id] = $this->get_advance_salary_info_by_id($value->user_id, $data['payment_month']);
							// get award info by employee id and payment month by monthly
                           // $data['award_info'][$value->user_id] = $this->get_award_info_by_id($value->user_id, $data['payment_month']);

						   // get award info by employee id for the biweekly
						   //for biweekly payment
							$pay_arr = $data['customer_payment_dates'][$value->user_id][$data['payment_month']];
							//echo "<pre>"; print_r($pay_arr); exit;
							foreach($pay_arr as $key1=>$val1) {
								$award_end_payment_date = date('Y-m-d', strtotime($val1['payment_date']));
								$data['award_info'][$value->user_id][$key1+1] = $this->get_award_info_by_id($value->user_id, $data['payment_month'], $val1['payment_start_date'], $award_end_payment_date);
							}
							// check hourly payment info
							// if exist count total hours in a month
							// get hourly payment info by id
                            if (!empty($value->hourly_rate_id)) {
                                //for monthly payment
								$data['total_hours'][$value->user_id] = $this->get_total_hours_in_month($value->user_id, $data['payment_month']);
								
								//for biweekly payment
								$pay_arr = $data['customer_payment_dates'][$value->user_id][$data['payment_month']];
								
								foreach($pay_arr as $key1=>$val1) {
									$end_payment_date = date('Y-m-d', strtotime($val1['payment_date']));
									$data['total_hours_biweekly'][$value->user_id][$key1+1] = $this->get_total_hours_in_month($value->user_id, $data['payment_month'], $val1['payment_start_date'], $end_payment_date);
								}
							}
                        }
                    }
                }
        }

        $data['subview'] = $this->load->view('admin/payroll/generate_payslip', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function receive_generated($salary_payment_id, $pay_frequency = NULL, $payment_start_date = NULL, $payment_end_date = NULL)
    {
		// check existing_recept_no by where
        $where = array('salary_payment_id' => $salary_payment_id);
        $check_existing_recipt_no = $this->payroll_model->check_by($where, 'tbl_salary_payslip');

        if (!empty($check_existing_recipt_no)) {
            $data['payslip_number'] = $check_existing_recipt_no->payslip_number;
        } else {
            $this->payroll_model->_table_name = "tbl_salary_payslip"; //table name
            $this->payroll_model->_primary_key = "payslip_id";
            $payslip_id = $this->payroll_model->save($where);

            $pdata['payslip_number'] = date('Ym') . $payslip_id;
            $this->payroll_model->save($pdata, $payslip_id);

            $payslip_email = config_item('payslip_email');
            if (!empty($payslip_email) && $payslip_email == 1) {
                $this->send_payslip($salary_payment_id, true);
            }
            redirect('admin/payroll/receive_generated/' . $salary_payment_id);

        }
        $data['title'] = lang('generate_payslip');
        $data['employee_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);
		$data['company_name'] = config_item('company_name');
		$data['company_address'] = config_item('company_address');
		
		
		//echo "<pre>"; print_r($data['employee_salary_info']); exit;
		
		$this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		$this->payroll_model->_table_name = "tbl_salary_payment"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['basic_payment_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		/*
		$this->payroll_model->_table_name = "tbl_user_salary_general_info"; // table name
		$this->payroll_model->_order_by = "id"; //
        $template_info = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id), FALSE);
		*/
		
		//echo "<pre>"; print_r($template_info); exit;
		
		//hours information
		if($data['basic_payment_info'][0]->salary_type != 'salary') {
			$this->payroll_model->_table_name = "tbl_employee_hours"; // table name
			$this->payroll_model->_order_by = "id"; // $id
			$data['employee_hours_info'] = $this->payroll_model->get_by(array('user_id' => $data['employee_salary_info']->user_id, 'payment_number' => $data['employee_salary_info']->payment_number), FALSE);
		} 
		//echo "<pre>"; print_r($data['employee_hours_info']); exit;
		//for different pay frequency
		if($pay_frequency != NULL) {
			$data['other_payment_details']['pay_frequency'] = $pay_frequency;
		}
		if($payment_start_date != NULL) {
			$data['other_payment_details']['payment_start_date'] = $payment_start_date;
		}
		if($payment_end_date != NULL) {
			$data['other_payment_details']['payment_end_date'] = $payment_end_date;
		}
		
        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//taxes information
		$this->payroll_model->_table_name = "tbl_salary_payment_tax"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['taxes_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//company contribution information
		$this->payroll_model->_table_name = "tbl_salary_payment_contribution"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['contribution_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
		
		//benefits information
		$this->payroll_model->_table_name = "tbl_salary_payment_benifit"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['benefits_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

		if($data['basic_payment_info'][0]->salary_type == 'salary') { 
			$data['subview'] = $this->load->view('admin/payroll/payslip_info_salaried', $data, TRUE);
		} else {
			$data['subview'] = $this->load->view('admin/payroll/payslip_info', $data, TRUE);
		}
		
        $this->load->view('admin/_layout_main', $data);
    }

    public function send_payslip($salary_payment_id, $auto = null)
    {
        $where = array('salary_payment_id' => $salary_payment_id);
        $check_existing_recipt_no = $this->payroll_model->check_by($where, 'tbl_salary_payslip');
        $data['payslip_number'] = $check_existing_recipt_no->payslip_number;

        $data['employee_salary_info'] = $this->payroll_model->get_salary_payment_info($salary_payment_id);

        $this->payroll_model->_table_name = "tbl_salary_payment_details"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['salary_payment_details_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_allowance"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['allowance_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);

        $this->payroll_model->_table_name = "tbl_salary_payment_deduction"; // table name
        $this->payroll_model->_order_by = "salary_payment_id"; // $id
        $data['deduction_info'] = $this->payroll_model->get_by(array('salary_payment_id' => $salary_payment_id), FALSE);
// get all employee salary info  by id
        $viewfile = $this->load->view('admin/payroll/payslip_info', $data, TRUE);

        $email_template = $this->payroll_model->check_by(array('email_group' => 'payslip_generated_email'), 'tbl_email_templates');

        $message = $email_template->template_body;
        $subject = $email_template->subject;

        $NAME = str_replace("{NAME}", $data['employee_salary_info']->fullname, $message);
        $month_year = str_replace("{MONTH_YEAR}", date('F  Y', strtotime($data['employee_salary_info']->payment_month)), $NAME);
        $message = str_replace("{SITE_NAME}", config_item('company_name'), $month_year);

        $data['message'] = $message;
        $message = $this->load->view('email_template', $data, TRUE);

        $params['subject'] = $subject;
        $params['message'] = $message;
        $params['resourceed_file'] = $viewfile;

        $login_info = $this->payroll_model->check_by(array('user_id' => $data['employee_salary_info']->user_id), 'tbl_users');
        $params['recipient'] = $login_info->email;

        $result = $this->payroll_model->send_email($params);

        if (!empty($result)) {
// save into activities
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'payroll',
                'module_field_id' => $salary_payment_id,
                'activity' => 'activity_payslip_send',
                'icon' => 'fa-list-ul',
                'value1' => $data['employee_salary_info']->fullname,
                'value2' => date('F Y', strtotime($data['employee_salary_info']->payment_month)),
            );
// Update into tbl_project
            $this->payroll_model->_table_name = "tbl_activities"; //table name
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            $message = lang('payslip_information_successfully_send');
        } else {
            $type = 'error';
            $message = lang('something_went_wrong');
        }
        if ($auto) {
            return true;
        } else {
            set_message($type, $message);
            redirect('admin/payroll/make_payment');
        }

    }

    public function provident_fund()
    {
        $data['title'] = "Provident Found Details";
// active check with current month
        $data['current_month'] = date('m');

        if ($this->input->post('year', TRUE)) { // if input year
            $data['year'] = $this->input->post('year', TRUE);
        } else { // else current year
            $data['year'] = date('Y'); // get current year
        }
// get all expense list by year and month
        $data['provident_fund_info'] = $this->get_provident_fund_info($data['year']);

        $data['subview'] = $this->load->view('admin/payroll/provident_fund_info', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function get_provident_fund_info($year, $month = NULL)
    {// this function is to create get monthy recap report
        if (!empty($month)) {
            if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                $start_date = $year . "-" . '0' . $month;
                $end_date = $year . "-" . '0' . $month;
            } else {
                $start_date = $year . "-" . $month;
                $end_date = $year . "-" . $month;
            }
            $provident_fund_info = $this->payroll_model->get_provident_fund_info_by_date($start_date, $end_date); // get all report by start date and in date
        } else {
            for ($i = 1; $i <= 12; $i++) { // query for months
                if ($i >= 1 && $i <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
                    $start_date = $year . "-" . '0' . $i;
                    $end_date = $year . "-" . '0' . $i;
                } else {
                    $start_date = $year . "-" . $i;
                    $end_date = $year . "-" . $i;
                }
                $provident_fund_info[$i] = $this->payroll_model->get_provident_fund_info_by_date($start_date, $end_date); // get all report by start date and in date
            }
        }

        return $provident_fund_info; // return the result
    }

    public function provident_fund_pdf($year, $month)
    {

        $data['provident_fund_info'] = $this->get_provident_fund_info($year, $month);

        $month_name = date('F', strtotime($year . '-' . $month)); // get full name of month by date query
        $data['monthyaer'] = $month_name . '  ' . $year;

        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/payroll/provident_fund_pdf', $data, TRUE);
        pdf_create($viewfile, slug_it(lang('provident_found_report') . ' - ' . $data['monthyaer']));
    }

    public function payroll_summary()
    {
        $data['title'] = lang('payroll_summary');
        $search_type = $this->input->post('search_type', true);
        if (!empty($search_type)) {
            $data['search_type'] = $search_type;
            if ($search_type == 'employee') {
                $data['user_id'] = $this->input->post('user_id', true);
            }
            if ($search_type == 'month') {
                $data['by_month'] = $this->input->post('by_month', true);
            }
            if ($search_type == 'period') {
                $data['start_month'] = $this->input->post('start_month', true);
                $data['end_month'] = $this->input->post('end_month', true);
            }
			if ($search_type == 'allemployee') {
               $data['user_id'] = '';
            }
			
        }
        $data['subview'] = $this->load->view('admin/payroll/payroll_summary', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function payment_historyList($user_id = null)
    {
		//echo $user_id; die;
		if (!empty($user_id)) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_salary_payment';
            $this->datatables->join_table = array('tbl_account_details', 'tbl_designations', 'tbl_departments');
            $this->datatables->join_where = array('tbl_salary_payment.user_id = tbl_account_details.user_id', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'tbl_departments.departments_id  = tbl_designations.departments_id');
            $this->datatables->column_order = array('tbl_account_details.fullname', 'tbl_account_details.employment_id','tbl_account_details.payment_type', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->column_search = array('tbl_account_details.fullname', 'tbl_account_details.employment_id', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->order = array('salary_payment_id' => 'desc');

            $where = array('tbl_salary_payment.user_id' => $user_id);
            $all_payment_history = make_datatables($where);
            //echo"<pre>"; print_r($all_payment_history); die;
            $pdata = array();
            foreach ($all_payment_history as $p_key => $v_history) {
				//echo"<pre>"; print_r($v_history); die;
                if (!empty($v_history)) {
					$user_id = $v_history->user_id;
					$salary_template_id = $v_history->salary_payment_id;
					$payment_start_date = $v_history->payment_start_date;
					$payment_end_date = $v_history->payment_end_date;
					
                    // $salary_payment_history = get_result('tbl_salary_payment_details', array('salary_payment_id' => $salary_template_id));
					
					$salary_payment_history = $this->db->where('user_id', $user_id)->where('payment_start_date', $payment_start_date)->where('payment_end_date', $payment_end_date)->get('tbl_salary_payment')->result();
					
                    $total_salary_amount = 0;
                    if (!empty($salary_payment_history)) {
                        foreach ($salary_payment_history as $v_payment_history) {
							//echo"<pre>"; print_r($v_payment_history); die;
                            /* if (is_numeric($v_payment_history->salary_payment_details_value)) {
                                if ($v_payment_history->salary_payment_details_label == 'overtime_salary') {
                                    $rate = $v_payment_history->salary_payment_details_value;
                                } elseif ($v_payment_history->salary_payment_details_label == 'hourly_rates') {
                                    $rate = $v_payment_history->salary_payment_details_value;
                                }
                                $total_salary_amount += $v_payment_history->salary_payment_details_value;
                            } */
							
							$total_salary_amount += $v_payment_history->grossPay;
                        }
                    }
					  
					
                    /* $salary_allowance_info = get_result('tbl_salary_payment_allowance', array('salary_payment_id' => $v_history->salary_payment_id));
					
                    $total_allowance = 0;
                    if (!empty($salary_allowance_info)) {
                        foreach ($salary_allowance_info as $v_salary_allowance_info) {
                            $total_allowance += $v_salary_allowance_info->salary_payment_allowance_value;
                        }
                    } */
                    if (empty($rate)) {
                        $rate = 0;
                    }
					
                    //$salary_deduction_info = get_result('tbl_salary_payment_deduction', array('salary_payment_id' => $v_history->salary_payment_id));
					
                    $salary_deduction_info = $this->db->where('salary_payment_id', $v_history->salary_payment_id)->get('tbl_salary_payment_deduction')->result();
					
					$total_deduction = 0;
                    if (!empty($salary_deduction_info)) {
                        foreach ($salary_deduction_info as $v_salary_deduction_info) {
                            $total_deduction += $v_salary_deduction_info->salary_payment_deduction_value;
                        }
                    }
					
					$salary_tax_info = $this->db->where('salary_payment_id', $v_history->salary_payment_id)->get('tbl_salary_payment_tax')->result();
					
					$total_tax = 0;
                    if (!empty($salary_tax_info)) {
                        foreach ($salary_tax_info as $v_salary_tax_info) {
                            $total_tax += $v_salary_tax_info->salary_payment_tax_value;
                        }
                    }
					
                     $deduction = $total_tax + $total_deduction + $rate;
					
                    $total_paid_amount = $total_salary_amount - $deduction;
					
					
                    // echo "<pre>"; print_r($v_history); die;
                    $action = null;
                    $psub_array = array();
                    $psub_array[] = date('jS F-Y', strtotime($v_history->payment_start_date)).'-'.date('jS F-Y', strtotime($v_history->payment_end_date));
					$psub_array[] = $v_history->salary_type;
                    $psub_array[] = display_date($v_history->paid_date);
                    $psub_array[] = display_money($v_history->grossPay, default_currency());
                    //$psub_array[] = display_money($total_deduction, default_currency());
                    //$psub_array[] = display_money($total_deduction, default_currency());
                    $psub_array[] = display_money($net_salary = $total_paid_amount, default_currency());

                    if (!empty($v_history->fine_deduction)) {
                        $fine_deduction = $v_history->fine_deduction;
                    } else {
                        $fine_deduction = 0;
                    }                                      
                    $psub_array[] = display_money($fine_deduction, default_currency());
                    $psub_array[] = display_money($net_salary - $fine_deduction, default_currency());
                    $psub_array[] = '<a href="' . base_url() . 'admin/payroll/salary_payment_details/' . $v_history->salary_payment_id . '"
                               class="btn btn-info btn-xs" title="' . lang('view') . '" data-toggle="modal"
                               data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>';
                    $pdata[] = $psub_array;
                }
            }
			//echo"<pre>"; print_r($pdata); die;
            render_table($pdata, $where);
        } else {
            redirect('admin/dashboard');
        }
    }
	
	public function payroll_payment_historyList($user_id = null)
    {
	//echo $user_id; exit;
        if (!empty($user_id)) {
			/* if($user_id !=='all'){ */
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_salary_payment';
            $this->datatables->join_table = array('tbl_account_details', 'tbl_designations', 'tbl_departments');
            $this->datatables->join_where = array('tbl_salary_payment.user_id = tbl_account_details.user_id', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'tbl_departments.departments_id  = tbl_designations.departments_id');
            $this->datatables->column_order = array('tbl_account_details.fullname', 'tbl_account_details.employment_id','tbl_account_details.payment_type', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->column_search = array('tbl_account_details.fullname', 'tbl_account_details.employment_id', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->order = array('salary_payment_id' => 'desc');
		
            $where = array('tbl_salary_payment.user_id' => $user_id);
            $all_payment_history = make_datatables($where);
		   /* }else{
			  $sql = "SELECT * FROM `tbl_salary_payment` LEFT JOIN `tbl_account_details` ON `tbl_salary_payment`.`user_id` = `tbl_account_details`.`user_id` LEFT JOIN `tbl_designations` ON `tbl_designations`.`designations_id` = `tbl_account_details`.`designations_id` LEFT JOIN `tbl_departments` ON `tbl_departments`.`departments_id` = `tbl_designations`.`departments_id`"; 
			  $all_payment_history = make_datatables($this->db->query($sql));
		   }
			 //echo $sql = $this->db->last_query();
            echo"<pre>"; print_r($all_payment_history);  die; */
            $pdata = array();
            foreach ($all_payment_history as $p_key => $v_history) {
                if (!empty($v_history)) {
                    $salary_payment_history = get_result('tbl_salary_payment_details', array('salary_payment_id' => $v_history->salary_payment_id));
                    $total_salary_amount = 0;
                    if (!empty($salary_payment_history)) {
                        foreach ($salary_payment_history as $v_payment_history) {
                            if (is_numeric($v_payment_history->salary_payment_details_value)) {
                                if ($v_payment_history->salary_payment_details_label == 'overtime_salary') {
                                    $rate = $v_payment_history->salary_payment_details_value;
                                } elseif ($v_payment_history->salary_payment_details_label == 'hourly_rates') {
                                    $rate = $v_payment_history->salary_payment_details_value;
                                }
                                $total_salary_amount += $v_payment_history->salary_payment_details_value;
                            }
                        }
                    }
                    $salary_allowance_info = get_result('tbl_salary_payment_allowance', array('salary_payment_id' => $v_history->salary_payment_id));
                    $total_allowance = 0;
                    if (!empty($salary_allowance_info)) {
                        foreach ($salary_allowance_info as $v_salary_allowance_info) {
                            $total_allowance += $v_salary_allowance_info->salary_payment_allowance_value;
                        }
                    }
					
					//calculate gross pay
					if(isset($v_history->grossPay) && !empty($v_history->grossPay)) {
						$gross_pay = $v_history->grossPay;
					} else {
						$gross_pay = $total_allowance;
					}
					
					
                    if (empty($rate)) {
                        $rate = 0;
                    }
                    $salary_deduction_info = get_result('tbl_salary_payment_deduction', array('salary_payment_id' => $v_history->salary_payment_id));
                    $total_deduction = 0;
                    if (!empty($salary_deduction_info)) {
                        foreach ($salary_deduction_info as $v_salary_deduction_info) {
                            $total_deduction += $v_salary_deduction_info->salary_payment_deduction_value;
                        }
                    }
					
					$salary_taxes_info = get_result('tbl_salary_payment_tax', array('salary_payment_id' => $v_history->salary_payment_id));
               
					if (!empty($salary_taxes_info)) {
						foreach ($salary_taxes_info as $v_salary_taxes_info) {
							$total_deduction += $v_salary_taxes_info->salary_payment_tax_value;
						}
					}

                    $total_paid_amount = $total_salary_amount + $total_allowance - $rate;

                    $action = null;
                    $psub_array = array();
					$psub_array[] = date('F-Y', strtotime($v_history->payment_month));
					$psub_array[] = date('D, d M Y', strtotime($v_history->payment_start_date))." - ".date('D, d M Y', strtotime($v_history->payment_end_date));
					$psub_array[] = date('D, d M Y', strtotime($v_history->paid_date));
                    //$psub_array[] = display_date($v_history->paid_date);
                    $psub_array[] = display_money($gross_pay, default_currency());
                   // $psub_array[] = display_money($total_deduction, default_currency());
                    $psub_array[] = display_money($net_salary = $gross_pay - $total_deduction, default_currency());

                    if (!empty($v_history->fine_deduction)) {
                        $fine_deduction = $v_history->fine_deduction;
                    } else {
                        $fine_deduction = 0;
                    }
                   // $psub_array[] = display_money($fine_deduction, default_currency());
                    $psub_array[] = display_money($net_salary - $fine_deduction, default_currency());
                    $psub_array[] = '<a href="' . base_url() . 'admin/payroll/salary_payment_details/' . $v_history->salary_payment_id . '/biweekly/'.strtotime($v_history->payment_start_date).'/'.strtotime($v_history->payment_end_date).'"
                               class="btn btn-info btn-xs" title="' . lang('view') . '" data-toggle="modal"
                               data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>';
                    $pdata[] = $psub_array;
                }
            }
            render_table($pdata, $where);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function payment_historyMonth($month = null)
    {
        if ($this->input->is_ajax_request()) {

            $this->load->model('datatables');
            $this->datatables->table = 'tbl_salary_payment';
            $this->datatables->join_table = array('tbl_account_details', 'tbl_designations', 'tbl_departments');
            $this->datatables->join_where = array('tbl_salary_payment.user_id = tbl_account_details.user_id', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'tbl_departments.departments_id  = tbl_designations.departments_id');
            $this->datatables->column_order = array('tbl_account_details.fullname', 'tbl_account_details.employment_id', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->column_search = array('tbl_account_details.fullname', 'tbl_account_details.employment_id', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->order = array('salary_payment_id' => 'desc');

            $where = array('tbl_salary_payment.payment_month' => $month);
            $fetch_data = make_datatables($where);

            $data = array();
            foreach ($fetch_data as $_key => $v_payroll) {

                $salary_payment_history = get_result('tbl_salary_payment_details', array('salary_payment_id' => $v_payroll->salary_payment_id));
                $total_salary_amount = 0;
                if (!empty($salary_payment_history)) {
                    foreach ($salary_payment_history as $v_payment_history) {
                        if (is_numeric($v_payment_history->salary_payment_details_value)) {
                            if ($v_payment_history->salary_payment_details_label == 'overtime_salary') {
                                $rate = $v_payment_history->salary_payment_details_value;
                            } elseif ($v_payment_history->salary_payment_details_label == 'hourly_rates') {
                                $rate = $v_payment_history->salary_payment_details_value;
                            }
                            $total_salary_amount += $v_payment_history->salary_payment_details_value;
                        }
                    }
                }
                $salary_allowance_info = get_result('tbl_salary_payment_allowance', array('salary_payment_id' => $v_payroll->salary_payment_id));
                $total_allowance = 0;
                if (!empty($salary_allowance_info)) {
                    foreach ($salary_allowance_info as $v_salary_allowance_info) {
                        $total_allowance += $v_salary_allowance_info->salary_payment_allowance_value;
                    }
                }
				
				//calculate gross pay
				if(isset($v_payroll->grossPay) && !empty($v_payroll->grossPay)) {
					$gross_pay = $v_payroll->grossPay;
				} else {
					$gross_pay = $total_allowance;
				}
				
				
                if (empty($rate)) {
                    $rate = 0;
                }
                $salary_deduction_info = get_result('tbl_salary_payment_deduction', array('salary_payment_id' => $v_payroll->salary_payment_id));
                $total_deduction = 0;
                if (!empty($salary_deduction_info)) {
                    foreach ($salary_deduction_info as $v_salary_deduction_info) {
                        $total_deduction += $v_salary_deduction_info->salary_payment_deduction_value;
                    }
                }
				
				$salary_taxes_info = get_result('tbl_salary_payment_tax', array('salary_payment_id' => $v_payroll->salary_payment_id));
               
                if (!empty($salary_taxes_info)) {
                    foreach ($salary_taxes_info as $v_salary_taxes_info) {
                        $total_deduction += $v_salary_taxes_info->salary_payment_tax_value;
                    }
                }

                $total_paid_amount = $total_salary_amount + $total_allowance - $rate;

                $action = null;
                $sub_array = array();
                $sub_array[] = date('F-Y', strtotime($v_payroll->payment_month));
				$sub_array[] = date('D, d M Y', strtotime($v_payroll->payment_start_date))." - ".date('D, d M Y', strtotime($v_payroll->payment_end_date));
                $sub_array[] = date('D, d M Y', strtotime($v_payroll->paid_date));
                $sub_array[] = display_money($gross_pay, default_currency());
				//$sub_array[] = display_money($total_paid_amount, default_currency());
               // $sub_array[] = display_money($total_deduction, default_currency());
                $sub_array[] = display_money($net_salary = $gross_pay - $total_deduction, default_currency());

                if (!empty($v_payroll->fine_deduction)) {
                    $fine_deduction = $v_payroll->fine_deduction;
                } else {
                    $fine_deduction = 0;
                }
              //  $sub_array[] = display_money($fine_deduction, default_currency());
                $sub_array[] = display_money($net_salary - $fine_deduction, default_currency());
                $sub_array[] = '<a href="' . base_url() . 'admin/payroll/salary_payment_details/' . $v_payroll->salary_payment_id . '/biweekly/'.strtotime($v_payroll->payment_start_date).'/'.strtotime($v_payroll->payment_end_date).'"
                               class="btn btn-info btn-xs" title="' . lang('view') . '" data-toggle="modal"
                               data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>';
                $data[] = $sub_array;
            }

            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function payment_historyPeriod($date = null)
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $date = explode('n', $date);
            $data['start_month'] = $date[0];
            $data['end_month'] = $date[1];
            $this->datatables->table = 'tbl_salary_payment';
            $this->datatables->join_table = array('tbl_account_details', 'tbl_designations', 'tbl_departments');
            $this->datatables->join_where = array('tbl_salary_payment.user_id = tbl_account_details.user_id', 'tbl_designations.designations_id  = tbl_account_details.designations_id', 'tbl_departments.departments_id  = tbl_designations.departments_id');
            $this->datatables->column_order = array('tbl_account_details.fullname', 'tbl_account_details.employment_id', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->column_search = array('tbl_account_details.fullname', 'tbl_account_details.employment_id', 'tbl_salary_payment.comments', 'tbl_salary_payment.payment_type', 'tbl_salary_payment.payment_month', 'tbl_salary_payment.fine_deduction', 'tbl_salary_payment.paid_date');
            $this->datatables->order = array('salary_payment_id' => 'desc');

            $where = array('tbl_salary_payment.payment_month >=' => $date[0], 'tbl_salary_payment.payment_month <=' => $date[1]);
            $fetch_data = make_datatables($where);
            $data = array();
			
			//echo"<pre>";  print_r($fetch_data); die;
			
            foreach ($fetch_data as $_key => $v_payroll) {

                $salary_payment_history = get_result('tbl_salary_payment_details', array('salary_payment_id' => $v_payroll->salary_payment_id));
                $total_salary_amount = 0;
                if (!empty($salary_payment_history)) {
                    foreach ($salary_payment_history as $v_payment_history) {
                        if (is_numeric($v_payment_history->salary_payment_details_value)) {
                            if ($v_payment_history->salary_payment_details_label == 'overtime_salary') {
                                $rate = $v_payment_history->salary_payment_details_value;
                            } elseif ($v_payment_history->salary_payment_details_label == 'hourly_rates') {
                                $rate = $v_payment_history->salary_payment_details_value;
                            }
                            $total_salary_amount += $v_payment_history->salary_payment_details_value;
                        }
                    }
                }
                $salary_allowance_info = get_result('tbl_salary_payment_allowance', array('salary_payment_id' => $v_payroll->salary_payment_id));
                $total_allowance = 0;
                if (!empty($salary_allowance_info)) {
                    foreach ($salary_allowance_info as $v_salary_allowance_info) {
                        $total_allowance += $v_salary_allowance_info->salary_payment_allowance_value;
                    }
                }
				
				//calculate gross pay
				if(isset($v_payroll->grossPay) && !empty($v_payroll->grossPay)) {
					$gross_pay = $v_payroll->grossPay;
				} else {
					$gross_pay = $total_allowance;
				}
				
				
                if (empty($rate)) {
                    $rate = 0;
                }
                $salary_deduction_info = get_result('tbl_salary_payment_deduction', array('salary_payment_id' => $v_payroll->salary_payment_id));
                $total_deduction = 0;
                if (!empty($salary_deduction_info)) {
                    foreach ($salary_deduction_info as $v_salary_deduction_info) {
                        $total_deduction += $v_salary_deduction_info->salary_payment_deduction_value;
                    }
                }
				
				$salary_taxes_info = get_result('tbl_salary_payment_tax', array('salary_payment_id' => $v_payroll->salary_payment_id));
               
                if (!empty($salary_taxes_info)) {
                    foreach ($salary_taxes_info as $v_salary_taxes_info) {
                        $total_deduction += $v_salary_taxes_info->salary_payment_tax_value;
                    }
                }

                $total_paid_amount = $total_salary_amount + $total_allowance - $rate;

                $action = null;
                $sub_array = array();
                $sub_array[] = date('F-Y', strtotime($v_payroll->payment_month));
                $sub_array[] = date('F-Y', strtotime($v_payroll->payment_month));
				$sub_array[] = date('D, d M Y', strtotime($v_payroll->payment_start_date))." - ".date('D, d M Y', strtotime($v_payroll->payment_end_date));
                $sub_array[] = date('D, d M Y', strtotime($v_payroll->paid_date));
                $sub_array[] = display_money($gross_pay, default_currency());
              //  $sub_array[] = display_money($total_deduction, default_currency());
                $sub_array[] = display_money($net_salary = $gross_pay - $total_deduction, default_currency());

                if (!empty($v_payroll->fine_deduction)) {
                    $fine_deduction = $v_payroll->fine_deduction;
                } else {
                    $fine_deduction = 0;
                }
              //  $sub_array[] = display_money($fine_deduction, default_currency());
                $sub_array[] = display_money($net_salary - $fine_deduction, default_currency());
                $sub_array[] = '<a href="' . base_url() . 'admin/payroll/salary_payment_details/' . $v_payroll->salary_payment_id . '/biweekly/'.strtotime($v_payroll->payment_start_date).'/'.strtotime($v_payroll->payment_end_date).'"
                               class="btn btn-info btn-xs" title="' . lang('view') . '" data-toggle="modal"
                               data-target="#myModal_lg"><span class="fa fa-list-alt"></span></a>';
                $data[] = $sub_array;
            }

            render_table($data);
        } else {
            redirect('admin/dashboard');
        }
    }

    public function payroll_summary_pdf($search_type, $pdf)
    {
        $data['title'] = lang('payroll_summary');
        if (!empty($search_type)) {
            $data['search_type'] = $search_type;
            if ($search_type == 'employee') {
                $data['user_id'] = $pdf;
                $user_info = $this->db->where('user_id', $pdf)->get('tbl_account_details')->row();
                $data['by'] = ' - ' . ' ' . ' ' . $user_info->fullname;
                $data['employee_payroll'] = $this->payroll_model->get_salary_payment_info($data['user_id'], true, 'employee');
            }
            if ($search_type == 'month') {
                $data['by_month'] = $pdf;
                $data['by'] = ' - ' . ' ' . date('F-Y', strtotime($pdf));
                $data['employee_payroll'] = $this->payroll_model->get_salary_payment_info($data['by_month'], true, 'month');
            }
            if ($search_type == 'period') {
                $date = explode('n', $pdf);
                $data['start_month'] = $date[0];
                $data['end_month'] = $date[1];
                $data['employee_payroll'] = $this->payroll_model->get_salary_payment_info($data, true, 'period');
                $data['by'] = ' - ' . ' ' . date('F-Y', strtotime($date[0])) . ' ' . lang('TO') . ' ' . date('F-Y', strtotime($date[1]));
            }
        }
        $this->load->helper('dompdf');
        $viewfile = $this->load->view('admin/payroll/payroll_summary_pdf', $data, TRUE);
        pdf_create($viewfile, slug_it(lang('payroll_summary') . ' ' . $data['by']));
    }

    public function clear_activities()
    {
        $this->payroll_model->_table_name = "tbl_activities"; //table name
        $this->payroll_model->delete_multiple(array('module' => 'payroll'));
        if (empty($_SERVER['HTTP_REFERER'])) {
            redirect('admin/dashboard');
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
	
	public function manage_salary_info($userID,$dptId = NULL)
    {
		//echo $dptId;die;
        $data['title'] = lang('Manage Salary Information');
        $data['employee_info'] = $this->db->where('user_id', $userID)->get('tbl_account_details')->result();		
        $data['salary_general_info'] = $this->db->where('user_id', $userID)->get('tbl_user_salary_general_info')->result();
		
		
		$user_info = $this->db->where('user_id', $userID)->get('tbl_users')->result();
		$location_id = $user_info[0]->user_location;
		$data['user_location'] = $this->db->where('location_id', $location_id)->get('tbl_warehouse_locations')->result();
		
		$data['userId'] = $userID;
        $data['hourly_grade'] = $this->db->get('tbl_hourly_rate')->result();
            // get all salary Template info
        $data['salary_grade'] = $this->db->get('tbl_salary_template')->result();
		//echo"<pre>"; print_r($data); die;
		
		if(!empty($data)){
		
        $data['subview'] = $this->load->view('admin/payroll/manage_salary_info', $data, TRUE);
		
        $this->load->view('admin/_layout_main', $data);
		}else{
			redirect('admin/payroll/manage_salary_details');
		}
    }
	
	public function new_earning($user_id, $er_id = null)
    {
		
		$data['title'] = lang('new earnings');

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
		$data['salary_info'] = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();
		
        if (!empty($er_id)) {
			
            $data['earning_info'] = $this->db->where('earning_id', $er_id)->get('tbl_employee_salary_earnings')->row();
        }
		
        $data['modal_subview'] = $this->load->view('admin/payroll/new_earning', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
    }
	
	public function save_earning_info($user_id, $earning_id = null)
    {
		//echo"<pre>"; print_r($_POST); die;
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $earning_data = $this->payroll_model->array_from_post(array('earning_type', 'earning_amount','excludeFrTx', 'effective_date'));
			
            $earning_data['user_id'] = $user_id;
			$earning_data['allowance_type'] = $_POST['salary_type'];
			
			$salaryID = $_POST['salary_template_id'];
			$hourlyID = $_POST['hourly_template_id'];
			if(!empty($salaryID)){
				$earning_data['salary_template_id'] = $salaryID;
			}else{
				$earning_data['salary_template_id'] = $hourlyID;
			}
			if($_POST['amtType']=='perhour'){
				   
			    }else{
					$earning_data['amtType'] = 'fix';
				}
				
            $this->payroll_model->_table_name = " tbl_employee_salary_earnings"; // table name
            $this->payroll_model->_primary_key = "earning_id"; // $id

            if (!empty($earning_id)) {
				
                $activity = 'activity_update_user_earning';
                $msg = lang('update_user_earning_info');
                $this->payroll_model->save($earning_data, $earning_id);
            } else {
				
                $activity = 'activity_new_user_earnings';
                $msg = lang('save_earning_info');
                $earning_id = $this->payroll_model->save($earning_data);
            }
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $earning_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->payroll_model->_table_name = 'tbl_activities';
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id . '/'); //redirect page
        } else {
            redirect('admin/payroll/manage_salary_info/'.$user_id);
        }
    }
	public function delete_employee_earning($user_id, $er_id)
    {
        $this->payroll_model->_table_name = "tbl_employee_salary_earnings"; // table name
        $this->payroll_model->_primary_key = "earning_id"; // $id
        $this->payroll_model->delete($er_id);
        
        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $er_id,
            'activity' => 'activity_delete_user_earning',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->payroll_model->_table_name = 'tbl_activities';
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);
        $type = 'success';
        $msg = lang('delete employee earning');
        set_message($type, $msg);
        redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id);
    }
	
	public function new_tax($user_id, $tx_id = null)
    {
		
		$data['title'] = lang('new Tax');
		
		$data['tax_categories'] = $this->db->select('*')->get('tbl_tax_categories')->result();

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
		$data['salary_info'] = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();
		$data['taxes'] = $this->db->get('tbl_taxes')->result();
		if (!empty($tx_id)) {
			
            $data['tax_info'] = $this->db->where('tax_id', $tx_id)->get('tbl_employee_salary_tax')->row();
        }
		
		//echo"<pre>"; print_r($data); die;
        $data['modal_subview'] = $this->load->view('admin/payroll/new_tax', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
		
	}
	   public function manage_taxable_gross_salary($id = null){
		//echo"<pre>"; print_r($_POST); die;
		$edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $gross_sdata = $this->payroll_model->array_from_post(array('user_id','taxable_gross_salary'));
 		 $user_id = $_POST['user_id'];
		 $checkExit = $this->db->where('user_id', $user_id)->get('tbl_taxable_gross_salary')->result();
                  
		 if (!empty($checkExit)) {
                $activity = 'activity update user gross salary taxable Income';
                $msg = lang('activity update user gross salary taxable Income');
                $this->db->set('user_id','taxable_gross_salary',false);
				$this->db->where('user_id',$user_id);
				$this->db->update('tbl_taxable_gross_salary',$gross_sdata);
            } else {
                $activity = 'activity gross salary taxable Income';
                $msg = lang('activity gross salary taxable Income');
                $this->db->insert('tbl_taxable_gross_salary',$gross_sdata);
				$id = $this->db->insert_id();
            }
			
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->payroll_model->_table_name = 'tbl_activities';
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id . '/'); 
			//redirect page
        } else {
            redirect('admin/payroll/manage_salary_info/'.$user_id);
        }
	}
	
	
	public function save_tax_info($user_id, $tx_id = null)
    {
		
        //echo"<pre>"; print_r($_POST); die;
		if(!empty($_POST['tax_type'])){
       // $edited = can_action('24', 'edited');
       // if (!empty($edited)) {
            $grossSalaryInfo = $this->db->where('user_id', $user_id)->get('tbl_taxable_gross_salary')->result();
			$grosspay = $grossSalaryInfo[0]->taxable_gross_salary;
			if(!empty($gp)){ 
			   $gp = $grosspay;
			}else{
				$gp = '10000';
			}
			if($_POST['taxCalc'] =='max'){
				$amt = $_POST['tax_amount'];
			}else if($_POST['taxCalc'] =='rng'){
			    $amt = $_POST['taxRangeAmount'];
			}else{
				$amt = $_POST['taxOthAmount'];
			}
			
			if($gp > $amt){				
				echo"kam hai";
			}else{
				echo"jyada hai";
			}
			
			$taxWithholding = '';
									
			if($_POST['taxCalc'] =='max'){
			   if($gp < $_POST['maxincome_threshold']){
				  $amt = $_POST['tax_amount'];
				  $withholding = $amt*100/$gp;				  
				  $taxWithholding = $withholding;		
				}					
			}else if($_POST['taxCalc'] =='rng'){
				$min = $_POST['minIncome'];
                $max = $_POST['maxIncome'];
				if($gp > $min && $gp < $max){
					$rangAmt = $_POST['taxRangeAmount'];
					$taxWithholding = $rangAmt*100/$gp; 					
				}
			}else{
			     $othAmt = $_POST['taxOthAmount'];
				 $taxAmtOth = $othAmt*100/$gp;
			     $taxWithholding = $taxAmtOth;	
			
			}
			
			
			$salaryID = $_POST['salary_template_id'];
			$hourlyID = $_POST['hourly_template_id'];			
			//echo"<pre>"; print_r($tax_data); die;
			
			
			if(!empty($salaryID)){
				$salary_template_id = $salaryID;
			}else{
				$salary_template_id = $hourlyID;
			}
	        $tax_data = array(
            'tax_type' => $_POST['tax_type'],
            'taxCalc' => $_POST['taxCalc'],
            'maxincome_threshold' => $_POST['maxincome_threshold'],
            'tax_amount' => $_POST['tax_amount'],
            'minIncome' => $_POST['minIncome'],
            'maxIncome' => $_POST['maxIncome'],
            'taxRangeAmount' => $_POST['taxRangeAmount'],
            'taxOthAmount' => $_POST['taxOthAmount'],
            'taxCategory' => $_POST['taxCategory'],
            'taxWithholding' =>round($taxWithholding, 2),
            'effective_date' => $_POST['effective_date'],
            'user_id' => $user_id,
            'pay_type' => $_POST['salary_type'],
            'salary_template_id' => $salary_template_id,
            'effective_date' => $_POST['effective_date']
            );
			//echo"<pre>"; print_r($tax_data); die;
            $this->payroll_model->_table_name = "tbl_employee_salary_tax"; // table name
            $this->payroll_model->_primary_key = "tax_id"; // $id

            if (!empty($tx_id)) {
                $activity = 'activity_update_user_tax';
                $msg = lang('update_user_tax_info');
                $this->db->set('user_id', 'tax_type', 'taxCalc', 'maxincome_threshold', 'tax_amount', 'minIncome', 'maxIncome', 'taxRangeAmount','taxOthAmount', 'effective_date', 'salary_template_id', 'pay_type',false);
				$this->db->where('tax_id',$tx_id);
				$this->db->update('tbl_employee_salary_tax',$tax_data);
            } else {
                $activity = 'activity_new_user_tax';
                $msg = lang('save_tax_info');
                $tx_id = $this->payroll_model->save($tax_data);
            }
			
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $tx_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->payroll_model->_table_name = 'tbl_activities';
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id . '/'); //redirect page
       // } else {
          //  redirect('admin/payroll/manage_salary_info/'.$user_id);
        //}
		
		}else{
			redirect('admin/payroll/manage_salary_info/'.$user_id);
		}
    }
	
	public function delete_employee_tax($user_id, $tx_id)
    {
        $this->payroll_model->_table_name = "tbl_employee_salary_tax"; // table name
        $this->payroll_model->_primary_key = "tax_id"; // $id
        $this->payroll_model->delete($tx_id);
        
        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $tx_id,
            'activity' => 'activity_delete_user_tax',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->payroll_model->_table_name = 'tbl_activities';
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);
        $type = 'success';
        $msg = lang('delete employee Tax type');
        set_message($type, $msg);
        redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id);
    }
	

	public function new_deduction($user_id, $ded_id = null)
    {
		
		$data['title'] = lang('new deduction');

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
		$data['salary_info'] = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();
		
        if (!empty($ded_id)) {			
            $data['deduction_info'] = $this->db->where('deduction_id', $ded_id)->get('tbl_employee_salary_deduction')->row();
        }
		
        $data['modal_subview'] = $this->load->view('admin/payroll/new_deduction', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
    }
	
	public function save_deduction_info($user_id, $deduction_id = null)
    {
		
		//echo"<pre>"; print_r($_POST); die;
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $deduction_data = $this->payroll_model->array_from_post(array('deduction_type','deductionBasedOn', 'deduction_amount','deductionPercent','excludeFrTx', 'effective_date'));
			
            $deduction_data['user_id'] = $user_id;
            $deduction_data['wage_type'] = $_POST['salary_type'];
			$salaryID = $_POST['salary_template_id'];
			$hourlyID = $_POST['hourly_template_id'];
			
			if(!empty($salaryID)){
				$deduction_data['salary_template_id'] = $salaryID;
			}else{
				$deduction_data['salary_template_id'] = $hourlyID;
			}
			
            $this->payroll_model->_table_name = "tbl_employee_salary_deduction"; // table name
            $this->payroll_model->_primary_key = "deduction_id"; // $id

            if (!empty($deduction_id)) {
                $activity = 'activity_update_user_salary_deduction';
                $msg = lang('update_user_salary_deduction_info');
                $this->payroll_model->save($deduction_data, $deduction_id);
            } else {
                $activity = 'activity_new_user_salary_deduction';
                $msg = lang('save_salary_deduction_info');
                $deduction_id = $this->payroll_model->save($deduction_data);
            }
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $deduction_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->payroll_model->_table_name = 'tbl_activities';
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id . '/'); //redirect page
        } else {
            redirect('admin/payroll/manage_salary_info/'.$user_id);
        }
    }
	public function delete_employee_deduction($user_id, $ded_id)
    {
        $this->payroll_model->_table_name = "tbl_employee_salary_deduction"; // table name
        $this->payroll_model->_primary_key = "deduction_id"; // $id
        $this->payroll_model->delete($ded_id);
        
        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $ded_id,
            'activity' => 'activity_delete_user_salary_deduction',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->payroll_model->_table_name = 'tbl_activities';
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);
        $type = 'success';
        $msg = lang('delete employee salary deduction');
        set_message($type, $msg);
        redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id);
    }
	
	public function new_contribution($user_id, $cont_id = null)
    {
		
		$data['title'] = lang('New Contributions');

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
		$data['company_category'] = $this->db->select('*')->get('tbl_contribution_category')->result();
		$data['salary_info'] = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();
		$data['company_categories'] = $this->db->select('*')->get('tbl_contribution_category')->result();
         //echo"<pre>"; print_r($data); die;


        if (!empty($cont_id)) {			
            $data['contribution_info'] = $this->db->where('id', $cont_id)->get('tbl_salary_company_contribution')->row();
        }
		
        $data['modal_subview'] = $this->load->view('admin/payroll/new_contribution', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
    }
    
	public function save_contribution_info($user_id, $cont_id = null)
    {
		
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $contribution_data = $this->payroll_model->array_from_post(array('contribution_type','contributionBasedOn','contribution_amount','contributionPercent', 'contribution_date'));
			
            $contribution_data['user_id'] = $user_id;
			$contribution_data['companyCategory'] = $_POST['companyCategory'];
			$contribution_data['wage_type'] = $_POST['salary_type'];
			$salaryID = $_POST['salary_template_id'];
			$hourlyID = $_POST['hourly_template_id'];
			
			if(!empty($salaryID)){
				$contribution_data['salary_template_id'] = $salaryID;
			}else{
				$contribution_data['salary_template_id'] = $hourlyID;
			}
            $this->payroll_model->_table_name = "tbl_salary_company_contribution"; // table name
            $this->payroll_model->_primary_key = "id"; // $id

            if (!empty($cont_id)) {
                $activity = 'activity_update_user_salary_contribution';
                $msg = lang('update_user_salary_contribution_info');
                $this->payroll_model->save($contribution_data, $cont_id);
            } else {
                $activity = 'activity_new_user_salary_contribution';
                $msg = lang('save_salary_contribution_info');
                $cont_id = $this->payroll_model->save($contribution_data);
            }
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $cont_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->payroll_model->_table_name = 'tbl_activities';
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id . '/'); //redirect page
        } else {
            redirect('admin/payroll/manage_salary_info/'.$user_id);
        }
    }
	
	public function delete_employee_contribution($user_id, $cont_id)
    {
        $this->payroll_model->_table_name = "tbl_salary_company_contribution"; // table name
        $this->payroll_model->_primary_key = "id"; // $id
        $this->payroll_model->delete($cont_id);
        
        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $cont_id,
            'activity' => 'activity_delete_user_salary_contribution',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->payroll_model->_table_name = 'tbl_activities';
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);
        $type = 'success';
        $msg = lang('delete employee salary contributions');
        set_message($type, $msg);
        redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id);
    }
	
	public function new_other($user_id, $oth_id = null)
    {
		
		$data['title'] = lang('New Other Contributions');

        $data['profile_info'] = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
		
		$data['salary_info'] = $this->db->where('user_id', $user_id)->get('tbl_user_salary_general_info')->row();
		
        if (!empty($oth_id)) {			
            $data['benifit_info'] = $this->db->where('id', $oth_id)->get('tbl_salary_other_benifits')->row();
        }
		//echo"<pre>"; print_r($data); die;
        $data['modal_subview'] = $this->load->view('admin/payroll/new_other', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
    }
	public function save_other_benifits($user_id, $bid = null)
    {
		//echo"<pre>"; print_r($_POST); die;
        $edited = can_action('24', 'edited');
        if (!empty($edited)) {
            $benifit_data = $this->payroll_model->array_from_post(array('benifit_type','benifitBasedOn','benifitPercent','benifit_amount', 'benifit_date'));
			
            $benifit_data['user_id'] = $user_id;
			
			$benifit_data['wage_type'] = $_POST['salary_type'];
			
			$salaryID = $_POST['salary_template_id'];
			$hourlyID = $_POST['hourly_template_id'];
			
			if(!empty($salaryID)){
				$benifit_data['salary_template_id'] = $salaryID;
			}else{
				$benifit_data['salary_template_id'] = $hourlyID;
			}
			
            $this->payroll_model->_table_name = "tbl_salary_other_benifits"; // table name
            $this->payroll_model->_primary_key = "id"; // $id

            if (!empty($bid)) {
                $activity = 'activity update user salary benifits';
                $msg = lang('update user salary benifits info');
                $this->payroll_model->save($benifit_data, $bid);
            } else {
                $activity = 'activity new user salary benifits';
                $msg = lang('save salary benifits info');
                $banifit_id = $this->payroll_model->save($benifit_data);
            }
            $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
            $activities = array(
                'user' => $this->session->userdata('user_id'),
                'module' => 'user',
                'module_field_id' => $banifit_id,
                'activity' => $activity,
                'icon' => 'fa-user',
                'value1' => $profile_info->fullname
            );
            $this->payroll_model->_table_name = 'tbl_activities';
            $this->payroll_model->_primary_key = "activities_id";
            $this->payroll_model->save($activities);

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id . '/'); //redirect page
        } else {
            redirect('admin/payroll/manage_salary_info/'.$user_id);
        }
    }
	
	public function delete_employee_benifit($user_id, $bid)
    {
        $this->payroll_model->_table_name = "tbl_salary_other_benifits"; // table name
        $this->payroll_model->_primary_key = "id"; // $id
        $this->payroll_model->delete($bid);
        
        $profile_info = $this->db->where('user_id', $user_id)->get('tbl_account_details')->row();
        $activities = array(
            'user' => $this->session->userdata('user_id'),
            'module' => 'user',
            'module_field_id' => $cont_id,
            'activity' => 'activity_delete_user_salary_contribution',
            'icon' => 'fa-user',
            'value1' => $profile_info->fullname
        );
        $this->payroll_model->_table_name = 'tbl_activities';
        $this->payroll_model->_primary_key = "activities_id";
        $this->payroll_model->save($activities);
        $type = 'success';
        $msg = lang('delete employee other benifits');
        set_message($type, $msg);
        redirect('admin/payroll/manage_salary_info/' . $profile_info->user_id);
    }
	
	
	
	
	//To add the attendace time for hourly employee in make payment page.
	public function add_time_manually($employee_id = NULL, $start_date = NULL, $end_date = NULL, $payment_number = NULL, $department_id = NULL, $payment_month = NULL) {
	
		$data['title'] = lang('view_timerequest');
        // retrive all data from department table
		$data['all_employee'] = $this->payroll_model->check_by(array('user_id' => $employee_id), 'tbl_account_details');
         //echo "<pre>"; print_r($data['all_employee']); exit;
	   // $data['all_employee'] = $this->payroll_model->get_all_employee();
		
		if(isset($employee_id) && !empty($employee_id)) {
			$data['employee_id'] = $employee_id;
		}
		if(isset($start_date) && !empty($start_date)) {
			$data['start_date'] = $start_date;
		}
		if(isset($end_date) && !empty($end_date)) {
			$data['end_date'] = $end_date;
		}
		if(isset($payment_number) && !empty($payment_number)) {
			$data['payment_number'] = $payment_number;
		}
		
		if(isset($department_id) && !empty($department_id)) {
			$data['department_id'] = $department_id;
		}
		
		if(isset($payment_month) && !empty($payment_month)) {
			$data['payment_month'] = $payment_month;
		}

		
		$data['added_hours'] = $this->payroll_model->check_by(array('user_id' => $employee_id, "payment_number" => $payment_number), 'tbl_employee_hours');

	
		$startDate = date("Y-m-d",$start_date);
		$endDate = date("Y-m-d",$end_date);
		$manageHourInfo = $this->db->where('user_id', $employee_id)->where('start_date', $startDate)->where('end_date', $endDate)->get('tbl_employee_hours')->result();
		   
		$data['manage_hour'] = $manageHourInfo;
		

        $data['subview'] = $this->load->view('admin/payroll/add_time_manually', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
		
	}
	
	// save attendance time for hourly employee in make payment page.
	public function saved_manual_time($employee_id = NULL)
    {

        $adata = $this->payroll_model->array_from_post(array('user_id', 'start_date', 'end_date', 'regular_hours', 'pto_hours', 'overtime_hours', 'payment_number', 'department_id', 'payment_month'));
        
		$datas = array(
            'user_id' => $adata['user_id'],
            'payment_number' => $adata['payment_number'],
			'payment_month' => $adata['payment_month'],
            'start_date' => $adata['start_date'],
            'end_date' => $adata['end_date'],
            'regular_hours' => $adata['regular_hours'],
            'pto_hours' => $adata['pto_hours'],
			'overtime_hours' => $adata['overtime_hours']
        );
        $this->payroll_model->_table_name = 'tbl_employee_hours';
        $this->payroll_model->_primary_key = "id";
		
		$where = array('user_id' => $adata['user_id'], 'payment_number' => $adata['payment_number']);
		$check_hourly_rate = $this->payroll_model->check_update('tbl_employee_hours', $where);
		
		if(!empty($check_hourly_rate)) {
			$this->payroll_model->save($datas, $check_hourly_rate[0]->id);
		} else {
			$this->payroll_model->save($datas);
		}
		
        $type = 'success';
        $msg = lang('Hours updated successfully.');
        set_message($type, $msg);
        redirect('admin/payroll/make_payment/0/'.$adata['department_id'].'/'.$adata['payment_month']);
        

    }
	
  public function company_contribution(){
	   //echo"dfgdsfsd"; die;
	   
	   //echo"<pre>"; print_r($_POST); die;
	   $data['title'] = lang('Company Contributions');
       $search_type = $this->input->post('search_type', true);
        if (!empty($search_type)) {
            $data['search_type'] = $search_type;
            if ($search_type == 'employee') {
                $data['user_id'] = $this->input->post('user_id', true);
				$data['category_id'] = $this->input->post('category_id', true);
            }
			
			
            if ($search_type == 'month') {
                $data['by_month'] = $this->input->post('by_month', true);
				$data['category_id'] = $this->input->post('category_id', true);
            }
			
            if ($search_type == 'period') {
                $data['start_month'] = $this->input->post('start_month', true);
                $data['end_month'] = $this->input->post('end_month', true);
            }
        }
		//echo"<pre>"; print_r($data); die;
        $data['subview'] = $this->load->view('admin/payroll/company_contribution', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);	   
   }
   
   
   
  public function delete_salary_earning($er_id, $tempId)
    {
        $this->payroll_model->_table_name = "tbl_salary_allowance"; // table name
        $this->payroll_model->_primary_key = "salary_allowance_id"; // $id
        $this->payroll_model->delete($er_id);
        
        $type = 'success';
        $msg = lang('delete salary template earning');
        set_message($type, $msg);
        redirect('admin/payroll/salary_template/' .$tempId);
    }
	
	public function tax_categories()
    {
		$data['title'] = 'Tax Categories';
		
		$this->db->select('*');
		$this->db->from('tbl_tax_categories');
		$data['tax_categories'] = $this->db->get()->result();
		
		$subview = 'tax_categories';
		$data['subview'] = $this->load->view('admin/payroll/tax_categories/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function delete_tax_category($id='') {
		
			
			$this->payroll_model->_table_name = 'tbl_tax_categories';
			$this->payroll_model->_primary_key = 'id';
			$this->payroll_model->delete($id);

			$type = 'success';
			$message = lang('Tax category deleted successfully.');
            

            set_message($type, $message);
        
			redirect('admin/payroll/tax_categories');
    
		
	}
	
	
	public function new_tax_category($id='')
    {
		
		$data['title'] = lang('Tax Categories');

        if (!empty($id)) {
			
            $data['tax_category_info'] = $this->db->where('id', $id)->get('tbl_tax_categories')->row();
        }
		
        $data['modal_subview'] = $this->load->view('admin/payroll/tax_categories/new_tax_category', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
    }
	
	public function save_tax_category($id='')
    {
		
            $tax_category_data = $this->payroll_model->array_from_post(array('tax_category_name'));
			
            $this->payroll_model->_table_name = "tbl_tax_categories"; // table name
            $this->payroll_model->_primary_key = "id"; // $id

			$msg = lang('Tax Category Updated Successfully.');
			
            if (!empty($id)) {
				
                $this->payroll_model->save($tax_category_data, $id);
            } else {
				
				$this->payroll_model->save($tax_category_data);
            }
            

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/tax_categories');
        
    }
	
	
	public function employee_allowance($s_template_id,$user_id) {
			   
			   $this->payroll_model->_table_name = 'tbl_employee_salary_earnings';
			   $this->payroll_model->_order_by = 'salary_template_id';
			   $allowance_info = $this->payroll_model->get_by(array('salary_template_id' => $s_template_id, 'effective_date <=' => date('Y-m-d'), 'user_id' => $user_id), FALSE);
			   return $allowance_info;
	}
	
	public function employee_award($user_id, $payment_start_date, $payment_end_date) {
			
			$this->payroll_model->_table_name = 'tbl_employee_award';
			$this->payroll_model->_order_by = 'user_id';
			$award_info = $this->payroll_model->get_by(array('user_id' => $user_id, 'given_date >=' => $payment_start_date, 'given_date <=' => $payment_end_date), FALSE);
			return $award_info;	
			
	}
	
	public function employee_deductions($salary_temlate_id, $user_id) {
			
			$this->payroll_model->_table_name = 'tbl_employee_salary_deduction';
			$this->payroll_model->_order_by = 'salary_template_id';
			$deduction_info = $this->payroll_model->get_by(array('salary_template_id' => $salary_temlate_id, 'user_id' => $user_id, 'effective_date <=' => date('Y-m-d')), FALSE);
			return $deduction_info;
				
		}
		
	public function employee_contributions($salary_temlate_id, $user_id) {
				$this->payroll_model->_table_name = 'tbl_salary_company_contribution';
				$this->payroll_model->_order_by = 'salary_template_id';
				$contribution_info = $this->payroll_model->get_by(array('salary_template_id' => $salary_temlate_id, 'user_id' => $user_id, 'contribution_date <=' => date('Y-m-d')), FALSE);
				return $contribution_info;
			}
			
	public function employee_benefits($salary_temlate_id, $user_id) {
			$this->payroll_model->_table_name = 'tbl_salary_other_benifits';
			$this->payroll_model->_order_by = 'salary_template_id';
			$benefit_info = $this->payroll_model->get_by(array('salary_template_id' => $salary_temlate_id, 'user_id' => $user_id, 'benifit_date <=' => date('Y-m-d')), FALSE);
			return $benefit_info;
		}
		
	public function employee_taxes($salary_temlate_id, $user_id) {		
			
			$taxes_info = $this->db->where('user_id', $user_id)->where('salary_template_id', $salary_temlate_id)->get('tbl_employee_salary_tax')->result();			
			return $taxes_info;
		}
	
	public function state_tax_report(){
		//echo"<pre>"; print_r($_POST); echo"</pre>";
	   //echo"dfgdsfsd"; die;
	    $data['title'] = lang('State tax report');
        $search_type = $this->input->post('search_type', true);
        if (!empty($search_type)) {
            $data['search_type'] = $search_type;
            if ($search_type == 'employee') {
                $data['user_id'] = $this->input->post('user_id', true);
            }
            if ($search_type == 'month') {
                $data['by_month'] = $this->input->post('by_month', true);
            }
            if ($search_type == 'period') {
                $data['start_month'] = $this->input->post('start_month', true);
                $data['end_month'] = $this->input->post('end_month', true);
            }
			
        }
		//echo"<pre>"; print_r($data);
        $data['subview'] = $this->load->view('admin/payroll/state_tax_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);	   
   }
		
	public function local_tax_report(){
	   //echo"dfgdsfsd"; die;
	    $data['title'] = lang('Local/City tax report');
        $search_type = $this->input->post('search_type', true);
        if (!empty($search_type)) {
            $data['search_type'] = $search_type;
            if ($search_type == 'employee') {
                $data['user_id'] = $this->input->post('user_id', true);
            }
            if ($search_type == 'month') {
                $data['by_month'] = $this->input->post('by_month', true);
            }
            if ($search_type == 'period') {
                $data['start_month'] = $this->input->post('start_month', true);
                $data['end_month'] = $this->input->post('end_month', true);
            }
        }
        $data['subview'] = $this->load->view('admin/payroll/local_tax_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);	   
   }

   
    public function federal_tax_report(){
	   //echo"dfgdsfsd"; die;
	    $data['title'] = lang('Federal tax report');
        $search_type = $this->input->post('search_type', true);
        if (!empty($search_type)) {
            $data['search_type'] = $search_type;
            if ($search_type == 'employee') {
                $data['user_id'] = $this->input->post('user_id', true);
            }
            if ($search_type == 'month') {
                $data['by_month'] = $this->input->post('by_month', true);
            }
            if ($search_type == 'period') {
                $data['start_month'] = $this->input->post('start_month', true);
                $data['end_month'] = $this->input->post('end_month', true);
            }
        }
		//echo"<pre>"; print_r($data); 
        $data['subview'] = $this->load->view('admin/payroll/federal_tax_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);	   
   }
   
   
   public function company_contribution_categories()
    {
		$data['title'] = 'Company Contributions Categories';
		
		$this->db->select('*');
		$this->db->from('tbl_contribution_category');
		$data['company_category'] = $this->db->get()->result();
		
		$subview = 'categories';
		$data['subview'] = $this->load->view('admin/payroll/companyCategories/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }
   
   public function delete_company_category($id='') {
		
			
			$this->payroll_model->_table_name = 'tbl_contribution_category';
			$this->payroll_model->_primary_key = 'id';
			$this->payroll_model->delete($id);

			$type = 'success';
			$message = lang('Ccategory deleted successfully.');
            

            set_message($type, $message);
        
			redirect('admin/payroll/company_contribution_categories');
    
		
	}
	
	
	public function new_company_category($id='')
    {
		
		$data['title'] = lang('Company Categories');

        if (!empty($id)) {
			
            $data['category_info'] = $this->db->where('id', $id)->get('tbl_contribution_category')->row();
        }
		
		
        $data['modal_subview'] = $this->load->view('admin/payroll/companyCategories/new_category', $data, FALSE);
		
        $this->load->view('admin/_layout_modal', $data);
    }
	
	public function save_company_category($id='')
    {
		
            $category_data = $this->payroll_model->array_from_post(array('category_name'));
			
            $this->payroll_model->_table_name = "tbl_contribution_category"; // table name
            $this->payroll_model->_primary_key = "id"; // $id

			$msg = lang('Category Updated Successfully.');
			
            if (!empty($id)) {
				
                $this->payroll_model->save($category_data, $id);
            } else {
				
				$this->payroll_model->save($category_data);
            }
            

            $type = 'success';
            set_message($type, $msg);
            redirect('admin/payroll/company_contribution_categories');
        
    }
   
   }