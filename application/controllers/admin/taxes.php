<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class taxes extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('application_model');
        //$this->load->model('taxes_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function index($action = null, $id = NULL)
    {	
	    $subview = 'taxes'; 
		$data['title'] = 'Tax List';
        $this->load->model('taxes_model');        
        $data['all_tax_history'] = $this->taxes_model->get_all_entries();
		$data['subview'] = $this->load->view('admin/taxes/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

	public function add_tax_manually($id=NULL)
    {
        $data['title'] = lang('view_taxrequest');
		$data['tax_info'] = $this->db->where('id', $id)->get('tbl_taxes')->row();			
		//echo"<pre>"; print_r($data ); die;
        $data['subview'] = $this->load->view('admin/taxes/add_tax_manually', $data, FALSE);
        $this->load->view('admin/_layout_modal', $data);
    }
	
	public function  saved_manual_tax($taxID = NULL){
		//echo"<pre>"; print_r($_POST); die;
		
		$this->taxes_model->_table_name = 'tbl_taxes';
        $this->taxes_model->_primary_key = 'id';
		$type = $this->input->post('tax_type', true);
		$taxCalculate = $this->input->post('taxCalc', true);
		
		$taxPayer = $this->input->post('taxPayer', true);
		$fillingType = $this->input->post('fillingType', true);
		$minIncome = $this->input->post('minIncome', true);
		$maxIncome = $this->input->post('maxIncome', true);
		$taxPercent = $this->input->post('taxPercent', true);
		$taxDtId = $this->input->post('taxDetailID', true);
		
        $data = array(
                'tax_type' =>$type, 
                'taxCalculate' =>$taxCalculate 
            );
        		
		if (!empty($taxID)) {               
                $msg = lang('Update Tax info');
					
						$this->db->where('id',$taxID);				
						$this->db->set('tax_type',$type);				
						$this->db->update('tbl_taxes');
						$j = 0 ;
						foreach($taxPayer as $payerVal){
							 $data2 = array(
								'taxPayer' =>$payerVal, 
								'fillingType' =>$fillingType[$j],
								'minIncome' =>$minIncome[$j],								 
								'maxIncome' =>$maxIncome[$j],								 
								'taxPercent' =>$taxPercent[$j]		
							);
							
							$this->db->where('id',$taxDtId[$j]);				
							$this->db->set($data2);				
							$this->db->update('tbl_tax_detail');
							
                         $j++;							
						}
				
				} else {						
					$this->db->insert('tbl_taxes',$data);
					$insert_id = $this->db->insert_id();
					
					if(!empty($insert_id)){
						$i = 0;
						//echo"<pre>"; print_r($taxPayer);					
						foreach($taxPayer as $payerVal){
							 $data1 = array(
								'tid' =>$insert_id, 
								'taxPayer' =>$payerVal, 
								'fillingType' =>$fillingType[$i],								 
								'minIncome' =>$minIncome[$i],								 
								'maxIncome' =>$maxIncome[$i],								 
								'taxPercent' =>$taxPercent[$i]								 
							);
							//echo"<pre>"; print_r($data1);
							                         
							$msg = lang('Save Tax info');
							$this->db->insert('tbl_tax_detail',$data1);
							$insertId  = $this->db->insert_id();
						    $arr[] = $insertId;							
							$i++; 
						}
						
					}
					
					
				}
				
				
				
        $type = "Success";
        set_message($type, $msg);
        redirect('admin/taxes/index');
	}
	
	public function delete_user_address($taxID = NULL){
		$this->db->where('id', $taxID);
        $this->db->delete('tbl_taxes');
		$msg = lang('Delete Tax info');
		$type = "Success";
        set_message($type, $msg);
        redirect('admin/taxes/index');
	}

}
