<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Warehouse_locations extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('application_model');

        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "99.8%",
                'height' => "400px"
            )
        );
    }

    public function index($action = null, $id = NULL)
    {
	
		$data['title'] = lang('warehouse_locations');
        $subview = 'warehouse_locations';
		/*
		$ch = curl_init('https://inventory.zoho.com/api/v1/settings/warehouses?organization_id=693442315');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
					
		// Set HTTP Header for POST request 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
			'Authorization: eb37ecbe4d345bc47f5471a9be066373'
		));
				
		// Submit the POST request
		$result = curl_exec($ch);
		curl_close($ch);
		$arr_result = json_decode($result);
		//echo "<pre>"; print_r(json_decode($result)); exit;
		foreach($arr_result->warehouses as $warehouse) {
			$data = array(
			'warehouse_name'=>$warehouse->warehouse_name,
			'warehouse_id'=>$warehouse->warehouse_id,
			'attention'=>$warehouse->attention,
			'is_primary'=>$warehouse->is_primary,
			'stree1'=>$warehouse->address,
			'street2'=>$warehouse->address1,
			'city'=>$warehouse->city,
			'country'=>$warehouse->country,
			'state'=>$warehouse->state,
			'zip_code'=>$warehouse->zip,
			'phone'=>$warehouse->phone,
			'email'=>$warehouse->email,
			'status'=>$warehouse->status,
			);

			$this->db->insert('tbl_warehouse_locations',$data);
		}
		*/
        //$data_query = $this->db->get('tbl_warehouse_locations');
		//$data['warehouses'] = $data_query->result();
		
		$data['subview'] = $this->load->view('admin/warehouse_locations/' . $subview, $data, TRUE);
        $this->load->view('admin/_layout_main', $data); //page load
    }

    public function warehouseList() {
		if ($this->input->is_ajax_request()) {
            $this->load->model('datatables');
            $this->datatables->table = 'tbl_warehouse_locations';
            $this->datatables->column_order = array('warehouse_name','stree1','street2','city','state','zip_code','phone','email');
            $this->datatables->column_search = array('warehouse_name','stree1','street2','city','state','zip_code','phone','email');
            $this->datatables->order = array('location_id' => 'asc');

            $fetch_data = make_datatables();

            $data = array();
            foreach ($fetch_data as $_key => $v_warehouse) {

                
                $sub_array = array();
                $sub_array[] = $v_warehouse->warehouse_name;
				$sub_array[] = $v_warehouse->stree1;
				$sub_array[] = $v_warehouse->street2;
				$sub_array[] = $v_warehouse->city;
				$sub_array[] = $v_warehouse->state;
				$sub_array[] = $v_warehouse->zip_code;
				$sub_array[] = $v_warehouse->phone;
				$sub_array[] = $v_warehouse->email;
				
                $data[] = $sub_array;

            }
            render_table($data);
			
			}
	}
	
	/*
	* update/refresh warehouses
	*/
	public function update_warehouses() {
		
		$ch = curl_init('https://inventory.zoho.com/api/v1/settings/warehouses?organization_id=693442315');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
					
		// Set HTTP Header for POST request 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
			'Authorization: eb37ecbe4d345bc47f5471a9be066373'
		));
				
		// Submit the POST request
		$result = curl_exec($ch);
		curl_close($ch);
		$arr_result = json_decode($result);
		
		foreach($arr_result->warehouses as $warehouse) {
		
				$data = array(
				'warehouse_name'=>$warehouse->warehouse_name,
				'warehouse_id'=>$warehouse->warehouse_id,
				'attention'=>$warehouse->attention,
				'is_primary'=>$warehouse->is_primary,
				'stree1'=>$warehouse->address,
				'street2'=>$warehouse->address1,
				'city'=>$warehouse->city,
				'country'=>$warehouse->country,
				'state'=>$warehouse->state,
				'zip_code'=>$warehouse->zip,
				'phone'=>$warehouse->phone,
				'email'=>$warehouse->email,
				'status'=>$warehouse->status,
				);
				
                $this->db->where('warehouse_id', $warehouse->warehouse_id)->update('tbl_warehouse_locations', $data);
                $exists = $this->db->where('warehouse_id', $warehouse->warehouse_id)->get('tbl_warehouse_locations');
                if ($exists->num_rows() == 0) {
                    $this->db->insert('tbl_warehouse_locations', $data);
                }
			
			}
			set_message('success', lang('Warehouse locations updated successfully'));
			redirect('admin/warehouse_locations');
	}

}
